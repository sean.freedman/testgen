Unfortuntley I didn't have time to fully finish this tool before leaving Turntide, however I do believe it is not far off.

> Please update this readme as you progress, until it is in a stable state.

I've wrote this very informal guide to whoever is interested, or has the unfortunate pleasure of finishing it. I've split it into three:
1. Why and what's left.
2. Concept
3. Usage.

Before that, here is a brief overview of the files in the tool:
```
  

│   .gitignore # Contains the ignore file type, folder etc for being included in the git repo tracking

│   example_gen_5.py # Example script to run a gen_5 controller, needs moved to relevent folder.

│   example_hbm.py # Example script read from the hbm power analyser, needs moved to relevent folder.

│   example_nidec.py # Example script to run a nidec controller, needs moved to relevent folder.

│   example_single.py # Example script to run a single DCDC, needs moved to relevent folder.

│   install.bat # Batch file to launch the installtion script depending on user options

│   README.md # where you read this from - needs updated as the tool finalises

│   testify.bat # The batch file that launches the tools server and web ui

│   test_script.py # this isnt included in git, but it is produced by the tool, regenerated for every change made.

│  

├───.streamlit

│       config.toml # the streamlit config file, atm just defines turntide color scheme and file upload size

│

│

├───Log

│   └───MunchPy # folder read for logs from munchpy

│

├───program

│   │   testify.py # the "main" of the tool.

│   │

│   ├───dependencies

│   │   └───torque_sensor

│   │           libusb0.dll # dll for usb control

│   │           stcommdll_v5u_x64.dll # torque sensor dll

│   │

│   ├───install

│   │       ps_script.ps1 #installation script for python/dependacies.

│   │       requirements.txt # packages to install, keep updated (look into pipreqs)

│   │

│   └───src

│       │   calcs.py # file containing the calculations used within the tool. was used when MATLAB wasnt an option

│       │   constants.py # quite a large file containing alot of reference constants, needs tidied. DEFAULT_PATHS is important

│       │   file_ops.py # file containing functions to move, zip delete files.

│       │   generator.py # file that generates the test points and the test script.

│       │   plot.py # file that produces the plots, some might redundent as we have moved to matlab.

│       │   profiles.py # file used to generate some of the ui content.

│       │   report_gen.py # file that is still relevnet but serves a differnt purpose than the name.

│       │

│       ├───equipment # This folder contains everything that refers to the test environment - or at least in concept.

│       │   │   can.py # contains functions/classes/methods for CAN control and comms

│       │   │   g4_function.py # Old, probably doesnt work, was used to characterise gen4 / Virya

│       │   │   modbus.py # contains functions/classes/methods for Modbus control and comms

│       │   │   t40b.py # Old, was used to get data from the power analyser/torque transducer - might still be useful for reference.

│       │   │   tenma.py # Used to control the tenma over serial - imo should be updated to the same "style" as can and modbus

│       │   │   torque_sense.py # Used to communicate with the old style transducer, again needs updated to so it has the same style.

│       │   │

│       │   ├───controllers # everything regarding to drives/ controllers ie. gen 5

│       │   │   ├───configs

│       │   │   │       gen_4.yaml # configuration files containing limits, can parameters etc.

│       │   │   │       gen_5.yaml # configuration files containing limits, can parameters etc.

│       │   │   │       nidec.yaml # configuration files containing limits, can parameters etc.

│       │   │   │

│       │   │   ├───databases

│       │   │   │       AM-RB-001_Hybrid_CAN_Protocol_v27.dbc # latest database file for AML

│       │   │   │       McLaren_P16_AR3.5_MCU_ECU_Communication_Extract_v6.0.0.arxml # latest database file for oxford

│       │   │   │

│       │   │   └───library

│       │   │       │   gen_5.py # code for communicating over can and creating an instance.

│       │   │       │   nidec.py # code for communicating over can and creating an instance.

│       │   │       │

│       │   │       ├───examples

│       │   │              read_gen5.py # can be deleted/ replaced with the files in the root folder.

│       │   │      

│       │   │      

│       │   │      

│       │   │      

│       │   │

│       │   ├───motors # everything relating to motors

│       │   │   └───configs

│       │   │           induction.yaml # induction machine configs

│       │   │           pmac.yaml # pmac machine configs, i.e bowfell, skiddaw etc

│       │   │

│       │   ├───power_analysers # everythng relating to the power analysers

│       │   │   ├───configs

│       │   │   │       hbm.yaml # used to define bitrate, ip , can signals etc.

│       │   │   │       zes_zimmer.yaml # blank or now, if its going to be used update.

│       │   │   │

│       │   │   ├───databases

│       │   │   │       gen4t.dbc # the can database for the hbm Gen4t.

│       │   │   │

│       │   │   └───library

│       │   │       │   hbm.py # the code to create an instance for the hbm power analyser and read values etc.

│       │   │       │

│       │   │       ├───examples

│       │   │       │       read_gen_4t.py # replace with the one in the root directory

│       │   │      

│       │   │      

│       │   │      

│       │   │

│       │   ├───power_supplies # everything relating to the power supplies.

│       │   │   ├───configs

│       │   │   │       dcdc.yaml # dcdc configurations settings, and can signal definitions etc.

│       │   │   │       tenma.yaml # not used, but as a placeholder for when the library file gets written

│       │   │   │

│       │   │   ├───databases

│       │   │   │       dcdc.dbc # can database file for the dcdc's

│       │   │   │       dcdc_can_interface.json # not sure, probably an old idea.

│       │   │   │

│       │   │   └───library

│       │   │       │   dcdc.py # code to create instance and control the dcdc

│       │   │       │

│       │   │       ├───examples

│       │   │               read_parallel.py # parrallel device is the same single, just a differnt topology

│       │   │               read_single.py # replace with the example from the root directory.

│       │   │        

│       │   │      

│       │   │      

│       │   │

│       │   └────torque_sensors # everything relating to the torque sensors, but tbh theyre not used atm as it goes through the power analyser

│       │       ├───configs

│       │       │       hbm.yaml

│       │       │       sensor_technologies.yaml

│       │       │

│       │       ├───databases

│       │        └───library

│       │

│       ├───scripts # the scripts used that arnt part of the tool

│       │   ├───analysis # analysis scripts - MATLAB

│       │   │   │   idet_table_gen.m # table generation

│       │   │   │   idq_characterisation.m # idq characterisation , precursor to the table generation.

│       │   │   │

│       │   │   ├───idet_table_src # ask nick turton

│       │   │       ├───functions

│       │   │       │       equivCircuitEqns.m

│       │   │       │       IDet_Motor.m

│       │   │       │       IDet_Regen.m

│       │   │       │       pLossInv.m

│       │   │       │       TmaxMotor.m

│       │   │       │       TmaxRegen.m

│       │   │       │

│       │   │       └───scripts

│       │   │               Id_Iq_tables.m

│       │   │               Plotting_Motor_Input.m

│       │   │               Plotting_Table_Output.m

│       │   │               Table_Generator.m

│       │   │  

│       │   │    

│       │   │      

│       │   │      

│       │   │      

│       │   │      

│       │   │      

│       │   │      

│       │   │      

│       │   │      

│       │   │      

│       │   │        

│       │   │

│       │   └───automation # the scripts that used to control execute the tests, when combined with the test inputs.

│       │       ├───characterisation

│       │       │   └───idq

│       │       │       │   gen_4_characterisation_idq_script.py # old, probably wont work but left as it might be a good reference

│       │       │       │   gen_5_characterisation_idq_script.md # old, ignore or update with the new version when created

│       │       │       │   gen_5_characterisation_idq_script.py # old, uses the old munchpy, but did work.

│       │       │       │   gen_5_characterisation_idq_script_can.py # can ignore, was a used a placeholder for the new script

│       │       │       │

│       │       │       └───functions

│       │       ├───encoder_alignment # an idea that was started but not finished

│       │       │   └───idq

│       │       │           gen_4_encoder_alignment_script.py

│       │       │

│       │       └───torque_speed

│       │           └───sweep

│       │                   gen_5_torque_speed_sweep_script.py # old, did work but probably needs updating alot, left for reference.

│       │

│       ├───ui

│       │   │   ui_analysis.py # calls all the anaylsis tabs

│       │   │   ui_debug.py # placeholder for some debugging while using the tool i.e. see all the data in the tool.

│       │   │   ui_hw.py # calls all the hardware related tabs, ie. dcdc , dyno configuration etc.

│       │   │   ui_plots.py # used to display the test points, timeline etc for the chosen test.

│       │   │   ui_profile.py # used to as the landing page for the test choice.

│       │   │   ui_report.py # not sure

│       │   │   ui_results.py # not sure

│       │   │   ui_tables.py #  sub tab of analysis, containts tbale generation configuration

│       │   │   ui_tester.py #

│       │   │   ui_testing.py

│       │   │   ui_test_cfg.py

│       │   │   ui_tools.py # some tools I put together along the way, i.e merge csvs, or view a color coded yaml.
```

## Why and what's left?

### Why
If you've heard it was ready, that was correct, but a lot of it has been re-written in attempt to make it modular, more concise and use better coding "practices" as my terrible skills *marginally* improved.

Largely these items below contributed to the longer development time, or so I tell myself 😱:
##### Matlab
Re-wrote post processing scripts in Matlab, so the data is manipulated, presented and saved in Matlab files. This should allow better integration into Turntides model based development path, as the people who care about the analysis are familiar with Matlab.
##### YAML
"[YAML](https://en.wikipedia.org/wiki/YAML)"-fy.. I have moved alot of the configuration parameters that are largely going to be unchanged to be contained and read from "human-readable" files. This should allow those unfamiliar with programming to view and edit the majority of parameters as the testing environment evolves, without breaking dependencies (unless the structure is modified, if you do this, be sure to update the reference paths in the code..).
##### Structure
File-structure / modularity, alot of changes were made to try and group equipment, libraries, configurations together, in a more intuitive manner. Although the number of files has increased, it should be more obvious where everything lives.
##### MunchPy
The removal of MunchPy, a python wrapper to interact with CANDI (AFAIK). The removal was communicated (in fact it was essential too as the tests script couldnt complete due to timeouts), however I focused on the re-write of the post-analysis scripts. This is where the majority of remaining work is, as it removed the ability to connect, read and write over CAN and Modbus, which essentially broke any automation scripts that had been developed. So far the following - I think - has been achieved. 
>I say "I think" because reliable and accessible test environments are far and few between at the time of writing the CAN and Modbus libraries, due to the increase of projects starting at the same time.

- **CAN Library**, you can setup and connect to a CAN bus, send, receive and decode messages and signals.
- **Modbus Library**, you can setup and connect to a Modbus client, send, receive and decode parameters values.
- **DCDC CAN library**, you create a DCDC instance, connect to, read and write values.
- **Nidec Dyno Modbus library**, you create a Nidec Dyno instance, connect to, read and write values.
- **HBM Gen4t CAN library**, you create a HBM Gen4t instance, connect to, read and write values (limited due to the hardware.. start, stop recording and zero transducer) . * This isn't tested yet.

### What's left?
There is **very likely** more work to do than outlined below.

#### Major
##### Controller / Unit under test CAN
The number one priority in my mind is to get `/program/src/equipment/controllers/library/gen_5.py` finished.

This will allow CAN communication to the GEN5 generation of products.
The reason this is needed , to do any testing, you need to be able to control the following:
1. Controller
2. Dyno
3. Power Supply
At the moment, the missing component is the controller control, however it is also the most difficult, the majority of the work has been done. 

The remaining work is to setup periodic CAN correctly, send and receive the required messages which are slightly different between each "project" (ie. Oxford has a [NMT](http://www.byteme.org.uk/canopenparent/canopen/nmt-protocol-network-managment-canopen/) message, while bowfell does not.) Additionally, as we predominantly used GEN5 with automotive products, in particular hybrid systems, a gearbox is used to step up/down the speed/torque from the eMotor to the driveline.
To this end, the customer usually transmits the "transmission speed" to the UUT. To replicate this behaviour, we have to read the eMotor speed via the inverter and transmit it back to the UUT as, you guessed it, transmission speed, but do so in a timely matter as there are checks involved in the UUTs software. There are also other considerations such as, but not limited to:
- Crash Signal
- [`__del__`](https://www.pythontutorial.net/python-oop/python-__del__/) method, to execute when the instance goes out of scope.

##### HBM GEN4t / Power Analyser 
Similarly to the Controller / Unit under test CAN issue, the CAN communication to and from the HBM GEN4t power analyser needs finished. (see: `/program/src/equipment/power_analysers/library/hbm.py`)
Again this is almost finished, I have been able to read the messages sent by the power analyser, however, sending record, and zero has not been tested.

This is needed to read the Torque over CAN and for future characterisation work.

##### YAML population
Lastly, as mentioned, YAMLS now contain the majority of information and parameters about a piece of equipment (as little or as much as you want), as the libraries and re-work developed the structure of the yamls changed, to try and make obscure references more intuitive. However, this means the parameters need populated and the current information is scattered around data sheets, namesplates, code, databases, brains 🤦‍♂️, sharepoint etc. This is important to input otherwise the wrong limits may be used or the program will complain about something not being initialised.

##### Re-write of Standard Test Scripts
The current test scripts that are available (`program\src\scripts\automation`) currently use the old MunchPy library which has been removed for now. New scripts need to be created to replicate the behaviour of the old scripts with the new CAN/Modbus libraries. The scripts could largley be left the same and replace the old instances/functions with the new ones.

It looks rubbish due to the Logger that outputs to a text file for debugging/evidence and [PEP8](https://pep8.org/) reformatting due to max line length = 80 :sick: 

#### Minor
##### Re-write "legacy" files
Before the re-write some "code" was produced to control some equipment, they are not in-line with the principles of the new CAN and Modbus libraries. 

- `program\src\equipment\g4_function.py` - Not 100% sure what this does, its used with jacobs Gen 4 scripts, which likely dont work anymore, but they could be used to make a new start on Gen4 integration into the tool.

- `program\src\equipment\t40b.py` - This was used to read the torque and speed from torque sensor via the power analyser during the legacy scripts, this will be replaced by  `program\src\equipment\power_analysers\library\hbm.py` and `program\src\equipment\modbus.py`

- `program\src\equipment\tenma.py` - used to control tenma power supplies, I think alongside `can.py` and `modbus.py` there should be a equivalent `serial.py` and `tenma.py`  should be created in `program\src\equipment\power_supplies\library` and used like `dcdc.py`.

- `program\src\equipment\torque_sense.py` - For the older style torque transducers. They do not communicate over CAN or Modbus, but do over USB. To this end a .dll is needed to decode the information (which is another problem.. libusb0.dll needs installed to handle the usb driver). Again, although this currently works I think it could be modified / rewritten in-line with the rest of the equipment scripts.

##### Todo's
There are several `# TODO` in the code that may or may need addressing, I have consolidated a list below, some of them are just ideas that I may have moved passed and forgot about, some are suggestions that I think should be done:
> *PLEASE NOTE*: Some may lead you down a rabbit hole 🤬.

```
38 results - 17 files

  

testgen • example_gen_5.py:

  1: # TODO: MOVE TO RELEVENT EXAMPLE SCRIPTS FOLDER AND MAKE SURE IT WORKS

  2  

  

testgen • example_hbm.py:

  1: # TODO: MOVE TO RELEVENT EXAMPLE SCRIPTS FOLDER AND MAKE SURE IT WORKS

  2  

  

testgen • example_nidec.py:

  1: # TODO: MOVE TO RELEVENT EXAMPLE SCRIPTS FOLDER AND MAKE SURE IT WORKS

  2  

  

testgen • example_single.py:

  1: # TODO: MOVE TO RELEVENT EXAMPLE SCRIPTS FOLDER AND MAKE SURE IT WORKS

  2  

  

testgen • test_script.py:

  594                      init_zero = False

  595:                     dyno.setMaximumReferenceClamp(max(SPEED_DEMANDS) * 1.2 / GEARBOX_RATIO)  # TODO: need better method

  596                      for step in range(len(SPEED_DEMANDS)):

  

  614  

  615:                         # TODO: Check if speed dmand is going to be greater than the speed limit. do if else, i.e if increasing set speed limits first, else set dyno first?

  616                          dyno.setPresetReference1(SPEED_DEMANDS[step] / GEARBOX_RATIO)

  

  749                                  np.abs(data_avg["Iq Current Reference (RMS) [A]"]),

  750:                             )  # TODO: Maybe arctan x/y?

  751                              data_avg["Is Current Reference Angle [Degrees]"] = np.rad2deg(data_avg["Is Current Reference Angle [Radians]"])

  

  756                                  np.abs(data_avg["Iq Current Feedback (RMS) [A]"]),

  757:                             )  # TODO: Change to arctan x/y?

  758                              data_avg["Is Current Feedback Angle [Degrees]"] = np.rad2deg(data_avg["Is Current Feedback Angle [Radians]"])

  

testgen • program\src\constants.py:

  296  GLOBAL_COLORMAP = "viridis"

  297: # TODO REMOVE HBM STUFF TO ITS OWN FILE i.e equipment>power_analyser>hbm.py

  298  HBM_CAN_DATABASE = r"C:\SVN\Dyno Configs\D7 [340kW]\CANanalyzer\IPK2200316" + FILE_EXTENSIONS["DBC"]

  

  334  # ------------------  GEN4 CONSTANTS ------------------

  335: # TODO : THIS was all provided by Jacob for the Gen4 / Ultraviolet / Virya project

  336  GEN4_ID_ID = "0x003F8E0A"

  

  355  # ------------------ INVERTER PARAMETER ------------------

  356: # TODO : Satish to provide guidance on the below or change to YAML.

  357  

  

testgen • program\src\generator.py:

   914          script.append("# HBM CAN Filter: " + torque_sensor_can_filter)

   915:     #!!! TODO:Issue with DLL reading on the path for libusb0.dll - needs inverstigating.

   916      else:

  

  1091          script.append("# HBM CAN Filter: " + torque_sensor_can_filter)

  1092:     #!!! TODO:Issue with DLL reading on the path for libusb0.dll - needs inverstigating.

  1093      else:

  

testgen • program\src\report_gen.py:

  86  

  87:     # TODO: determine test type depending on tab open , not test selected

  88  

  

testgen • program\src\equipment\torque_sense.py:

  170  """

  171: # TODO: ADD more functions, i.e. get/set cap rate, shaft temp, calib date

  

testgen • program\src\equipment\controllers\configs\gen_5.yaml:

  1: # TODO: POPULATE THE CONFIGS, OXFORDS IS THE MOS TUP TO DATE.

  2  # +--------------------------------------------------------------------------+

  

testgen • program\src\equipment\controllers\configs\nidec.yaml:

  1: # TODO: Should this be NIDEC file or Dyno file, i.e. to include b2b stuff.

  2  # NEEDs populated

  

testgen • program\src\equipment\controllers\library\nidec.py:

   55          """

   56:         # TODO: GO THROUGH LOGIC AND DETERMINE IF SAFE

   57          try:

  

   91  

   92:         # TODO: get Ben D, to check the if value != 0 stuff is correct

   93          # Voltage

  

  102  

  103:         # TODO: check if this a flaot / percentage

  104          # Current

  

  154          # Torque

  155:         # TODO: check the doc against the nidec manual

  156          def drive_mode(self) -> bool:

  

  170          # Temperature

  171:         # TODO: check the doc against the nidec manual

  172          def monitored_temperature_1(self) -> float:

  

  183  

  184:         # TODO: check the doc against the nidec manual

  185          def monitored_temperature_2(self) -> float:

  

  196  

  197:         # TODO: check the doc against the nidec manual

  198          def monitored_temperature_3(self) -> float:

  

  248  

  249:         # TODO: check the doc against the nidec manual

  250          def drive_healthy(self) -> bool:

  

  283  

  284:         # TODO: verifiy and edit if this a flaot / percentage

  285          def symmetrical_current_limit(self, value: int) -> bool:

  

  296  

  297:         # TODO: value check against motor spec

  298          # Speed

  

  310  

  311:         # TODO: value check against motor spec and max speed if possible

  312          def speed_target(self, value: int) -> bool:

  

testgen • program\src\equipment\power_supplies\library\dcdc.py:

  642  

  643:             # TODO: Better in loop ie if there was three dcdc.

  644              current_value = (current_value[0] + current_value[1]) / len(current_value)

  

testgen • program\src\equipment\torque_sensors\configs\hbm.yaml:

  1: # TODO: IS THIS NEEDED... CURRENTLY IN POWER ANALYSER>CONFIGS

  2  placeholder:

  

testgen • program\src\scripts\automation\torque_speed\sweep\gen_5_torque_speed_sweep_script.py:

  493                      init_zero = False

  494:                     dyno.setMaximumReferenceClamp(max(SPEED_DEMANDS) * 1.2 / GEARBOX_RATIO)  # TODO: need better method

  495                      for step in range(len(SPEED_DEMANDS)):

  

  513  

  514:                         # TODO: Check if speed dmand is going to be greater than the speed limit. do if else, i.e if increasing set speed limits first, else set dyno first?

  515                          dyno.setPresetReference1(SPEED_DEMANDS[step] / GEARBOX_RATIO)

  

  648                                  np.abs(data_avg["Iq Current Reference (RMS) [A]"]),

  649:                             )  # TODO: Maybe arctan x/y?

  650                              data_avg["Is Current Reference Angle [Degrees]"] = np.rad2deg(data_avg["Is Current Reference Angle [Radians]"])

  

  655                                  np.abs(data_avg["Iq Current Feedback (RMS) [A]"]),

  656:                             )  # TODO: Change to arctan x/y?

  657                              data_avg["Is Current Feedback Angle [Degrees]"] = np.rad2deg(data_avg["Is Current Feedback Angle [Radians]"])

  

testgen • program\src\ui\ui_tables.py:

  217                      json_file = ""

  218:             # TODO : seperate here to seperate function

  219              # get the name of the uplaoded file

  

  253                      "parameters": {

  254:                         # TODO : Pole pair needs taken from the motor parameters

  255                          "motor": {

  

testgen • program\src\ui\ui_testing.py:

   53  

   54:         # TODO: Put code below into a function

   55          if test_dict["type"] == "Torque Speed Sweep":

  

  138  

  139:         # TODO: Put code below into a function

  140          elif test_dict["type"] in [
```

##### Motor Temperature Dwelling Functionality. 
I integrated the following code into the MunchPy library, as it made the most sense (at least to me..) at the time.

It needs moved/integrated/re-written to somewhere sensible, I would also make it "generic", as in not just idq injection but torque/speed sweep compatible if possible:

```python
    def checkMotorTemp(

        self,

        upper_temperature_limit: float,

        lower_temperature_limit: float,

        ramp_value: float,

        id_demand_in: float,

        iq_demand_in: float,

        id_demand_ID: int,

        iq_demand_ID: int,

    ):

        """

        Check the motor temperature and if it is above the upper temperature limit then ramp current down until the motor cools to the lower temperature limit.

  

        Parameters:

            upper_temperature_limit (float): Upper temperature limit

            lower_temperature_limit (float): Lower temperature limit

            ramp_value (float): Ramp value

            id_demand_in (float): Id demand

            iq_demand_in (float): Iq demand

            id_demand_ID (int): Id demand ID

            iq_demand_ID (int): Iq demand ID

  

        Returns:

            None

        """

  

        # check parameter types and values

        if not (

            isinstance(upper_temperature_limit, float)

            and isinstance(lower_temperature_limit, float)

            and isinstance(ramp_value, float)

        ):

            raise TypeError("Expected parameter types do not match")

        if not lower_temperature_limit < upper_temperature_limit:

            raise ValueError(

                "Upper temperature limit should be less than lower temperature limit"

            )

        if not 0 < ramp_value:

            raise ValueError("Ramp value should be a positive and non zero number")

  

        id_demand = id_demand_in

        iq_demand = iq_demand_in

  

        # id demand is always negative.

        id_ramp = -ramp_value

  

        # if iq_demand is positive then set iq_ramp to ramp_value, else set iq_ramp to -ramp_value.

        if iq_demand >= 0.0:

            iq_ramp = ramp_value

        else:

            iq_ramp = -ramp_value

  

        post_cutback = False

  

        current_motor_temperature = self.getMotorTemperature()

  

        # if the motor temperature is greater than the upper temperature limit

        if current_motor_temperature > upper_temperature_limit:

            print("reduce current")

            # while the motor temperature is greater than the lower temperature limit

            while current_motor_temperature > lower_temperature_limit:

  

                # if absolute value of iq is more than 0 ramp the iq demand down over each loop

                if abs(iq_demand) > 0.0:

                    iq_demand -= iq_ramp

                    self.idqInjection(

                        idq_IDs=[id_demand_ID, iq_demand_ID],

                        idq_values=[None, iq_demand],

                    )

  

                # else iq is less than 0 then set iq demand to 0

                else:

                    iq_demand = 0.0

                    self.idqInjection(

                        idq_IDs=[id_demand_ID, iq_demand_ID],

                        idq_values=[None, iq_demand],

                    )

  

                # if id demand is more than id_demand_in ramp the id demand down over each loop

                if id_demand < 0.0:

                    id_demand -= id_ramp

                    self.idqInjection(

                        idq_IDs=[id_demand_ID, iq_demand_ID],

                        idq_values=[id_demand, None],

                    )

  

                # else id demand is less than 0 then set id demand to 0

                else:

                    id_demand = 0.0

                    self.idqInjection(

                        idq_IDs=[id_demand_ID, iq_demand_ID],

                        idq_values=[id_demand, None],

                    )

  

                current_motor_temperature = self.getMotorTemperature()

                print(current_motor_temperature)

                time.sleep(0.5)

                post_cutback = True

  
  

        current_motor_temperature = self.getMotorTemperature()

        # if the motor temperature is less than or equal to the lower temperature limit

        if (current_motor_temperature <= lower_temperature_limit) and post_cutback == True:

  

            # while id demand and iq demand are not equal to the id demand in and iq demand in

            while id_demand != id_demand_in or iq_demand != iq_demand_in:

  

                # if id demand is less than id_demand_in ramp the id demand up over each loop

                if id_demand > id_demand_in:

  

                    id_demand += id_ramp

                    self.idqInjection(

                        idq_IDs=[id_demand_ID, iq_demand_ID],

                        idq_values=[id_demand, None],

                    )

  

                # else id demand is more than id_demand_in then set id demand to id_demand_in

                else:

  

                    id_demand = id_demand_in

                    self.idqInjection(

                        idq_IDs=[id_demand_ID, iq_demand_ID],

                        idq_values=[id_demand, None],

                    )

  

                # if iq demand is less than iq_demand_in ramp the iq demand up over each loop

                if abs(iq_demand) < abs(iq_demand_in):

  

                    iq_demand += iq_ramp

                    self.idqInjection(

                        idq_IDs=[id_demand_ID, iq_demand_ID],

                        idq_values=[None, iq_demand],

                    )

  

                # else iq demand is more than iq_demand_in then set iq demand to iq_demand_in

                else:

                    iq_demand = iq_demand_in

                    self.idqInjection(

                        idq_IDs=[id_demand_ID, iq_demand_ID],

                        idq_values=[None, iq_demand],

                    )

  

                time.sleep(0.5)

  

        # if the motor temperature is below the upper temperature

        elif post_cutback == False:

            print("Continue motor in range")

            id_demand = id_demand_in

            iq_demand = iq_demand_in

            self.idqInjection(

                idq_IDs=[id_demand_ID, iq_demand_ID], idq_values=[id_demand, iq_demand]

            )

  

        return
```

##### NOT ALL DOCSTRINGS/COMMENTS ARE TRUE.
Please try and check the docstrings/ comments and update as appropriate, some of them reference old implementation, try handing off the block of code to chat GPT and ask to amend the docstring and comments to speed this up 🤯.

##### PYTHON CAN ISSUES.
It seems there are limitations / drawbacks to using open-source libraries. One of those is, it seems the developers of the [`python-can`](https://python-can.readthedocs.io/en/stable/) library do not have access to IXXAT hardware. Due to this make sure you have a particular version of `python-can==3.3.4` installed, as I'm not sure the installation via `requirements.txt` is able to handle the version correctly as another package `cantools` also installs `python-can`.

I believe there are two issues that I have noticed.
1. [Periodic can values can not be modified while periodic can is active](https://python-can.readthedocs.io/en/stable/interfaces/ixxat.html#ixxat-virtual-communication-interface), this is due to periodic can is handled by IXXATs onboard chip. To overcome this , you need to stop periodic, write the values, then restart.
2. [Some change to the IXXAT drivers? ](https://github.com/hardbyte/python-can/issues/1285)causing some nested bus? - i'm not going to pretend to understand it 🤔, this is what prevents you using the latest of `python-can` 

## Concept
The tool it self consists of GUI, where the user can choose and configure predefined tests. Once configured, a test script is then generated and executed, during which some data is recorded and stored. Additionally, the implementation of result analysis is done (but no limited to) via Matlab scripts, that are at the moment maintained by the motor control team.

### GUI
The GUI is developed using `python` with the [`streamlit`](https://streamlit.io/) package used for the UI framework. I've found this the easiest frame work to use, however it does have its drawbacks, in the fact that it uses a web browser for the front end, this means I havn't been able to (at least for now) create a executable that can just be distributed and deployed - this could be a benefit as Turntide looks towards a web UI for some of its customer facing products. 

To resolve / workaround the executable issue, a batch file and powershell script has been developed to help the user install and launch the tool and its dependencies. 

The majority of the UI based code can be found in : `program\src\ui`, which is then further broken down into the tabs, and sub tab tiers.

### Test Script Generation
The test script generation is done in two steps. 

#### Step one
The first, is the generation of the test steps, where arrays are created for the test inputs for example, if I chose to do the idq characterisation test, with 25A steps for 1 second from 0A to 100A, at 500rpm, with a speed limit of 120%, the array produced would be as follows:

```python
SPEED_DEMANDS               = [500.0, 500.0, 500.0, 500.0, 500.0, 500.0, 500.0, 500.0, 500.0, 500.0, 500.0, 500.0, 500.0, 500.0, 500.0, 500.0, 500.0]

IQ_DEMANDS                  = [0.0, -25.0, -50.0, -75.0, -100.0, 0.0, -25.0, -50.0, -75.0, 0.0, -25.0, -50.0, -75.0, 0.0, -25.0, -50.0, 0.0]

ID_DEMANDS                  = [0.0, -25.0, -50.0, -75.0, -100.0, 0.0, -25.0, -50.0, -75.0, 0.0, -25.0, -50.0, -75.0, 0.0, -25.0, -50.0, 0.0]

SPEED_LIMITS_FORWARD        = [600.0, 600.0, 600.0, 600.0, 600.0, 600.0, 600.0, 600.0, 600.0, 600.0, 600.0, 600.0, 600.0, 600.0, 600.0, 600.0, 600.0]

SPEED_LIMITS_REVERSE        = [600.0, 600.0, 600.0, 600.0, 600.0, 600.0, 600.0, 600.0, 600.0, 600.0, 600.0, 600.0, 600.0, 600.0, 600.0, 600.0, 600.0]

DEMAND_TIME                 = [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0]

LOG_POINT                   = [True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True]
```

This along with the parameters filled in through the GUI for the test environment settings will be the first half of the test script. 

This is all produced by - in this example - the function `idq_script()` in `program\src\generator.py` and completes the "first half" of the dynamically generated test script: `test_script.py`.

This is then merged with the chosen test from GUI / `program\src\scripts\automation`. 

> Note that currently at the bottom of the script generator functions, it is merged using:
```python
    file_name = path_export + test_export_name + test_export_format

    if os.path.exists(file_name):

        interp_function = open(file_name, "w")

        interp_function.writelines("%s\n" % i for i in script)

        interp_function.close()

    else:

        interp_function = open(file_name, "w")

        interp_function.writelines("%s\n" % i for i in script)

        interp_function.close()

    path = file_name

  

    return path
```

> there are a couple of issues here, one, it seems I have found and replaced something with same name as the variable, that's caused the name to become `interp_fucntion`, although this doesn't effect the code, it makes might contribute to some confusion as to what the function is doing. 
> Secondly, I have included a function in `program\src\file_ops.py` `file_merge()` which I think achieves the same thing, so maybe just reference that.

```python
def file_merge(first_file_path, second_file_path):

    """

    Merge two files together.

  

    Args:

        first_file_path (str): The path to the first file.

        second_file_path (str): The path to the second file.

  

    Returns:

        None

  

    """

  

    f1 = open(first_file_path, "a+")

    f1.seek(0)

    f2 = open(second_file_path, "r")

  

    f1.write(f2.read())

  

    f1.close()

    f2.close()
```
#### Step two
The Second part is to get the "bottom half" of the test script by basically copying and pasting (done programmatically) the chosen test file.

Sticking with the example of idq characterisation, whatever is in the file `program\src\scripts\automation\characterisation\idq\gen_5_characterisation_idq_script.py` will be appended .

Visually:
![Test generation](readme/images/test_generation.png)

### Results
The results are stored as a csv's (and a .log file from the tests logger) with a file name that was input in the GUI (the default filenaming is detemined by the datetime and test chosen), I would encourage if we are going to be using large datasets to look into something like [Apaches' Parquet](https://parquet.apache.org/) or [HDF5](https://www.hdfgroup.org/solutions/hdf5/)

### Analysis
From this point it is up to the user to further analyse the results, either manually or using one of the built in analysis scripts : `program\src\scripts\analysis`
This is a folder I expect to be updated and maintained alongside `program\src\scripts\automation`, so each test script has a coupled analysis script.

## Usage.

In general the tool attempts to follow a left to right , top to bottom flow for each test.

### Idq Characterisation Example

The typical usage for the Idq Characterisation script is as follows:

#### Test choice

1. Choose the `Testing` tab, this will be bring up all the configuration options for the test.
2. Chose the `Profile` tab, this sill bring up the initial test script configuration.
3. Choose a test script to execute 
4. Choose a relevant profile (ie. forward motoring).
![test_profile](readme/images/test_profile.png)
You will notice the plot on the right will change depending on which test type and profile you choose, reflecting the test script with the currently selected configuration and parameters (this will be initial values to start with, until you modify the test with your desired inputs.)

**Idq Characterisation: Forward Motoring**

![idq_fwd_mot](readme/images/idq_fwd_mot.png)
**Idq Characterisation: All**
![idq_all](readme/images/idq_all.png)
There is also extra tabs on the plot for different "views" of the test:
![timeline](readme/images/(timeline.png)
The Timeline view will show how the test will progress for each test point.

![table](readme/images/table.png)
The Table view shows you the same information but in a spreadsheet.

The plots are also interactive:
![interactive_zoomed](readme/images/interactive_zoomed.png)

#### Hardware configuration
![](Pasted%20image%2020230505104705.png)
In the Hardware tab, there are further options
- Project
- Dyno
- DCDC
- CAN
- Login
##### Project
In the project tab the user can select the Turntide project and add any notes.
![project](readme/images/project.png)
##### Dyno
In the Dyno tab the user can select the dyno being used and the gearbox ratio to apply.

![dyno](readme/images/dyno.png)
##### DCDC
In the DCDC Tab (perhaps name should be changes to Power Supply) the user can select the DC Link voltage for the test and the current limits for the power supply.

> Note: The initial value is the mid point of the voltage breakpoints for the project.
> The Min/Max is the minimum of the project and hw limits.

**Please fix the DC Link Current (-)**: it looks from this screenshot you can keep increasing/decreasing pass the limit.

![dcdc](readme/images/dcdc.png)
##### CAN
In this tab the user can configure the CAN devices the script will be using.
> Note: this may need amending as we now use YAMLS for the majority of this.

![CAN](readme/images/CAN.png)
##### Login
If the new Munchpy still allows a login function, this can be used to set the password, as tests like Idq injection needs the user to login into the engineering user settings to set certain locked properties.

The number under the field counts the number of characters..
![Login](Login.png)

#### Test Configuration
The test configuration tab is used to configure the test points and options.
![config_tab](readme/images/config_tab.png)

##### Logging
The logging tab is used configure when the test should record data.
2. Logs data when the step of the test is increasing (absolute) (ie. 0A -> -25A).
3. Logs data when the step is decreasing (absolute) (ie -100A -> 0A)
4. How long to log the data for (after the delay period)
5. Delay logging is used to help remove transient data.
6. The delay period, useful for removing/not recording the transient.
![logging_tab](readme/images/logging_tab.png)
##### Speed
Here the user can set the speed for the test and the speed limits.
2. locked until the "offset" limit is implemented (ie. if the user wants to apply a flat speed limit offset, if the speed test point is 800rpm and the user requests an offset limit of 100rpm, the speed limit will be set to 900rpm).
3. As percentage speed limit is chosen, the speed limits are using a percentage of the test speed point, (ie. if the test point is at 1000rpm, the speed limit is set to 1200rpm)
4. Depending on the test type, this number input is available, for idq characterisation a constant speed is used, which is set here.
![](readme/images/Pasted%20image%2020230505110432.png)
##### Idq
This tab configures the idq points to generate.
2. The minimum current the test should go to (absolute).
3. The maximum current the test should go to (absolute).
4. Current step size (absolute)
5. The period that the test point should be applied for.
6. **Step method** needs clarified / checked. There was an option to go from 0 _> 25 A, 0 > 50A, 0 > 75A to try and cool the motor, however I think this option is ignored and it used the ramp methods : 0 > 25> 50> 100A.
7. The property ID of Idq mode block.
8. The property ID for Idq axis saturation
9. The value of the Axis :  (0 = equal sat, 1 = D-axis , 2 = Qaxis (check the model))
10. The property ID for the Id current injection
11. The property ID for Iq current injection.
12. May not be used anymore but it was used to restart the test at a certain test point due to the motor overheating, if this isn't the case anymore I think it should be reintroduced.
![idq_tab](readme/images/idq_tab.png)
##### Temperature
This was used for temperature dwelling, the code this needs re-written as mentioned above.


![temperature](readme/images/temperature.png)

If the temperature goes above the upper limit, the test script will reduce the current by the ramp step until it is zero, it will then sit at the test speed until the temperature is below the lower limit, it will then build back up the current by the ramp step to the previous test point and continue the test, until the upper limit is hit again, this allows the test to continue without the motor getting to hot and allows some hysteresis. 
![tempdraw](readme/images/tempdraw.png)

**UNFORTUNATLEY I HAVE NO TIME LEFT, AS I AM WRITING THIS ON MY LAST DAY, THE MAJORITY OF THE IMPORTANT STUFF HAS BEEN COVERED**