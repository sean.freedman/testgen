# TODO: MOVE TO RELEVENT EXAMPLE SCRIPTS FOLDER AND MAKE SURE IT WORKS

import time
from pprint import pprint

from program.src.constants import DEFAULT_PATHS
from program.src.equipment.power_analysers.library.hbm import Gen4T
from program.src.file_ops import read_yaml_file

print("Start of example script to read the DCDC power supply")

hbm_path = DEFAULT_PATHS["equipment"]["configs"]["power_analysers"]["hbm"]

print(f"HBM path: {hbm_path}")

config = read_yaml_file(hbm_path)

# Create an instance of the DCDC class, pass the path to the config file and connect to the device.
hbm = Gen4T(config["gen4t"])
dyno = hbm.read.Dyno
print("Start of print statements for HBM object")

time.sleep(1)

print(f"HBM BUS: {hbm._bus}")
pprint(f"HBM Database: {hbm._db}")

end_time = time.time() + 2

print(f" DYNO SPEED: {dyno.Measured}")
