# TODO: MOVE TO RELEVENT EXAMPLE SCRIPTS FOLDER AND MAKE SURE IT WORKS

import time

from program.src.constants import DEFAULT_PATHS
from program.src.equipment.controllers.library.nidec import NidecDyno
from program.src.file_ops import read_yaml_file

# Test Parameters
TEST_SPEED = 300
TIME_AT_SPEED = 5
DYNO_LOCATION = "160_kw"

# Initilisation
dyno_path = DEFAULT_PATHS["equipment"]["configs"]["controllers"]["nidec"]
config = read_yaml_file(dyno_path)
# Create an instance of the Dyno class, pass the path to the config file and connect to the device.
dyno = NidecDyno(config[DYNO_LOCATION])

print("Start of example script to read the Dyno power supply")
print(f"Dyno path: {dyno_path}")
print("Start of print statements for Dyno object")

# Print all read commands
time.sleep(1)
print(f"Dyno Running? : {dyno.read.run()} BOOL")
print(f"Dyno DC Bus Voltage : {dyno.read.dc_bus_voltage()} V")
print(f"Dyno Direction : {dyno.read.direction()} BOOL")
print(f"Dyno Healthy? : {dyno.read.drive_healthy()} BOOL")
print(f"Dyno Drive Mode : {dyno.read.drive_mode()} BOOL")
print(f"Dyno Inverter Temperature : {dyno.read.inverter_temperature()} C")
print(f"Dyno Monitored Temperature 1 : {dyno.read.monitored_temperature_1()} C")
print(f"Dyno Monitored Temperature 2 : {dyno.read.monitored_temperature_2()} C")
print(f"Dyno Monitored Temperature 3 : {dyno.read.monitored_temperature_3()} C")
print(f"Dyno Protection Accumulator : {dyno.read.motor_protection_accumulator()} int")  #
print(f"Dyno Speed Actual : {dyno.read.speed()} rpm")
print(f"Dyno Speed Maximum : {dyno.read.speed_max()} rpm")
print(f"Dyno Speed Target : {dyno.read.speed_target()} rpm")
print(f"Dyno Symertrical Current Limit : {dyno.read.symmetrical_current_limit()} A")

# Warn User test will begin
time.sleep(1)
print(f"========================")
print(f"Running Test in 5 Second")
print(f"========================")

i = 5
while i > 0:
    time.sleep(1)
    i = i - 1
    print(f"{i}s...")

try:
    # Set the speed to 0rpm
    dyno.write.speed_target(0)
    print(f"Dyno Speed Target : {dyno.read.speed_target()} rpm")

    # Set the maximum speed limits to 300rpm
    dyno.write.speed_max(TEST_SPEED)
    print(f"Dyno Maximum Speed Limit : {dyno.read.speed_target()} rpm")

    # Enable the dyno's drive
    dyno.write.enable(True)
    print(f"Dyno Enabled: {dyno.read.enable()} BOOL")

    # Set the direction to Reverse
    dyno.write.direction(False)
    print(f"Dyno Direction : {dyno.read.direction()} BOOL")

    # Set the drive to run
    dyno.write.run(True)
    print(f"Dyno Running : {dyno.read.run()} BOOL")

    # Set the Dynos speed target to 100rpm
    dyno.write.speed_target(100)
    print(f"Dyno Speed Target : {dyno.read.speed_target()} rpm")

    # Loop until time counter expires and print current dyno speed
    end_time = time.time() + TIME_AT_SPEED
    while time.time() < end_time:
        print(f"Current Dyno Speed: {dyno.read.speed()} rpm")
        time.sleep(0.5)

    print(f"Setting speed to 0rpm")
    dyno.write.speed_target(0)
    end_time = time.time() + 5

    while (time.time() < end_time) and (dyno.read.speed() > 20):
        print(f"Current Dyno Speed: {dyno.read.speed()} rpm")
        time.sleep(0.5)
    print(f"Current Dyno Speed: {dyno.read.speed()} rpm")
    dyno.write.run(False)
    dyno.write.enable(False)

    time.sleep(1)
    print(f"========================")
    print(f"      Test Succesful    ")
    print(f"========================")

# If an error occurs, the Dyno outputs will be set to zero and disabled.
except Exception as e:
    print(f"!!!!!!!!!!!!!!!!!!!!!!!!")
    print(f"      Test ERROR       ")
    print(f"!!!!!!!!!!!!!!!!!!!!!!!!")
    print(f"Error: {e}")
    # Turn set the dyno target speed to
    dyno.write.speed_target(0)
    print(f"Dyno Target Speed: {dyno.read.speed_target()}")

    # While the speed is greater than 10rpm (Dyno side) wait until it is <= 10rpm, check every 0.1s
    while dyno.read.speed() > 10:
        print(f"Dyno Actual Speed: {dyno.read.speed()}")
        time.sleep(0.1)

    # Once the speed is less than 10rpm, set the max speed limits to 100rpm (Dyno Side).
    dyno.write.speed_max(100)
    print(f"Dyno Target Speed: {dyno.read.speed_max()}")

    time.sleep(0.5)

    # Disable the output
    dyno.write.run(False)
    time.sleep(0.5)
    dyno.write.enable(False)


# If for some reason the user interrupts the program, the Dyno outputs will be set to zero and disabled.
except KeyboardInterrupt:
    print(f"!!!!!!!!!!!!!!!!!!!!!!!!")
    print(f"      Test ERROR       ")
    print(f"!!!!!!!!!!!!!!!!!!!!!!!!")
    print(f"Keyboard Interupt")

    # Turn set the dyno target speed to
    dyno.write.speed_target(0)
    print(f"Dyno Target Speed: {dyno.read.speed_target()}")

    # While the speed is greater than 10rpm (Dyno side) wait until it is <= 10rpm, check every 0.1s
    while dyno.read.speed() > 10:
        print(f"Dyno Actual Speed: {dyno.read.speed()}")
        time.sleep(0.1)

    # Once the speed is less than 10rpm, set the max speed limits to 100rpm (Dyno Side).
    dyno.write.speed_max(100)
    print(f"Dyno Target Speed: {dyno.read.speed_max()}")

    time.sleep(0.5)

    # Disable the output
    dyno.write.run(False)
    time.sleep(0.5)
    dyno.write.enable(False)

print("End of print statements for Dyno object")

print("End of example script to read the Dyno power supply")
