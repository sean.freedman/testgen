# TODO: MOVE TO RELEVENT EXAMPLE SCRIPTS FOLDER AND MAKE SURE IT WORKS

import time
from pprint import pprint

from program.src.constants import DEFAULT_PATHS
from program.src.equipment.power_supplies.library.dcdc import DCDC
from program.src.file_ops import read_yaml_file

print("Start of example script to read the DCDC power supply")

dcdc_path = DEFAULT_PATHS["equipment"]["configs"]["power_supplies"]["dcdc"]
topology = DCDC.DCDC_SINGLE

print(f"DCDC path: {dcdc_path}")
print(f"DCDC topology: {topology}")

config = read_yaml_file(dcdc_path)

# Create an instance of the DCDC class, pass the path to the config file and connect to the device.
dcdc = DCDC(topology, config["dcdc"])
dcdc.write.output_enable()
print("Start of print statements for DCDC object")

print(f"DCDC BUS: {dcdc._bus}")
pprint(f"DCDC Database: {dcdc._db}")

# # Input Voltages
time.sleep(1)
print(f"DCDC Input Voltage: {dcdc.read.input_voltages()}")
print(f"DCDC Average Input Voltage: {dcdc.read.average_input_voltage()}")

# Input Currents
print(f"DCDC Input Current: {dcdc.read.input_currents()}")
print(f"DCDC Average Input Current: {dcdc.read.average_input_current()}")

# Output Voltages
print(f"DCDC Output Voltage: {dcdc.read.output_voltages()}")
print(f"DCDC Average Output Voltage: {dcdc.read.average_output_voltage()}")

# Output Currents
print(f"DCDC Output Current: {dcdc.read.output_currents()}")
print(f"DCDC Average Output Current: {dcdc.read.average_output_current()}")

# Igbt Temperatures
print(f"DCDC IGBT Temperature: {dcdc.read.igbt_temperatures()}")
print(f"DCDC Average IGBT Temperature: {dcdc.read.average_igbt_temperature()}")

try:
    # Set the DCDC current limit to 100A voltage target to 100V and enable the output(s).
    dcdc.write.output_current_limits(100.0, 100.0)
    time.sleep(0.5)

    # Enable the output(s).
    dcdc.write.output_enable(True)
    time.sleep(0.5)
    dcdc.write.output_voltage(100.0)
    time.sleep(1)

    # Print the output currents and voltages for 3 seconds, every 0.5 seconds.
    endtime = time.time() + 3
    while time.time() < endtime:
        # Output Voltages
        print(f"DCDC Output Voltage: {dcdc.read.output_voltages()}")
        print(f"DCDC Average Output Voltage: {dcdc.read.average_output_voltage()}")

        # Output Currents
        print(f"DCDC Output Current: {dcdc.read.output_currents()}")
        print(f"DCDC Average Output Current: {dcdc.read.average_output_current()}")
        time.sleep(0.5)

    # Set the DCDC outputs to zero and disable the output(s).
    dcdc.write.output_voltage(0.0)
    dcdc.write.output_enable(False)
    time.sleep(1)
    print(f"DCDC Output Voltage: {dcdc.read.output_voltages()}")
    print(f"DCDC Average Output Voltage: {dcdc.read.average_output_voltage()}")
    dcdc.write.output_current_limits(0.0, 0.0)
    time.sleep(1)
    print(f"DCDC Output Current: {dcdc.read.output_currents()}")
    print(f"DCDC Average Output Current: {dcdc.read.average_output_current()}")

# If an error occurs, the DCDC outputs will be set to zero and disabled.
except Exception as e:
    print(f"Error: {e}")
    dcdc.write.output_voltage(0.0)
    time.sleep(1)
    dcdc.write.output_current_limits(0.0, 0.0)
    time.sleep(1)
    print(f"DCDC Output Current: {dcdc.read.output_currents()}")
    print(f"DCDC Average Output Current: {dcdc.read.average_output_current()}")
    try:
        dcdc.write.output_enable(False)

    except Exception as e:
        print(f"Error: {e}")

# If for some reason the user interrupts the program, the DCDC outputs will be set to zero and disabled.
except KeyboardInterrupt:
    print(f"Keyboard Interrupt")
    dcdc.write.output_voltage(0.0)
    time.sleep(1)
    dcdc.write.output_current_limits(0.0, 0.0)
    time.sleep(1)
    print(f"DCDC Output Current: {dcdc.read.output_currents()}")
    print(f"DCDC Average Output Current: {dcdc.read.average_output_current()}")
    try:
        dcdc.write.output_enable(False)
    except Exception as e:
        print(f"Error: {e}")

print("End of print statements for DCDC object")

print("End of example script to read the DCDC power supply")
