@ECHO off
cls
:start
SET this_scripts_directory=%~dp0
SET PowerShellScriptPath=%this_scripts_directory%\program\install\ps_script.ps1
ECHO.
ECHO #############################
ECHO [    Testify Tool Install   ]
ECHO #############################
ECHO. 
ECHO    [1] Install Dependancies and Launch Testify Tool
ECHO.
ECHO    [2] Install Python 3.10.8, Paths, Dependancies and Launch TestGen Tool [Powershell]
ECHO. 
ECHO. !!!! PLEASE NOTE: 
ECHO.   This may change your path variables, if you have other versions of python installed.
ECHO.   If you do not want to change your path variables, please select option 1, but be aware
ECHO.   that some dependancies may not work with your version of python.. looking at you MatlabEngine.

set choice=
set /p choice=Type the [number] of the option you want to use.
ECHO. 

if not '%choice%'=='' set choice=%choice:~0,1%
if '%choice%'=='1' goto install_dep
if '%choice%'=='2' goto install_full

ECHO "%choice%" is not valid, try again
ECHO.
goto start

:install_dep
ECHO Installing Dependancies and Launching TestGen
python -m pip install -r %this_scripts_directory%\program\install\requirements.txt
python -m streamlit run %this_scripts_directory%\program\testify.py
goto end

:install_full
ECHO Installing Python 3.10.8, Paths, Dependancies and Launching TestGen Tool
PowerShell -NoProfile -ExecutionPolicy Bypass -Command "& {Start-Process PowerShell -ArgumentList '-NoProfile -ExecutionPolicy Bypass -File ""%PowerShellScriptPath%""' -Verb RunAs}"
goto end

:end
pause