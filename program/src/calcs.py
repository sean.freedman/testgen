import numpy as np


def rms_to_peak(rms_value):
    """_summary_

    Args:
        rms_value (_type_): rms value to convert to peak

    Returns:
        _type_: converted peak value from rms
    """
    peak_value = rms_value * np.sqrt(2)
    return peak_value


def peak_to_rms(peak_value):
    """_summary_

    Args:
        peak_value (_type_): peak value to convert to rms

    Returns:
        _type_: converted rms value from peak
    """
    rms_value = peak_value / np.sqrt(2)
    return rms_value


def torque_em_rms(pole_pairs, psi_m, i_d, i_q, l):
    """_summary_

    Args:
        pole_pairs (_type_): number of machine pole pairs
        psi_m (_type_): machine flux rms
        i_d (_type_): direct current rms
        i_q (_type_): quadrature current rms
        l_d (_type_): direct inductance
        l_q (_type_): quadrature inductance

    Returns:
        _type_: electro-magenetic torque
    """
    T_em = (3 * (pole_pairs)) * ((psi_m * i_q) + (l) * (i_d * i_q))
    return T_em


def torque_em_peak(pole_pairs, psi_m, i_d, i_q, l_d, l_q):
    """_summary_

    Args:
        pole_pairs (_type_): number of machine pole pairs
        psi_m (_type_): machine flux peak
        i_d (_type_): direct current peak
        i_q (_type_): quadrature current peak
        l_d (_type_): direct inductance
        l_q (_type_): quadrature inductance

    Returns:
        _type_: electro-magenetic torque
    """
    T_em = (3 * (pole_pairs / 2)) * ((psi_m * i_q) + (l_d - l_q) * (i_d * i_q))
    return T_em


def inductance_rms(torque, pole_pairs, i_d, i_q, psi_m):
    """_summary_

    Args:
        torque (_type_): _description_
        pole_pairs (_type_): _description_
        i_d (_type_): _description_
        i_q (_type_): _description_
        psi_m (_type_): _description_

    Returns:
        _type_: _description_
    """
    l = ((torque) / (3 * pole_pairs * i_d * i_q)) - (psi_m / i_d)
    return l


def inductance_d(u_q, w_e, pole_pairs, psi_m, i_d):
    """_summary_

    Args:
        u_q (_type_): _description_
        w_e (_type_): _description_
        pole_pairs (_type_): _description_
        psi_m (_type_): _description_
        i_d (_type_): _description_

    Returns:
        _type_: _description_
    """
    l_d = ((u_q / (w_e * pole_pairs)) - psi_m) / i_d
    return l_d


def inductance_q(u_d, n_rads, pole_pairs, i_q):
    """_summary_

    Args:
        u_d (_type_): _description_
        w_e (_type_): _description_
        i_q (_type_): _description_

    Returns:
        _type_: _description_
    """
    l_q = (-1 * u_d) / (n_rads * pole_pairs * i_q)
    return l_q


def psi_d(l_d, i_d, psi_m):
    """_summary_

    Args:
        l_d (_type_): _description_
        i_d (_type_): _description_
        psi_m (_type_): _description_

    Returns:
        _type_: _description_
    """
    psi_d = l_d * i_d + psi_m
    return psi_d


def psi_q(l_q, i_q):
    """_summary_

    Args:
        l_d (_type_): _description_
        i_q (_type_): _description_

    Returns:
        _type_: _description_
    """
    psi_q = l_q * i_q
    return psi_q


def psi_m(torque, pole_pairs, i_q):
    """_summary_

    Args:
        torque (_type_): _description_
        pole_pairs (_type_): _description_
        i_q (_type_): _description_

    Returns:
        _type_: _description_
    """
    psi_m = torque / (3 * pole_pairs * i_q)
    return psi_m
