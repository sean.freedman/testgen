import math
import os

FILE_EXTENSIONS = {
    "CSV": ".csv",
    "JSON": ".json",
    "PNG": ".png",
    "PDF": ".pdf",
    "XLSX": ".xlsx",
    "XLS": ".xls",
    "TXT": ".txt",
    "LOG": ".log",
    "PY": ".py",
    "HTML": ".html",
    "XML": ".xml",
    "YAML": ".yaml",
    "YML": ".yml",
    "DOCX": ".docx",
    "DOC": ".doc",
    "MAT": ".mat",
    "LOG": ".log",
    "DBC": ".dbc",
    "EDS": ".eds",
    "ZIP": ".zip",
}

DEFAULT_FILE_SUFFIX = {
    "alerts": "_ALERTS",
    "inverter_properties": "_INVERTER_PROPERTIES",
    "current_sensor_latencies": "_CURRENT_SENSOR_LATENCY_TABLE",
    "idq": {
        "torque": {
            "eletro_magnetic": "_IDQ_TORQUE_EM",
            "measured": "_IDQ_TORQUE_MEASURED",
        },
        "tables": {
            "excel": "_IDQ_TABLES_EXCEL",
            "matlab": "_IDQ_TABLES_MATLAB",
        },
    },
    "torque_averaged": "_TAVG",
    "transducer": "_TRANSDUCER",
    "table_gen": {
        "data": "_interpolated_data",
        "table": "_interpolated_table",
    },
}

DEFAULT_FILE_PREFIX = {
    "idq": {
        "results": {
            "characterisation": "IDQ_CHAR_RESULTS_",
        },
        "tables": {
            "tables": "IDQ_TABLES_",
        },
    },
    "torque_speed": {
        "results": {
            "sweep": "TRQ_SPD_SWEEP_RESULTS_",
        },
    },
}

### PATHS ###
CURRENT_PATH = os.getcwd()

SCRIPT_PATH = CURRENT_PATH + r"\program\src\scripts" + "\\"

DEFAULT_PATHS = {
    "root": CURRENT_PATH,
    "equipment": {
        "configs": {
            "controllers": {
                "gen_5": CURRENT_PATH + r"\program\src\equipment\controllers\configs\gen_5" + FILE_EXTENSIONS["YAML"],
                "gen_4": CURRENT_PATH + r"\program\src\equipment\controllers\configs\gen_4" + FILE_EXTENSIONS["YAML"],
                "nidec": CURRENT_PATH + r"\program\src\equipment\controllers\configs\nidec" + FILE_EXTENSIONS["YAML"],
            },
            "motors": {
                "induction": CURRENT_PATH + r"\program\src\equipment\motors\configs\induction" + FILE_EXTENSIONS["YAML"],
                "pmac": CURRENT_PATH + r"\program\src\equipment\motors\configs\pmac" + FILE_EXTENSIONS["YAML"],
            },
            "power_analysers": {
                "hbm": CURRENT_PATH + r"\program\src\equipment\power_analysers\configs\hbm" + FILE_EXTENSIONS["YAML"],
                "zes_zimmmer": CURRENT_PATH + r"\program\src\equipment\power_analysers\configs\zes_zimmer" + FILE_EXTENSIONS["YAML"],
            },
            "power_supplies": {
                "dcdc": CURRENT_PATH + r"\program\src\equipment\power_supplies\configs\dcdc" + FILE_EXTENSIONS["YAML"],
                "tenma": CURRENT_PATH + r"\program\src\equipment\power_supplies\configs\tenma" + FILE_EXTENSIONS["YAML"],
            },
            "torque_sensors": {
                "hbm": CURRENT_PATH + r"\program\src\equipment\torque_sensors\configs\hbm" + FILE_EXTENSIONS["YAML"],
                "sensor_technologies": CURRENT_PATH + r"\program\src\equipment\torque_sensors\configs\sensor_technologies" + FILE_EXTENSIONS["YAML"],
            },
        },
        "library": {
            "controllers": {
                "gen_5": CURRENT_PATH + r"\program\src\equipment\controllers\library\gen_5" + FILE_EXTENSIONS["PY"],
                "gen_4": CURRENT_PATH + r"\program\src\equipment\controllers\library\gen_4" + FILE_EXTENSIONS["PY"],
                "nidec": CURRENT_PATH + r"\program\src\equipment\controllers\library\nidec" + FILE_EXTENSIONS["PY"],
            },
            "power_analysers": {
                "hbm": CURRENT_PATH + r"\program\src\equipment\power_analysers\library\hbm" + FILE_EXTENSIONS["PY"],
                "zes_zimmmer": CURRENT_PATH + r"\program\src\equipment\power_analysers\library\zes_zimmer" + FILE_EXTENSIONS["PY"],
            },
            "power_supplies": {
                "dcdc": CURRENT_PATH + r"\program\src\equipment\power_supplies\library\dcdc" + FILE_EXTENSIONS["PY"],
                "tenma": CURRENT_PATH + r"\program\src\equipment\power_supplies\library\tenma" + FILE_EXTENSIONS["PY"],
            },
            "torque_sensors": {
                "hbm": CURRENT_PATH + r"\program\src\equipment\torque_sensors\library\hbm" + FILE_EXTENSIONS["PY"],
                "sensor_technologies": CURRENT_PATH + r"\program\src\equipment\torque_sensors\library\sensor_technologies" + FILE_EXTENSIONS["PY"],
            },
            "databases": {
                "controllers": {
                    "gen_5": CURRENT_PATH + r"\program\src\equipment\controllers\library\gen_5" + FILE_EXTENSIONS["DBC"],
                    "gen_4": CURRENT_PATH + r"\program\src\equipment\controllers\library\gen_4" + FILE_EXTENSIONS["DBC"],
                },
                "power_analysers": {
                    "hbm": CURRENT_PATH + r"\program\src\equipment\power_analysers\databases\hbm" + FILE_EXTENSIONS["DBC"],
                },
                "power_supplies": {
                    "dcdc": CURRENT_PATH + r"program\src\equipment\power_supplies\databases\dcdc" + FILE_EXTENSIONS["DBC"],
                },
                "torque_sensors": {
                    "hbm": CURRENT_PATH + r"\program\src\equipment\torque_sensors\library\hbm" + FILE_EXTENSIONS["DBC"],
                    "sensor_technologies": CURRENT_PATH + r"\program\src\equipment\torque_sensors\library\sensor_technologies" + FILE_EXTENSIONS["DBC"],
                },
            },
        },
    },
    "script": {
        "root": SCRIPT_PATH,
        "test": CURRENT_PATH + r"\test_script" + FILE_EXTENSIONS["PY"],
        "automation": {
            "characterisation": {
                "idq": {
                    "gen_5": SCRIPT_PATH + r"\automation\characterisation\idq\gen_5_characterisation_idq_script" + FILE_EXTENSIONS["PY"],
                    "gen_4": SCRIPT_PATH + r"\automation\characterisation\idq\gen_4_characterisation_idq_script" + FILE_EXTENSIONS["PY"],
                },
            },
            "torque_speed": {
                "sweep": {
                    "gen_5": SCRIPT_PATH + r"\automation\torque_speed\sweep\gen_5_torque_speed_sweep_script" + FILE_EXTENSIONS["PY"],
                    "gen_4": SCRIPT_PATH + r"\automation\torque_speed\sweep\gen_4_torque_speed_sweep_script" + FILE_EXTENSIONS["PY"],
                },
            },
            "encoder_alignment": {
                "idq": {
                    "gen_5": SCRIPT_PATH + "gen_5_encoder_alignment_script" + FILE_EXTENSIONS["PY"],
                    "gen_4": SCRIPT_PATH + "gen_4_encoder_alignment_script" + FILE_EXTENSIONS["PY"],
                },
            },
        },
        "analysis": {
            "root": CURRENT_PATH + "\\" + r"program\src\scripts\analysis",
            "idet_src": CURRENT_PATH + "\\" + r"program\src\scripts\analysis\idet_table_src",
            "temp": CURRENT_PATH + "\\" + r"program\src\scripts\analysis\temp",
        },
    },
    "log": {
        "console": CURRENT_PATH + r"\console_out" + FILE_EXTENSIONS["LOG"],
        "test": CURRENT_PATH + r"\Log",
    },
    "folders": {
        "remote": {
            "characterisation": {
                "idq": {
                    "results": {
                        "gen_5": r"N:\UK\Engineering\Documents\Motor Control\Dyno Test Reports\characterisation\idq\gen_5\results",
                        "gen_4": r"N:\UK\Engineering\Documents\Motor Control\Dyno Test Reports\characterisation\idq\gen_4\results",
                    },
                    "tables": {
                        "gen_5": r"N:\UK\Engineering\Documents\Motor Control\Dyno Test Reports\characterisation\idq\gen_5\tables",
                        "gen_4": r"N:\UK\Engineering\Documents\Motor Control\Dyno Test Reports\characterisation\idq\gen_4\tables",
                    },
                },
                "qt": {
                    "results": {
                        "gen_5": r"N:\UK\Engineering\Documents\Motor Control\Dyno Test Reports\characterisation\qt\gen_5\results",
                        "gen_4": r"N:\UK\Engineering\Documents\Motor Control\Dyno Test Reports\characterisation\qt\gen_4\results",
                    },
                    "tables": {
                        "gen_5": r"N:\UK\Engineering\Documents\Motor Control\Dyno Test Reports\characterisation\qt\gen_5\tables",
                        "gen_4": r"N:\UK\Engineering\Documents\Motor Control\Dyno Test Reports\characterisation\qt\gen_4\tables",
                    },
                },
                "torque_speed": {
                    "sweep": {
                        "gen_5": r"N:\UK\Engineering\Documents\Motor Control\Dyno Test Reports\torque_speed\sweep\gen_5",
                        "gen_4": r"N:\UK\Engineering\Documents\Motor Control\Dyno Test Reports\torque_speed\sweep\gen_4",
                    },
                },
            },
        },
        "export": CURRENT_PATH + "\\",
    },
    "files": {
        "idq": {
            "data": {
                "results": {
                    "measured": None,
                    "transducer": None,
                    "alerts": None,
                    "zip": None,
                    "base": None,
                    "report": None,
                },
                "inverter": {
                    "current_sensor_latencies": None,
                    "meta_data": None,
                },
            },
        },
        "torque_speed": {
            "data": {
                "results": {
                    "measured": None,
                    "alerts": None,
                },
                "inverter": {
                    "current_sensor_latencies": None,
                    "meta_data": None,
                },
            },
        },
    },
}

OPERATING_QUADRANTS = [
    "Forward_Motoring",
    "Reverse_Generating",
    "Reverse_Motoring",
    "Forward_Generating",
    "Motoring",
    "Generating",
    "Forward",
    "Reverse",
    "All",
]

AVAILABLE_PROJECTS = ["Hawkshead", "Derwent", "Bowfell", "Oxford", "Blencathra: PLACEHOLDER", "Ultraviolet : PLACEHOLDER", "Virya : PLACEHOLDER", "Other: PLACEHOLDER"]
FIELD_INVERTER_NAME = AVAILABLE_PROJECTS[1]
AVAILABLE_SAMPLE_LETTERS = ["A", "B", "C", "D", "Other"]
FIELD_INVERTER_SAMPLE_LETTER = AVAILABLE_SAMPLE_LETTERS[1]
AVAILABLE_SAMPLE_NUMBERS = ["1", "2", "3", "4"]
FIELD_INVERTER_SAMPLE_NUMBER = AVAILABLE_SAMPLE_NUMBERS[1]
MOTOR_MANUFACTURES = [
    "Turntide",
    "Ashwoods",
    "Remy",
    "Avid",
    "Yasa",
    "Integral Powertrain",
    "Emrax",
    "Other",
]
SAMPLE_LETTER = ["A", "B", "C", "D", "Other"]
SAMPLE_NUMBER = ["1", "2", "3", "4", "N/A"]
DYNO_LOCATIONS = ["D7: 340kW", "D8: 160kW", "D4: 100kW", "DX: B2B"]
TEST_TYPES = [
    "Torque Speed Sweep",
    "Idq Characterisation Gen 5",
    "Idq Characterisation Gen 4: PARTIAL - BUT NOT AVAILABLE",
    "Idq Injection",
    "Idq Injection Gen 4: PARTIAL - BUT NOT AVAILABLE",
    "Udq Injection: PLACEHOLDER",
    "Encoder Alignment: PLACEHOLDER",
    "Back EMF Calculation: PLACEHOLDER",
    "Encoder Latency Table: PLACEHOLDER",
    "Current Latency Table: PLACEHOLDER",
]
OPERATING_QUADRANTS = [
    "Forward_Motoring",
    "Reverse_Generating",
    "Reverse_Motoring",
    "Forward_Generating",
    "Motoring",
    "Generating",
    "Forward",
    "Reverse",
    "All",
]


TORQUE_SENSORS = ["HBM", "TS", "None"]
CAN_DEVICES = ["vector", "ixxat"]
CAN_BITRATES = [1000000, 500000, 250000, 125000]
INTERNAL_NAME = "test_script"
LV_PSU_VOLTAGE = 16.00

FLT_ZERO = 0.0

LOGIN_TIMER = 10
DATE_TIME_FORMAT = "%Y_%m_%d_%H_%M_%S_"
GLOBAL_COLORMAP = "viridis"
# TODO REMOVE HBM STUFF TO ITS OWN FILE i.e equipment>power_analyser>hbm.py
HBM_CAN_DATABASE = r"C:\SVN\Dyno Configs\D7 [340kW]\CANanalyzer\IPK2200316" + FILE_EXTENSIONS["DBC"]
HBM_CAN_FILTER = ["Torque & Speed", "Placeholder_1"]
HBM_CAN_DEFAULTS = {
    "Torque ID": "0x3AC",
    "Torque Name": "TT_MUT_Torque",
    "Speed ID": "0x3AB",
    "Speed Name": "TT_MUT_Speed_rpm",
}
THIRTY_TWO_ZEROS = "00000000000000000000000000000000"

NUMBER_FORMATTERS = {
    "nano": "%0.9f",
    "micro": "%0.6f",
    "milli": "%0.3f",
    "kilo": "%0.3f",
    "mega": "%0.6f",
    "giga": "%0.9f",
}

FLOATS = {
    "nano": 1e-9,
    "micro": 1e-6,
    "milli": 1e-3,
    "zero": 0.0,
    "kilo": 1e3,
    "mega": 1e6,
    "giga": 1e9,
}


# ------------------  GEN5 CONSTANTS ------------------
GEN5_ID_ID = "0x40570029"
GEN5_IQ_ID = "0x4057002A"
GEN5_TMODE_ID = "0x40570027"
GEN5_TMODE_AXIS_ID = "0x40570028"

# ------------------  GEN4 CONSTANTS ------------------
# TODO : THIS was all provided by Jacob for the Gen4 / Ultraviolet / Virya project
GEN4_ID_ID = "0x003F8E0A"
GEN4_IQ_ID = "0x003F9610"

GEN4_BITRATE = 500000

GEN4_EDS = "C:\DVT\common\program\EDS\Gen4_pc0x0705302d_rev0x0001002a" + FILE_EXTENSIONS["EDS"]

FIXED_POINT_SCALE_D = int(65535 / 1110.72)
FIXED_POINT_SCALE_Q = int(65535 / 1091.72)

BRIDGE_ENABLE = 0xF
BRIDGE_SWITCH_ON = 0x7
BRIDGE_DISABLE = 0x6

STATUS_WORD_ENABLED = 1079
STATUS_WORD_DISABLED = 1073
IXXAT_CHANNEL = 0


# ------------------ INVERTER PARAMETER ------------------
# TODO : Satish to provide guidance on the below or change to YAML.

EMPTY_JSON = {
    "PROJECT_MOTOR_VERSION": {
        "version": {
            "DataType": "uint32",
            "Dimension": 1,
            "Description": "version",
            "Value": math.nan,
        },
        "ConfigRevision": {
            "DataType": "uint32",
            "Dimension": 1,
            "Description": "Configuration revision number",
            "Value": math.nan,
        },
        "ConfigDescription": {
            "DataType": "char",
            "Dimension": 1,
            "Description": "Description of Lookup tables data",
            "Value": math.nan,
        },
        "Cfg_SectorBrkPtr": {
            "DataType": "float32",
            "Dimension": 1,
            "Description": "Cfg_SectorBrkPt",
            "Value": math.nan,
        },
        "Cfg_RotorSpeedBrkPtr": {
            "DataType": "float32",
            "Dimension": 1,
            "Description": "RotorSpeed Breakpoint",
            "Value": math.nan,
        },
        "Cfg_TorqueBrkPtr": {
            "DataType": "float32",
            "Dimension": 1,
            "Description": "Torque Demand Breakpoint",
            "Value": math.nan,
        },
        "Cfg_VoltageBrkPtr": {
            "DataType": "float32",
            "Dimension": 1,
            "Description": "Dc-link Voltage Breakpoint",
            "Value": math.nan,
        },
        "Cfg_MotorRsTemp1Ptr": {
            "DataType": "float32",
            "Dimension": 1,
            "Description": "Cfg_MotorRsTempBrkPt",
            "Value": math.nan,
        },
        "Cfg_MotorRsTD1BrkPtr": {
            "DataType": "float32",
            "Dimension": 1,
            "Description": "Cfg_MotorRsTD",
            "Value": math.nan,
        },
        "Cfg_MotorRsTemp2Ptr": {
            "DataType": "float32",
            "Dimension": 1,
            "Description": "Cfg_MotorRsTempBrkPt",
            "Value": math.nan,
        },
        "Cfg_MotorRsTD2BrkPtr": {
            "DataType": "float32",
            "Dimension": 1,
            "Description": "Cfg_MotorRsTD",
            "Value": math.nan,
        },
        "Cfg_MotorTemp1CutbackTDPtr": {
            "DataType": "float32",
            "Dimension": 1,
            "Description": "Cfg_MotorTemp1CutbackTD",
            "Value": math.nan,
        },
        "Cfg_MotorTemp1CutbackBrkPtr": {
            "DataType": "float32",
            "Dimension": 1,
            "Description": "Cfg_MotorTemp1CutbackBrkPt",
            "Value": math.nan,
        },
        "Cfg_MotorTemp2CutbackTDPtr": {
            "DataType": "float32",
            "Dimension": 1,
            "Description": "Cfg_MotorTemp2CutbackTD",
            "Value": math.nan,
        },
        "Cfg_MotorTemp2CutbackBrkPtr": {
            "DataType": "float32",
            "Dimension": 1,
            "Description": "Cfg_MotorTemp2CutbackBrkPt",
            "Value": math.nan,
        },
        "Cfg_ModInputBrkPtr": {
            "DataType": "float32",
            "Dimension": 1,
            "Description": "Cfg_ModInputBrkPt",
            "Value": math.nan,
        },
        "Cfg_IqCurrentToFluxBrkPtr": {
            "DataType": "float32",
            "Dimension": 1,
            "Description": "Cfg_IqCurrentToFluxBrkPtr",
            "Value": math.nan,
        },
        "Cfg_IdCurrentBrkPtr": {
            "DataType": "float32",
            "Dimension": 1,
            "Description": "Cfg_IdCurrentBrkPtr",
            "Value": math.nan,
        },
        "Cfg_IqCurrentBrkPtr": {
            "DataType": "float32",
            "Dimension": 1,
            "Description": "Cfg_IqCurrentBrkPtr",
            "Value": math.nan,
        },
        "Cfg_AngleHoldRadTblPtr": {
            "DataType": "float32",
            "Dimension": 1,
            "Description": "Cfg_AngleHoldRadTbl",
            "Value": math.nan,
        },
        "Cfg_RotorMagnetFluxTblPtr": {
            "DataType": "float32",
            "Dimension": 1,
            "Description": "Cfg_RotorMagnetFluxTblPtr",
            "Value": math.nan,
        },
        "Cfg_IdBrakingTblPtr": {
            "DataType": "float32",
            "Dimension": 3,
            "Description": "Cfg_IdBrakingTbl ",
            "Value": math.nan,
        },
        "Cfg_IqBrakingTblPtr": {
            "DataType": "float32",
            "Dimension": 3,
            "Description": "Cfg_IqBrakingTbl",
            "Value": math.nan,
        },
        "Cfg_IdMotoringTblPtr": {
            "DataType": "float32",
            "Dimension": 3,
            "Description": "Cfg_IdMotoringTbl",
            "Value": math.nan,
        },
        "Cfg_IqMotoringTblPtr": {
            "DataType": "float32",
            "Dimension": 3,
            "Description": "Cfg_IqMotoringTbl",
            "Value": math.nan,
        },
        "Cfg_TpeakVdcMtrTblPtr": {
            "DataType": "float32",
            "Dimension": 2,
            "Description": "Cfg_TpeakVdcMtrTbl",
            "Value": math.nan,
        },
        "Cfg_TpeakVdcBreakTblPtr": {
            "DataType": "float32",
            "Dimension": 2,
            "Description": "Cfg_TpeakVdcBreakTbl",
            "Value": math.nan,
        },
        "Cfg_LdAxisTblPtr": {
            "DataType": "float32",
            "Dimension": 2,
            "Description": "Cfg_LdAxisTblPtr",
            "Value": math.nan,
        },
        "Cfg_LqAxisTblPtr": {
            "DataType": "float32",
            "Dimension": 2,
            "Description": "Cfg_LqAxisTblPtr",
            "Value": math.nan,
        },
        "I_controller_Ki_d": {
            "DataType": "float32",
            "Dimension": 1,
            "Description": "I_controller_Ki_d",
            "Value": math.nan,
        },
        "I_controller_Ki_q": {
            "DataType": "float32",
            "Dimension": 1,
            "Description": "I_controller_Ki_q",
            "Value": math.nan,
        },
        "I_controller_Kp_d": {
            "DataType": "float32",
            "Dimension": 1,
            "Description": "I_controller_Kp_d",
            "Value": math.nan,
        },
        "I_controller_Kp_q": {
            "DataType": "float32",
            "Dimension": 1,
            "Description": "I_controller_Kp_q",
            "Value": math.nan,
        },
        "IsMax": {
            "DataType": "float32",
            "Dimension": 1,
            "Description": "IsMax",
            "Value": math.nan,
        },
        "Ke_Line_Rms": {
            "DataType": "float32",
            "Dimension": 1,
            "Description": "Ke_Line_Rms",
            "Value": math.nan,
        },
        "MI_controller_Ki": {
            "DataType": "float32",
            "Dimension": 1,
            "Description": "MI_controller_Ki",
            "Value": math.nan,
        },
        "MI_controller_Kp": {
            "DataType": "float32",
            "Dimension": 1,
            "Description": "MI_controller_Kp",
            "Value": math.nan,
        },
        "Npp": {
            "DataType": "float32",
            "Dimension": 1,
            "Description": "Npp",
            "Value": math.nan,
        },
        "Rs": {
            "DataType": "float32",
            "Dimension": 1,
            "Description": "Rs",
            "Value": math.nan,
        },
        "QciErrorThreshold": {
            "DataType": "float32",
            "Dimension": 1,
            "Description": "QciErrorThreshold",
            "Value": math.nan,
        },
        "IdqTrackErrThreshold": {
            "DataType": "float32",
            "Dimension": 1,
            "Description": "IdqTrackErrThreshold",
            "Value": math.nan,
        },
        "VectorPIEnable": {
            "DataType": "float32",
            "Dimension": 1,
            "Description": "VectorPIEnable",
            "Value": math.nan,
        },
        "Ld_Config": {
            "DataType": "float32",
            "Dimension": 1,
            "Description": "Ld_Config",
            "Value": math.nan,
        },
        "Lq_Config": {
            "DataType": "float32",
            "Dimension": 1,
            "Description": "Ld_Config",
            "Value": math.nan,
        },
        "OverSpeedFaultDelay": {
            "DataType": "uint32",
            "Dimension": 1,
            "Description": "OverSpeedFaultDelay",
            "Value": math.nan,
        },
        "OverTempFaultDelay": {
            "DataType": "uint32",
            "Dimension": 1,
            "Description": "OverTempFaultDelay",
            "Value": math.nan,
        },
        "BlobInfo": {
            "TargetSector": "Configuration",
            "TargetProcessor": "MCP",
            "BlockID": "0x57",
            "PropertyID": "0x1122",
            "FileDescriptor": "MotorCfgTable",
            "BootloaderTriggerAddress": "539164673",
            "TriggerValue": "1",
        },
    }
}
