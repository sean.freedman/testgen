from typing import Union

import can
import cantools


class WritePeriodicSignalError(Exception):
    """Exception raised when failed to write periodic signal."""

    pass


class SignalNotFoundError(Exception):
    """Exception raised when a CAN signal is not found in a received message."""

    pass


class CanDevice:
    """
    A class representing a CAN device.

    Attributes:
    - _interface: A string representing the interface to use (e.g. "socketcan").
    - _channel: An integer representing the channel to use (e.g. 0 for socketcan).
    - _bitrate: An integer representing the bitrate of the connection.
    - _config: A dictionary representing the configuration options for the connection.

    Methods:
    - __init__(self): Initializes a new instance of the CanDevice class.
    """

    def __init__(self) -> None:
        """
        Initializes a new instance of the CanDevice class.
        """
        self._interface: Union[str, None] = None
        self._channel: Union[int, None] = None
        self._bitrate: Union[int, None] = None
        self._config: Union[dict, None] = None

    def setup_can_bus(self, config: dict) -> can.interface.Bus:
        """
        Set up a CAN bus with the provided configuration and return a can.interface.Bus instance.

        Parameters:
        -----------
        config : dict
            A dictionary containing the configuration information for the CAN bus. The dictionary should include the
            following keys and values:
            {
                "can": {
                    "interface": <str>,   # Interface type (e.g. "socketcan", "vector")
                    "channel": <int>,    # Channel number (e.g. 0, 1, 2)
                    "bitrate": <int>     # Bitrate (e.g. 500000)
                }
            }

        Returns:
        --------
        can.interface.Bus
            A can.interface.Bus object representing the CAN bus configured with the specified parameters.

        Raises:
        -------
        ValueError
            If the specified interface type is not recognized or the channel is out of range.
        can.CanError
            If there is an error setting up the bus with the specified configuration.
        """

        # Extract interface, channel and bitrate from the configuration dictionary
        self._config = config
        self._interface = self._config["can"]["interface"]
        self._channel = self._config["can"]["channel"]
        self._bitrate = self._config["can"]["bitrate"]

        # Adjust the channel number if the interface is Vector.
        if self._interface == "vector":
            self._channel = self._channel - 1

        try:
            # Create the CAN bus object with the extracted configuration.
            return can.interface.Bus(channel=self._channel, bustype=self._interface, bitrate=self._bitrate)

        except can.CanError as e:
            # Raise an error and print a message if there is an issue setting up the bus with the specified configuration.
            raise ValueError(f"Failed to setup the CAN bus: {e}") from e

        except (ValueError, TypeError) as e:
            # Raise an error if the provided interface or channel are invalid.
            raise ValueError(f"Invalid interface or channel number: {e}") from e

    def load_database(self, db: str) -> cantools.db.Database:
        """
        Load the given CAN database file into memory and return a cantools Database instance.

        Parameters:
        -----------
        db : str
            The file path of the CAN database to load.

        Returns:
        --------
        cantools.db.Database
            An instance of the loaded CAN database.

        Raises:
        -------
        FileNotFoundError
            If the provided database file could not be found.
        """

        try:
            # Load the CAN database from the specified file path
            return cantools.database.load_file(db)

        except FileNotFoundError as e:
            # Raise an error and print a message if the file was not found
            print(f"{e} - Failed to load the database.")
            raise

    def setup_notifier(self, bus, listeners) -> None:
        """
        Set up a new notifier with the provided listeners.

        Parameters:
        -----------
        listeners : List[Union[Callable[[can.Message], None], Tuple[Callable[[can.Message], None], Dict]]]
            A list of listener functions or tuples where each tuple contains a listener function and a dictionary of
            message filters.

        Returns:
        --------
        None

        Raises:
        -------
        TypeError
            If the provided listeners argument is not a list.
        """

        # Check if the provided argument is a list.
        if not isinstance(listeners, list):
            raise TypeError("Listeners argument must be a list.")

        # Create a new notifier with the current bus and the provided listeners.
        return can.Notifier(bus, listeners)

    def setup_listener(self) -> can.Listener:
        """
        Set up a new listener for the current bus.

        Returns:
        --------
        can.Listener
            A new listener object that has been set up to receive messages on the current bus.
        """

        # Create a new listener object for the current bus.
        return can.Listener()

    def get_database_values(self, database) -> dict:
        """
        NOT TESTED
        Get the initial and limits of the signals in the CAN database.

        Parameters:
        -----------
        None

        Returns:
        --------
        None
        """

        for message in database.messages:
            for signal in message.signals:
                self.database.setdefault(message.name, {})
                self.database[message.name].setdefault(signal.name, {})
                # print the attributes of the signal

                # Add checks for signal attributes before accessing them
                if hasattr(signal, "initial"):
                    self.database[message.name][signal.name]["initial"] = signal.initial
                    print(signal.initial)
                else:
                    self.database[message.name][signal.name]["initial"] = None

                if hasattr(signal, "maximum"):
                    self.database[message.name][signal.name]["maximum"] = signal.maximum

                else:
                    self.database[message.name][signal.name]["maximum"] = None

                if hasattr(signal, "minimum"):
                    self.database[message.name][signal.name]["minimum"] = signal.minimum
                else:
                    self.database[message.name][signal.name]["minimum"] = None

                self.database[message.name][signal.name]["value"] = None

    def read_message(self, message_recieved, message_name_lookup: str) -> can.Message:
        """
        Read a CAN message.

        Parameters:
        -----------
        message_name_lookup : str
            The name of the CAN message to look up.
        timeout : float, optional
            The amount of time to wait for the CAN message, in seconds. Default is 1.0.
        sleep_time: float, optional
            The amount of time to sleep between CAN message reads, in seconds. Default is 0.1.

        Returns:
        --------
        can.Message
            The received CAN message object.

        Raises:
        -------
        TimeoutError
            If the waiting time for the CAN message expires.
        """

        # Look up the message object for the desired message name.
        message = self._db.get_message_by_name(message_name_lookup)

        if message_recieved is not None and message_recieved.arbitration_id == message.frame_id:
            # Return the CAN message if it's found.
            return message_recieved

    def read_signal(self, message_recieved, message_name_lookup: str, signal_lookup: str):
        """
        Read a CAN signal.
        Parameters
        ----------
        message_name_lookup : str
            The name of the CAN message to look up.
        signal_lookup : str
            The name of the CAN signal to look up.
        timeout : float, optional
            The amount of time to wait for the CAN signal, in seconds. Default is 1.0.
        sleep_time: float, optional
            The amount of time to sleep between CAN message reads, in seconds. Default is 0.1.
        Returns
        -------
        signal_value
            The value of the CAN signal.
        Raises
        ------
        TimeoutError
            If the waiting time for the CAN signal expires.
        SignalNotFoundError
            If the CAN message with the correct arbitration ID is received but does not contain the desired signal.
        """
        # Read the CAN message first.
        can_message = self.read_message(message_recieved, message_name_lookup)

        # Decode the message to get the signal value.
        decoded_message = self._db.decode_message(can_message.arbitration_id, can_message.data)

        if signal_lookup in decoded_message:
            signal_value = decoded_message[signal_lookup]
            # Return the signal value if it's found.
            return signal_value

        else:
            raise CanDevice.SignalNotFoundError(f"CAN message {message_name_lookup} ({can_message.arbitration_id}) received but does not contain signal {signal_lookup}")

    def read_periodic_message(self, peroidic_dictionary: dict, message_name_lookup: str) -> dict:
        """
        For a dictionary (in this case of periodic messages), search the keys (message)
        return the value of the key (in this case the signals for that message).

        Parameters:
        -----------
        peroidic_dictionary : dict
            The dictionary of periodic messages.
        message_name_lookup : str
            The name of the CAN message to look up.

        Returns:
        --------
        dict
            The received CAN message object.
        """

        # If message name is in the periodic dictionary
        if message_name_lookup in peroidic_dictionary:
            # Return the signals in the message
            return peroidic_dictionary[message_name_lookup]

    def read_periodic_signal(self, periodic_dictionary: dict, message_name_lookup: str, signal_lookup: str):
        """
        Read a CAN signal from a periodic message.

        Parameters:
        -----------
        periodic_dictionary : dict
            The dictionary of periodic messages.
        message_name_lookup : str
            The name of the CAN message to look up.
        signal_lookup : str
            The name of the CAN signal to look up.

        Returns:
        --------
        signal_value
            The value of the CAN signal.
        """

        # Get the dictionary values associcated in the dictionary with the message to lookup.
        signals = self.read_periodic_message(periodic_dictionary, message_name_lookup)

        # if the signal is the signal_lookup, return the value
        if signal_lookup in signals:
            signal_value = signals[signal_lookup]
            # Return the signal value if it's found.
            return signal_value

    def read_can_values(self, dictionary, message_received) -> dict:
        # get the message name
        message_name = self._db.get_message_by_frame_id(message_received.arbitration_id).name

        # decode message received
        try:
            decoded_message = self._db.decode_message(message_received.arbitration_id, message_received.data)
        except Exception as error:
            # handle decoding errors here
            print(f"Error: {error}")
            return dictionary

        # if the decoded message name is not none and within the dictionary, update the values of the signals within the dictionary.
        if message_name is not None and message_name in dictionary:
            if dictionary[message_name] is not None:
                for signal in dictionary[message_name]:
                    if signal in decoded_message:
                        dictionary[message_name][signal] = decoded_message[signal]

            return dictionary

    def write_periodic_signal(self, message_lookup: str, signal_lookup: str, value: float, period: float = 0.1) -> Union[None, WritePeriodicSignalError]:
        """
        periodically sends a Controller Area Network (CAN) message with a given signal value

        Args:
            can_messages (dict): The dictionary containing CAN messages and their signals.
            message_lookup (str): The name of the CAN message to modify.
            signal_lookup (str): The name of the signal within the message to modify.
            value (float): The value to assign to the specified signal.
            period (float): The interval at which to send the message, in seconds. Default is 0.1.

        Returns:
            None if successful; otherwise, WritePeriodicSignalError

        Raises:
            ValueError: Raised when the period is less than or equal to 0.0
            KeyError: Raised when the message_lookup is not found in the database
            WritePeriodicSignalError: Raised when there is a failure to send periodic CAN message
        """
        can_messages = self._periodic_can_messages
        # Check if the period is valid
        if period <= 0.0:
            raise ValueError(f"period must be greater than 0.0, requested: {period}")

        try:
            # Get the CanMessage from the database
            can_message = self._db.get_message_by_name(message_lookup)
        except KeyError:
            # If the message is not found raise an error
            raise CanDevice.WritePeriodicSignalError(f"{message_lookup} not found in database")

        # Update the given CAN signal value
        for signal in can_message.signals:
            if signal.name == signal_lookup:
                can_messages[message_lookup][signal_lookup] = value
                break

        try:
            # Try to send the modified CAN message periodically
            self.write_periodic_message(can_messages, period)

        except ConnectionError as error:
            # If there is a connection failure during sending, raise an error
            raise CanDevice.WritePeriodicSignalError(f"Failed to send periodic CAN message: {str(error)}")

    def write_periodic_message(self, messages: dict, period: float = 0.1) -> None:
        """
        Set up and send periodic CAN messages based on a dictionary of message names and signal values.

        Parameters
        ----------
        messages : dict
            A dictionary of CAN message names as keys and dictionaries of signal names and their corresponding values as values.
        period : float, optional
            The interval in seconds at which to send each periodic message. Default is 0.1 seconds.

        Returns
        -------
        None

        """
        if period <= 0.0:
            raise ValueError(f"Period must be greater than 0.0, requested: {period}")

        # Iterate over each CAN message in the dictionary "messages".
        for message_name in messages:
            signal_dictionary = {}

            # Look up the CAN message object from the database.
            can_msg = self._db.get_message_by_name(message_name)

            # For each signal in the message, check if it is in the current message's signal list, and get its value.
            for signal in can_msg.signals:
                if signal.name in messages[message_name]:
                    signal_dictionary[signal.name] = messages[message_name][signal.name]

            # Encode the message with the signal values.
            encoded_data = can_msg.encode(signal_dictionary)
            updated_message = can.Message(arbitration_id=can_msg.frame_id, data=encoded_data, is_extended_id=False)

            # Send the message periodically.
            if self._interface == "ixxat":
                """
                WHY DO I NEED TO STOP ALL PERIODIC TASKS BEFORE SENDING A NEW ONE?
                INFO: https://python-can.readthedocs.io/en/master/interfaces/ixxat.html
                The send_periodic() method is supported natively through the on-board cyclic transmit list.
                Modifying cyclic messages is not possible.
                You will need to stop it, and then start a new periodic message.
                """
                self._bus.stop_all_periodic_tasks()

            self._bus.send_periodic(updated_message, period)

    def write_signal(self, message_lookup: str, signal_lookup: str, value) -> None:
        """
        Write a CAN signal.

        Parameters
        ----------
        message_lookup : str
            The name of the CAN message that the signal belongs to.
        signal_lookup : str
            The name of the CAN signal to be written.
        value :
            The value of the CAN signal to be written.

        Returns
        -------
        None

        """

        try:
            # Get the CAN message object from the database.
            message = self._db.get_message_by_name(message_lookup)

            # Encode the signal value into a CAN message.
            message = message.encode({signal_lookup: value})

            # Send the CAN message containing the signal.
            self.write_message(message)

        except Exception as e:
            print(f"An error occurred while writing the signal: {e}")

    def write_message(self, message: can.Message) -> None:
        """
        Write a CAN message.

        Parameters
        ----------
        message : can.Message
            The CAN message to be written.

        Returns
        -------
        None.

        """

        # Send the CAN message.
        self._bus.send(message)
