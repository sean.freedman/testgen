import time

from program.src.constants import DEFAULT_PATHS
from program.src.equipment.controllers.library.gen_5 import Blencathra, Bowfell, Derwent, Hawkshead, Oxford
from program.src.file_ops import read_yaml_file

print("Start of example script to read the Gen5 controller")

project = "oxford"
gen_5_path = DEFAULT_PATHS["equipment"]["configs"]["controllers"]["gen_5"]
print(f"Project: {project}")
print(f"Gen5 path: {gen_5_path}")

config = read_yaml_file(gen_5_path)

# switch configs based on project
if project == "bowfell":
    # Bowfell
    mcu = Bowfell(config["bowfell"])
    print("Start of print statements for Bowfell object")

    # Print the bus and database
    print(f"Bus: {mcu.bus}")
    print(f"Database: {mcu.db}")

    # Print MCU based measurements
    print(f"MCU bridge state: {mcu.read.bridge_state()}")
    print(f"MCU torque demand: {mcu.read.torque_demand()}")
    print(f"MCU torque demand FUSA: {mcu.read.torque_demand_fusa()}")
    print(f"MCU torque demand actual: {mcu.read.torque_demand_actual()}")
    print(f"MCU speed: {mcu.read.speed()}")
    print(f"MCU DC link voltage: {mcu.read.dc_link_voltage()}")
    print(f"MCU DC link current: {mcu.read.dc_link_current()}")
    print(f"MCU inverter temperature: {mcu.read.inverter_temperature()}")
    print(f"MCU motor temperature: {mcu.read.motor_temperature()}")
    print(f"MCU D-axis current feedback: {mcu.read.id_feedback()}")
    print(f"MCU Q-axis current feedback: {mcu.read.iq_feedback()}")
    print(f"MCU D-axis voltage feedback: {mcu.read.ud_feedback()}")
    print(f"MCU Q-axis voltage feedback: {mcu.read.uq_feedback()}")
    print(f"MCU modulation index: {mcu.read.mod_index()}")
    print(f"MCU switching frequency: {mcu.read.switching_frequency()}")

    print("End of print statements for MCU object")

    print("End of example script to read the Bowfell controller")

elif project == "derwent":
    # Derwent
    mcu = Derwent(config["derwent"])
    print("Start of print statements for Derwent object")

    # Print the bus and database
    print(f"Bus: {mcu.bus}")
    print(f"Database: {mcu.db}")

    # Print MCU based measurements
    print(f"MCU bridge state: {mcu.read.bridge_state()}")
    print(f"MCU torque demand: {mcu.read.torque_demand()}")
    print(f"MCU torque demand FUSA: {mcu.read.torque_demand_fusa()}")
    print(f"MCU torque demand actual: {mcu.read.torque_demand_actual()}")
    print(f"MCU speed: {mcu.read.speed()}")
    print(f"MCU DC link voltage: {mcu.read.dc_link_voltage()}")
    print(f"MCU DC link current: {mcu.read.dc_link_current()}")
    print(f"MCU inverter temperature: {mcu.read.inverter_temperature()}")
    print(f"MCU motor temperature: {mcu.read.motor_temperature()}")
    print(f"MCU D-axis current feedback: {mcu.read.id_feedback()}")
    print(f"MCU Q-axis current feedback: {mcu.read.iq_feedback()}")
    print(f"MCU D-axis voltage feedback: {mcu.read.ud_feedback()}")
    print(f"MCU Q-axis voltage feedback: {mcu.read.uq_feedback()}")
    print(f"MCU modulation index: {mcu.read.mod_index()}")
    print(f"MCU switching frequency: {mcu.read.switching_frequency()}")
    print("End of print statements for MCU object")
    print("End of example script to read the Derwent controller")

elif project == "hawkshead":
    # Hawkshead
    mcu = Hawkshead(config["hawkshead"])
    print("Start of print statements for Hawkshead object")

    # Print the bus and database
    print(f"Bus: {mcu.bus}")
    print(f"Database: {mcu.db}")

    # Print MCU based measurements
    print(f"MCU bridge state: {mcu.read.bridge_state()}")
    print(f"MCU torque demand: {mcu.read.torque_demand()}")
    print(f"MCU torque demand FUSA: {mcu.read.torque_demand_fusa()}")
    print(f"MCU torque demand actual: {mcu.read.torque_demand_actual()}")
    print(f"MCU speed: {mcu.read.speed()}")
    print(f"MCU DC link voltage: {mcu.read.dc_link_voltage()}")
    print(f"MCU DC link current: {mcu.read.dc_link_current()}")
    print(f"MCU inverter temperature: {mcu.read.inverter_temperature()}")
    print(f"MCU motor temperature: {mcu.read.motor_temperature()}")
    print(f"MCU D-axis current feedback: {mcu.read.id_feedback()}")
    print(f"MCU Q-axis current feedback: {mcu.read.iq_feedback()}")
    print(f"MCU D-axis voltage feedback: {mcu.read.ud_feedback()}")
    print(f"MCU Q-axis voltage feedback: {mcu.read.uq_feedback()}")
    print(f"MCU modulation index: {mcu.read.mod_index()}")
    print(f"MCU switching frequency: {mcu.read.switching_frequency()}")

    print("End of print statements for MCU object")

    print("End of example script to read the Hawkshead controller")

elif project == "blencathra":
    # Blencathra
    mcu = Blencathra(config["blencathra"])
    print("Start of print statements for Blencathra object")

    # Print the bus and database
    print(f"Bus: {mcu.bus}")
    print(f"Database: {mcu.db}")

    # Print MCU based measurements
    print(f"MCU bridge state: {mcu.read.bridge_state()}")
    print(f"MCU torque demand: {mcu.read.torque_demand()}")
    print(f"MCU torque demand FUSA: {mcu.read.torque_demand_fusa()}")
    print(f"MCU torque demand actual: {mcu.read.torque_demand_actual()}")
    print(f"MCU speed: {mcu.read.speed()}")
    print(f"MCU DC link voltage: {mcu.read.dc_link_voltage()}")
    print(f"MCU DC link current: {mcu.read.dc_link_current()}")
    print(f"MCU inverter temperature: {mcu.read.inverter_temperature()}")
    print(f"MCU motor temperature: {mcu.read.motor_temperature()}")
    print(f"MCU D-axis current feedback: {mcu.read.id_feedback()}")
    print(f"MCU Q-axis current feedback: {mcu.read.iq_feedback()}")
    print(f"MCU D-axis voltage feedback: {mcu.read.ud_feedback()}")
    print(f"MCU Q-axis voltage feedback: {mcu.read.uq_feedback()}")
    print(f"MCU modulation index: {mcu.read.mod_index()}")
    print(f"MCU switching frequency: {mcu.read.switching_frequency()}")

    print("End of print statements for MCU object")

    print("End of example script to read the Blencathra controller")

elif project == "oxford":
    # Oxford
    mcu = Oxford(config["oxford"])
    print("Start of print statements for Oxford object")

    # Print the bus and database
    print(f"Bus: {mcu.bus}")
    #print(f"Database: {mcu.db}")
    time.sleep(1)
    # Print MCU based measurements
    print(f"MCU bridge state: {mcu.read.bridge_state()}")
    print(f"MCU torque demand actual: {mcu.read.torque_demand_actual()}")
    print(f"MCU speed: {mcu.read.speed()}")
    print(f"MCU DC link voltage: {mcu.read.dc_link_voltage()}")
    print(f"MCU DC link current: {mcu.read.dc_link_current()}")
    print(f"MCU inverter temperature: {mcu.read.inverter_temperature()}")
    print(f"MCU inverter temperature switch: {mcu.read.inverter_temperature_switch()}")
    print(f"MCU inverter temperature capacitor: {mcu.read.inverter_temperature_capacitor()}")
    print(f"MCU inverter temperature dc link: {mcu.read.inverter_temperature_dc_link()}")
    print(f"MCU inverter temperature ac link: {mcu.read.inverter_temperature_ac_link()}")
    print(f"MCU motor temperature: {mcu.read.motor_temperature()}")
    print(f"MCU motor temperature stator core: {mcu.read.motor_temperature_stator_core()}")
    print(f"MCU motor temperature stator winding: {mcu.read.motor_temperature_stator_winding()}")
    print(f"MCU motor temperature cavity: {mcu.read.motor_temperature_cavity()}")
    print(f"MCU D-axis current feedback: {mcu.read.id_feedback()}")
    print(f"MCU Q-axis current feedback: {mcu.read.iq_feedback()}")
    print(f"MCU D-axis voltage feedback: {mcu.read.ud_feedback()}")
    print(f"MCU Q-axis voltage feedback: {mcu.read.uq_feedback()}")
    print(f"MCU modulation index: {mcu.read.mod_index()}")
    print(f"MCU switching frequency: {mcu.read.switching_frequency()}")
    print(f"MCU table voltage index: {mcu.read.table_voltage_index()}")
    print(f"MCU table speed index: {mcu.read.table_speed_index()}")
    print(f"MCU table torque index: {mcu.read.table_torque_index()}")
    print(f"MCU table ke index: {mcu.read.table_ke_index()}")
    print("End of print statements for MCU object")

    print("End of example script to read the Oxford controller")
