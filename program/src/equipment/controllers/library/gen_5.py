import time
from typing import Union

from program.src.equipment.can import CanDevice


class BridgeState:
    TorqueEnabled = 0
    ActiveShortCircuit = 1
    ActiveDischarge = 2
    Test = 3
    Fault = 4
    HighImpedance = 5


class Bowfell(CanDevice):
    def __init__(self, config):
        """
        Initialize the Bowfell controller.

        Parameters
        ----------
        config : dict
            The configuration dictionary.

        Returns
        -------
        None.

        """
        super().__init__()

        self._config = config
        self._parameters = self._config["parameters"]
        self._limits = self._parameters["limits"]
        self._message = self._config["can"]["messages"]
        self._signal = self._config["can"]["signals"]
        self._init_vals = self._config["can"]["initial_values"]
        self._periodic_can_messages: Union[dict, None] = None

        # Set up the CAN bus using the provided configuration
        self._bus = self.setup_can_bus(self._config)

        # Load the CAN message database
        self._db = self.load_database(self._config["can"]["database"])

        # Set the listener to receive CAN messages
        self._listener = self.setup_listener()

        # Set up the notifier to notify the listener when a message is received
        self._notifier = self.setup_notifier(self._bus, [self._listener])

        # Create a write object for the Bowfell class to send CAN messages
        self.write = Bowfell.Write(self)

        # Initialize periodic CAN messages
        self._periodic_can_messages = self.write.initialise_periodic_messages()

        # Create a read object for the Bowfell class to receive CAN messages, have to setup read after periodic messages
        self.read = Bowfell.Read(self)

        # Write the periodic CAN messages at a specified time interval based on configuration
        self.write_periodic_message(self._periodic_can_messages, self._config["can"]["period"])

        # Update CAN values on every message received by the listener
        self._listener.on_message_received = self.read.update_can_values

    def __del__(self):
        # docstring
        """
        Clean up the Bowfell controller by turning off torque demand and disabling its output,
        shutting down the bus, and deleting the instance.


        Parameters
        ----------

        Returns
        -------
        None."""

        try:
            # Set the torque demand to 0
            self.write.torque_demand(0.0)
            time.sleep(0.5)

            # Disable the bridge and shutdown the bus
            self.write.bridge_enable(False)
            self._bus.shutdown()

        except Exception as e:
            print(e)

    class Write:
        """
        Class for writing to the Bowfell inverter.

        Attributes
        ----------
        _controller : Controller
            The controller object.
        _message : dict
            The message dictionary.
        _signal : dict
            The signal dictionary.
        _parameters : dict
            The parameters dictionary.
        _reset : dict
            The reset dictionary.
        _limits : dict
            The limits dictionary.

        Methods
        -------
        reset_can_signals()
            Reset the can signals.
        torque_demand(torque_demand)
            Set the torque demand.
        torque_demand_fusa(torque_demand)
            Set the torque demand.
        speed(speed)
            Set the speed.
        """

        def __init__(self, controller):
            self._controller = controller
            self._message = controller._message
            self._signal = controller._signal
            self._parameters = controller._parameters
            self._init_vals = controller._init_vals
            self._limits = controller._limits

        def initialise_periodic_messages(self):
            """
            Initialise the periodic CAN messages, using the configuration.

            Returns

            -------
            periodic_can_messages : dict
                The periodic CAN messages.

            """

            periodic_can_messages = {
                # TX MESSAGES
                self._message["torque"]["qm"]["demand"]: {
                    # VC_Trq
                    self._signal["torque"]["qm"]["demand"]: self._init_vals["torque"]["qm"]["demand"],
                    self._signal["torque"]["qm"]["limits"]["drive"]: self._init_vals["torque"]["qm"]["limits"]["drive"],
                    self._signal["torque"]["qm"]["limits"]["brake"]: self._init_vals["torque"]["qm"]["limits"]["brake"],
                },
                self._message["torque"]["fusa"]["demand"]: {
                    # VC_Trq_FuSa
                    self._signal["torque"]["fusa"]["demand"]: self._init_vals["torque"]["qm"]["demand"],
                    self._signal["torque"]["fusa"]["limits"]["drive"]: self._init_vals["torque"]["qm"]["limits"]["drive"],
                    self._signal["torque"]["fusa"]["limits"]["brake"]: self._init_vals["torque"]["qm"]["limits"]["brake"],
                },
                self._message["dc_link"]["limits"]["current"]["charge"]: {
                    # VC_Bat
                    self._signal["dc_link"]["limits"]["current"]["charge"]: self._init_vals["dc_link"]["limits"]["current"]["charge"],
                    self._signal["dc_link"]["limits"]["current"]["discharge"]: self._init_vals["dc_link"]["limits"]["current"]["discharge"],
                    self._signal["dc_link"]["limits"]["voltage"]["max"]: self._init_vals["dc_link"]["limits"]["voltage"]["max"],
                    self._signal["dc_link"]["limits"]["voltage"]["min"]: self._init_vals["dc_link"]["limits"]["voltage"]["min"],
                },
                self._message["crash"]["active_discharge"]: {
                    # VC_CrashInd
                    self._signal["crash"]["active_discharge"]: self._init_vals["crash"]["active_discharge"],
                },
                self._message["bridge"]["enable"]: {
                    # VC_SpdCmd
                    self._signal["bridge"]["enable"]: self._init_vals["bridge"]["enable"],
                    self._signal["speed"]["limits"]["forward"]: self._init_vals["speed"]["limits"]["forward"],
                    self._signal["speed"]["limits"]["reverse"]: self._init_vals["speed"]["limits"]["reverse"],
                },
                self._message["speed"]["transmission"]: {
                    # VC_TransSpd
                    self._signal["speed"]["transmission"]: self._init_vals["speed"]["transmission"],
                },
                self._message["status"]["contactor"]: {
                    # PDU_Status_4
                    self._signal["status"]["contactor"]: self._init_vals["status"]["contactor"],
                },
                # RX MESSAGES
                self._message["dc_link"]["feedback"]["voltage"]: {
                    # MC_Sts
                    self._signal["dc_link"]["feedback"]["voltage"]: self._init_vals["dc_link"]["feedback"]["voltage"],
                    self._signal["dc_link"]["feedback"]["current"]: self._init_vals["dc_link"]["feedback"]["current"],
                    self._signal["temperature"]["motor"]["motor"]: self._init_vals["temperature"]["motor"]["motor"],
                },
                self._message["temperature"]["feedback"]["inverter"]["inverter"]: {
                    # MC_Sts_2
                    self._signal["temperature"]["inverter"]["inverter"]: self._init_vals["temperature"]["inverter"]["inverter"],
                    self._signal["bridge"]["state"]: self._init_vals["bridge"]["state"],
                    self._signal["status"]["fault"]: self._init_vals["status"]["fault"],
                    self._signal["status"]["kl_30"]: self._init_vals["status"]["kl_30"],
                },
                self._message["torque"]["qm"]["feedback"]: {
                    # MC_Trq
                    self._signal["torque"]["qm"]["feedback"]: self._init_vals["torque"]["qm"]["feedback"],
                    self._signal["torque"]["qm"]["max_drive_available"]: self._init_vals["torque"]["qm"]["max_drive_available"],
                    self._signal["torque"]["qm"]["max_brake_available"]: self._init_vals["torque"]["qm"]["max_brake_available"],
                },
                self._message["speed"]["feedback"]: {
                    # MC_Vel
                    self._signal["speed"]["feedback"]: self._init_vals["speed"]["feedback"],
                },
                self._message["motor_control"]["feedback"]["ud"]: {
                    # MC_V2_Debug_1
                    self._signal["motor_control"]["feedback"]["ud"]: self._init_vals["motor_control"]["feedback"]["ud"],
                    self._signal["motor_control"]["feedback"]["uq"]: self._init_vals["motor_control"]["feedback"]["uq"],
                    self._signal["motor_control"]["feedback"]["id"]: self._init_vals["motor_control"]["feedback"]["id"],
                    self._signal["motor_control"]["feedback"]["iq"]: self._init_vals["motor_control"]["feedback"]["iq"],
                    self._signal["motor_control"]["feedback"]["mod_index"]: self._init_vals["motor_control"]["feedback"]["mod_index"],
                    self._signal["motor_control"]["feedback"]["switching_frequency"]: self._init_vals["motor_control"]["feedback"]["switching_frequency"],
                },
                self._message["status"]["alert_id"]: {
                    # MC_Debug_3_Status
                    self._signal["status"]["alert_id"]: self._init_vals["status"]["alert_id"],
                    self._signal["status"]["block_id"]: self._init_vals["status"]["block_id"],
                    self._signal["status"]["status"]: self._init_vals["status"]["status"],
                    self._signal["status"]["placeholder"]: self._init_vals["status"]["placeholder"],
                },
                self._message["fault"]["encoder"]: {
                    # MC_Sts_3
                    self._signal["fault"]["encoder"]: self._init_vals["fault"]["encoder"],
                    self._signal["fault"]["motor"]["temperature"]: self._init_vals["motor"]["temperature"],
                    self._signal["fault"]["motor"]["speed"]: self._init_vals["motor"]["speed"],
                    self._signal["fault"]["inverter"]["current"]["over_charge"]: self._init_vals["fault"]["inverter"]["current"]["over_charge"],
                    self._signal["fault"]["inverter"]["voltage"]["over_voltage"]: self._init_vals["fault"]["inverter"]["voltage"]["over_voltage"],
                    self._signal["fault"]["torque"]["command"]: self._init_vals["fault"]["torque"]["command"],
                    self._signal["fault"]["torque"]["estimation"]: self._init_vals["fault"]["torque"]["estimation"],
                    self._signal["fault"]["torque"]["monitor"]: self._init_vals["fault"]["torque"]["monitor"],
                    self._signal["fault"]["status"]["hv_request"]: self._init_vals["fault"]["status"]["hv_request"],
                    self._signal["fault"]["status"]["amber"]: self._init_vals["fault"]["status"]["amber"],
                    self._signal["fault"]["status"]["stop"]: self._init_vals["fault"]["status"]["stop"],
                },
            }

            return periodic_can_messages

        def reset_can_signals(self):
            """
            Reset the can signals.

            Returns
            -------
            None.

            """
            self._controller.write_signal(self._message["torque"]["qm"]["demand"], self._signal["torque"]["qm"]["demand"], self._reset["torque"]["qm"]["demand"])
            self._controller.write_signal(self._message["torque"]["fusa"]["demand"], self._signal["torque"]["fusa"]["demand"], self._reset["torque"]["fusa"]["demand"])
            self._controller.write_signal(self._message["torque"]["qm"]["limits"]["drive"], self._signal["torque"]["qm"]["limits"]["drive"], self._reset["torque"]["qm"]["limits"]["drive"])
            self._controller.write_signal(self._message["torque"]["fusa"]["limits"]["drive"], self._signal["torque"]["fusa"]["limits"]["drive"], self._reset["torque"]["fusa"]["limits"]["drive"])
            self._controller.write_signal(self._message["torque"]["qm"]["limits"]["brake"], self._signal["torque"]["qm"]["limits"]["brake"], self._reset["torque"]["qm"]["limits"]["brake"])
            self._controller.write_signal(self._message["torque"]["fusa"]["limits"]["brake"], self._signal["torque"]["fusa"]["limits"]["brake"], self._reset["torque"]["fusa"]["limits"]["brake"])
            self._controller.write_signal(self._message["speed"]["limits"]["forward"], self._signal["speed"]["limits"]["forward"], self._reset["speed"]["limits"]["forward"])
            self._controller.write_signal(self._message["speed"]["limits"]["reverse"], self._signal["speed"]["limits"]["reverse"], self._reset["speed"]["limits"]["reverse"])
            self._controller.write_signal(self._message["dc_link"]["limits"]["voltage"]["max"], self._signal["dc_link"]["limits"]["voltage"]["max"], self._reset["dc_link"]["limits"]["voltage"]["max"])
            self._controller.write_signal(self._message["dc_link"]["limits"]["voltage"]["min"], self._signal["dc_link"]["limits"]["voltage"]["min"], self._reset["dc_link"]["limits"]["voltage"]["min"])
            self._controller.write_signal(
                self._message["dc_link"]["limits"]["current"]["charge"], self._signal["dc_link"]["limits"]["current"]["charge"], self._reset["dc_link"]["limits"]["current"]["charge"]
            )
            self._controller.write_signal(
                self._message["dc_link"]["limits"]["current"]["discharge"], self._signal["dc_link"]["limits"]["current"]["discharge"], self._reset["dc_link"]["limits"]["current"]["discharge"]
            )
            self._controller.write_signal(self._message["status"]["contactor"], self._signal["status"]["contactor"], self._reset["status"]["contactor"])
            self._controller.write_signal(self._message["speed"]["transmission"], self._signal["speed"]["transmission"], self._reset["speed"]["transmission"])
            self._controller.write_signal(self._message["crash"]["active_discharge"], self._signal["crash"]["active_discharge"], self._reset["crash"]["active_discharge"])
            self._controller.write_signal(self._message["bridge"]["enable"], self._signal["bridge"]["enable"], self._reset["bridge"]["enable"])

        def bridge_enable(self, enabled: bool):
            """
            Enable or disable the bridge.

            Parameters
            ----------
            enable : bool
                True to enable the bridge, False to disable.

            Returns
            -------
            bool
                True if the bridge state is the expected one, False otherwise.

            """

            if enabled:
                self._controller.write_signal(self._message["bridge"]["enable"], self._signal["bridge"]["enable"], self._parameters["bridge"]["enable"])
                time.sleep(2)
                success = self._controller.get_bridge_state() == BridgeState.TorqueEnabled
            else:
                self._controller.write_signal(self._message["bridge"]["enable"], self._signal["bridge"]["enable"], self._parameters["bridge"]["disable"])
                time.sleep(0.5)
                success = self._controller.get_bridge_state() != BridgeState.TorqueEnabled

            return success

        def crash_event(self, crashed):
            """
            Set the crash event.

            Parameters
            ----------
            crashed
                True if the crash event is active, False otherwise.

            Returns
            -------
            None.
            """

            return self._controller.write_signal(self._message["crash"]["active_discharge"], self._signal["crash"]["active_discharge"], self._parameters["crash_value"] if crashed else 0)

        def current_limits(self, discharge_limit: float, charge_limit: float):
            """
            Set the current limits.

            Parameters
            ----------
            discharge_limit : float
                The discharge limit.
            charge_limit : float
                The charge limit.

            Returns
            -------
            None.
            """

            discharge_limit = max(min(discharge_limit, self._limits["dc_link"]["current"]["discharge"]), -self._limits["dc_link"]["current"]["discharge"])
            charge_limit = max(min(charge_limit, self._limits["dc_link"]["current"]["charge"]), -self._limits["dc_link"]["current"]["charge"])
            return self._controller.write_signal(
                self._message["dc_link"]["limits"]["current"]["discharge"], self._signal["dc_link"]["limits"]["current"]["discharge"], discharge_limit
            ) and self._controller.write_signal(self._message["dc_link"]["limits"]["current"]["charge"], self._signal["dc_link"]["limit"]["current"]["charge"], charge_limit)

        def speed_limits(self, forward_limit: float, reverse_limit: float):
            """
            Set the speed limits.

            Parameters
            ----------
            forward_limit : float
                The forward limit.
            reverse_limit : float
                The reverse limit.

            Returns
            -------
            None.
            """

            forward_limit = max(min(forward_limit, self._limits["speed"]["forward"]), -self._limits["speed"]["forward"])
            reverse_limit = max(min(reverse_limit, self._limits["speed"]["reverse"]), -self._limits["speed"]["reverse"])
            return self._controller.write_signal(self._message["speed"]["limits"]["forward"], self._signal["speed"]["limits"]["forward"], forward_limit) and self._controller.write_signal(
                self._message["speed"]["limits"]["reverse"], self._signal["speed"]["limits"]["reverse"], reverse_limit
            )

        def torque_demand(self, torque_demand: float):
            """
            Set the torque demand.

            Parameters
            ----------
            torque_demand : float
                The torque demand.

            Returns
            -------
            None.
            """

            torque_demand = max(min(torque_demand, self._limits["torque"]["drive"]), self._limits["torque"]["brake"])
            torque_drive_limit = min(self._limits["torque"]["drive"], abs(torque_demand) * 1.2)
            torque_brake_limit = min(self._limits["torque"]["drive"], abs(torque_demand) * 1.2)
            return (
                self._controller.write_signal(self._message["torque"]["qm"]["limits"]["drive"], self._signal["torque"]["qm"]["limits"]["drive"], torque_drive_limit)
                and self._controller.write_signal(self._message["torque"]["fusa"]["limits"]["drive"], self._signal["torque"]["fusa"]["limits"]["drive"], torque_drive_limit)
                and self._controller.write_signal(self._message["torque"]["qm"]["limits"]["brake"], self._signal["torque"]["qm"]["limits"]["brake"], torque_brake_limit)
                and self._controller.write_signal(self._message["torque"]["fusa"]["limits"]["brake"], self._signal["torque"]["fusa"]["limits"]["brake"], torque_brake_limit)
                and self._controller.write_signal(self._message["torque"]["qm"]["demand"], self._signal["torque"]["qm"]["demand"], torque_demand)
                and self._controller.write_signal(self._message["torque"]["fusa"]["demand"], self._signal["torque"]["fusa"]["demand"], torque_demand)
            )

    class Read:
        """
        Read class for the Bowfell controller.

        Attributes
        ----------
        _controller : Gen5
            The Gen5 controller.
        _message : dict
            The message dictionary.
        _signal : dict
            The signal dictionary.
        _parameters : dict
            The parameters dictionary.
        _limits : dict
            The limits dictionary.

        Methods
        -------
        bridge_state() -> int
            Get the bridge state.
        alert_id()
            Get the alert ID.
        block_id()
            Get the block ID.
        torque_demand() -> float
            Get the torque demand.
        torque_demand_fusa() -> float
            Get the torque demand, in the saftey layer.
        torque_demand_actual() -> float
            Get the actual torque demand, this is the torque demand after derating (if any applied).
        speed() -> float
            Get the speed.
        dc_link_voltage() -> float
            Get the DC link voltage.
        dc_link_current() -> float
            Get the DC link current.
        inverter_temperature() -> float
            Get the inverter temperature.
        motor_temperature() -> float
            Get the motor temperature.
        id_feedback() -> float
            Get the D-axis current feedback.
        iq_feedback() -> float
            Get the Q-axis current feedback.
        ud_feedback() -> float
            Get the D-axis voltage feedback.
        uq_feedback() -> float
            Get the Q-axis voltage feedback.
        mod_index() -> float
            Get the modulation index.
        switching_frequency() -> float
            Get the switching frequency.
        """

        def __init__(self, controller):
            self._controller = controller
            self._message = controller._message
            self._signal = controller._signal
            self._parameters = controller._parameters
            self._limits = controller._limits
            self._periodic_can_messages = self._controller._periodic_can_messages

        def update_can_values(self, message_received):
            """
            Update the CAN values.

            Parameters
            ----------
            message_received : can.Message
                The CAN message received.
            """
            self._controller.read_can_values(self._periodic_can_messages, message_received)

        def bridge_state(self) -> int:
            """
            Get the bridge state.

            Returns
            -------
            int
                The bridge state.

            """
            bridge_state = BridgeState.HighImpedance

            value = self._controller.read_periodic_signal(self._periodic_can_messages, self._message["bridge"]["state"], self._signal["bridge"]["state"])

            if value == 3:
                bridge_state = BridgeState.TorqueEnabled
            elif value == 2:
                bridge_state = BridgeState.ActiveDischarge
            elif value == 1:
                bridge_state = BridgeState.ActiveShortCircuit

            return bridge_state

        def alert_id(self):
            """
            Get the alert ID.

            Returns
            -------
            hex(int)
                The alert ID.
            """
            return hex(int(self._controller.read_periodic_signal(self._periodic_can_messages, self._message["status"]["alert_id"], self._signal["status"]["alert_id"])))

        def block_id(self):
            """
            Get the block ID.

            Returns
            -------
            hex(int)
                The block ID.
            """

            return hex(int(self._controller.read_periodic_signal(self._periodic_can_messages, self._message["status"]["block_id"], self._signal["status"]["block_id"])))

        def torque_demand(self) -> float:
            """
            Get the torque demand.

            Returns
            -------
            float
                The torque demand.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["torque"]["qm"]["demand"], self._signal["torque"]["qm"]["demand"])

        def torque_demand_fusa(self) -> float:
            """
            Get the torque demand.

            Returns
            -------
            float
                The torque demand.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["torque"]["fusa"]["demand"], self._signal["torque"]["fusa"]["demand"])

        def torque_demand_actual(self) -> float:
            """
            Get the actual torque demand, this is the torque demand after derating (if any applied).

            Returns
            -------
            float
                The derated torque demand.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["torque"]["qm"]["feedback"]["actual"], self._signal["torque"]["qm"]["feedback"]["actual"])

        def speed(self) -> float:
            """
            Get the speed.

            Returns
            -------
            float
                The speed.
            """

            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["speed"]["feedback"], self._signal["speed"]["feedback"])

        def dc_link_voltage(self) -> float:
            """
            Get the DC link voltage.

            Returns
            -------
            float
                The DC link voltage.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["dc_link"]["feedback"]["voltage"], self._signal["dc_link"]["feedback"]["voltage"])

        def dc_link_current(self) -> float:
            """
            Get the DC link current.

            Returns
            -------
            float
                The DC link current.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["dc_link"]["feedback"]["current"], self._signal["dc_link"]["feedback"]["current"])

        def inverter_temperature(self) -> float:
            """
            Get the inverter temperature.

            Returns
            -------
            float
                The inverter temperature.
            """
            return self._controller.read_periodic_signal(
                self._periodic_can_messages, self._message["temperature"]["feedback"]["inverter"]["inverter"], self._signal["temperature"]["feedback"]["inverter"]["inverter"]
            )

        def motor_temperature(self) -> float:
            """
            Get the motor temperature.

            Returns
            -------
            float
                The motor temperature.
            """
            return self._controller.read_periodic_signal(
                self._periodic_can_messages, self._message["temperature"]["feedback"]["motor"]["motor"], self._signal["temperature"]["feedback"]["motor"]["motor"]
            )

        def id_feedback(self) -> float:
            """
            Get the Id current (Stationary frame current, D-axis) feedback.

            Returns
            -------
            float
                The Id current (Stationary frame current, D-axis) current feedback.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["motor_control"]["feedback"]["id"], self._signal["motor_control"]["feedback"]["id"])

        def iq_feedback(self) -> float:
            """
            Get the Iq current (Stationary frame current, Q-axis) feedback.

            Returns
            -------
            float
                The Iq current (Stationary frame current, Q-axis) current feedback.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["motor_control"]["feedback"]["iq"], self._signal["motor_control"]["feedback"]["iq"])

        def ud_feedback(self) -> float:
            """
            Get the Ud voltage (Stationary frame voltage, D-axis) feedback.

            Returns
            -------
            float
                The Ud voltage (Stationary frame voltage, D-axis) voltage feedback.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["motor_control"]["feedback"]["ud"], self._signal["motor_control"]["feedback"]["ud"])

        def uq_feedback(self) -> float:
            """
            Get the Uq voltage (Stationary frame voltage, Q-axis) feedback.

            Returns
            -------
            float
                The Uq voltage (Stationary frame voltage, Q-axis) voltage feedback.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["motor_control"]["feedback"]["uq"], self._signal["motor_control"]["feedback"]["uq"])

        def mod_index(self) -> float:
            """
            Get the modulation index.

            Returns
            -------
            float
                The modulation index.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["motor_control"]["mod_index"], self._signal["motor_control"]["mod_index"])

        def switching_frequency(self) -> float:
            """
            Get the switching frequency.

            Returns
            -------
            float
                The switching frequency.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["motor_control"]["switching_frequency"], self._signal["motor_control"]["switching_frequency"])


class Derwent(CanDevice):
    def __init__(self, config):
        """
        Initialize the Derwent controller.

        Parameters
        ----------
        config : dict
            The configuration dictionary.

        Returns
        -------
        None.

        """
        super().__init__()

        self._config = config
        self._parameters = self._config["parameters"]
        self._limits = self._parameters["limits"]
        self._message = self._config["can"]["messages"]
        self._signal = self._config["can"]["signals"]
        self._init_vals = self._config["can"]["initial_values"]
        self._periodic_can_messages: Union[dict, None] = None

        # Set up the CAN bus using the provided configuration
        self._bus = self.setup_can_bus(self._config)

        # Load the CAN message database
        self._db = self.load_database(self._config["can"]["database"])

        # Set the listener to receive CAN messages
        self._listener = self.setup_listener()

        # Set up the notifier to notify the listener when a message is received
        self._notifier = self.setup_notifier(self._bus, [self._listener])

        # Create a write object for the Derwent class to send CAN messages
        self.write = Derwent.Write(self)

        # Initialize periodic CAN messages
        self._periodic_can_messages = self.write.initialise_periodic_messages()

        # Create a read object for the Derwent class to receive CAN messages, have to setup read after periodic messages
        self.read = Derwent.Read(self)

        # Write the periodic CAN messages at a specified time interval based on configuration
        self.write_periodic_message(self._periodic_can_messages, self._config["can"]["period"])

        # Update CAN values on every message received by the listener
        self._listener.on_message_received = self.read.update_can_values

    def __del__(self):
        # docstring
        """
        Clean up the Derwent controller by turning off torque demand and disabling its output,
        shutting down the bus, and deleting the instance.


        Parameters
        ----------

        Returns
        -------
        None."""

        try:
            # Set the torque demand to 0
            self.write.torque_demand(0.0)
            time.sleep(0.5)

            # Disable the bridge and shutdown the bus
            self.write.bridge_enable(False)
            self._bus.shutdown()

        except Exception as e:
            print(e)

    class Write:
        """
        Class for writing to the Bowfell inverter.

        Attributes
        ----------
        _controller : Controller
            The controller object.
        _message : dict
            The message dictionary.
        _signal : dict
            The signal dictionary.
        _parameters : dict
            The parameters dictionary.
        _reset : dict
            The reset dictionary.
        _limits : dict
            The limits dictionary.

        Methods
        -------
        reset_can_signals()
            Reset the can signals.
        torque_demand(torque_demand)
            Set the torque demand.
        torque_demand_fusa(torque_demand)
            Set the torque demand.
        speed(speed)
            Set the speed.
        """

        def __init__(self, controller):
            self._controller = controller
            self._message = controller._message
            self._signal = controller._signal
            self._parameters = controller._parameters
            self._init_vals = controller._init_vals
            self._limits = controller._limits

        def initialise_periodic_messages(self):
            """
            Initialise the periodic CAN messages, using the configuration.

            Returns

            -------
            periodic_can_messages : dict
                The periodic CAN messages.

            """

            periodic_can_messages = {
                # TX MESSAGES
                self._message["torque"]["qm"]["demand"]: {
                    # VC_Trq
                    self._signal["torque"]["qm"]["demand"]: self._init_vals["torque"]["qm"]["demand"],
                    self._signal["torque"]["qm"]["limits"]["drive"]: self._init_vals["torque"]["qm"]["limits"]["drive"],
                    self._signal["torque"]["qm"]["limits"]["brake"]: self._init_vals["torque"]["qm"]["limits"]["brake"],
                },
                self._message["torque"]["fusa"]["demand"]: {
                    # VC_Trq_FuSa
                    self._signal["torque"]["fusa"]["demand"]: self._init_vals["torque"]["qm"]["demand"],
                    self._signal["torque"]["fusa"]["limits"]["drive"]: self._init_vals["torque"]["qm"]["limits"]["drive"],
                    self._signal["torque"]["fusa"]["limits"]["brake"]: self._init_vals["torque"]["qm"]["limits"]["brake"],
                },
                self._message["dc_link"]["limits"]["current"]["charge"]: {
                    # VC_Bat
                    self._signal["dc_link"]["limits"]["current"]["charge"]: self._init_vals["dc_link"]["limits"]["current"]["charge"],
                    self._signal["dc_link"]["limits"]["current"]["discharge"]: self._init_vals["dc_link"]["limits"]["current"]["discharge"],
                    self._signal["dc_link"]["limits"]["voltage"]["max"]: self._init_vals["dc_link"]["limits"]["voltage"]["max"],
                    self._signal["dc_link"]["limits"]["voltage"]["min"]: self._init_vals["dc_link"]["limits"]["voltage"]["min"],
                },
                self._message["crash"]["active_discharge"]: {
                    # VC_CrashInd
                    self._signal["crash"]["active_discharge"]: self._init_vals["crash"]["active_discharge"],
                },
                self._message["bridge"]["enable"]: {
                    # VC_SpdCmd
                    self._signal["bridge"]["enable"]: self._init_vals["bridge"]["enable"],
                    self._signal["speed"]["limits"]["forward"]: self._init_vals["speed"]["limits"]["forward"],
                    self._signal["speed"]["limits"]["reverse"]: self._init_vals["speed"]["limits"]["reverse"],
                },
                self._message["speed"]["transmission"]: {
                    # VC_TransSpd
                    self._signal["speed"]["transmission"]: self._init_vals["speed"]["transmission"],
                },
                self._message["status"]["contactor"]: {
                    # PDU_Status_4
                    self._signal["status"]["contactor"]: self._init_vals["status"]["contactor"],
                },
                # RX MESSAGES
                self._message["dc_link"]["feedback"]["voltage"]: {
                    # MC_Sts
                    self._signal["dc_link"]["feedback"]["voltage"]: self._init_vals["dc_link"]["feedback"]["voltage"],
                    self._signal["dc_link"]["feedback"]["current"]: self._init_vals["dc_link"]["feedback"]["current"],
                    self._signal["temperature"]["motor"]["motor"]: self._init_vals["temperature"]["motor"]["motor"],
                },
                self._message["temperature"]["feedback"]["inverter"]["inverter"]: {
                    # MC_Sts_2
                    self._signal["temperature"]["inverter"]["inverter"]: self._init_vals["temperature"]["inverter"]["inverter"],
                    self._signal["bridge"]["state"]: self._init_vals["bridge"]["state"],
                    self._signal["status"]["fault"]: self._init_vals["status"]["fault"],
                    self._signal["status"]["kl_30"]: self._init_vals["status"]["kl_30"],
                },
                self._message["torque"]["qm"]["feedback"]: {
                    # MC_Trq
                    self._signal["torque"]["qm"]["feedback"]: self._init_vals["torque"]["qm"]["feedback"],
                    self._signal["torque"]["qm"]["max_drive_available"]: self._init_vals["torque"]["qm"]["max_drive_available"],
                    self._signal["torque"]["qm"]["max_brake_available"]: self._init_vals["torque"]["qm"]["max_brake_available"],
                },
                self._message["speed"]["feedback"]: {
                    # MC_Vel
                    self._signal["speed"]["feedback"]: self._init_vals["speed"]["feedback"],
                },
                self._message["motor_control"]["feedback"]["ud"]: {
                    # MC_V2_Debug_1
                    self._signal["motor_control"]["feedback"]["ud"]: self._init_vals["motor_control"]["feedback"]["ud"],
                    self._signal["motor_control"]["feedback"]["uq"]: self._init_vals["motor_control"]["feedback"]["uq"],
                    self._signal["motor_control"]["feedback"]["id"]: self._init_vals["motor_control"]["feedback"]["id"],
                    self._signal["motor_control"]["feedback"]["iq"]: self._init_vals["motor_control"]["feedback"]["iq"],
                    self._signal["motor_control"]["feedback"]["mod_index"]: self._init_vals["motor_control"]["feedback"]["mod_index"],
                    self._signal["motor_control"]["feedback"]["switching_frequency"]: self._init_vals["motor_control"]["feedback"]["switching_frequency"],
                },
                self._message["status"]["alert_id"]: {
                    # MC_Debug_3_Status
                    self._signal["status"]["alert_id"]: self._init_vals["status"]["alert_id"],
                    self._signal["status"]["block_id"]: self._init_vals["status"]["block_id"],
                    self._signal["status"]["status"]: self._init_vals["status"]["status"],
                    self._signal["status"]["placeholder"]: self._init_vals["status"]["placeholder"],
                },
                self._message["fault"]["encoder"]: {
                    # MC_Sts_3
                    self._signal["fault"]["encoder"]: self._init_vals["fault"]["encoder"],
                    self._signal["fault"]["motor"]["temperature"]: self._init_vals["motor"]["temperature"],
                    self._signal["fault"]["motor"]["speed"]: self._init_vals["motor"]["speed"],
                    self._signal["fault"]["inverter"]["current"]["over_charge"]: self._init_vals["fault"]["inverter"]["current"]["over_charge"],
                    self._signal["fault"]["inverter"]["voltage"]["over_voltage"]: self._init_vals["fault"]["inverter"]["voltage"]["over_voltage"],
                    self._signal["fault"]["torque"]["command"]: self._init_vals["fault"]["torque"]["command"],
                    self._signal["fault"]["torque"]["estimation"]: self._init_vals["fault"]["torque"]["estimation"],
                    self._signal["fault"]["torque"]["monitor"]: self._init_vals["fault"]["torque"]["monitor"],
                    self._signal["fault"]["status"]["hv_request"]: self._init_vals["fault"]["status"]["hv_request"],
                    self._signal["fault"]["status"]["amber"]: self._init_vals["fault"]["status"]["amber"],
                    self._signal["fault"]["status"]["stop"]: self._init_vals["fault"]["status"]["stop"],
                },
            }

            return periodic_can_messages

        def reset_can_signals(self):
            """
            Reset the can signals.

            Returns
            -------
            None.

            """
            self._controller.write_signal(self._message["torque"]["qm"]["demand"], self._signal["torque"]["qm"]["demand"], self._reset["torque"]["qm"]["demand"])
            self._controller.write_signal(self._message["torque"]["fusa"]["demand"], self._signal["torque"]["fusa"]["demand"], self._reset["torque"]["fusa"]["demand"])
            self._controller.write_signal(self._message["torque"]["qm"]["limits"]["drive"], self._signal["torque"]["qm"]["limits"]["drive"], self._reset["torque"]["qm"]["limits"]["drive"])
            self._controller.write_signal(self._message["torque"]["fusa"]["limits"]["drive"], self._signal["torque"]["fusa"]["limits"]["drive"], self._reset["torque"]["fusa"]["limits"]["drive"])
            self._controller.write_signal(self._message["torque"]["qm"]["limits"]["brake"], self._signal["torque"]["qm"]["limits"]["brake"], self._reset["torque"]["qm"]["limits"]["brake"])
            self._controller.write_signal(self._message["torque"]["fusa"]["limits"]["brake"], self._signal["torque"]["fusa"]["limits"]["brake"], self._reset["torque"]["fusa"]["limits"]["brake"])
            self._controller.write_signal(self._message["speed"]["limits"]["forward"], self._signal["speed"]["limits"]["forward"], self._reset["speed"]["limits"]["forward"])
            self._controller.write_signal(self._message["speed"]["limits"]["reverse"], self._signal["speed"]["limits"]["reverse"], self._reset["speed"]["limits"]["reverse"])
            self._controller.write_signal(self._message["dc_link"]["limits"]["voltage"]["max"], self._signal["dc_link"]["limits"]["voltage"]["max"], self._reset["dc_link"]["limits"]["voltage"]["max"])
            self._controller.write_signal(self._message["dc_link"]["limits"]["voltage"]["min"], self._signal["dc_link"]["limits"]["voltage"]["min"], self._reset["dc_link"]["limits"]["voltage"]["min"])
            self._controller.write_signal(
                self._message["dc_link"]["limits"]["current"]["charge"], self._signal["dc_link"]["limits"]["current"]["charge"], self._reset["dc_link"]["limits"]["current"]["charge"]
            )
            self._controller.write_signal(
                self._message["dc_link"]["limits"]["current"]["discharge"], self._signal["dc_link"]["limits"]["current"]["discharge"], self._reset["dc_link"]["limits"]["current"]["discharge"]
            )
            self._controller.write_signal(self._message["status"]["contactor"], self._signal["status"]["contactor"], self._reset["status"]["contactor"])
            self._controller.write_signal(self._message["speed"]["transmission"], self._signal["speed"]["transmission"], self._reset["speed"]["transmission"])
            self._controller.write_signal(self._message["crash"]["active_discharge"], self._signal["crash"]["active_discharge"], self._reset["crash"]["active_discharge"])
            self._controller.write_signal(self._message["bridge"]["enable"], self._signal["bridge"]["enable"], self._reset["bridge"]["enable"])

        def bridge_enable(self, enabled: bool):
            """
            Enable or disable the bridge.

            Parameters
            ----------
            enable : bool
                True to enable the bridge, False to disable.

            Returns
            -------
            bool
                True if the bridge state is the expected one, False otherwise.

            """

            if enabled:
                self._controller.write_signal(self._message["bridge"]["enable"], self._signal["bridge"]["enable"], self._parameters["bridge"]["enable"])
                time.sleep(2)
                success = self._controller.get_bridge_state() == BridgeState.TorqueEnabled
            else:
                self._controller.write_signal(self._message["bridge"]["enable"], self._signal["bridge"]["enable"], self._parameters["bridge"]["disable"])
                time.sleep(0.5)
                success = self._controller.get_bridge_state() != BridgeState.TorqueEnabled

            return success

        def crash_event(self, crashed):
            """
            Set the crash event.

            Parameters
            ----------
            crashed
                True if the crash event is active, False otherwise.

            Returns
            -------
            None.
            """

            return self._controller.write_signal(self._message["crash"]["active_discharge"], self._signal["crash"]["active_discharge"], self._parameters["crash_value"] if crashed else 0)

        def current_limits(self, discharge_limit: float, charge_limit: float):
            """
            Set the current limits.

            Parameters
            ----------
            discharge_limit : float
                The discharge limit.
            charge_limit : float
                The charge limit.

            Returns
            -------
            None.
            """

            discharge_limit = max(min(discharge_limit, self._limits["dc_link"]["current"]["discharge"]), -self._limits["dc_link"]["current"]["discharge"])
            charge_limit = max(min(charge_limit, self._limits["dc_link"]["current"]["charge"]), -self._limits["dc_link"]["current"]["charge"])
            return self._controller.write_signal(
                self._message["dc_link"]["limits"]["current"]["discharge"], self._signal["dc_link"]["limits"]["current"]["discharge"], discharge_limit
            ) and self._controller.write_signal(self._message["dc_link"]["limits"]["current"]["charge"], self._signal["dc_link"]["limit"]["current"]["charge"], charge_limit)

        def speed_limits(self, forward_limit: float, reverse_limit: float):
            """
            Set the speed limits.

            Parameters
            ----------
            forward_limit : float
                The forward limit.
            reverse_limit : float
                The reverse limit.

            Returns
            -------
            None.
            """

            forward_limit = max(min(forward_limit, self._limits["speed"]["forward"]), -self._limits["speed"]["forward"])
            reverse_limit = max(min(reverse_limit, self._limits["speed"]["reverse"]), -self._limits["speed"]["reverse"])
            return self._controller.write_signal(self._message["speed"]["limits"]["forward"], self._signal["speed"]["limits"]["forward"], forward_limit) and self._controller.write_signal(
                self._message["speed"]["limits"]["reverse"], self._signal["speed"]["limits"]["reverse"], reverse_limit
            )

        def torque_demand(self, torque_demand: float):
            """
            Set the torque demand.

            Parameters
            ----------
            torque_demand : float
                The torque demand.

            Returns
            -------
            None.
            """

            torque_demand = max(min(torque_demand, self._limits["torque"]["drive"]), self._limits["torque"]["brake"])
            torque_drive_limit = min(self._limits["torque"]["drive"], abs(torque_demand) * 1.2)
            torque_brake_limit = min(self._limits["torque"]["drive"], abs(torque_demand) * 1.2)
            return (
                self._controller.write_signal(self._message["torque"]["qm"]["limits"]["drive"], self._signal["torque"]["qm"]["limits"]["drive"], torque_drive_limit)
                and self._controller.write_signal(self._message["torque"]["fusa"]["limits"]["drive"], self._signal["torque"]["fusa"]["limits"]["drive"], torque_drive_limit)
                and self._controller.write_signal(self._message["torque"]["qm"]["limits"]["brake"], self._signal["torque"]["qm"]["limits"]["brake"], torque_brake_limit)
                and self._controller.write_signal(self._message["torque"]["fusa"]["limits"]["brake"], self._signal["torque"]["fusa"]["limits"]["brake"], torque_brake_limit)
                and self._controller.write_signal(self._message["torque"]["qm"]["demand"], self._signal["torque"]["qm"]["demand"], torque_demand)
                and self._controller.write_signal(self._message["torque"]["fusa"]["demand"], self._signal["torque"]["fusa"]["demand"], torque_demand)
            )

    class Read:
        """
        Read class for the Derwent controller.

        Attributes
        ----------
        _controller : Gen5
            The Gen5 controller.
        _message : dict
            The message dictionary.
        _signal : dict
            The signal dictionary.
        _parameters : dict
            The parameters dictionary.
        _limits : dict
            The limits dictionary.

        Methods
        -------
        bridge_state() -> int
            Get the bridge state.
        alert_id()
            Get the alert ID.
        block_id()
            Get the block ID.
        torque_demand() -> float
            Get the torque demand.
        torque_demand_fusa() -> float
            Get the torque demand, in the saftey layer.
        torque_demand_actual() -> float
            Get the actual torque demand, this is the torque demand after derating (if any applied).
        speed() -> float
            Get the speed.
        dc_link_voltage() -> float
            Get the DC link voltage.
        dc_link_current() -> float
            Get the DC link current.
        inverter_temperature() -> float
            Get the inverter temperature.
        motor_temperature() -> float
            Get the motor temperature.
        id_feedback() -> float
            Get the D-axis current feedback.
        iq_feedback() -> float
            Get the Q-axis current feedback.
        ud_feedback() -> float
            Get the D-axis voltage feedback.
        uq_feedback() -> float
            Get the Q-axis voltage feedback.
        mod_index() -> float
            Get the modulation index.
        switching_frequency() -> float
            Get the switching frequency.
        """

        def __init__(self, controller):
            self._controller = controller
            self._message = controller._message
            self._signal = controller._signal
            self._parameters = controller._parameters
            self._limits = controller._limits
            self._periodic_can_messages = self._controller._periodic_can_messages

        def update_can_values(self, message_received):
            """
            Update the CAN values.

            Parameters
            ----------
            message_received : can.Message
                The CAN message received.
            """
            self._controller.read_can_values(self._periodic_can_messages, message_received)

        def bridge_state(self) -> int:
            """
            Get the bridge state.

            Returns
            -------
            int
                The bridge state.

            """
            bridge_state = BridgeState.HighImpedance

            value = self._controller.read_periodic_signal(self._periodic_can_messages, self._message["bridge"]["state"], self._signal["bridge"]["state"])

            if value == 3:
                bridge_state = BridgeState.TorqueEnabled
            elif value == 2:
                bridge_state = BridgeState.ActiveDischarge
            elif value == 1:
                bridge_state = BridgeState.ActiveShortCircuit

            return bridge_state

        def alert_id(self):
            """
            Get the alert ID.

            Returns
            -------
            hex(int)
                The alert ID.
            """
            return hex(int(self._controller.read_periodic_signal(self._periodic_can_messages, self._message["status"]["alert_id"], self._signal["status"]["alert_id"])))

        def block_id(self):
            """
            Get the block ID.

            Returns
            -------
            hex(int)
                The block ID.
            """

            return hex(int(self._controller.read_periodic_signal(self._periodic_can_messages, self._message["status"]["block_id"], self._signal["status"]["block_id"])))

        def torque_demand(self) -> float:
            """
            Get the torque demand.

            Returns
            -------
            float
                The torque demand.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["torque"]["qm"]["demand"], self._signal["torque"]["qm"]["demand"])

        def torque_demand_fusa(self) -> float:
            """
            Get the torque demand.

            Returns
            -------
            float
                The torque demand.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["torque"]["fusa"]["demand"], self._signal["torque"]["fusa"]["demand"])

        def torque_demand_actual(self) -> float:
            """
            Get the actual torque demand, this is the torque demand after derating (if any applied).

            Returns
            -------
            float
                The derated torque demand.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["torque"]["qm"]["feedback"]["actual"], self._signal["torque"]["qm"]["feedback"]["actual"])

        def speed(self) -> float:
            """
            Get the speed.

            Returns
            -------
            float
                The speed.
            """

            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["speed"]["feedback"], self._signal["speed"]["feedback"])

        def dc_link_voltage(self) -> float:
            """
            Get the DC link voltage.

            Returns
            -------
            float
                The DC link voltage.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["dc_link"]["feedback"]["voltage"], self._signal["dc_link"]["feedback"]["voltage"])

        def dc_link_current(self) -> float:
            """
            Get the DC link current.

            Returns
            -------
            float
                The DC link current.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["dc_link"]["feedback"]["current"], self._signal["dc_link"]["feedback"]["current"])

        def inverter_temperature(self) -> float:
            """
            Get the inverter temperature.

            Returns
            -------
            float
                The inverter temperature.
            """
            return self._controller.read_periodic_signal(
                self._periodic_can_messages, self._message["temperature"]["feedback"]["inverter"]["inverter"], self._signal["temperature"]["feedback"]["inverter"]["inverter"]
            )

        def motor_temperature(self) -> float:
            """
            Get the motor temperature.

            Returns
            -------
            float
                The motor temperature.
            """
            return self._controller.read_periodic_signal(
                self._periodic_can_messages, self._message["temperature"]["feedback"]["motor"]["motor"], self._signal["temperature"]["feedback"]["motor"]["motor"]
            )

        def id_feedback(self) -> float:
            """
            Get the Id current (Stationary frame current, D-axis) feedback.

            Returns
            -------
            float
                The Id current (Stationary frame current, D-axis) current feedback.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["motor_control"]["feedback"]["id"], self._signal["motor_control"]["feedback"]["id"])

        def iq_feedback(self) -> float:
            """
            Get the Iq current (Stationary frame current, Q-axis) feedback.

            Returns
            -------
            float
                The Iq current (Stationary frame current, Q-axis) current feedback.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["motor_control"]["feedback"]["iq"], self._signal["motor_control"]["feedback"]["iq"])

        def ud_feedback(self) -> float:
            """
            Get the Ud voltage (Stationary frame voltage, D-axis) feedback.

            Returns
            -------
            float
                The Ud voltage (Stationary frame voltage, D-axis) voltage feedback.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["motor_control"]["feedback"]["ud"], self._signal["motor_control"]["feedback"]["ud"])

        def uq_feedback(self) -> float:
            """
            Get the Uq voltage (Stationary frame voltage, Q-axis) feedback.

            Returns
            -------
            float
                The Uq voltage (Stationary frame voltage, Q-axis) voltage feedback.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["motor_control"]["feedback"]["uq"], self._signal["motor_control"]["feedback"]["uq"])

        def mod_index(self) -> float:
            """
            Get the modulation index.

            Returns
            -------
            float
                The modulation index.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["motor_control"]["mod_index"], self._signal["motor_control"]["mod_index"])

        def switching_frequency(self) -> float:
            """
            Get the switching frequency.

            Returns
            -------
            float
                The switching frequency.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["motor_control"]["switching_frequency"], self._signal["motor_control"]["switching_frequency"])


class Blencathra(CanDevice):
    def __init__(self, config):
        """
        Initialize the Blencathra controller.

        Parameters
        ----------
        config : dict
            The configuration dictionary.

        Returns
        -------
        None.

        """
        super().__init__()

        self._config = config
        self._parameters = self._config["parameters"]
        self._limits = self._parameters["limits"]
        self._message = self._config["can"]["messages"]
        self._signal = self._config["can"]["signals"]
        self._init_vals = self._config["can"]["initial_values"]
        self._periodic_can_messages: Union[dict, None] = None

        # Set up the CAN bus using the provided configuration
        self._bus = self.setup_can_bus(self._config)

        # Load the CAN message database
        self._db = self.load_database(self._config["can"]["database"])

        # Set the listener to receive CAN messages
        self._listener = self.setup_listener()

        # Set up the notifier to notify the listener when a message is received
        self._notifier = self.setup_notifier(self._bus, [self._listener])

        # Create a write object for the Blencathra class to send CAN messages
        self.write = Blencathra.Write(self)

        # Initialize periodic CAN messages
        self._periodic_can_messages = self.write.initialise_periodic_messages()

        # Create a read object for the Blencathra class to receive CAN messages, have to setup read after periodic messages
        self.read = Blencathra.Read(self)

        # Write the periodic CAN messages at a specified time interval based on configuration
        self.write_periodic_message(self._periodic_can_messages, self._config["can"]["period"])

        # Update CAN values on every message received by the listener
        self._listener.on_message_received = self.read.update_can_values

    def __del__(self):
        # docstring
        """
        Clean up the Blencathra controller by turning off torque demand and disabling its output,
        shutting down the bus, and deleting the instance.


        Parameters
        ----------

        Returns
        -------
        None."""

        try:
            # Set the torque demand to 0
            self.write.torque_demand(0.0)
            time.sleep(0.5)

            # Disable the bridge and shutdown the bus
            self.write.bridge_enable(False)
            self._bus.shutdown()

        except Exception as e:
            print(e)

    class Write:
        """
        Class for writing to the Bowfell inverter.

        Attributes
        ----------
        _controller : Controller
            The controller object.
        _message : dict
            The message dictionary.
        _signal : dict
            The signal dictionary.
        _parameters : dict
            The parameters dictionary.
        _reset : dict
            The reset dictionary.
        _limits : dict
            The limits dictionary.

        Methods
        -------
        reset_can_signals()
            Reset the can signals.
        torque_demand(torque_demand)
            Set the torque demand.
        torque_demand_fusa(torque_demand)
            Set the torque demand.
        speed(speed)
            Set the speed.
        """

        def __init__(self, controller):
            self._controller = controller
            self._message = controller._message
            self._signal = controller._signal
            self._parameters = controller._parameters
            self._init_vals = controller._init_vals
            self._limits = controller._limits

        def initialise_periodic_messages(self):
            """
            Initialise the periodic CAN messages, using the configuration.

            Returns

            -------
            periodic_can_messages : dict
                The periodic CAN messages.

            """

            periodic_can_messages = {
                # TX MESSAGES
                self._message["torque"]["qm"]["demand"]: {
                    # VC_Trq
                    self._signal["torque"]["qm"]["demand"]: self._init_vals["torque"]["qm"]["demand"],
                    self._signal["torque"]["qm"]["limits"]["drive"]: self._init_vals["torque"]["qm"]["limits"]["drive"],
                    self._signal["torque"]["qm"]["limits"]["brake"]: self._init_vals["torque"]["qm"]["limits"]["brake"],
                },
                self._message["torque"]["fusa"]["demand"]: {
                    # VC_Trq_FuSa
                    self._signal["torque"]["fusa"]["demand"]: self._init_vals["torque"]["qm"]["demand"],
                    self._signal["torque"]["fusa"]["limits"]["drive"]: self._init_vals["torque"]["qm"]["limits"]["drive"],
                    self._signal["torque"]["fusa"]["limits"]["brake"]: self._init_vals["torque"]["qm"]["limits"]["brake"],
                },
                self._message["dc_link"]["limits"]["current"]["charge"]: {
                    # VC_Bat
                    self._signal["dc_link"]["limits"]["current"]["charge"]: self._init_vals["dc_link"]["limits"]["current"]["charge"],
                    self._signal["dc_link"]["limits"]["current"]["discharge"]: self._init_vals["dc_link"]["limits"]["current"]["discharge"],
                    self._signal["dc_link"]["limits"]["voltage"]["max"]: self._init_vals["dc_link"]["limits"]["voltage"]["max"],
                    self._signal["dc_link"]["limits"]["voltage"]["min"]: self._init_vals["dc_link"]["limits"]["voltage"]["min"],
                },
                self._message["crash"]["active_discharge"]: {
                    # VC_CrashInd
                    self._signal["crash"]["active_discharge"]: self._init_vals["crash"]["active_discharge"],
                },
                self._message["bridge"]["enable"]: {
                    # VC_SpdCmd
                    self._signal["bridge"]["enable"]: self._init_vals["bridge"]["enable"],
                    self._signal["speed"]["limits"]["forward"]: self._init_vals["speed"]["limits"]["forward"],
                    self._signal["speed"]["limits"]["reverse"]: self._init_vals["speed"]["limits"]["reverse"],
                },
                self._message["speed"]["transmission"]: {
                    # VC_TransSpd
                    self._signal["speed"]["transmission"]: self._init_vals["speed"]["transmission"],
                },
                self._message["status"]["contactor"]: {
                    # PDU_Status_4
                    self._signal["status"]["contactor"]: self._init_vals["status"]["contactor"],
                },
                # RX MESSAGES
                self._message["dc_link"]["feedback"]["voltage"]: {
                    # MC_Sts
                    self._signal["dc_link"]["feedback"]["voltage"]: self._init_vals["dc_link"]["feedback"]["voltage"],
                    self._signal["dc_link"]["feedback"]["current"]: self._init_vals["dc_link"]["feedback"]["current"],
                    self._signal["temperature"]["motor"]["motor"]: self._init_vals["temperature"]["motor"]["motor"],
                },
                self._message["temperature"]["feedback"]["inverter"]["inverter"]: {
                    # MC_Sts_2
                    self._signal["temperature"]["inverter"]["inverter"]: self._init_vals["temperature"]["inverter"]["inverter"],
                    self._signal["bridge"]["state"]: self._init_vals["bridge"]["state"],
                    self._signal["status"]["fault"]: self._init_vals["status"]["fault"],
                    self._signal["status"]["kl_30"]: self._init_vals["status"]["kl_30"],
                },
                self._message["torque"]["qm"]["feedback"]: {
                    # MC_Trq
                    self._signal["torque"]["qm"]["feedback"]: self._init_vals["torque"]["qm"]["feedback"],
                    self._signal["torque"]["qm"]["max_drive_available"]: self._init_vals["torque"]["qm"]["max_drive_available"],
                    self._signal["torque"]["qm"]["max_brake_available"]: self._init_vals["torque"]["qm"]["max_brake_available"],
                },
                self._message["speed"]["feedback"]: {
                    # MC_Vel
                    self._signal["speed"]["feedback"]: self._init_vals["speed"]["feedback"],
                },
                self._message["motor_control"]["feedback"]["ud"]: {
                    # MC_V2_Debug_1
                    self._signal["motor_control"]["feedback"]["ud"]: self._init_vals["motor_control"]["feedback"]["ud"],
                    self._signal["motor_control"]["feedback"]["uq"]: self._init_vals["motor_control"]["feedback"]["uq"],
                    self._signal["motor_control"]["feedback"]["id"]: self._init_vals["motor_control"]["feedback"]["id"],
                    self._signal["motor_control"]["feedback"]["iq"]: self._init_vals["motor_control"]["feedback"]["iq"],
                    self._signal["motor_control"]["feedback"]["mod_index"]: self._init_vals["motor_control"]["feedback"]["mod_index"],
                    self._signal["motor_control"]["feedback"]["switching_frequency"]: self._init_vals["motor_control"]["feedback"]["switching_frequency"],
                },
                self._message["status"]["alert_id"]: {
                    # MC_Debug_3_Status
                    self._signal["status"]["alert_id"]: self._init_vals["status"]["alert_id"],
                    self._signal["status"]["block_id"]: self._init_vals["status"]["block_id"],
                    self._signal["status"]["status"]: self._init_vals["status"]["status"],
                    self._signal["status"]["placeholder"]: self._init_vals["status"]["placeholder"],
                },
                self._message["fault"]["encoder"]: {
                    # MC_Sts_3
                    self._signal["fault"]["encoder"]: self._init_vals["fault"]["encoder"],
                    self._signal["fault"]["motor"]["temperature"]: self._init_vals["motor"]["temperature"],
                    self._signal["fault"]["motor"]["speed"]: self._init_vals["motor"]["speed"],
                    self._signal["fault"]["inverter"]["current"]["over_charge"]: self._init_vals["fault"]["inverter"]["current"]["over_charge"],
                    self._signal["fault"]["inverter"]["voltage"]["over_voltage"]: self._init_vals["fault"]["inverter"]["voltage"]["over_voltage"],
                    self._signal["fault"]["torque"]["command"]: self._init_vals["fault"]["torque"]["command"],
                    self._signal["fault"]["torque"]["estimation"]: self._init_vals["fault"]["torque"]["estimation"],
                    self._signal["fault"]["torque"]["monitor"]: self._init_vals["fault"]["torque"]["monitor"],
                    self._signal["fault"]["status"]["hv_request"]: self._init_vals["fault"]["status"]["hv_request"],
                    self._signal["fault"]["status"]["amber"]: self._init_vals["fault"]["status"]["amber"],
                    self._signal["fault"]["status"]["stop"]: self._init_vals["fault"]["status"]["stop"],
                },
            }

            return periodic_can_messages

        def reset_can_signals(self):
            """
            Reset the can signals.

            Returns
            -------
            None.

            """
            self._controller.write_signal(self._message["torque"]["qm"]["demand"], self._signal["torque"]["qm"]["demand"], self._reset["torque"]["qm"]["demand"])
            self._controller.write_signal(self._message["torque"]["fusa"]["demand"], self._signal["torque"]["fusa"]["demand"], self._reset["torque"]["fusa"]["demand"])
            self._controller.write_signal(self._message["torque"]["qm"]["limits"]["drive"], self._signal["torque"]["qm"]["limits"]["drive"], self._reset["torque"]["qm"]["limits"]["drive"])
            self._controller.write_signal(self._message["torque"]["fusa"]["limits"]["drive"], self._signal["torque"]["fusa"]["limits"]["drive"], self._reset["torque"]["fusa"]["limits"]["drive"])
            self._controller.write_signal(self._message["torque"]["qm"]["limits"]["brake"], self._signal["torque"]["qm"]["limits"]["brake"], self._reset["torque"]["qm"]["limits"]["brake"])
            self._controller.write_signal(self._message["torque"]["fusa"]["limits"]["brake"], self._signal["torque"]["fusa"]["limits"]["brake"], self._reset["torque"]["fusa"]["limits"]["brake"])
            self._controller.write_signal(self._message["speed"]["limits"]["forward"], self._signal["speed"]["limits"]["forward"], self._reset["speed"]["limits"]["forward"])
            self._controller.write_signal(self._message["speed"]["limits"]["reverse"], self._signal["speed"]["limits"]["reverse"], self._reset["speed"]["limits"]["reverse"])
            self._controller.write_signal(self._message["dc_link"]["limits"]["voltage"]["max"], self._signal["dc_link"]["limits"]["voltage"]["max"], self._reset["dc_link"]["limits"]["voltage"]["max"])
            self._controller.write_signal(self._message["dc_link"]["limits"]["voltage"]["min"], self._signal["dc_link"]["limits"]["voltage"]["min"], self._reset["dc_link"]["limits"]["voltage"]["min"])
            self._controller.write_signal(
                self._message["dc_link"]["limits"]["current"]["charge"], self._signal["dc_link"]["limits"]["current"]["charge"], self._reset["dc_link"]["limits"]["current"]["charge"]
            )
            self._controller.write_signal(
                self._message["dc_link"]["limits"]["current"]["discharge"], self._signal["dc_link"]["limits"]["current"]["discharge"], self._reset["dc_link"]["limits"]["current"]["discharge"]
            )
            self._controller.write_signal(self._message["status"]["contactor"], self._signal["status"]["contactor"], self._reset["status"]["contactor"])
            self._controller.write_signal(self._message["speed"]["transmission"], self._signal["speed"]["transmission"], self._reset["speed"]["transmission"])
            self._controller.write_signal(self._message["crash"]["active_discharge"], self._signal["crash"]["active_discharge"], self._reset["crash"]["active_discharge"])
            self._controller.write_signal(self._message["bridge"]["enable"], self._signal["bridge"]["enable"], self._reset["bridge"]["enable"])

        def bridge_enable(self, enabled: bool):
            """
            Enable or disable the bridge.

            Parameters
            ----------
            enable : bool
                True to enable the bridge, False to disable.

            Returns
            -------
            bool
                True if the bridge state is the expected one, False otherwise.

            """

            if enabled:
                self._controller.write_signal(self._message["bridge"]["enable"], self._signal["bridge"]["enable"], self._parameters["bridge"]["enable"])
                time.sleep(2)
                success = self._controller.get_bridge_state() == BridgeState.TorqueEnabled
            else:
                self._controller.write_signal(self._message["bridge"]["enable"], self._signal["bridge"]["enable"], self._parameters["bridge"]["disable"])
                time.sleep(0.5)
                success = self._controller.get_bridge_state() != BridgeState.TorqueEnabled

            return success

        def crash_event(self, crashed):
            """
            Set the crash event.

            Parameters
            ----------
            crashed
                True if the crash event is active, False otherwise.

            Returns
            -------
            None.
            """

            return self._controller.write_signal(self._message["crash"]["active_discharge"], self._signal["crash"]["active_discharge"], self._parameters["crash_value"] if crashed else 0)

        def current_limits(self, discharge_limit: float, charge_limit: float):
            """
            Set the current limits.

            Parameters
            ----------
            discharge_limit : float
                The discharge limit.
            charge_limit : float
                The charge limit.

            Returns
            -------
            None.
            """

            discharge_limit = max(min(discharge_limit, self._limits["dc_link"]["current"]["discharge"]), -self._limits["dc_link"]["current"]["discharge"])
            charge_limit = max(min(charge_limit, self._limits["dc_link"]["current"]["charge"]), -self._limits["dc_link"]["current"]["charge"])
            return self._controller.write_signal(
                self._message["dc_link"]["limits"]["current"]["discharge"], self._signal["dc_link"]["limits"]["current"]["discharge"], discharge_limit
            ) and self._controller.write_signal(self._message["dc_link"]["limits"]["current"]["charge"], self._signal["dc_link"]["limit"]["current"]["charge"], charge_limit)

        def speed_limits(self, forward_limit: float, reverse_limit: float):
            """
            Set the speed limits.

            Parameters
            ----------
            forward_limit : float
                The forward limit.
            reverse_limit : float
                The reverse limit.

            Returns
            -------
            None.
            """

            forward_limit = max(min(forward_limit, self._limits["speed"]["forward"]), -self._limits["speed"]["forward"])
            reverse_limit = max(min(reverse_limit, self._limits["speed"]["reverse"]), -self._limits["speed"]["reverse"])
            return self._controller.write_signal(self._message["speed"]["limits"]["forward"], self._signal["speed"]["limits"]["forward"], forward_limit) and self._controller.write_signal(
                self._message["speed"]["limits"]["reverse"], self._signal["speed"]["limits"]["reverse"], reverse_limit
            )

        def torque_demand(self, torque_demand: float):
            """
            Set the torque demand.

            Parameters
            ----------
            torque_demand : float
                The torque demand.

            Returns
            -------
            None.
            """

            torque_demand = max(min(torque_demand, self._limits["torque"]["drive"]), self._limits["torque"]["brake"])
            torque_drive_limit = min(self._limits["torque"]["drive"], abs(torque_demand) * 1.2)
            torque_brake_limit = min(self._limits["torque"]["drive"], abs(torque_demand) * 1.2)
            return (
                self._controller.write_signal(self._message["torque"]["qm"]["limits"]["drive"], self._signal["torque"]["qm"]["limits"]["drive"], torque_drive_limit)
                and self._controller.write_signal(self._message["torque"]["fusa"]["limits"]["drive"], self._signal["torque"]["fusa"]["limits"]["drive"], torque_drive_limit)
                and self._controller.write_signal(self._message["torque"]["qm"]["limits"]["brake"], self._signal["torque"]["qm"]["limits"]["brake"], torque_brake_limit)
                and self._controller.write_signal(self._message["torque"]["fusa"]["limits"]["brake"], self._signal["torque"]["fusa"]["limits"]["brake"], torque_brake_limit)
                and self._controller.write_signal(self._message["torque"]["qm"]["demand"], self._signal["torque"]["qm"]["demand"], torque_demand)
                and self._controller.write_signal(self._message["torque"]["fusa"]["demand"], self._signal["torque"]["fusa"]["demand"], torque_demand)
            )

    class Read:
        """
        Read class for the Blencathra controller.

        Attributes
        ----------
        _controller : Gen5
            The Gen5 controller.
        _message : dict
            The message dictionary.
        _signal : dict
            The signal dictionary.
        _parameters : dict
            The parameters dictionary.
        _limits : dict
            The limits dictionary.

        Methods
        -------
        bridge_state() -> int
            Get the bridge state.
        alert_id()
            Get the alert ID.
        block_id()
            Get the block ID.
        torque_demand() -> float
            Get the torque demand.
        torque_demand_fusa() -> float
            Get the torque demand, in the saftey layer.
        torque_demand_actual() -> float
            Get the actual torque demand, this is the torque demand after derating (if any applied).
        speed() -> float
            Get the speed.
        dc_link_voltage() -> float
            Get the DC link voltage.
        dc_link_current() -> float
            Get the DC link current.
        inverter_temperature() -> float
            Get the inverter temperature.
        motor_temperature() -> float
            Get the motor temperature.
        id_feedback() -> float
            Get the D-axis current feedback.
        iq_feedback() -> float
            Get the Q-axis current feedback.
        ud_feedback() -> float
            Get the D-axis voltage feedback.
        uq_feedback() -> float
            Get the Q-axis voltage feedback.
        mod_index() -> float
            Get the modulation index.
        switching_frequency() -> float
            Get the switching frequency.
        """

        def __init__(self, controller):
            self._controller = controller
            self._message = controller._message
            self._signal = controller._signal
            self._parameters = controller._parameters
            self._limits = controller._limits
            self._periodic_can_messages = self._controller._periodic_can_messages

        def update_can_values(self, message_received):
            """
            Update the CAN values.

            Parameters
            ----------
            message_received : can.Message
                The CAN message received.
            """
            self._controller.read_can_values(self._periodic_can_messages, message_received)

        def bridge_state(self) -> int:
            """
            Get the bridge state.

            Returns
            -------
            int
                The bridge state.

            """
            bridge_state = BridgeState.HighImpedance

            value = self._controller.read_periodic_signal(self._periodic_can_messages, self._message["bridge"]["state"], self._signal["bridge"]["state"])

            if value == 3:
                bridge_state = BridgeState.TorqueEnabled
            elif value == 2:
                bridge_state = BridgeState.ActiveDischarge
            elif value == 1:
                bridge_state = BridgeState.ActiveShortCircuit

            return bridge_state

        def alert_id(self):
            """
            Get the alert ID.

            Returns
            -------
            hex(int)
                The alert ID.
            """
            return hex(int(self._controller.read_periodic_signal(self._periodic_can_messages, self._message["status"]["alert_id"], self._signal["status"]["alert_id"])))

        def block_id(self):
            """
            Get the block ID.

            Returns
            -------
            hex(int)
                The block ID.
            """

            return hex(int(self._controller.read_periodic_signal(self._periodic_can_messages, self._message["status"]["block_id"], self._signal["status"]["block_id"])))

        def torque_demand(self) -> float:
            """
            Get the torque demand.

            Returns
            -------
            float
                The torque demand.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["torque"]["qm"]["demand"], self._signal["torque"]["qm"]["demand"])

        def torque_demand_fusa(self) -> float:
            """
            Get the torque demand.

            Returns
            -------
            float
                The torque demand.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["torque"]["fusa"]["demand"], self._signal["torque"]["fusa"]["demand"])

        def torque_demand_actual(self) -> float:
            """
            Get the actual torque demand, this is the torque demand after derating (if any applied).

            Returns
            -------
            float
                The derated torque demand.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["torque"]["qm"]["feedback"]["actual"], self._signal["torque"]["qm"]["feedback"]["actual"])

        def speed(self) -> float:
            """
            Get the speed.

            Returns
            -------
            float
                The speed.
            """

            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["speed"]["feedback"], self._signal["speed"]["feedback"])

        def dc_link_voltage(self) -> float:
            """
            Get the DC link voltage.

            Returns
            -------
            float
                The DC link voltage.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["dc_link"]["feedback"]["voltage"], self._signal["dc_link"]["feedback"]["voltage"])

        def dc_link_current(self) -> float:
            """
            Get the DC link current.

            Returns
            -------
            float
                The DC link current.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["dc_link"]["feedback"]["current"], self._signal["dc_link"]["feedback"]["current"])

        def inverter_temperature(self) -> float:
            """
            Get the inverter temperature.

            Returns
            -------
            float
                The inverter temperature.
            """
            return self._controller.read_periodic_signal(
                self._periodic_can_messages, self._message["temperature"]["feedback"]["inverter"]["inverter"], self._signal["temperature"]["feedback"]["inverter"]["inverter"]
            )

        def motor_temperature(self) -> float:
            """
            Get the motor temperature.

            Returns
            -------
            float
                The motor temperature.
            """
            return self._controller.read_periodic_signal(
                self._periodic_can_messages, self._message["temperature"]["feedback"]["motor"]["motor"], self._signal["temperature"]["feedback"]["motor"]["motor"]
            )

        def id_feedback(self) -> float:
            """
            Get the Id current (Stationary frame current, D-axis) feedback.

            Returns
            -------
            float
                The Id current (Stationary frame current, D-axis) current feedback.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["motor_control"]["feedback"]["id"], self._signal["motor_control"]["feedback"]["id"])

        def iq_feedback(self) -> float:
            """
            Get the Iq current (Stationary frame current, Q-axis) feedback.

            Returns
            -------
            float
                The Iq current (Stationary frame current, Q-axis) current feedback.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["motor_control"]["feedback"]["iq"], self._signal["motor_control"]["feedback"]["iq"])

        def ud_feedback(self) -> float:
            """
            Get the Ud voltage (Stationary frame voltage, D-axis) feedback.

            Returns
            -------
            float
                The Ud voltage (Stationary frame voltage, D-axis) voltage feedback.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["motor_control"]["feedback"]["ud"], self._signal["motor_control"]["feedback"]["ud"])

        def uq_feedback(self) -> float:
            """
            Get the Uq voltage (Stationary frame voltage, Q-axis) feedback.

            Returns
            -------
            float
                The Uq voltage (Stationary frame voltage, Q-axis) voltage feedback.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["motor_control"]["feedback"]["uq"], self._signal["motor_control"]["feedback"]["uq"])

        def mod_index(self) -> float:
            """
            Get the modulation index.

            Returns
            -------
            float
                The modulation index.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["motor_control"]["mod_index"], self._signal["motor_control"]["mod_index"])

        def switching_frequency(self) -> float:
            """
            Get the switching frequency.

            Returns
            -------
            float
                The switching frequency.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["motor_control"]["switching_frequency"], self._signal["motor_control"]["switching_frequency"])


class Hawkshead(CanDevice):
    def __init__(self, config):
        """
        Initialize the Hawkshead controller.

        Parameters
        ----------
        config : dict
            The configuration dictionary.

        Returns
        -------
        None.

        """
        super().__init__()

        self._config = config
        self._parameters = self._config["parameters"]
        self._limits = self._parameters["limits"]
        self._message = self._config["can"]["messages"]
        self._signal = self._config["can"]["signals"]
        self._init_vals = self._config["can"]["initial_values"]
        self._periodic_can_messages: Union[dict, None] = None

        # Set up the CAN bus using the provided configuration
        self._bus = self.setup_can_bus(self._config)

        # Load the CAN message database
        self._db = self.load_database(self._config["can"]["database"])

        # Set the listener to receive CAN messages
        self._listener = self.setup_listener()

        # Set up the notifier to notify the listener when a message is received
        self._notifier = self.setup_notifier(self._bus, [self._listener])

        # Create a write object for the Hawkshead class to send CAN messages
        self.write = Hawkshead.Write(self)

        # Initialize periodic CAN messages
        self._periodic_can_messages = self.write.initialise_periodic_messages()

        # Create a read object for the Hawkshead class to receive CAN messages, have to setup read after periodic messages
        self.read = Hawkshead.Read(self)

        # Write the periodic CAN messages at a specified time interval based on configuration
        self.write_periodic_message(self._periodic_can_messages, self._config["can"]["period"])

        # Update CAN values on every message received by the listener
        self._listener.on_message_received = self.read.update_can_values

    def __del__(self):
        # docstring
        """
        Clean up the Hawkshead controller by turning off torque demand and disabling its output,
        shutting down the bus, and deleting the instance.


        Parameters
        ----------

        Returns
        -------
        None."""

        try:
            # Set the torque demand to 0
            self.write.torque_demand(0.0)
            time.sleep(0.5)

            # Disable the bridge and shutdown the bus
            self.write.bridge_enable(False)
            self._bus.shutdown()

        except Exception as e:
            print(e)

    class Write:
        """
        Class for writing to the Bowfell inverter.

        Attributes
        ----------
        _controller : Controller
            The controller object.
        _message : dict
            The message dictionary.
        _signal : dict
            The signal dictionary.
        _parameters : dict
            The parameters dictionary.
        _reset : dict
            The reset dictionary.
        _limits : dict
            The limits dictionary.

        Methods
        -------
        reset_can_signals()
            Reset the can signals.
        torque_demand(torque_demand)
            Set the torque demand.
        torque_demand_fusa(torque_demand)
            Set the torque demand.
        speed(speed)
            Set the speed.
        """

        def __init__(self, controller):
            self._controller = controller
            self._message = controller._message
            self._signal = controller._signal
            self._parameters = controller._parameters
            self._init_vals = controller._init_vals
            self._limits = controller._limits

        def initialise_periodic_messages(self):
            """
            Initialise the periodic CAN messages, using the configuration.

            Returns

            -------
            periodic_can_messages : dict
                The periodic CAN messages.

            """
            periodic_can_messages = {
                # TX MESSAGES
                self._message["torque"]["qm"]["demand"]: {
                    # VC_Trq
                    self._signal["torque"]["qm"]["demand"]: self._init_vals["torque"]["qm"]["demand"],
                    self._signal["torque"]["qm"]["limits"]["drive"]: self._init_vals["torque"]["qm"]["limits"]["drive"],
                    self._signal["torque"]["qm"]["limits"]["brake"]: self._init_vals["torque"]["qm"]["limits"]["brake"],
                },
                self._message["torque"]["fusa"]["demand"]: {
                    # VC_Trq_FuSa
                    self._signal["torque"]["fusa"]["demand"]: self._init_vals["torque"]["qm"]["demand"],
                    self._signal["torque"]["fusa"]["limits"]["drive"]: self._init_vals["torque"]["qm"]["limits"]["drive"],
                    self._signal["torque"]["fusa"]["limits"]["brake"]: self._init_vals["torque"]["qm"]["limits"]["brake"],
                },
                self._message["dc_link"]["limits"]["current"]["charge"]: {
                    # VC_Bat
                    self._signal["dc_link"]["limits"]["current"]["charge"]: self._init_vals["dc_link"]["limits"]["current"]["charge"],
                    self._signal["dc_link"]["limits"]["current"]["discharge"]: self._init_vals["dc_link"]["limits"]["current"]["discharge"],
                    self._signal["dc_link"]["limits"]["voltage"]["max"]: self._init_vals["dc_link"]["limits"]["voltage"]["max"],
                    self._signal["dc_link"]["limits"]["voltage"]["min"]: self._init_vals["dc_link"]["limits"]["voltage"]["min"],
                },
                self._message["crash"]["active_discharge"]: {
                    # VC_CrashInd
                    self._signal["crash"]["active_discharge"]: self._init_vals["crash"]["active_discharge"],
                },
                self._message["bridge"]["enable"]: {
                    # VC_SpdCmd
                    self._signal["bridge"]["enable"]: self._init_vals["bridge"]["enable"],
                    self._signal["speed"]["limits"]["forward"]: self._init_vals["speed"]["limits"]["forward"],
                    self._signal["speed"]["limits"]["reverse"]: self._init_vals["speed"]["limits"]["reverse"],
                },
                self._message["speed"]["transmission"]: {
                    # VC_TransSpd
                    self._signal["speed"]["transmission"]: self._init_vals["speed"]["transmission"],
                },
                self._message["status"]["contactor"]: {
                    # PDU_Status_4
                    self._signal["status"]["contactor"]: self._init_vals["status"]["contactor"],
                },
                # RX MESSAGES
                self._message["dc_link"]["feedback"]["voltage"]: {
                    # MC_Sts
                    self._signal["dc_link"]["feedback"]["voltage"]: self._init_vals["dc_link"]["feedback"]["voltage"],
                    self._signal["dc_link"]["feedback"]["current"]: self._init_vals["dc_link"]["feedback"]["current"],
                    self._signal["temperature"]["motor"]["motor"]: self._init_vals["temperature"]["motor"]["motor"],
                },
                self._message["temperature"]["feedback"]["inverter"]["inverter"]: {
                    # MC_Sts_2
                    self._signal["temperature"]["inverter"]["inverter"]: self._init_vals["temperature"]["inverter"]["inverter"],
                    self._signal["bridge"]["state"]: self._init_vals["bridge"]["state"],
                    self._signal["status"]["fault"]: self._init_vals["status"]["fault"],
                    self._signal["status"]["kl_30"]: self._init_vals["status"]["kl_30"],
                },
                self._message["torque"]["qm"]["feedback"]: {
                    # MC_Trq
                    self._signal["torque"]["qm"]["feedback"]: self._init_vals["torque"]["qm"]["feedback"],
                    self._signal["torque"]["qm"]["max_drive_available"]: self._init_vals["torque"]["qm"]["max_drive_available"],
                    self._signal["torque"]["qm"]["max_brake_available"]: self._init_vals["torque"]["qm"]["max_brake_available"],
                },
                self._message["speed"]["feedback"]: {
                    # MC_Vel
                    self._signal["speed"]["feedback"]: self._init_vals["speed"]["feedback"],
                },
                self._message["motor_control"]["feedback"]["ud"]: {
                    # MC_V2_Debug_1
                    self._signal["motor_control"]["feedback"]["ud"]: self._init_vals["motor_control"]["feedback"]["ud"],
                    self._signal["motor_control"]["feedback"]["uq"]: self._init_vals["motor_control"]["feedback"]["uq"],
                    self._signal["motor_control"]["feedback"]["id"]: self._init_vals["motor_control"]["feedback"]["id"],
                    self._signal["motor_control"]["feedback"]["iq"]: self._init_vals["motor_control"]["feedback"]["iq"],
                    self._signal["motor_control"]["feedback"]["mod_index"]: self._init_vals["motor_control"]["feedback"]["mod_index"],
                    self._signal["motor_control"]["feedback"]["switching_frequency"]: self._init_vals["motor_control"]["feedback"]["switching_frequency"],
                },
                self._message["status"]["alert_id"]: {
                    # MC_Debug_3_Status
                    self._signal["status"]["alert_id"]: self._init_vals["status"]["alert_id"],
                    self._signal["status"]["block_id"]: self._init_vals["status"]["block_id"],
                    self._signal["status"]["status"]: self._init_vals["status"]["status"],
                    self._signal["status"]["placeholder"]: self._init_vals["status"]["placeholder"],
                },
                self._message["fault"]["encoder"]: {
                    # MC_Sts_3
                    self._signal["fault"]["encoder"]: self._init_vals["fault"]["encoder"],
                    self._signal["fault"]["motor"]["temperature"]: self._init_vals["motor"]["temperature"],
                    self._signal["fault"]["motor"]["speed"]: self._init_vals["motor"]["speed"],
                    self._signal["fault"]["inverter"]["current"]["over_charge"]: self._init_vals["fault"]["inverter"]["current"]["over_charge"],
                    self._signal["fault"]["inverter"]["voltage"]["over_voltage"]: self._init_vals["fault"]["inverter"]["voltage"]["over_voltage"],
                    self._signal["fault"]["torque"]["command"]: self._init_vals["fault"]["torque"]["command"],
                    self._signal["fault"]["torque"]["estimation"]: self._init_vals["fault"]["torque"]["estimation"],
                    self._signal["fault"]["torque"]["monitor"]: self._init_vals["fault"]["torque"]["monitor"],
                    self._signal["fault"]["status"]["hv_request"]: self._init_vals["fault"]["status"]["hv_request"],
                    self._signal["fault"]["status"]["amber"]: self._init_vals["fault"]["status"]["amber"],
                    self._signal["fault"]["status"]["stop"]: self._init_vals["fault"]["status"]["stop"],
                },
            }

            return periodic_can_messages

        def reset_can_signals(self):
            """
            Reset the can signals.

            Returns
            -------
            None.

            """
            self._controller.write_signal(self._message["torque"]["qm"]["demand"], self._signal["torque"]["qm"]["demand"], self._reset["torque"]["qm"]["demand"])
            self._controller.write_signal(self._message["torque"]["fusa"]["demand"], self._signal["torque"]["fusa"]["demand"], self._reset["torque"]["fusa"]["demand"])
            self._controller.write_signal(self._message["torque"]["qm"]["limits"]["drive"], self._signal["torque"]["qm"]["limits"]["drive"], self._reset["torque"]["qm"]["limits"]["drive"])
            self._controller.write_signal(self._message["torque"]["fusa"]["limits"]["drive"], self._signal["torque"]["fusa"]["limits"]["drive"], self._reset["torque"]["fusa"]["limits"]["drive"])
            self._controller.write_signal(self._message["torque"]["qm"]["limits"]["brake"], self._signal["torque"]["qm"]["limits"]["brake"], self._reset["torque"]["qm"]["limits"]["brake"])
            self._controller.write_signal(self._message["torque"]["fusa"]["limits"]["brake"], self._signal["torque"]["fusa"]["limits"]["brake"], self._reset["torque"]["fusa"]["limits"]["brake"])
            self._controller.write_signal(self._message["speed"]["limits"]["forward"], self._signal["speed"]["limits"]["forward"], self._reset["speed"]["limits"]["forward"])
            self._controller.write_signal(self._message["speed"]["limits"]["reverse"], self._signal["speed"]["limits"]["reverse"], self._reset["speed"]["limits"]["reverse"])
            self._controller.write_signal(self._message["dc_link"]["limits"]["voltage"]["max"], self._signal["dc_link"]["limits"]["voltage"]["max"], self._reset["dc_link"]["limits"]["voltage"]["max"])
            self._controller.write_signal(self._message["dc_link"]["limits"]["voltage"]["min"], self._signal["dc_link"]["limits"]["voltage"]["min"], self._reset["dc_link"]["limits"]["voltage"]["min"])
            self._controller.write_signal(
                self._message["dc_link"]["limits"]["current"]["charge"], self._signal["dc_link"]["limits"]["current"]["charge"], self._reset["dc_link"]["limits"]["current"]["charge"]
            )
            self._controller.write_signal(
                self._message["dc_link"]["limits"]["current"]["discharge"], self._signal["dc_link"]["limits"]["current"]["discharge"], self._reset["dc_link"]["limits"]["current"]["discharge"]
            )
            self._controller.write_signal(self._message["status"]["contactor"], self._signal["status"]["contactor"], self._reset["status"]["contactor"])
            self._controller.write_signal(self._message["speed"]["transmission"], self._signal["speed"]["transmission"], self._reset["speed"]["transmission"])
            self._controller.write_signal(self._message["crash"]["active_discharge"], self._signal["crash"]["active_discharge"], self._reset["crash"]["active_discharge"])
            self._controller.write_signal(self._message["bridge"]["enable"], self._signal["bridge"]["enable"], self._reset["bridge"]["enable"])

        def bridge_enable(self, enabled: bool):
            """
            Enable or disable the bridge.

            Parameters
            ----------
            enable : bool
                True to enable the bridge, False to disable.

            Returns
            -------
            bool
                True if the bridge state is the expected one, False otherwise.

            """

            if enabled:
                self._controller.write_signal(self._message["bridge"]["enable"], self._signal["bridge"]["enable"], self._parameters["bridge"]["enable"])
                time.sleep(2)
                success = self._controller.get_bridge_state() == BridgeState.TorqueEnabled
            else:
                self._controller.write_signal(self._message["bridge"]["enable"], self._signal["bridge"]["enable"], self._parameters["bridge"]["disable"])
                time.sleep(0.5)
                success = self._controller.get_bridge_state() != BridgeState.TorqueEnabled

            return success

        def crash_event(self, crashed):
            """
            Set the crash event.

            Parameters
            ----------
            crashed
                True if the crash event is active, False otherwise.

            Returns
            -------
            None.
            """

            return self._controller.write_signal(self._message["crash"]["active_discharge"], self._signal["crash"]["active_discharge"], self._parameters["crash_value"] if crashed else 0)

        def current_limits(self, discharge_limit: float, charge_limit: float):
            """
            Set the current limits.

            Parameters
            ----------
            discharge_limit : float
                The discharge limit.
            charge_limit : float
                The charge limit.

            Returns
            -------
            None.
            """

            discharge_limit = max(min(discharge_limit, self._limits["dc_link"]["current"]["discharge"]), -self._limits["dc_link"]["current"]["discharge"])
            charge_limit = max(min(charge_limit, self._limits["dc_link"]["current"]["charge"]), -self._limits["dc_link"]["current"]["charge"])
            return self._controller.write_signal(
                self._message["dc_link"]["limits"]["current"]["discharge"], self._signal["dc_link"]["limits"]["current"]["discharge"], discharge_limit
            ) and self._controller.write_signal(self._message["dc_link"]["limits"]["current"]["charge"], self._signal["dc_link"]["limit"]["current"]["charge"], charge_limit)

        def speed_limits(self, forward_limit: float, reverse_limit: float):
            """
            Set the speed limits.

            Parameters
            ----------
            forward_limit : float
                The forward limit.
            reverse_limit : float
                The reverse limit.

            Returns
            -------
            None.
            """

            forward_limit = max(min(forward_limit, self._limits["speed"]["forward"]), -self._limits["speed"]["forward"])
            reverse_limit = max(min(reverse_limit, self._limits["speed"]["reverse"]), -self._limits["speed"]["reverse"])
            return self._controller.write_signal(self._message["speed"]["limits"]["forward"], self._signal["speed"]["limits"]["forward"], forward_limit) and self._controller.write_signal(
                self._message["speed"]["limits"]["reverse"], self._signal["speed"]["limits"]["reverse"], reverse_limit
            )

        def torque_demand(self, torque_demand: float):
            """
            Set the torque demand.

            Parameters
            ----------
            torque_demand : float
                The torque demand.

            Returns
            -------
            None.
            """

            torque_demand = max(min(torque_demand, self._limits["torque"]["drive"]), self._limits["torque"]["brake"])
            torque_drive_limit = min(self._limits["torque"]["drive"], abs(torque_demand) * 1.2)
            torque_brake_limit = min(self._limits["torque"]["drive"], abs(torque_demand) * 1.2)
            return (
                self._controller.write_signal(self._message["torque"]["qm"]["limits"]["drive"], self._signal["torque"]["qm"]["limits"]["drive"], torque_drive_limit)
                and self._controller.write_signal(self._message["torque"]["fusa"]["limits"]["drive"], self._signal["torque"]["fusa"]["limits"]["drive"], torque_drive_limit)
                and self._controller.write_signal(self._message["torque"]["qm"]["limits"]["brake"], self._signal["torque"]["qm"]["limits"]["brake"], torque_brake_limit)
                and self._controller.write_signal(self._message["torque"]["fusa"]["limits"]["brake"], self._signal["torque"]["fusa"]["limits"]["brake"], torque_brake_limit)
                and self._controller.write_signal(self._message["torque"]["qm"]["demand"], self._signal["torque"]["qm"]["demand"], torque_demand)
                and self._controller.write_signal(self._message["torque"]["fusa"]["demand"], self._signal["torque"]["fusa"]["demand"], torque_demand)
            )

    class Read:
        """
        Read class for the Gen5 controller.

        Attributes
        ----------
        _controller : Gen5
            The Gen5 controller.
        _message : dict
            The message dictionary.
        _signal : dict
            The signal dictionary.
        _parameters : dict
            The parameters dictionary.
        _limits : dict
            The limits dictionary.

        Methods
        -------
        bridge_state() -> int
            Get the bridge state.
        alert_id()
            Get the alert ID.
        block_id()
            Get the block ID.
        torque_demand() -> float
            Get the torque demand.
        torque_demand_fusa() -> float
            Get the torque demand, in the saftey layer.
        torque_demand_actual() -> float
            Get the actual torque demand, this is the torque demand after derating (if any applied).
        speed() -> float
            Get the speed.
        dc_link_voltage() -> float
            Get the DC link voltage.
        dc_link_current() -> float
            Get the DC link current.
        inverter_temperature() -> float
            Get the inverter temperature.
        motor_temperature() -> float
            Get the motor temperature.
        id_feedback() -> float
            Get the D-axis current feedback.
        iq_feedback() -> float
            Get the Q-axis current feedback.
        ud_feedback() -> float
            Get the D-axis voltage feedback.
        uq_feedback() -> float
            Get the Q-axis voltage feedback.
        mod_index() -> float
            Get the modulation index.
        switching_frequency() -> float
            Get the switching frequency.
        """

        def __init__(self, controller):
            """
            Initialize the Read class.

            Parameters
            ----------
            controller : Gen5
                The Gen5 controller.

            Returns
            -------
                None.
            """
            self._controller = controller
            self._message = controller._message
            self._signal = controller._signal
            self._parameters = controller._parameters
            self._limits = controller._limits
            self._periodic_can_messages = self._controller._periodic_can_messages

        def update_can_values(self, message_received):
            """
            Update the CAN values.

            Parameters
            ----------
            message_received : can.Message
                The CAN message received.
            """
            self._controller.read_can_values(self._periodic_can_messages, message_received)

        def bridge_state(self) -> int:
            """
            Get the bridge state.

            Returns
            -------
            int
                The bridge state.

            """
            bridge_state = BridgeState.HighImpedance

            value = self._controller.read_periodic_signal(self._periodic_can_messages, self._message["bridge"]["state"], self._signal["bridge"]["state"])

            if value == 3:
                bridge_state = BridgeState.TorqueEnabled
            elif value == 2:
                bridge_state = BridgeState.ActiveDischarge
            elif value == 1:
                bridge_state = BridgeState.ActiveShortCircuit

            return bridge_state

        def alert_id(self):
            """
            Get the alert ID.

            Returns
            -------
            hex(int)
                The alert ID.
            """
            return hex(int(self._controller.read_periodic_signal(self._periodic_can_messages, self._message["status"]["alert_id"], self._signal["status"]["alert_id"])))

        def block_id(self):
            """
            Get the block ID.

            Returns
            -------
            hex(int)
                The block ID.
            """

            return hex(int(self._controller.read_periodic_signal(self._periodic_can_messages, self._message["status"]["block_id"], self._signal["status"]["block_id"])))

        def torque_demand(self) -> float:
            """
            Get the torque demand.

            Returns
            -------
            float
                The torque demand.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["torque"]["qm"]["demand"], self._signal["torque"]["qm"]["demand"])

        def torque_demand_fusa(self) -> float:
            """
            Get the torque demand.

            Returns
            -------
            float
                The torque demand.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["torque"]["fusa"]["demand"], self._signal["torque"]["fusa"]["demand"])

        def torque_demand_actual(self) -> float:
            """
            Get the actual torque demand, this is the torque demand after derating (if any applied).

            Returns
            -------
            float
                The derated torque demand.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["torque"]["qm"]["feedback"]["actual"], self._signal["torque"]["qm"]["feedback"]["actual"])

        def speed(self) -> float:
            """
            Get the speed.

            Returns
            -------
            float
                The speed.
            """

            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["speed"]["feedback"], self._signal["speed"]["feedback"])

        def dc_link_voltage(self) -> float:
            """
            Get the DC link voltage.

            Returns
            -------
            float
                The DC link voltage.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["dc_link"]["feedback"]["voltage"], self._signal["dc_link"]["feedback"]["voltage"])

        def dc_link_current(self) -> float:
            """
            Get the DC link current.

            Returns
            -------
            float
                The DC link current.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["dc_link"]["feedback"]["current"], self._signal["dc_link"]["feedback"]["current"])

        def inverter_temperature(self) -> float:
            """
            Get the inverter temperature.

            Returns
            -------
            float
                The inverter temperature.
            """
            return self._controller.read_periodic_signal(
                self._periodic_can_messages, self._message["temperature"]["feedback"]["inverter"]["inverter"], self._signal["temperature"]["feedback"]["inverter"]["inverter"]
            )

        def motor_temperature(self) -> float:
            """
            Get the motor temperature.

            Returns
            -------
            float
                The motor temperature.
            """
            return self._controller.read_periodic_signal(
                self._periodic_can_messages, self._message["temperature"]["feedback"]["motor"]["motor"], self._signal["temperature"]["feedback"]["motor"]["motor"]
            )

        def id_feedback(self) -> float:
            """
            Get the Id current (Stationary frame current, D-axis) feedback.

            Returns
            -------
            float
                The Id current (Stationary frame current, D-axis) current feedback.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["motor_control"]["feedback"]["id"], self._signal["motor_control"]["feedback"]["id"])

        def iq_feedback(self) -> float:
            """
            Get the Iq current (Stationary frame current, Q-axis) feedback.

            Returns
            -------
            float
                The Iq current (Stationary frame current, Q-axis) current feedback.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["motor_control"]["feedback"]["iq"], self._signal["motor_control"]["feedback"]["iq"])

        def ud_feedback(self) -> float:
            """
            Get the Ud voltage (Stationary frame voltage, D-axis) feedback.

            Returns
            -------
            float
                The Ud voltage (Stationary frame voltage, D-axis) voltage feedback.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["motor_control"]["feedback"]["ud"], self._signal["motor_control"]["feedback"]["ud"])

        def uq_feedback(self) -> float:
            """
            Get the Uq voltage (Stationary frame voltage, Q-axis) feedback.

            Returns
            -------
            float
                The Uq voltage (Stationary frame voltage, Q-axis) voltage feedback.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["motor_control"]["feedback"]["uq"], self._signal["motor_control"]["feedback"]["uq"])

        def mod_index(self) -> float:
            """
            Get the modulation index.

            Returns
            -------
            float
                The modulation index.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["motor_control"]["mod_index"], self._signal["motor_control"]["mod_index"])

        def switching_frequency(self) -> float:
            """
            Get the switching frequency.

            Returns
            -------
            float
                The switching frequency.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["motor_control"]["switching_frequency"], self._signal["motor_control"]["switching_frequency"])


class Oxford(CanDevice):
    def __init__(self, config):
        """
        Initialize the Oxford controller.

        Parameters
        ----------
        config : dict
            The configuration dictionary.

        Returns
        -------
        None.

        """
        super().__init__()

        self._config = config
        self._parameters = self._config["parameters"]
        self._limits = self._parameters["limits"]
        self._message = self._config["can"]["messages"]
        self._signal = self._config["can"]["signals"]
        self._init_vals = self._config["can"]["initial_values"]
        self._periodic_can_messages: Union[dict, None] = None

        # Set up the CAN bus using the provided configuration
        self._bus = self.setup_can_bus(self._config)

        # Load the CAN message database
        self._db = self.load_database(self._config["can"]["database"])

        # Set the listener to receive CAN messages
        self._listener = self.setup_listener()

        # Set up the notifier to notify the listener when a message is received
        self._notifier = self.setup_notifier(self._bus, [self._listener])

        # Create a write object for the Oxford class to send CAN messages
        self.write = Oxford.Write(self)

        # Initialize periodic CAN messages
        self._periodic_can_messages = self.write.initialise_periodic_messages()

        # Create a read object for the Oxford class to receive CAN messages, have to setup read after periodic messages
        self.read = Oxford.Read(self)

        # Write the periodic CAN messages at a specified time interval based on configuration
        self.write_periodic_message(self._periodic_can_messages, self._config["can"]["period"])

        # Update CAN values on every message received by the listener
        self._listener.on_message_received = self.read.update_can_values

    def __del__(self):
        # docstring
        """
        Clean up the Oxford controller by turning off torque demand and disabling its output,
        shutting down the bus, and deleting the instance.


        Parameters
        ----------

        Returns
        -------
        None."""

        try:
            # Set the torque demand to 0
            self.write.torque_demand(0.0)
            time.sleep(0.5)

            # Disable the bridge and shutdown the bus
            self.write.bridge_enable(False)
            self._bus.shutdown()

        except Exception as e:
            print(e)

    class Write:
        def __init__(self, controller):
            self._controller = controller
            self._message = controller._message
            self._signal = controller._signal
            self._parameters = controller._parameters
            self._init_vals = controller._init_vals
            self._limits = controller._limits

        def initialise_periodic_messages(self):
            """
            Initialise the periodic CAN messages, using the configuration.

            Returns

            -------
            periodic_can_messages : dict
                The periodic CAN messages.

            """

            periodic_can_messages = {
                # HSU_MCU1_Command
                self._message["speed"]["limits"]["forward"]: {
                    # limits
                    self._signal["speed"]["limits"]["forward"]: self._init_vals["speed"]["limits"]["forward"],
                    self._signal["speed"]["limits"]["reverse"]: self._signal["speed"]["limits"]["reverse"],
                    self._signal["torque"]["qm"]["limits"]["drive"]: self._signal["torque"]["qm"]["limits"]["drive"],
                    self._signal["torque"]["qm"]["limits"]["brake"]: self._signal["torque"]["qm"]["limits"]["brake"],
                },
                self._message["speed"]["limits"]["forward"]: {
                    # limits
                    self._signal["speed"]["limits"]["forward"]: self._init_vals["speed"]["limits"]["forward"],
                    self._signal["speed"]["limits"]["reverse"]: self._signal["speed"]["limits"]["reverse"],
                    self._signal["torque"]["qm"]["limits"]["drive"]: self._signal["torque"]["qm"]["limits"]["drive"],
                    self._signal["torque"]["qm"]["limits"]["brake"]: self._signal["torque"]["qm"]["limits"]["brake"],
                },
            }

            return periodic_can_messages

        def reset_can_signals(self):
            """
            Reset the can signals.

            Returns
            -------
            None.

            """
            self._controller.write_signal(self._message["torque"]["qm"]["demand"], self._signal["torque"]["qm"]["demand"], self._reset["torque"]["qm"]["demand"])
            self._controller.write_signal(self._message["torque"]["fusa"]["demand"], self._signal["torque"]["fusa"]["demand"], self._reset["torque"]["fusa"]["demand"])
            self._controller.write_signal(self._message["torque"]["qm"]["limits"]["drive"], self._signal["torque"]["qm"]["limits"]["drive"], self._reset["torque"]["qm"]["limits"]["drive"])
            self._controller.write_signal(self._message["torque"]["fusa"]["limits"]["drive"], self._signal["torque"]["fusa"]["limits"]["drive"], self._reset["torque"]["fusa"]["limits"]["drive"])
            self._controller.write_signal(self._message["torque"]["qm"]["limits"]["brake"], self._signal["torque"]["qm"]["limits"]["brake"], self._reset["torque"]["qm"]["limits"]["brake"])
            self._controller.write_signal(self._message["torque"]["fusa"]["limits"]["brake"], self._signal["torque"]["fusa"]["limits"]["brake"], self._reset["torque"]["fusa"]["limits"]["brake"])
            self._controller.write_signal(self._message["speed"]["limits"]["forward"], self._signal["speed"]["limits"]["forward"], self._reset["speed"]["limits"]["forward"])
            self._controller.write_signal(self._message["speed"]["limits"]["reverse"], self._signal["speed"]["limits"]["reverse"], self._reset["speed"]["limits"]["reverse"])
            self._controller.write_signal(self._message["dc_link"]["limits"]["voltage"]["max"], self._signal["dc_link"]["limits"]["voltage"]["max"], self._reset["dc_link"]["limits"]["voltage"]["max"])
            self._controller.write_signal(self._message["dc_link"]["limits"]["voltage"]["min"], self._signal["dc_link"]["limits"]["voltage"]["min"], self._reset["dc_link"]["limits"]["voltage"]["min"])
            self._controller.write_signal(
                self._message["dc_link"]["limits"]["current"]["charge"], self._signal["dc_link"]["limits"]["current"]["charge"], self._reset["dc_link"]["limits"]["current"]["charge"]
            )
            self._controller.write_signal(
                self._message["dc_link"]["limits"]["current"]["discharge"], self._signal["dc_link"]["limits"]["current"]["discharge"], self._reset["dc_link"]["limits"]["current"]["discharge"]
            )
            self._controller.write_signal(self._message["status"]["contactor"], self._signal["status"]["contactor"], self._reset["status"]["contactor"])
            self._controller.write_signal(self._message["speed"]["transmission"], self._signal["speed"]["transmission"], self._reset["speed"]["transmission"])
            self._controller.write_signal(self._message["crash"]["active_discharge"], self._signal["crash"]["active_discharge"], self._reset["crash"]["active_discharge"])
            self._controller.write_signal(self._message["bridge"]["enable"], self._signal["bridge"]["enable"], self._reset["bridge"]["enable"])

        def bridge_enable(self, enabled: bool):
            """
            Enable or disable the bridge.

            Parameters
            ----------
            enable : bool
                True to enable the bridge, False to disable.

            Returns
            -------
            bool
                True if the bridge state is the expected one, False otherwise.

            """

            if enabled:
                self._controller.write_signal(self._message["bridge"]["enable"], self._signal["bridge"]["enable"], self._parameters["bridge"]["enable"])
                time.sleep(2)
                success = self._controller.get_bridge_state() == BridgeState.TorqueEnabled
            else:
                self._controller.write_signal(self._message["bridge"]["enable"], self._signal["bridge"]["enable"], self._parameters["bridge"]["disable"])
                time.sleep(0.5)
                success = self._controller.get_bridge_state() != BridgeState.TorqueEnabled

            return success

        def crash_event(self, crashed):
            """
            Set the crash event.

            Parameters
            ----------
            crashed
                True if the crash event is active, False otherwise.

            Returns
            -------
            None.
            """

            return self._controller.write_signal(self._message["crash"]["active_discharge"], self._signal["crash"]["active_discharge"], self._parameters["crash_value"] if crashed else 0)

        def current_limits(self, discharge_limit: float, charge_limit: float):
            """
            Set the current limits.

            Parameters
            ----------
            discharge_limit : float
                The discharge limit.
            charge_limit : float
                The charge limit.

            Returns
            -------
            None.
            """

            discharge_limit = max(min(discharge_limit, self._limits["dc_link"]["current"]["discharge"]), -self._limits["dc_link"]["current"]["discharge"])
            charge_limit = max(min(charge_limit, self._limits["dc_link"]["current"]["charge"]), -self._limits["dc_link"]["current"]["charge"])
            return self._controller.write_signal(
                self._message["dc_link"]["limits"]["current"]["discharge"], self._signal["dc_link"]["limits"]["current"]["discharge"], discharge_limit
            ) and self._controller.write_signal(self._message["dc_link"]["limits"]["current"]["charge"], self._signal["dc_link"]["limit"]["current"]["charge"], charge_limit)

        def speed_limits(self, forward_limit: float, reverse_limit: float):
            """
            Set the speed limits.

            Parameters
            ----------
            forward_limit : float
                The forward limit.
            reverse_limit : float
                The reverse limit.

            Returns
            -------
            None.
            """

            forward_limit = max(min(forward_limit, self._limits["speed"]["forward"]), -self._limits["speed"]["forward"])
            reverse_limit = max(min(reverse_limit, self._limits["speed"]["reverse"]), -self._limits["speed"]["reverse"])
            return self._controller.write_signal(self._message["speed"]["limits"]["forward"], self._signal["speed"]["limits"]["forward"], forward_limit) and self._controller.write_signal(
                self._message["speed"]["limits"]["reverse"], self._signal["speed"]["limits"]["reverse"], reverse_limit
            )

        def torque_demand(self, torque_demand: float):
            """
            Set the torque demand.

            Parameters
            ----------
            torque_demand : float
                The torque demand.

            Returns
            -------
            None.
            """

            torque_demand = max(min(torque_demand, self._limits["torque"]["drive"]), self._limits["torque"]["brake"])
            torque_drive_limit = min(self._limits["torque"]["drive"], abs(torque_demand) * 1.2)
            torque_brake_limit = min(self._limits["torque"]["drive"], abs(torque_demand) * 1.2)
            return (
                self._controller.write_signal(self._message["torque"]["qm"]["limits"]["drive"], self._signal["torque"]["qm"]["limits"]["drive"], torque_drive_limit)
                and self._controller.write_signal(self._message["torque"]["fusa"]["limits"]["drive"], self._signal["torque"]["fusa"]["limits"]["drive"], torque_drive_limit)
                and self._controller.write_signal(self._message["torque"]["qm"]["limits"]["brake"], self._signal["torque"]["qm"]["limits"]["brake"], torque_brake_limit)
                and self._controller.write_signal(self._message["torque"]["fusa"]["limits"]["brake"], self._signal["torque"]["fusa"]["limits"]["brake"], torque_brake_limit)
                and self._controller.write_signal(self._message["torque"]["qm"]["demand"], self._signal["torque"]["qm"]["demand"], torque_demand)
                and self._controller.write_signal(self._message["torque"]["fusa"]["demand"], self._signal["torque"]["fusa"]["demand"], torque_demand)
            )

    class Read:
        """
        Read class for the Oxford controller.

        Attributes
        ----------
        _controller : Gen5
            The Gen5 controller.
        _message : dict
            The message dictionary.
        _signal : dict
            The signal dictionary.
        _parameters : dict
            The parameters dictionary.
        _limits : dict
            The limits dictionary.

        Methods
        -------
        bridge_state() -> int
            Get the bridge state.
        alert_id()
            Get the alert ID.
        block_id()
            Get the block ID.
        torque_demand() -> float
            Get the torque demand.
        torque_demand_fusa() -> float
            Get the torque demand, in the saftey layer.
        torque_demand_actual() -> float
            Get the actual torque demand, this is the torque demand after derating (if any applied).
        speed() -> float
            Get the speed.
        dc_link_voltage() -> float
            Get the DC link voltage.
        dc_link_current() -> float
            Get the DC link current.
        inverter_temperature() -> float
            Get the inverter temperature.
            [!] - Uses switch temperature for script compatibility.
        inverter_temperature_switch() -> float
            Get the inverter switch temperature.
        inverter_temperature_capacitor() -> float
            Get the inverter capacitor temperature.
        inverter_temperature_ac_link() -> float
            Get the inverter AC link temperature.
        inverter_temperature_dc_link() -> float
            Get the inverter DC link temperature.
        motor_temperature() -> float
            Get the motor temperature.
            [!] - Uses stator winding temperature for script compatibility.
        motor_temperature_stator_core() -> float
            Get the motor stator core temperature.
        motor_temperature_stator_winding() -> float
            Get the motor stator winding temperature.
        motor_temperature_cavity() -> float
            Get the motor temperature cavity.
        id_feedback() -> float
            Get the D-axis current feedback.
        iq_feedback() -> float
            Get the Q-axis current feedback.
        ud_feedback() -> float
            Get the D-axis voltage feedback.
        uq_feedback() -> float
            Get the Q-axis voltage feedback.
        mod_index() -> float
            Get the modulation index.
        switching_frequency() -> float
            Get the switching frequency.
        table_voltage_index() -> float
            Get the table voltage index.
        table_speed_index() -> float
            Get the table speed index.
        table_torque_index() -> float
            Get the table torque index.
        table_ke_index() -> float
            Get the table ke index.

        """

        def __init__(self, controller):
            self._controller = controller
            self._message = self._controller._message
            self._signal = self._controller._signal
            self._parameters = self._controller._parameters
            self._limits = self._controller._limits
            self._periodic_can_messages = self._controller._periodic_can_messages

        def update_can_values(self, message_received):
            """
            Update the CAN values.

            Parameters
            ----------
            message_received : can.Message
                The CAN message received.
            """
            self._controller.read_can_values(self._periodic_can_messages, message_received)

        def bridge_state(self) -> int:
            """
            Get the bridge state.

            Returns
            -------
            int
                The bridge state.

            """
            bridge_state = BridgeState.HighImpedance

            value = self._controller.read_periodic_signal(self._periodic_can_messages, self._message["bridge"]["state"], self._signal["bridge"]["state"])

            if value == 3:
                bridge_state = BridgeState.TorqueEnabled
            elif value == 2:
                bridge_state = BridgeState.ActiveDischarge
            elif value == 1:
                bridge_state = BridgeState.ActiveShortCircuit

            return bridge_state

        def alert_id(self):
            """
            Get the alert ID.

            Returns
            -------
            hex(int)
                The alert ID.
            """
            return hex(int(self._controller.read_periodic_signal(self._periodic_can_messages, self._message["status"]["alert_id"], self._signal["status"]["alert_id"])))

        def block_id(self):
            """
            Get the block ID.

            Returns
            -------
            hex(int)
                The block ID.
            """

            return hex(int(self._controller.read_periodic_signal(self._periodic_can_messages, self._message["status"]["block_id"], self._signal["status"]["block_id"])))

        def torque_demand_actual(self) -> float:
            """
            Get the actual torque demand, this is the torque demand after derating (if any applied).

            Returns
            -------
            float
                The derated torque demand.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["torque"]["qm"]["feedback"]["actual"], self._signal["torque"]["qm"]["feedback"]["actual"])

        def speed(self) -> float:
            """
            Get the speed.

            Returns
            -------
            float
                The speed.
            """

            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["speed"]["feedback"], self._signal["speed"]["feedback"])

        def dc_link_voltage(self) -> float:
            """
            Get the DC link voltage.

            Returns
            -------
            float
                The DC link voltage.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["dc_link"]["feedback"]["voltage"], self._signal["dc_link"]["feedback"]["voltage"])

        def dc_link_current(self) -> float:
            """
            Get the DC link current.

            Returns
            -------
            float
                The DC link current.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["dc_link"]["feedback"]["current"], self._signal["dc_link"]["feedback"]["current"])

        def inverter_temperature(self) -> float:
            """
            Get the inverter temperature.
            [!] - This is for compliance with scripts, it reads the same value as inverter_temperature_switch.

            Returns
            -------
            float
                The inverter temperature.
            """
            return self._controller.read_periodic_signal(
                self._periodic_can_messages, self._message["temperature"]["feedback"]["inverter"]["switch"], self._signal["temperature"]["feedback"]["inverter"]["switch"]
            )

        def inverter_temperature_switch(self) -> float:
            """
            Get the inverter switch temperature.

            Returns
            -------
            float
                The inverter switch temperature.
            """
            return self._controller.read_periodic_signal(
                self._periodic_can_messages, self._message["temperature"]["feedback"]["inverter"]["switch"], self._signal["temperature"]["feedback"]["inverter"]["switch"]
            )

        def inverter_temperature_capacitor(self) -> float:
            """
            Get the inverter capacitor temperature.

            Returns
            -------
            float
                The inverter capacitor temperature.
            """
            return self._controller.read_periodic_signal(
                self._periodic_can_messages, self._message["temperature"]["feedback"]["inverter"]["capacitor"], self._signal["temperature"]["feedback"]["inverter"]["capacitor"]
            )

        def inverter_temperature_ac_link(self) -> float:
            """
            Get the inverter AC link temperature.

            Returns
            -------
            float
                The inverter AC link temperature.
            """
            return self._controller.read_periodic_signal(
                self._periodic_can_messages, self._message["temperature"]["feedback"]["inverter"]["ac_link"], self._signal["temperature"]["feedback"]["inverter"]["ac_link"]
            )

        def inverter_temperature_dc_link(self) -> float:
            """
            Get the inverter DC link temperature.

            Returns
            -------
            float
                The inverter DC link temperature.
            """
            return self._controller.read_periodic_signal(
                self._periodic_can_messages, self._message["temperature"]["feedback"]["inverter"]["dc_link"], self._signal["temperature"]["feedback"]["inverter"]["dc_link"]
            )

        def motor_temperature_stator_core(self) -> float:
            """
            Get the motor stator core temperature.

            Returns
            -------
            float
                The motor stator core temperature.
            """
            return self._controller.read_periodic_signal(
                self._periodic_can_messages, self._message["temperature"]["feedback"]["motor"]["core"], self._signal["temperature"]["feedback"]["motor"]["core"]
            )

        def motor_temperature_stator_winding(self) -> float:
            """
            Get the motor stator winding temperature.

            Returns
            -------
            float
                The motor stator winding temperature.
            """
            return self._controller.read_periodic_signal(
                self._periodic_can_messages, self._message["temperature"]["feedback"]["motor"]["winding"], self._signal["temperature"]["feedback"]["motor"]["winding"]
            )

        def motor_temperature_cavity(self) -> float:
            """
            Get the motor cavity temperature.

            Returns
            -------
            float
                The motor cavity temperature.
            """
            return self._controller.read_periodic_signal(
                self._periodic_can_messages, self._message["temperature"]["feedback"]["motor"]["cavity"], self._signal["temperature"]["feedback"]["motor"]["cavity"]
            )

        def motor_temperature(self) -> float:
            """
            Get the motor temperature.
            [!] - This is for compliance with scripts, this takes the same value as motor_temperature_winding.

            Returns
            -------
            float
                The motor temperature.
            """
            return self._controller.read_periodic_signal(
                self._periodic_can_messages, self._message["temperature"]["feedback"]["motor"]["winding"], self._signal["temperature"]["feedback"]["motor"]["winding"]
            )

        def id_feedback(self) -> float:
            """
            Get the Id current (Stationary frame current, D-axis) feedback.

            Returns
            -------
            float
                The Id current (Stationary frame current, D-axis) current feedback.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["motor_control"]["feedback"]["id"], self._signal["motor_control"]["feedback"]["id"])

        def iq_feedback(self) -> float:
            """
            Get the Iq current (Stationary frame current, Q-axis) feedback.

            Returns
            -------
            float
                The Iq current (Stationary frame current, Q-axis) current feedback.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["motor_control"]["feedback"]["iq"], self._signal["motor_control"]["feedback"]["iq"])

        def ud_feedback(self) -> float:
            """
            Get the Ud voltage (Stationary frame voltage, D-axis) feedback.

            Returns
            -------
            float
                The Ud voltage (Stationary frame voltage, D-axis) voltage feedback.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["motor_control"]["feedback"]["ud"], self._signal["motor_control"]["feedback"]["ud"])

        def uq_feedback(self) -> float:
            """
            Get the Uq voltage (Stationary frame voltage, Q-axis) feedback.

            Returns
            -------
            float
                The Uq voltage (Stationary frame voltage, Q-axis) voltage feedback.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["motor_control"]["feedback"]["uq"], self._signal["motor_control"]["feedback"]["uq"])

        def mod_index(self) -> float:
            """
            Get the modulation index.

            Returns
            -------
            float
                The modulation index.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["motor_control"]["mod_index"], self._signal["motor_control"]["mod_index"])

        def switching_frequency(self) -> float:
            """
            Get the switching frequency.

            Returns
            -------
            float
                The switching frequency.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["motor_control"]["switching_frequency"], self._signal["motor_control"]["switching_frequency"])

        def table_torque_index(self) -> float:
            """
            Get the table torque index.

            Returns
            -------
            float
                The table torque index.
            """
            return self._controller.read_periodic_signal(
                self._periodic_can_messages, self._message["motor_control"]["tables"]["index"]["torque"], self._signal["motor_control"]["tables"]["index"]["torque"]
            )

        def table_speed_index(self) -> float:
            """
            Get the table speed index.

            Returns
            -------
            float
                The table speed index.
            """
            return self._controller.read_periodic_signal(
                self._periodic_can_messages, self._message["motor_control"]["tables"]["index"]["speed"], self._signal["motor_control"]["tables"]["index"]["speed"]
            )

        def table_voltage_index(self) -> float:
            """
            Get the table voltage index.

            Returns
            -------
            float
                The table voltage index.
            """
            return self._controller.read_periodic_signal(
                self._periodic_can_messages, self._message["motor_control"]["tables"]["index"]["voltage"], self._signal["motor_control"]["tables"]["index"]["voltage"]
            )

        def table_ke_index(self) -> float:
            """
            Get the table ke index.

            Returns
            -------
            float
                The table ke index.
            """
            return self._controller.read_periodic_signal(self._periodic_can_messages, self._message["motor_control"]["tables"]["index"]["ke"], self._signal["motor_control"]["tables"]["index"]["ke"])
