import time

from program.src.constants import DEFAULT_PATHS
from program.src.equipment.modbus import ModBusDevice
from program.src.file_ops import read_yaml_file


class NidecDyno(ModBusDevice):
    """
    A class that represents a Dyno Inverter and provides methods to interact with it using Modbus communication.

    Attributes
    ----------
    DEFAULT_DYNO_CONFIG : dict
        The default configuration for the Dyno converter.
    """

    # Read the default YAML configuration file for the DCDC converter
    DEFAULT_DYNO_CONFIG = read_yaml_file(DEFAULT_PATHS["equipment"]["configs"]["controllers"]["nidec"])

    # constants
    RPM_SCALING = 10.0
    PERCENT_SCALING = 10.0

    def __init__(self, config=DEFAULT_DYNO_CONFIG):
        """
        Constructor for NidecDyno class.

        Args:
            host (str): IP address of the inverter.
            port (int): Port number of the inverter.
            unit_id (int): Unit ID of the inverter.
        """
        # Initialize the superclass CanDevice
        super().__init__()

        # Set the configuration attributes
        self._config = config

        # Set up the Modbus using the provided configuration
        self._client = self.setup_modbus(config)

        # Create read and write objects for the Nidec class to send Modbus messages
        self.read = NidecDyno.Read(self)
        self.write = NidecDyno.Write(self)

    def __del__(self):
        """
        Clean up the Dyno by sending 0 to it's speed demand, disabling its output once zero speed has been achieved,
        shutting down the modbus connection, and deleting the instance.

        Returns
        -------
        None
        """
        # TODO: GO THROUGH LOGIC AND DETERMINE IF SAFE
        try:
            # Turn off the output voltage and current limits
            self.write.speed_target(0)

            # While the speed is greater than 10rpm (Dyno side) wait until it is <= 10rpm, check every 0.1s
            while self.read.speed() > 10:
                time.sleep(0.1)

            # Once the speed is less than 10rpm, set the max speed limits to 100rpm (Dyno Side).
            self.write.speed_max(100)
            time.sleep(0.5)

            # Disable the output and shutdown the bus
            self.write.run(False)
            time.sleep(0.5)
            self.write.enable(False)
            self._client.close()

        except Exception as e:
            print(e)

    class Read:
        """
        Class for reading from the dyno.
        """

        def __init__(self, dyno):
            """
            Constructor for Read class.

            Args:
                parent (NidecDyno): The parent NidecDyno object.
            """
            self._dyno = dyno

        # TODO: get Ben D, to check the if value != 0 stuff is correct
        # Voltage
        def dc_bus_voltage(self) -> float:
            """
            Get the DC bus voltage.

            Returns:
                float: The DC bus voltage.
            """
            return self._dyno.read_parameter(5, 2, 2)

        # TODO: check if this a flaot / percentage
        # Current
        def symmetrical_current_limit(self) -> float:
            """
            Get the symmetrical current limit.

            Returns:
                float: The symmetrical current limit.
            """
            return self._dyno.read_parameter(4, 7, 2, NidecDyno.PERCENT_SCALING)

        def speed_max(self) -> float:
            """
            Get the maximum speed.

            Returns:
                float: The maximum speed.
            """
            return self._dyno.read_parameter(1, 6, 2, NidecDyno.RPM_SCALING)

        def speed_target(self) -> float:
            """
            Get the speed target.

            Returns:
                float: The speed target.
            """
            return self._dyno.read_parameter(1, 21, 2, NidecDyno.RPM_SCALING)

        def speed(self) -> float:
            """
            Get the current speed.

            Returns:
                float: The current speed.
            """
            return self._dyno.read_parameter(3, 2, 2, NidecDyno.RPM_SCALING)

        def direction(self) -> bool:
            """
            Get the direction.

            Returns:
                bool: The direction of speed relative to the dyno motor.
            """
            value = self._dyno.read_parameter(6, 33, 1)
            result = None
            if value is not None:
                result = True if value != 0 else False
            return result

        # Torque
        # TODO: check the doc against the nidec manual
        def drive_mode(self) -> bool:
            """
            Get the drive mode.

            Returns:
                bool: The drive mode.

            Args:
                value (bool): The drive mode.
                    true: Torque mode.
                    false: Speed mode.
            """
            return self._dyno.read_parameter(4, 11, 1)

        # Temperature
        # TODO: check the doc against the nidec manual
        def monitored_temperature_1(self) -> float:
            """
            Get the monitored temperature 1.

            Returns:
                float: The monitored temperature 1.

            Args:
                value (float): The monitored temperature 1.
            """
            return self._dyno.read_parameter(7, 4, 2)

        # TODO: check the doc against the nidec manual
        def monitored_temperature_2(self) -> float:
            """
            Get the monitored temperature 2.

            Returns:
                float: The monitored temperature 2.

                Args:
                    value (float): The monitored temperature 2.
            """
            return self._dyno.read_parameter(7, 5, 2)

        # TODO: check the doc against the nidec manual
        def monitored_temperature_3(self) -> float:
            """
            Get the monitored temperature 3.

            Returns:
                float: The monitored temperature 3.

                Args:
                    value (float): The monitored temperature 3.
            """
            return self._dyno.read_parameter(7, 6, 2)

        def inverter_temperature(self) -> float:
            """
            Get the inverter temperature.

            Returns:
                float: The inverter temperature.
            """
            return self._dyno.read_parameter(7, 34, 2)

        def enable(self) -> bool:
            """
            Get the status of the drive enable flag.

            Returns:
                bool: Drive enable status.
                    true: Drive enabled.
                    false: Drive disabled.
            """
            value = self._dyno.read_parameter(6, 15, 1)
            result = None
            if value is not None:
                result = True if value != 0 else False
            return result

        def run(self) -> bool:
            """
            Get the status of the drive run flag.

            Returns:
                bool: Drive run status.
                    true: Bridge enable.
                    false: Bridge disable.
            """
            value = self._dyno.read_parameter(6, 34, 1)
            result = None
            if value is not None:
                result = True if value != 0 else False
            return result

        # TODO: check the doc against the nidec manual
        def drive_healthy(self) -> bool:
            """
            Get the status of the drive healthy flag.

            Returns:
                bool: Drive healthy status.1010
                    true: Drive healthy.
                    false: Drive unhealthy.
            """
            return self._dyno.read_parameter(10, 1, 1)

        def motor_protection_accumulator(self) -> int:
            """
            Get the motor protection accumulator.

            Returns:
                int: The motor protection accumulator.
            """
            return self._dyno.read_parameter(4, 19, 2)

    class Write:
        """
        Class for writing to the dyno.
        """

        def __init__(self, dyno):
            """
            Constructor for Write class.

            Args:
                parent (NidecDyno): The parent NidecDyno object.
            """
            self._dyno = dyno

        # TODO: verifiy and edit if this a flaot / percentage
        def symmetrical_current_limit(self, value: int) -> bool:
            """
            Set the symmetrical current limit.

            Args:
                value (int): The symmetrical current limit.

            Returns:
                bool: True if successful.
            """
            return self._dyno.write_parameter(4, 7, 2, value, NidecDyno.PERCENT_SCALING)

        # TODO: value check against motor spec
        # Speed
        def speed_max(self, value: int) -> bool:
            """
            Set the maximum speed.

            Args:
                value (int): The maximum speed.

            Returns:
                bool: True if successful.
            """
            return self._dyno.write_parameter(1, 6, 2, value, NidecDyno.RPM_SCALING)

        # TODO: value check against motor spec and max speed if possible
        def speed_target(self, value: int) -> bool:
            """
            Set the speed target.

            Args:
                value (int): The speed target.

            Returns:
                bool: True if successful.
            """
            return self._dyno.write_parameter(1, 21, 2, value, NidecDyno.RPM_SCALING)

        def direction(self, value: bool) -> bool:
            """
            Set the direction.

            Args:
                value (bool): The direction of speed relative to the dyno motor.

            Returns:
                bool: True if successful.
            """
            return self._dyno.write_parameter(6, 33, 1, 1 if value else 0)

        def drive_mode(self, value: bool) -> bool:
            """
            Set the torque mode.

            Args:
                value (bool): The torque mode.
                    true: Torque mode.
                    false: Speed mode.
            """
            return self._dyno.write_parameter(4, 11, 1, 1 if value else 0)

        # Status
        def enable(self, value: bool) -> bool:
            """
            Set the status of the drive enable flag.

            Args:
                value (bool): The enable.
                    true: Drive enabled.
                    false: Drive disabled.

            Returns:
                bool: True if successful.
            """
            time.sleep(1)
            return self._dyno.write_parameter(6, 15, 1, 1 if value else 0)

        def run(self, value: bool) -> bool:
            """
            Set the status of the drive run flag.

            Args:
                value (bool): Drive run flag.
                    true: Set bridge enable.
                    false: Set bridge disable.

            Returns:
                bool: True if successful.
            """
            return self._dyno.write_parameter(6, 34, 1, 1 if value else 0)

        def external_trip(self, value: bool) -> bool:
            """
            Set the status of the external trip flag.

            Args:
                value (bool): External trip flag.
                    true: External trip.
                    false: No external trip.

            Returns:
                bool: True if successful.
            """
            return self._dyno.write_parameter(10, 32, 1, 1 if value else 0)
