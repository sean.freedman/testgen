import time


def bridge_control(node, control_word):
    """_summary_

    Args:
        node (_type_): _description_
        control_word (_type_): _description_

    Returns:
        _type_: _description_
    """
    
    if control_word == 0xF:
        enable_val = node.sdo[0x6040]
        enable_val.write(0x6)
        time.sleep(0.2)
        enable_val.write(0x7)
        time.sleep(0.2)
        enable_val.write(control_word)
        time.sleep(0.2)
    if control_word == 0x6:
        enable_val = node.sdo[0x6040]
        enable_val.write(control_word)
        time.sleep(0.2)
    bridge_status = node.sdo[0x6041]
    statusword = bridge_status.phys

    return statusword


def poke16(node, address, value, fixed_point_scale, offset: int = 0):
    """_summary_

    Args:
        node (_type_): _description_
        address (_type_): _description_
        value (_type_): _description_
        fixed_point_scale (_type_): _description_
        offset (int, optional): _description_. Defaults to 0.

    Returns:
        _type_: _description_
    """
    poke_addr = node.sdo[0x5603][3]
    poke_val = node.sdo[0x5603][4]
    poke_addr.write(address + offset)
    poke_val.write(f"{(value * fixed_point_scale)}")

    return [True]


def sdo_write(node, address, sub_index, value):

    sdo_write_location = node.sdo[address][sub_index]
    sdo_write_location.write(f"{(value)}")

    return[True]