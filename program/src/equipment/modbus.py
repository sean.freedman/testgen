from typing import Union

from pyModbusTCP.client import ModbusClient


class ModBusDevice:
    """
    Class for controlling Nidec Dyno inverter.

    Attributes:
        DEFAULT_SCALING (float): Default scaling factor.
        DEFAULT_IP (str): Default IP address.
        DEFAULT_PORT (int): Default port number.
        DEFAULT_UNIT_ID (int): Default unit ID.
    """

    # constants
    DEFAULT_SCALING = 1.0

    def __init__(self) -> None:
        """
        Initializes a new instance of the ModBusDevice class.
        """
        self._ip: Union[str, None] = None
        self._port: Union[int, None] = None
        self._id: Union[int, None] = None
        self._config: Union[dict, None] = None

    def setup_modbus(self, config: dict) -> ModbusClient:
        """
        Set up a Modbus with the provided configuration and return a pyModbusTCP.client.ModbusClient instance.

        Parameters:
        -----------
        config : dict
            A dictionary containing the configuration information for the CAN bus. The dictionary should include the
            following keys and values:
            {
                "config": {
                    "ip": <str>,    # IP Address of the ModBus Device (e.g. "192.168.1.1")
                    "port": <int>,  # Port of the ModBus Device on the the network number (e.g. 0, 1, 2)
                    "id": <int>     # Unit Id (e.g. 0, 1, 2)
                }
            }

        Returns:
        --------
        ModbusClient
            A ModbusClient object representing the ModBus Device configured with the specified parameters.
        """
        # attributes
        self._client = None
        self._config = config
        self._ip = self._config["modbus"]["ip"]
        self._port = self._config["modbus"]["port"]
        self._id = self._config["modbus"]["id"]

        # initialization
        self._client = ModbusClient(self._ip, self._port, auto_open=True, auto_close=False)
        self._client.unit_id = self._id
        if not self._client.open():
            print(self._client.last_except_as_full_txt)

        return self._client

    @staticmethod
    def determine_modbus_address(menu, parameter, is_32_bit):
        """
        Determine the Modbus address for a given menu and parameter.

        Args:
            menu (int): Menu number.
            parameter (int): Parameter number.
            is_32_bit (bool): True if the parameter is 32 bit.

        Returns:
            int: The Modbus address.
        """

        # determine address
        multiplier = 256 if parameter > 99 else 100
        address = (menu * multiplier) + parameter - 1
        if is_32_bit:
            address += 0x4000
        return address

    def read_parameter(self, menu, parameter, register_count, scaling=DEFAULT_SCALING):
        """
        Read a value from the ModBusDevice.

        Args:
            menu (int): Menu number.
            parameter (int): Parameter number.
            register_count (int): Number of registers to read.
            scaling (float): Scaling factor.

        Returns:
            float: The value.
        """
        address = ModBusDevice.determine_modbus_address(menu, parameter, register_count == 2)
        register_values = self._client.read_holding_registers(address, register_count)
        value = None
        if register_values is None:
            print(self._client.last_except_as_full_txt)
        else:
            value = register_values[0]
            if len(register_values) > 1:
                value = (register_values[0] << 16) | register_values[1]
        return value / scaling

    def write_parameter(self, menu, parameter, register_count, value, scaling=DEFAULT_SCALING):
        """
        Write a value to the ModBusDevice.

        Args:
            menu (int): Menu number.
            parameter (int): Parameter number.
            register_count (int): Number of registers to write.
            value (int): The value.
            scaling (float): Scaling factor.

        Returns:
            bool: True if successful.
        """

        # determine address
        address = ModBusDevice.determine_modbus_address(menu, parameter, register_count == 2)
        int_scaled_value = int(value * scaling)
        success = False
        if register_count == 1:
            success = self._client.write_single_register(address, value)
        else:
            register_values = [(int_scaled_value >> 16) & 0xFFFF, int_scaled_value & 0xFFFF]
            success = self._client.write_multiple_registers(address, register_values)
        return success
