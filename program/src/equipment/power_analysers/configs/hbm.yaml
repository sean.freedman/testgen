# +--------------------------------------------------------------------------+
# |                                                                          |
# |                          HBM: Gen 4 tethered                             |
# |                                                                          |
# +--------------------------------------------------------------------------+

gen4t:
  ip: "192.168.1.50"
  port: 8003

  # GEN4T: CAN ================================================================
  can:
    database: "program\\src\\equipment\\power_analysers\\databases\\gen4t.dbc"
    bitrate: 1000000
    interface: "ixxat"
    channel: 0
    period: 0.01
  # GEN4T: CAN: SIGNALS ========================================================
    signals:
      system:
        efficiency:
          power:
            in: "Drive_Eff_P_in"
            out: "Drive_Eff_P_out"
            loss: "Drive_Eff_P_loss"
          percentage: "Drive_Eff__"

      dyno:
        gearbox_ratio: "TT_Dyno_Gearbox_Ratio"

      motor:
        pole_pairs: "TT_MUT_Pole_Pairs"
        
        torque:
          measured: "TT_UUT_SEVCON_T_Em"
          calculated: "MOTOR_TORQUE_CALCULATED_NOT_SET"
        
        speed:
          measured:
            mechanical: 
              rpm: "TT_MUT_Speed_rpm"
              rads: "TT_MUT_Speed_rads"
              hz: "TT_MUT_W_e"
              
        efficiency:
          power:
            in: "Motor_Eff_P_in"
            out: "Motor_Eff_P_out"
            loss: "Motor_Eff_P_loss"
          percentage: "Motor_Eff__"

        power:
          mechanical: "TT_MUT_Power_Mech"

      controller:
        phase:
          voltage:
            rms:
              u: "Inverter_out_U_1"
              v: "Inverter_out_U_2"
              w: "Inverter_out_U_3"
              sum: "Inverter_out___U"
          
            peak:
              u: "U_PHASE_PEAK_NOT_SET"
              v: "V_PHASE_PEAK_NOT_SET"
              w: "W_PHASE_PEAK_NOT_SET"
              sum: "SUM_PHASE_PEAK_NOT_SET"
        
          current:
            rms:
              u: "Inverter_out_I_1"
              v: "Inverter_out_I_2"
              w: "Inverter_out_I_3"
              sum: "Inverter_out___I"

            peak:
              u: "CONTROLLER_PHASE_CURRENT_PEAK_U_NOT_SET"
              v: "CONTROLLER_PHASE_CURRENT_PEAK_V_NOT_SET"
              w: "CONTROLLER_PHASE_CURRENT_PEAK_W_NOT_SET"
              sum: "CONTROLLER_PHASE_CURRENT_PEAK_SUM_NOT_SET"

          power:
            u: "Inverter_out_P_1"
            v: "Inverter_out_P_2"
            w: "Inverter_out_P_3"
            sum: "Inverter_out__"

        power:
          p: "Inverter_out_P"
          s: "Inverter_out_S"
          q: "Inverter_out_Q"     

        stationary_frame:
          voltage:
            rms:
              d: "TT_UUT_U_d"
              q: "TT_UUT_U_q"
              s: "CONTROLLER_VOLTAGE_RMS_S_NOT_SET"

            peak:
              d: "TT_UUT_I_d"
              q: "TT_UUT_I_q"
              s: "CONTROLLER_VOLTAGE_PEAK_S_NOT_SET"
          
          current:
            rms:
              d: "TT_UUT_I_d"
              q: "TT_UUT_I_q"
              s: "TT_UUT_I_s"

            peak:
              d: "CONTROLLER_PEAK_PEAK_D_NOT_SET"
              q: "CONTROLLER_PEAK_PEAK_Q_NOT_SET"
              s: "CONTROLLER_PEAK_PEAK_S_NOT_SET"

        rotating_frame:

          voltage:
            rms:
              alpha: "TT_UUT_U_alpha"
              beta: "TT_UUT_U_beta"

            peak:
              alpha: "CONTROLLER_ROTATE_VOLTS_ALPHA_PEAK_NOT_SET"
              beta: "CONTROLLER_ROTATE_VOLTS_BETA_PEAK_NOT_SET"

          current:
            rms:
              alpha: "TT_UUT_I_Alpha"
              beta: "TT_UUT_I_Beta"

            peak:
              alpha: "CONTROLLER_ROTATE_CURRENT_ALPHA_PEAK_NOT_SET"
              beta: "CONTROLLER_ROTATE_CURRENT_BETA_PEAK_NOT_SET"
        
        efficiency:
          power:
            in: "Inverter_Eff_P_in"
            out: "Inverter_Eff_P_out"
            loss: "Inverter_Eff_P_loss"
          percentage: "Inverter_Eff__"

      power_supply:
        
        rms:
          current: "Powersource_out_I"
          voltage: "Powersource_out_U"
          power: "Powersource_out_P"
        
        peak:
          current: "POWERSOURCE_PEAK_CURRENT_NOT_SET"
          voltage: "POWERSOURCE_PEAK_VOLTAGE_NOT_SET" 
          power: "POWERSOURCE_PEAK_POWER_NOT_SET"
      
    # GEN4T: CAN: MESSAGES ========================================================
    messages:
      
      system:
        efficiency:
          power:
            in: "GHS_3A4"
            out: "Drive_Eff_P_out"
            loss: "GHS_3A7"
          percentage: "GHS_3A6"

      dyno:
        gearbox_ratio: "GHS_3A8"

      motor:
        pole_pairs: "TT_MUT_Pole_Pairs"
        
        torque:
          measured: "TT_UUT_SEVCON_T_Em"
          calculated: "MOTOR_TORQUE_CALCULATED_NOT_SET"
        
        speed:
          measured:
            mechanical: 
              rpm: "GHS_3AB"
              rads: "GHS_3AD"
              hz: "GHS_3AE"
              
        efficiency:
          power:
            in: "GHS_3A0"
            out: "GHS_3A1"
            loss: "GHS_3A3"
          percentage: "GHS_3A2"

        power:
          mechanical: "TT_MUT_Power_Mech"

      controller:
        phase:
          voltage:
            rms:
              u: "GHS_38E"
              v: "GHS_38F"
              w: "GHS_390"
              sum: "GHS_391"
          
            peak:
              u: "U_PHASE_PEAK_NOT_SET"
              v: "V_PHASE_PEAK_NOT_SET"
              w: "W_PHASE_PEAK_NOT_SET"
              sum: "SUM_PHASE_PEAK_NOT_SET"
        
          current:
            rms:
              u: "GHS_38A"
              v: "GHS_38B"
              w: "GHS_38C"
              sum: "GHS_38D"

            peak:
              u: "CONTROLLER_PHASE_CURRENT_PEAK_U_NOT_SET"
              v: "CONTROLLER_PHASE_CURRENT_PEAK_V_NOT_SET"
              w: "CONTROLLER_PHASE_CURRENT_PEAK_W_NOT_SET"
              sum: "CONTROLLER_PHASE_CURRENT_PEAK_SUM_NOT_SET"

          power:
            u: "GHS_3C1"
            v: "GHS_3C2"
            w: "GHS_3C3"
            sum: "GHS_395"

        power:
          p: "GHS_392"
          s: "GHS_393"
          q: "GHS_394"     

        stationary_frame:
          voltage:
            rms:
              d: "GHS_3B7"
              q: "GHS_3B8"
              s: "CONTROLLER_VOLTAGE_RMS_S_NOT_SET"

            peak:
              d: "TT_UUT_u_d"
              q: "TT_UUT_u_q"
              s: "CONTROLLER_VOLTAGE_PEAK_S_NOT_SET"
          
          current:
            rms:
              d: "GHS_3B4"
              q: "GHS_3B5"
              s: "GHS_3B6"

            peak:
              d: "CONTROLLER_PEAK_PEAK_D_NOT_SET"
              q: "CONTROLLER_PEAK_PEAK_Q_NOT_SET"
              s: "CONTROLLER_PEAK_PEAK_S_NOT_SET"

        rotating_frame:

          voltage:
            rms:
              alpha: "GHS_3B2"
              beta: "GHS_3B3"

            peak:
              alpha: "CONTROLLER_ROTATE_VOLTS_ALPHA_PEAK_NOT_SET"
              beta: "CONTROLLER_ROTATE_VOLTS_BETA_PEAK_NOT_SET"

          current:
            rms:
              alpha: "GHS_3B0"
              beta: "GHS_3B1"

            peak:
              alpha: "CONTROLLER_ROTATE_CURRENT_ALPHA_PEAK_NOT_SET"
              beta: "CONTROLLER_ROTATE_CURRENT_BETA_PEAK_NOT_SET"
        
        efficiency:
          power:
            in: "GHS_39C"
            out: "Inverter_Eff_P_out"
            loss: "GHS_39F"
          percentage: "GHS_39E"

      power_supply:
        
        rms:
          current: "GHS_387"
          voltage: "GHS_388"
          power: "GHS_389"
        
        peak:
          current: "POWERSOURCE_PEAK_CURRENT_NOT_SET"
          voltage: "POWERSOURCE_PEAK_VOLTAGE_NOT_SET" 
          power: "POWERSOURCE_PEAK_POWER_NOT_SET"
      