from src.constants import DEFAULT_PATHS
from src.equipment.power_analysers.library.hbm import Controller, Dyno, Gen4T, Mut, PowerSupply

print("Start of example script to read the Gen4T power analyser")

gen_4t_path = DEFAULT_PATHS["equipment"]["power_analysers"]["hbm"]

print(f"Gen4T path: {gen_4t_path}")

# Create an instance of the Gen4T class
gen4t = Gen4T(gen_4t_path)

# Create instances of the measurement classes
gen4t_dyno = Dyno(gen4t)
gen4t_mut = Mut(gen4t)
gen4t_power_supply = PowerSupply(gen4t)
gen4t_controller = Controller(gen4t)

# Print the bus and database
print("Start of print statements for Gen4T object")
print(f"Bus: {gen4t.bus}")
print(f"Database: {gen4t.db}")
print("End of print statements for Gen4T object")

# Print Dyno based measurements
print("Start of print statements for Gen4T.Dyno object")
print(f"Dyno Speed: {gen4t_dyno.Measured.mechanical_speed()}")
print(f"Dyno Torque: {gen4t_dyno.Measured.torque()}")
print("End of print statements for Gen4T.Dyno object")

# Print MUT based measurements
print("Start of print statements for Gen4T.Mut object")
print(f"MUT Mechanical Speed: {gen4t_mut.Measured.mechanical_speed()}")
print(f"MUT Electrical Speed: {gen4t_mut.Measured.electrical_speed()}")
print(f"MUT Torque: {gen4t_mut.Measured.torque()}")
print("End of print statements for Gen4T.Mut object")

# Print Power Supply based measurements
print("Start of print statements for Gen4T.PowerSupply object")
print(f"Power Supply Voltage (RMS): {gen4t_power_supply.Measured.voltage_rms()}")
print(f"Power Supply Current (RMS): {gen4t_power_supply.Measured.current_rms()}")
print(f"Power Supply Power (RMS): {gen4t_power_supply.Measured.power_rms()}")
print(f"Power Supply Voltage (Peak): {gen4t_power_supply.Measured.voltage_peak()}")
print(f"Power Supply Current (Peak): {gen4t_power_supply.Measured.current_peak()}")
print(f"Power Supply Power (Peak): {gen4t_power_supply.Measured.power_peak()}")
print(f"Power Supply Power Factor: {gen4t_power_supply.Measured.power_factor()}")
print("End of print statements for Gen4T.PowerSupply object")

# Print Controller based measurements
print("Start of print statements for Gen4T.Controller object")
print(f"Controller Phase Voltage (RMS): {gen4t_controller.Measured.phase_voltage_rms()}")
print(f"Controller U1 (RMS): {gen4t_controller.Measured.u1_rms()}")
print(f"Controller U2 (RMS): {gen4t_controller.Measured.u2_rms()}")
print(f"Controller U3 (RMS): {gen4t_controller.Measured.u3_rms()}")
print(f"Controller Phase Current (RMS): {gen4t_controller.Measured.phase_current_rms()}")
print(f"Controller I1 (RMS): {gen4t_controller.Measured.i1_rms()}")
print(f"Controller I2 (RMS): {gen4t_controller.Measured.i2_rms()}")
print(f"Controller I3 (RMS): {gen4t_controller.Measured.i3_rms()}")
print(f"Controller Phase Voltage (Peak): {gen4t_controller.Measured.phase_voltage_peak()}")
print(f"Controller U1 (Peak): {gen4t_controller.Measured.u1_peak()}")
print(f"Controller U2 (Peak): {gen4t_controller.Measured.u2_peak()}")
print(f"Controller U3 (Peak): {gen4t_controller.Measured.u3_peak()}")
print(f"Controller Phase Current (Peak): {gen4t_controller.Measured.phase_current_peak()}")
print(f"Controller I1 (Peak): {gen4t_controller.Measured.i1_peak()}")
print(f"Controller I2 (Peak): {gen4t_controller.Measured.i2_peak()}")
print(f"Controller I3 (Peak): {gen4t_controller.Measured.i3_peak()}")
print("End of print statements for Gen4T.Controller object")

# Print Controller calculated measurements
print("Start of print statements for Gen4T.Controller.Calculated object")
print(f"Controller U_Alpha (RMS): {gen4t_controller.Calculated.u_alpha_rms()}")
print(f"Controller U_Beta (RMS): {gen4t_controller.Calculated.u_beta_rms()}")
print(f"Controller U_Alpha (Peak): {gen4t_controller.Calculated.u_alpha_peak()}")
print(f"Controller U_Beta (Peak): {gen4t_controller.Calculated.u_beta_peak()}")
print(f"Controller I_Alpha (RMS): {gen4t_controller.Calculated.i_alpha_rms()}")
print(f"Controller I_Beta (RMS): {gen4t_controller.Calculated.i_beta_rms()}")
print(f"Controller I_Alpha (Peak): {gen4t_controller.Calculated.i_alpha_peak()}")
print(f"Controller I_Beta (Peak): {gen4t_controller.Calculated.i_beta_peak()}")
print(f"Controller U_d (RMS): {gen4t_controller.Calculated.u_d_rms()}")
print(f"Controller U_q (RMS): {gen4t_controller.Calculated.u_q_rms()}")
print(f"Controller U_d (Peak): {gen4t_controller.Calculated.u_d_peak()}")
print(f"Controller U_q (Peak): {gen4t_controller.Calculated.u_q_peak()}")
print(f"Controller I_d (RMS): {gen4t_controller.Calculated.i_d_rms()}")
print(f"Controller I_q (RMS): {gen4t_controller.Calculated.i_q_rms()}")
print(f"Controller I_d (Peak): {gen4t_controller.Calculated.i_d_peak()}")
print(f"Controller I_q (Peak): {gen4t_controller.Calculated.i_q_peak()}")
print("End of print statements for Gen4T.Controller.Calculated object")


print("End of example script to read the Gen4T power analyser")
