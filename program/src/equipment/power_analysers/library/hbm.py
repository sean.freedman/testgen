from typing import Union

from program.src.equipment.can import CanDevice


class Gen4T(CanDevice):
    """
    Class representing a Gen4T device that inherits from CanDevice.
    """

    def __init__(self, config):
        # Initialize the superclass CanDevice
        super().__init__()

        # Extract CAN messages, and signals from the configuration
        self._config = config
        self._message = self._config["can"]["messages"]
        self._signal = self._config["can"]["signals"]
        self._can_values: Union[dict, None] = None

        # Set up the CAN bus using the provided configuration
        self._bus = self.setup_can_bus(self._config)

        # Load the CAN message database
        self._db = self.load_database(self._config["can"]["database"])

        # Set the listener to receive CAN messages
        self._listener = self.setup_listener()

        # Set up the notifier to notify the listener when a message is received
        self._notifier = self.setup_notifier(self._bus, [self._listener])

        self._can_values = self.can_value_struct()

        # Create a write object for the DCDC class to send CAN messages
        self.write = Gen4T.Write(self)

        # Create a read object for the DCDC class to receive CAN messages, have to setup read after periodic messages
        self.read = Gen4T.Read(self)

        # Update CAN values on every message received by the listener
        self._listener.on_message_received = self.read.update_can_values

    def __del__(self):
        """
        Clean up the HBM GEN4T Power analyser shutting down the bus, and deleting the instance.

        Returns
        -------
        None
        """
        try:
            # Shutdown the bus
            self._bus.shutdown()

        except Exception as e:
            print(e)

    def can_value_struct(self):
        can_message = {
            # SYSTEM
            # EFFICIENCY: POWER
            self._message["system"]["efficiency"]["power"]["in"]: {
                self._signal["system"]["efficiency"]["power"]["in"]: None,
            },
            self._message["system"]["efficiency"]["power"]["out"]: {
                self._signal["system"]["efficiency"]["power"]["out"]: None,
            },
            self._message["system"]["efficiency"]["power"]["loss"]: {
                self._signal["system"]["efficiency"]["power"]["loss"]: None,
            },
            self._message["system"]["efficiency"]["percentage"]: {
                self._signal["system"]["efficiency"]["percentage"]: None,
            },
            # DYNO
            self._message["dyno"]["gearbox_ratio"]: {
                self._signal["dyno"]["gearbox_ratio"]: None,
            },
            # MOTOR
            self._message["motor"]["pole_pairs"]: {
                self._signal["motor"]["pole_pairs"]: None,
            },
            # MOTOR: TORQUE
            self._message["motor"]["torque"]["measured"]: {
                self._signal["motor"]["torque"]["measured"]: None,
            },
            self._message["motor"]["torque"]["calculated"]: {
                self._signal["motor"]["torque"]["calculated"]: None,
            },
            # MOTOR: SPEED
            self._message["motor"]["speed"]["measured"]["mechanical"]["rpm"]: {
                self._signal["motor"]["speed"]["measured"]["mechanical"]["rpm"]: None,
            },
            self._message["motor"]["speed"]["measured"]["mechanical"]["rads"]: {
                self._signal["motor"]["speed"]["measured"]["mechanical"]["rads"]: None,
            },
            self._message["motor"]["speed"]["measured"]["mechanical"]["hz"]: {
                self._signal["motor"]["speed"]["measured"]["mechanical"]["hz"]: None,
            },
            # MOTOR: EFFICIENCY
            self._message["motor"]["efficiency"]["power"]["in"]: {
                self._signal["motor"]["efficiency"]["power"]["in"]: None,
            },
            self._message["motor"]["efficiency"]["power"]["out"]: {
                self._signal["motor"]["efficiency"]["power"]["out"]: None,
            },
            self._message["motor"]["efficiency"]["power"]["loss"]: {
                self._signal["motor"]["efficiency"]["power"]["loss"]: None,
            },
            self._message["motor"]["efficiency"]["percentage"]: {
                self._signal["motor"]["efficiency"]["percentage"]: None,
            },
            # MOTOR: POWER
            self._message["motor"]["power"]["mechanical"]: {
                self._signal["motor"]["power"]["mechanical"]: None,
            },
            # CONTROLLER
            # CONTROLLER : PHASE : VOLTAGE : RMS
            self._message["controller"]["phase"]["voltage"]["rms"]["u"]: {
                self._signal["controller"]["phase"]["voltage"]["rms"]["u"]: None,
            },
            self._message["controller"]["phase"]["voltage"]["rms"]["v"]: {
                self._signal["controller"]["phase"]["voltage"]["rms"]["v"]: None,
            },
            self._message["controller"]["phase"]["voltage"]["rms"]["w"]: {
                self._signal["controller"]["phase"]["voltage"]["rms"]["w"]: None,
            },
            self._message["controller"]["phase"]["voltage"]["rms"]["sum"]: {
                self._signal["controller"]["phase"]["voltage"]["rms"]["sum"]: None,
            },
            # CONTROLLER : PHASE : VOLTAGE : PEAK
            self._message["controller"]["phase"]["voltage"]["peak"]["u"]: {
                self._signal["controller"]["phase"]["voltage"]["peak"]["u"]: None,
            },
            self._message["controller"]["phase"]["voltage"]["peak"]["v"]: {
                self._signal["controller"]["phase"]["voltage"]["peak"]["v"]: None,
            },
            self._message["controller"]["phase"]["voltage"]["peak"]["w"]: {
                self._signal["controller"]["phase"]["voltage"]["peak"]["w"]: None,
            },
            self._message["controller"]["phase"]["voltage"]["peak"]["sum"]: {
                self._signal["controller"]["phase"]["voltage"]["peak"]["sum"]: None,
            },
            # CONTROLLER : PHASE : CURRENT : RMS
            self._message["controller"]["phase"]["voltage"]["rms"]["u"]: {
                self._signal["controller"]["phase"]["voltage"]["rms"]["u"]: None,
            },
            self._message["controller"]["phase"]["voltage"]["rms"]["v"]: {
                self._signal["controller"]["phase"]["voltage"]["rms"]["v"]: None,
            },
            self._message["controller"]["phase"]["voltage"]["rms"]["w"]: {
                self._signal["controller"]["phase"]["voltage"]["rms"]["w"]: None,
            },
            self._message["controller"]["phase"]["voltage"]["rms"]["sum"]: {
                self._signal["controller"]["phase"]["voltage"]["rms"]["sum"]: None,
            },
            # CONTROLLER : PHASE : CURRENT : PEAK
            self._message["controller"]["phase"]["voltage"]["peak"]["u"]: {
                self._signal["controller"]["phase"]["voltage"]["peak"]["u"]: None,
            },
            self._message["controller"]["phase"]["voltage"]["peak"]["v"]: {
                self._signal["controller"]["phase"]["voltage"]["peak"]["v"]: None,
            },
            self._message["controller"]["phase"]["voltage"]["peak"]["w"]: {
                self._signal["controller"]["phase"]["voltage"]["peak"]["w"]: None,
            },
            self._message["controller"]["phase"]["voltage"]["peak"]["sum"]: {
                self._signal["controller"]["phase"]["voltage"]["peak"]["sum"]: None,
            },
            # CONTROLLER : PHASE : POWER
            self._message["controller"]["phase"]["power"]["u"]: {
                self._signal["controller"]["phase"]["power"]["u"]: None,
            },
            self._message["controller"]["phase"]["power"]["v"]: {
                self._signal["controller"]["phase"]["power"]["v"]: None,
            },
            self._message["controller"]["phase"]["power"]["w"]: {
                self._signal["controller"]["phase"]["power"]["w"]: None,
            },
            self._message["controller"]["phase"]["power"]["sum"]: {
                self._signal["controller"]["phase"]["power"]["sum"]: None,
            },
            # CONTROLLER : POWER : QUANTITY
            self._message["controller"]["power"]["p"]: {
                self._signal["controller"]["power"]["p"]: None,
            },
            self._message["controller"]["power"]["s"]: {
                self._signal["controller"]["power"]["s"]: None,
            },
            self._message["controller"]["power"]["q"]: {
                self._signal["controller"]["power"]["q"]: None,
            },
            # CONTROLLER : STATIONARY FRAME : VOLTAGE
            self._message["controller"]["stationary_frame"]["voltage"]["rms"]["d"]: {
                self._signal["controller"]["stationary_frame"]["voltage"]["rms"]["d"]: None,
            },
            self._message["controller"]["stationary_frame"]["voltage"]["rms"]["q"]: {
                self._signal["controller"]["stationary_frame"]["voltage"]["rms"]["q"]: None,
            },
            self._message["controller"]["stationary_frame"]["voltage"]["rms"]["s"]: {
                self._signal["controller"]["stationary_frame"]["voltage"]["rms"]["s"]: None,
            },
            self._message["controller"]["stationary_frame"]["voltage"]["peak"]["d"]: {
                self._signal["controller"]["stationary_frame"]["voltage"]["peak"]["d"]: None,
            },
            self._message["controller"]["stationary_frame"]["voltage"]["peak"]["q"]: {
                self._signal["controller"]["stationary_frame"]["voltage"]["peak"]["q"]: None,
            },
            self._message["controller"]["stationary_frame"]["voltage"]["peak"]["s"]: {
                self._signal["controller"]["stationary_frame"]["voltage"]["peak"]["s"]: None,
            },
            # CONTROLLER : STATIONARY FRAME : CURRENT
            self._message["controller"]["stationary_frame"]["current"]["rms"]["d"]: {
                self._signal["controller"]["stationary_frame"]["current"]["rms"]["d"]: None,
            },
            self._message["controller"]["stationary_frame"]["current"]["rms"]["q"]: {
                self._signal["controller"]["stationary_frame"]["current"]["rms"]["q"]: None,
            },
            self._message["controller"]["stationary_frame"]["current"]["rms"]["s"]: {
                self._signal["controller"]["stationary_frame"]["current"]["rms"]["s"]: None,
            },
            self._message["controller"]["stationary_frame"]["current"]["peak"]["d"]: {
                self._signal["controller"]["stationary_frame"]["current"]["peak"]["d"]: None,
            },
            self._message["controller"]["stationary_frame"]["current"]["peak"]["q"]: {
                self._signal["controller"]["stationary_frame"]["current"]["peak"]["q"]: None,
            },
            self._message["controller"]["stationary_frame"]["current"]["peak"]["s"]: {
                self._signal["controller"]["stationary_frame"]["current"]["peak"]["s"]: None,
            },
            # CONTROLLER : ROTATING FRAME : CURRNET
            self._message["controller"]["rotating_frame"]["current"]["rms"]["alpha"]: {
                self._signal["controller"]["rotating_frame"]["current"]["rms"]["alpha"]: None,
            },
            self._message["controller"]["rotating_frame"]["current"]["rms"]["beta"]: {
                self._signal["controller"]["rotating_frame"]["current"]["rms"]["beta"]: None,
            },
            self._message["controller"]["rotating_frame"]["current"]["peak"]["alpha"]: {
                self._signal["controller"]["rotating_frame"]["current"]["peak"]["alpha"]: None,
            },
            self._message["controller"]["rotating_frame"]["current"]["peak"]["beta"]: {
                self._signal["controller"]["rotating_frame"]["current"]["peak"]["beta"]: None,
            },
            # CONTROLLER : EFFICIENCY
            self._message["controller"]["efficiency"]["power"]["in"]: {
                self._signal["controller"]["efficiency"]["power"]["in"]: None,
            },
            self._message["controller"]["efficiency"]["power"]["out"]: {
                self._signal["controller"]["efficiency"]["power"]["out"]: None,
            },
            self._message["controller"]["efficiency"]["power"]["loss"]: {
                self._signal["controller"]["efficiency"]["power"]["loss"]: None,
            },
            self._message["controller"]["efficiency"]["percentage"]: {
                self._signal["controller"]["efficiency"]["percentage"]: None,
            },
            # POWERSOURCE : RMS
            self._message["power_supply"]["rms"]["current"]: {
                self._signal["power_supply"]["rms"]["current"]: None,
            },
            self._message["power_supply"]["rms"]["voltage"]: {
                self._signal["power_supply"]["rms"]["voltage"]: None,
            },
            self._message["power_supply"]["rms"]["power"]: {
                self._signal["power_supply"]["rms"]["power"]: None,
            },
            # POWERSOURCE : PEAK
            self._message["power_supply"]["peak"]["current"]: {
                self._signal["power_supply"]["peak"]["current"]: None,
            },
            self._message["power_supply"]["peak"]["voltage"]: {
                self._signal["power_supply"]["peak"]["voltage"]: None,
            },
            self._message["power_supply"]["peak"]["power"]: {
                self._signal["power_supply"]["peak"]["power"]: None,
            },
        }
        return can_message

    class Write:
        def __init__(self, power_analyser):
            self._power_analyser = power_analyser
            self._message = power_analyser._message
            self._signal = power_analyser._signal

        def zero_torque_measurement(self):
            """
            Zero the torque measurement.

            Returns
            -------
            None.

            """
            zero_trigger = {
                "arbitration_id": 0x3DD,
                "data": [0x80, 0x40, 0x01, 0x00, 0x08, 0x00, 0x01, 0x00],
                "is_extended_id": False,
                "is_remote_frame": False,
                "is_error_frame": False,
                "dlc": 8,
                "is_fd": False,
                "is_rx": False,
            }

            self.write_message(zero_trigger)

    class Read:
        def __init__(self, power_analyser):
            self.power_analyser = power_analyser
            self._can_values = self.power_analyser._can_values
            self.dyno = self.Dyno(self)

        def update_can_values(self, message_received):
            self._power_analyser.read_can_values(self._can_values, message_received)

        class Dyno:
            """
            A class representing a dynamometer device that extends the Gen4T class.
            Attributes:
                config (dict): A dictionary containing the configuration information for the dynamometer measurements from the Gen4T.
                measured (Measured): An instance of the Measured class for reading measured values from the dynamometer via the Gen4T.

            Methods:
                __init__(self, config): Initializes a Dyno instance.
            """

            def __init__(self):
                """
                Initializes a Dyno instance.

                Args:
                    config (dict): A dictionary containing the configuration information for the dynamometer measurements from the Gen4T.

                Returns:
                    None
                """
                self.measured = self.Measured(self)

            class Measured:
                """
                A nested class representing a container for methods to read measured values from a dynamometer.

                Attributes:
                    _dyno (Dyno): A reference to the Dyno instance this Measured instance is associated with.
                    _message (dict): A dictionary containing the message format for reading signals from the dynamometer via the Gen4T.
                    _signal (dict): A dictionary containing the signal format for reading signals from the dynamometer via the Gen4T.

                Methods:
                    __init__(self, dyno): Initializes a Measured instance.
                    mechanical_speed(self): Returns the mechanical speed measured by the dynamometer via the Gen4T and connected torque transducer.
                    torque(self): Returns the torque measured by the dynamometer via the Gen4T and connected torque transducer.
                """

                def __init__(self):
                    """
                    Initializes a Measured instance.

                    Args:
                        dyno (Dyno): A reference to the Dyno instance this Measured instance is associated with.

                    Returns:
                        None
                    """
                    pass

                def mechanical_speed(self) -> float:
                    """
                    Read the mechanical speed from the dynamometer via the Gen4T and connected torque transducer.

                    Returns:
                        float: The mechanical speed of the dynamometer.
                    """
                    return self._power_analyser.read_periodic_signal(
                        self._can_values, self._message["speed"]["measured"]["mechanical"]["dyno"], self._signal["speed"]["measured"]["mechanical"]["dyno"]
                    )

                def torque(self) -> float:
                    """
                    Read the torque from the dynamometer via the Gen4T and connected torque transducer.

                    Returns:
                        float: The torque of the dynamometer.
                    """
                    return self._power_analyser.read_periodic_signal(self._message["torque"]["measured"]["dyno"], self._signal["torque"]["measured"]["dyno"])

        class Mut:

            """
            Class representing a motor under test that inherits from Gen4T.
            Attributes:
                measured (Measured): An instance of the Measured class for reading measured values from the motor under test via the Gen4T and connected torque transducer.

            Methods:
                __init__(self, config): Initializes a Mut instance.
            """

            def __init__(self, power_analyser):
                """
                Initializes a Mut instance.

                Args:
                    config (dict): A dictionary containing the configuration information for the motor under test measurements from the Gen4T and connected torque transducer.

                Returns:
                    None
                """
                super().__init__()
                self._power_analyser = power_analyser
                self._message = power_analyser._message
                self._signal = power_analyser._signal
                self.measured = self.Measured(self)

            class Measured:
                """
                A nested class representing a container for methods to read measured values from a motor under test via the Gen4T and connected torque transducer.

                Attributes:
                    _mut (Mut): A reference to the Mut instance this Measured instance is associated with.
                    _message (dict): A dictionary containing the message format for reading signals from the motor under test via the Gen4T and connected torque transducer.
                    _signal (dict): A dictionary containing the signal format for reading signals from the motor under test via the Gen4T and connected torque transducer.

                Methods:
                    __init__(self, mut): Initializes a Measured instance.
                    mechanical_speed(self): Returns the mechanical speed measured by the motor under test via the Gen4T and connected torque transducer.
                    electrical_speed(self): Returns the electrical speed measured by the motor under test via the Gen4T and connected torque transducer.
                    torque(self): Returns the torque measured by the motor under test via the Gen4T and connected torque transducer.
                """

                def __init__(self, mut):
                    """
                    Initializes a Measured instance.

                    Args:
                        mut (Mut): A reference to the Mut instance to which this Measured instance is associated.

                    Returns:
                        None
                    """
                    self._mut = mut
                    self._message = mut._message
                    self._signal = mut._signal

                def mechanical_speed(self) -> float:
                    """
                    Returns the mechanical speed measured by the motor under test via the Gen4T and connected torque transducer.

                    Args:
                        None

                    Returns:
                        float: The mechanical speed of the motor under test.
                    """
                    return self._mut.read_periodic_signal(self._message["speed"]["measured"]["mechanical"]["mut"], self._signal["speed"]["measured"]["mechanical"]["mut"])

                def electrical_speed(self) -> float:
                    """
                    Returns the electrical speed measured by the motor under test via the Gen4T and connected torque transducer.

                    Args:
                        None

                    Returns:
                        float: The electrical speed of the motor under test.
                    """
                    return self._mut.read_periodic_signal(self._message["speed"]["measured"]["electrical"]["mut"], self._signal["speed"]["measured"]["electrical"]["mut"])

                def torque(self) -> float:
                    """
                    Returns the torque measured by the motor under test via the Gen4T and connected torque transducer.

                    Args:
                        None

                    Returns:
                        float: The torque measured by the motor under test.
                    """
                    return self._mut.read_periodic_signal(self._message["torque"]["measured"]["mut"], self._signal["torque"]["measured"]["mut"])

        class PowerSupply:
            """
            A class representing a power supply that extends the Gen4T class.

            Attributes:
                config (dict): A dictionary containing the configuration information for the power supply measurements from the Gen4T.
                measured (Measured): An instance of the Measured class for reading measured values from the power supply via the Gen4T.

            Methods:
                __init__(self, config): Initializes a PowerSupply instance.
            """

            def __init__(self, power_analyser):
                """
                Initializes a PowerSupply instance.

                Args:
                    config (dict): A dictionary containing the configuration information for the power supply measurements from the Gen4T.

                Returns:
                    None
                """
                super().__init__()
                self._power_analyser = power_analyser
                self._message = power_analyser._message
                self._signal = power_analyser._signal
                self._can_values = power_analyser._can_values
                self.measured = self.Measured(self)

            class Measured:
                """
                A nested class representing a container for methods to read measured values from a power supply.

                Attributes:
                    _power_supply (PowerSupply): A reference to the PowerSupply instance this Measured instance is associated with.
                    _message (dict): A dictionary containing the message format for reading signals from the power supply via the Gen4T.
                    _signal (dict): A dictionary containing the signal format for reading signals from the power supply via the Gen4T.

                Methods:
                    __init__(self, power_supply): Initializes a Measured instance.
                    voltage_rms(self): Returns the root mean square (RMS) voltage measured by the power supply via the Gen4T.
                    current_rms(self): Returns the RMS current measured by the power supply via the Gen4T.
                    power_rms(self): Returns the RMS power measured by the power supply via the Gen4T.
                    voltage_peak(self): Returns the peak voltage measured by the power supply via the Gen4T.
                    current_peak(self): Returns the peak current measured by the power supply via the Gen4T.
                    power_peak(self): Returns the peak power measured by the power supply via the Gen4T.
                    power_factor(self): Returns the power factor measured by the power supply via the Gen4T.
                """

                def __init__(self, power_analyser):
                    """
                    Initializes a Measured instance.

                    Args:
                        power_supply (PowerSupply): A reference to the PowerSupply instance to associate this Measured instance with.

                    Returns:
                        None
                    """
                    self._power_analyser = power_analyser
                    self._message = self._power_analyser._message
                    self._signal = self._power_analyser._signal
                    self._can_values = self._power_analyser._can_values

                def voltage_rms(self) -> float:
                    """
                    Returns the root mean square (RMS) voltage measured by the power supply via the Gen4T.

                    Args:
                        None

                    Returns:
                        float: The RMS voltage measured by the power supply via the Gen4T.
                    """
                    return self._power_analyser.read_periodic_signal(self._can_values, self._message["voltage"]["rms"]["power_supply"], self._signal["voltage"]["rms"]["power_supply"])

                def current_rms(self) -> float:
                    """
                    Returns the RMS current measured by the power supply via the Gen4T.

                    Args:
                        None

                    Returns:
                        float: The RMS current measured by the power supply via the Gen4T.
                    """
                    return self._power_analyser.read_periodic_signal(self._message["current"]["rms"]["power_supply"], self._signal["current"]["rms"]["power_supply"])

                def power_rms(self) -> float:
                    """
                    Returns the RMS power measured by the power supply via the Gen4T.

                    Args:
                        None

                    Returns:
                        float: The RMS power measured by the power supply via the Gen4T.
                    """
                    return self._power_analyser.read_periodic_signal(self._message["power"]["electrical"]["rms"]["power_supply"], self._signal["power"]["electrical"]["rms"]["power_supply"])

                def voltage_peak(self) -> float:
                    """
                    Returns the peak voltage measured by the power supply via the Gen4T.

                    Args:
                        None

                    Returns:
                        float: The peak voltage measured by the power supply via the Gen4T.
                    """
                    return self._power_analyser.read_periodic_signal(self._message["voltage"]["peak"]["power_supply"], self._signal["voltage"]["peak"]["power_supply"])

                def current_peak(self) -> float:
                    """
                    Returns the peak current measured by the power supply via the Gen4T.

                    Args:
                        None

                    Returns:
                        float: The peak current measured by the power supply via the Gen4T.
                    """
                    return self._power_analyser.read_periodic_signal(self._message["current"]["peak"]["power_supply"], self._signal["current"]["peak"]["power_supply"])

                def power_peak(self) -> float:
                    """
                    Returns the peak power measured by the power supply via the Gen4T.

                    Args:
                        None

                    Returns:
                        float: The peak power measured by the power supply via the Gen4T.
                    """
                    return self._power_analyser.read_periodic_signal(self._message["power"]["electrical"]["peak"]["power_supply"], self._signal["power"]["electrical"]["peak"]["power_supply"])

                def power_factor(self) -> float:
                    """
                    Returns the power factor measured by the power supply via the Gen4T.

                    Args:
                        None

                    Returns:
                        float: The power factor measured by the power supply via the Gen4T.
                    """
                    return self._power_analyser.read_periodic_signal(self._message["power_factor"]["power_supply"], self._signal["power_factor"]["power_supply"])

        class Controller:
            """
            A class representing a controller that extends the Gen4T class.

            Attributes:
                config (dict): A dictionary containing the configuration information for the controller measurements from the Gen4T.
                measured (Measured): An instance of the Measured class for reading measured values from the controller via the Gen4T.

            Methods:
                __init__(self, config): Initializes a Controller instance.
            """

            def __init__(self, config):
                """
                Initializes a Controller instance.

                Args:
                    config (dict): A dictionary containing the configuration information for the controller measurements from the Gen4T.

                Returns:
                    None
                """
                super().__init__(config)
                self.measured = self.Measured(self)

            class Measured:
                """
                A nested class representing a container for methods to read measured values from a controller.

                Attributes:
                    _controller (Controller): A reference to the Controller instance this Measured instance is associated with.
                    _message (dict): A dictionary containing the message format for reading signals from the controller via the Gen4T.
                    _signal (dict): A dictionary containing the signal format for reading signals from the controller via the Gen4T.

                Methods:
                    __init__(self, controller): Initializes a Measured instance.
                    phase_voltage_rms(self): Returns the root mean square (RMS) voltage of the controller phase.
                    u1_rms(self): Returns the RMS voltage of phase U1 of the controller.
                    u2_rms(self): Returns the RMS voltage of phase U2 of the controller.
                    u3_rms(self): Returns the RMS voltage of phase U3 of the controller.
                    phase_current_rms(self): Returns the RMS current of the controller phase.
                    i1_rms(self): Returns the RMS current of phase I1 of the controller.
                    i2_rms(self): Returns the RMS current of phase I2 of the controller.
                    i3_rms(self): Returns the RMS current of phase I3 of the controller.
                    phase_voltage_peak(self): Returns the peak voltage of the controller phase.
                    u1_peak(self): Returns the peak voltage of phase U1 of the controller.
                    u2_peak(self): Returns the peak voltage of phase U2 of the controller.
                    u3_peak(self): Returns the peak voltage of phase U3 of the controller.
                    phase_current_peak(self): Returns the peak current of the controller phase.
                    i1_peak(self): Returns the peak current of phase I1 of the controller.
                    i2_peak(self): Returns the peak current of phase I2 of the controller.
                    i3_peak(self): Returns the peak current of phase I3 of the controller.
                """

                def __init__(self, controller):
                    """
                    Initializes the Measured class of a controller device.

                    Parameters
                    ----------
                    controller : Controller
                        The controller instance.

                    """
                    self._controller = controller
                    self._message = controller._message
                    self._signal = controller._signal

                def phase_voltage_rms(self) -> float:
                    """
                    Read the RMS voltage of the controller via the Gen4T.

                    Returns
                    -------
                    float
                        The RMS voltage of the controller via the Gen4T.

                    """
                    return self._controller.read_periodic_signal(self._message["voltage"]["phase"]["rms"]["sum"], self._signal["voltage"]["phase"]["rms"]["sum"])

                def u1_rms(self) -> float:
                    """
                    Read the RMS voltage of phase U1 of the controller via the Gen4T.

                    Returns
                    -------
                    float
                        The RMS voltage of phase U1 of the controller via the Gen4T.

                    """
                    return self._controller.read_periodic_signal(self._message["voltage"]["phase"]["rms"]["u1"], self._signal["voltage"]["phase"]["rms"]["u1"])

                def u2_rms(self) -> float:
                    """
                    Read the RMS voltage of phase U2 of the controller via the Gen4T.

                    Returns
                    -------
                    float
                        The RMS voltage of phase U2 of the controller via the Gen4T.

                    """
                    return self._controller.read_periodic_signal(self._message["voltage"]["phase"]["rms"]["u2"], self._signal["voltage"]["phase"]["rms"]["u2"])

                def u3_rms(self) -> float:
                    """
                    Read the RMS voltage of phase U3 of the controller via the Gen4T.

                    Returns
                    -------
                    float
                        The RMS voltage of phase U3 of the controller via the Gen4T.

                    """
                    return self._controller.read_periodic_signal(self._message["voltage"]["phase"]["rms"]["u3"], self._signal["voltage"]["phase"]["rms"]["u3"])

                def phase_current_rms(self) -> float:
                    """
                    Read the RMS current of the controller via the Gen4T.

                    Returns
                    -------
                    float
                        The RMS current of the controller via the Gen4T.

                    """
                    return self._controller.read_periodic_signal(self._message["current"]["phase"]["rms"]["sum"], self._signal["current"]["phase"]["rms"]["sum"])

                def i1_rms(self) -> float:
                    """
                    Read the RMS current of phase I1 of the controller via the Gen4T.

                    Returns
                    -------
                    float
                        The RMS current of phase I1 of the controller via the Gen4T.

                    """
                    return self._controller.read_periodic_signal(self._message["current"]["phase"]["rms"]["i1"], self._signal["current"]["phase"]["rms"]["i1"])

                def i2_rms(self) -> float:
                    """
                    Read the RMS current of phase I2 of the controller via the Gen4T.

                    Returns
                    -------
                    float
                        The RMS current of phase I2 of the controller via the Gen4T.

                    """
                    return self._controller.read_periodic_signal(self._message["current"]["phase"]["rms"]["i2"], self._signal["current"]["phase"]["rms"]["i2"])

                def i3_rms(self) -> float:
                    """
                    Read the RMS current of phase I3 of the controller via the Gen4T.

                    Returns
                    -------
                    float
                        The RMS current of phase I3 of the controller via the Gen4T.

                    """
                    return self._controller.read_periodic_signal(self._message["current"]["phase"]["rms"]["i3"], self._signal["current"]["phase"]["rms"]["i3"])

                def phase_voltage_peak(self) -> float:
                    """
                    Read the peak phase voltage of the controller via the Gen4T.

                    Args:
                        None

                    Returns:
                        float: The peak phase voltage of the controller via the Gen4T.
                    """
                    return self._controller.read_periodic_signal(self._message["voltage"]["phase"]["peak"]["sum"], self._signal["voltage"]["phase"]["peak"]["sum"])

                def u1_peak(self) -> float:
                    """
                    Returns the peak voltage measured on phase u1 of the controller via the Gen4T.

                    Args:
                        None

                    Returns:
                        float: The peak voltage measured on phase u1 of the controller via the Gen4T.
                    """
                    return self._controller.read_periodic_signal(self._message["voltage"]["phase"]["peak"]["u1"], self._signal["voltage"]["phase"]["peak"]["u1"])

                def u2_peak(self) -> float:
                    """
                    Returns the peak voltage measured on phase u2 of the controller via the Gen4T.

                    Args:
                        None

                    Returns:
                        float: The peak voltage measured on phase u2 of the controller via the Gen4T.
                    """
                    return self._controller.read_periodic_signal(self._message["voltage"]["phase"]["peak"]["u2"], self._signal["voltage"]["phase"]["peak"]["u2"])

                def u3_peak(self) -> float:
                    """
                    Returns the peak voltage measured on phase u3 of the controller via the Gen4T.

                    Args:
                        None

                    Returns:
                        float: The peak voltage measured on phase u3 of the controller via the Gen4T.
                    """
                    return self._controller.read_periodic_signal(self._message["voltage"]["phase"]["peak"]["u3"], self._signal["voltage"]["phase"]["peak"]["u3"])

                def phase_current_peak(self) -> float:
                    """
                    Returns the peak phase current measured of the controller via the Gen4T.

                    Args:
                        None

                    Returns:
                        float: The peak phase current measured of the controller via the Gen4T.
                    """
                    return self._controller.read_periodic_signal(self._message["current"]["phase"]["peak"]["sum"], self._signal["current"]["phase"]["peak"]["sum"])

                def i1_peak(self) -> float:
                    """
                    Returns the peak current measured on phase i1 of the controller via the Gen4T.

                    Args:
                        None

                    Returns:
                        float: The peak current measured on phase i1 of the controller via the Gen4T.
                    """
                    return self._controller.read_periodic_signal(self._message["current"]["phase"]["peak"]["i1"], self._signal["current"]["phase"]["peak"]["i1"])

                def i2_peak(self) -> float:
                    """
                    Returns the peak current measured on phase i2 of the controller via the Gen4T.

                    Args:
                        None

                    Returns:
                        float: The peak current measured on phase i2 of the controller via the Gen4T.
                    """
                    return self._controller.read_periodic_signal(self._message["current"]["phase"]["peak"]["i2"], self._signal["current"]["phase"]["peak"]["i2"])

                def i3_peak(self) -> float:
                    """
                    Returns the peak current measured on phase i3 of the controller via the Gen4T.

                    Args:
                        None

                    Returns:
                        float: The peak current measured on phase i3 of the controller via the Gen4T.
                    """
                    return self._controller.read_periodic_signal(self._message["current"]["phase"]["peak"]["i3"], self._signal["current"]["phase"]["peak"]["i3"])

            class Calculated:
                """
                A nested class representing a container for methods to read calculated values from a controller via the Gen4T.

                Attributes:
                    _controller (Controller): A reference to the Controller instance this Measured instance is associated with.
                    _message (dict): A dictionary containing the message format for reading signals from the controller via the Gen4T.
                    _signal (dict): A dictionary containing the signal format for reading signals from the controller via the Gen4T.

                Methods:
                    u_alpha_rms() -> float:
                        Reads and returns the root mean square (RMS) voltage value for Alpha in the rotating frame.

                    u_beta_rms() -> float:
                        Reads and returns the root mean square (RMS) voltage value for Beta in the rotating frame.

                    u_alpha_peak() -> float:
                        Reads and returns the peak voltage value for Alpha in the rotating frame.

                    u_beta_peak() -> float:
                        Reads and returns the peak voltage value for Beta in the rotating frame.

                    i_alpha_rms() -> float:
                        Reads and returns the root mean square (RMS) current value for Alpha in the rotating frame.

                    i_beta_rms() -> float:
                        Reads and returns the root mean square (RMS) current value for Beta in the rotating frame.

                    i_alpha_peak() -> float:
                        Reads and returns the peak current value for Alpha in the rotating frame.

                    i_beta_peak() -> float:
                        Reads and returns the peak current value for Beta in the rotating frame.

                    u_d_rms() -> float:
                        Reads and returns the root mean square (RMS) voltage value for the D-axis in the stationary frame.

                    u_q_rms() -> float:
                        Reads and returns the root mean square (RMS) voltage value for the Q-axis in the stationary frame.

                    u_d_peak() -> float:
                        Reads and returns the peak voltage value for the D-axis in the stationary frame.

                    u_q_peak() -> float:
                        Reads and returns the peak voltage value for the Q-axis in the stationary frame.

                    i_d_rms() -> float:
                        Reads and returns the root mean square (RMS) current value for the D-axis in the stationary frame.

                    i_q_rms() -> float:
                        Reads and returns the root mean square (RMS) current value for the Q-axis in the stationary frame.

                    i_d_peak() -> float:
                        Reads and returns the peak current value for the D-axis in the stationary frame.

                    i_q_peak() -> float:
                        Reads and returns the peak current value for the Q-axis in the stationary frame.
                """

                def __init__(self, controller):
                    """
                    Initializes a new instance of the Calculated class with a reference to the Controller instance this
                    Measured instance is associated with.

                    Args:
                        controller (Controller): The Controller instance this Calculated instance is associated with.
                    """
                    self._controller = controller
                    self._message = controller._message
                    self._signal = controller._signal

                def u_alpha_rms(self) -> float:
                    """
                    Reads and returns the root mean square (RMS) voltage value for Alpha in the rotating frame.

                    Returns:
                        float: The RMS voltage value for Alpha in the rotating frame.
                    """
                    return self._controller.read_periodic_signal(self._message["rotating_frame"]["voltage"]["rms"]["alpha"], self._signal["rotating_frame"]["voltage"]["rms"]["alpha"])

                def u_beta_rms(self) -> float:
                    """
                    Reads and returns the root mean square (RMS) voltage value for Beta in the rotating frame.

                    Returns:
                        float: The RMS voltage value for Beta in the rotating frame.
                    """
                    return self._controller.read_periodic_signal(self._message["rotating_frame"]["voltage"]["rms"]["beta"], self._signal["rotating_frame"]["voltage"]["rms"]["beta"])

                def u_alpha_peak(self) -> float:
                    """
                    Reads and returns the peak voltage value for Alpha in the rotating frame.

                    Returns:
                        float: The peak voltage value for Alpha in the rotating frame.
                    """
                    return self._controller.read_periodic_signal(self._message["rotating_frame"]["voltage"]["peak"]["alpha"], self._signal["rotating_frame"]["voltage"]["peak"]["alpha"])

                def u_beta_peak(self) -> float:
                    """
                    Reads and returns the peak voltage value for Beta in the rotating frame.

                    Returns:
                        float: The peak voltage value for Beta in the rotating frame.
                    """
                    return self._controller.read_periodic_signal(self._message["rotating_frame"]["voltage"]["peak"]["beta"], self._signal["rotating_frame"]["voltage"]["peak"]["beta"])

                def i_alpha_rms(self) -> float:
                    """
                    Reads and returns the root mean square (RMS) current value for Alpha in the rotating frame.

                    Returns:
                        float: The RMS current value for Alpha in the rotating frame.
                    """
                    return self._controller.read_periodic_signal(self._message["rotating_frame"]["current"]["rms"]["alpha"], self._signal["rotating_frame"]["current"]["rms"]["alpha"])

                def i_beta_rms(self) -> float:
                    """
                    Reads and returns the root mean square (RMS) current value for Beta in the rotating frame.

                    Returns:
                        float: The RMS current value for Beta in the rotating frame.
                    """
                    return self._controller.read_periodic_signal(self._message["rotating_frame"]["current"]["rms"]["beta"], self._signal["rotating_frame"]["current"]["rms"]["beta"])

                def i_alpha_peak(self) -> float:
                    """
                    Reads and returns the peak current value for Alpha in the rotating frame.

                    Returns:
                        float: The peak current value for Alpha in the rotating frame.
                    """
                    return self._controller.read_periodic_signal(self._message["rotating_frame"]["current"]["peak"]["alpha"], self._signal["rotating_frame"]["current"]["peak"]["alpha"])

                def i_beta_peak(self) -> float:
                    """
                    Reads and returns the peak current value for Beta in the rotating frame.

                    Returns:
                        float: The peak current value for Beta in the rotating frame.
                    """
                    return self._controller.read_periodic_signal(self._message["rotating_frame"]["current"]["peak"]["beta"], self._signal["rotating_frame"]["current"]["peak"]["beta"])

                def u_d_rms(self) -> float:
                    """
                    Reads and returns the RMS value of the D-axis voltage component in the stationary frame.

                    Returns:
                        float: The RMS value of the D-axis voltage component in the stationary frame.
                    """
                    return self._controller.read_periodic_signal(self._message["stationary_frame"]["voltage"]["rms"]["ud"], self._signal["stationary_frame"]["voltage"]["rms"]["ud"])

                def u_q_rms(self) -> float:
                    """
                    Reads and returns the RMS value of the Q-axis voltage component in the stationary frame.

                    Returns:
                        float: The RMS value of the Q-axis voltage component in the stationary frame.
                    """
                    return self._controller.read_periodic_signal(self._message["stationary_frame"]["voltage"]["rms"]["uq"], self._signal["stationary_frame"]["voltage"]["rms"]["uq"])

                def u_d_peak(self) -> float:
                    """
                    Reads and returns the peak value of the D-axis voltage component in the stationary frame.

                    Returns:
                        float: The peak value of the D-axis voltage component in the stationary frame.
                    """
                    return self._controller.read_periodic_signal(self._message["stationary_frame"]["voltage"]["peak"]["ud"], self._signal["stationary_frame"]["voltage"]["peak"]["ud"])

                def u_q_peak(self) -> float:
                    """
                    Reads and returns the peak value of the Q-axis voltage component in the stationary frame.

                    Returns:
                        float: The peak value of the Q-axis voltage component in the stationary frame.
                    """
                    return self._controller.read_periodic_signal(self._message["stationary_frame"]["voltage"]["peak"]["uq"], self._signal["stationary_frame"]["voltage"]["peak"]["uq"])

                def i_d_rms(self) -> float:
                    """
                    Reads and returns the RMS value of the D-axis current component in the stationary frame.

                    Returns:
                        float: The RMS value of the D-axis current component in the stationary frame.
                    """
                    return self._controller.read_periodic_signal(self._message["stationary_frame"]["current"]["rms"]["id"], self._signal["stationary_frame"]["current"]["rms"]["id"])

                def i_q_rms(self) -> float:
                    """
                    Reads and returns the RMS value of the Q-axis current component in the stationary frame.

                    Returns:
                        float: The RMS value of the Q-axis current component in the stationary frame.
                    """
                    return self._controller.read_periodic_signal(self._message["stationary_frame"]["current"]["rms"]["iq"], self._signal["stationary_frame"]["current"]["rms"]["iq"])

                def i_d_peak(self) -> float:
                    """
                    Reads and returns the peak value of the D-axis current component in the stationary frame.

                    Returns:
                        float: The peak value of the D-axis current component in the stationary frame.
                    """
                    return self._controller.read_periodic_signal(self._message["stationary_frame"]["current"]["peak"]["id"], self._signal["stationary_frame"]["current"]["peak"]["id"])

                def i_q_peak(self) -> float:
                    """
                    Reads and returns the peak value of the Q-axis current component in the stationary frame.

                    Returns:
                        float: The peak value of the Q-axis current component in the stationary frame.
                    """
                    return self._controller.read_periodic_signal(self._message["stationary_frame"]["current"]["peak"]["iq"], self._signal["stationary_frame"]["current"]["peak"]["iq"])
