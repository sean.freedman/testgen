import time
from typing import Union

from program.src.constants import DEFAULT_PATHS
from program.src.equipment.can import CanDevice
from program.src.file_ops import read_yaml_file


class DCDC(CanDevice):
    """
    A class that represents a DCDC converter and provides methods to interact with it using CAN communication.

    Attributes
    ----------
    DEFAULT_DCDC_CONFIG : dict
        The default configuration for the DCDC converter.
    DCDC_SINGLE : str
        String representation of the DCDC single topology.
    DCDC_PARALLEL : str
        String representation of the DCDC parallel topology.
    DEFAULT_DCDC_TOPOLOGY : str
        The default DCDC topology to use.
    _topology : str
        The current DCDC topology.
    _config : dict
        The current DCDC configuration.
    _message : dict
        # The messages sent to the DCDC.
    _signal : dict
        Signals received from the DCDC.

    Methods
    -------
    __init__(self, topology=DEFAULT_DCDC_TOPOLOGY, config=DEFAULT_DCDC_CONFIG)
        Initializes the DCDC converter.
    __del__(self)
        Deletes the DCDC converter instance.
    """

    # Read the default YAML configuration file for the DCDC converter
    DEFAULT_DCDC_CONFIG = read_yaml_file(DEFAULT_PATHS["equipment"]["configs"]["power_supplies"]["dcdc"])
    DCDC_SINGLE = "single"
    DCDC_PARALLEL = "parallel"
    DEFAULT_DCDC_TOPOLOGY = DCDC_SINGLE

    def __init__(self, topology=DEFAULT_DCDC_TOPOLOGY, config=DEFAULT_DCDC_CONFIG):
        """
        Initializes the DCDC converter with the specified topology and configuration values.

        Parameters
        ----------
        topology : str, optional
            The DCDC topology model (single or parallel). The default is DEFAULT_DCDC_TOPOLOGY.
        config : dict, optional
            The DCDC configuration. The default is DEFAULT_DCDC_CONFIG.

        Returns
        -------
        None
        """

        # Initialize the superclass CanDevice
        super().__init__()

        # Set the topology and configuration attributes
        self._topology = topology
        self._config = config

        # Extract parameters, limits, CAN messages, and signals from the configuration
        self._parameters = self._config["parameters"]
        self._limits = self._parameters["limits"]
        self._message = self._config["can"]["messages"]
        self._signal = self._config["can"]["signals"]
        self._init_vals = self._config["can"]["initial_values"]
        self._periodic_can_messages: Union[dict, None] = None

        # Set up the CAN bus using the provided configuration
        self._bus = self.setup_can_bus(self._config)

        # Load the CAN message database
        self._db = self.load_database(self._config["can"]["database"])

        # Set the listener to receive CAN messages
        self._listener = self.setup_listener()

        # Set up the notifier to notify the listener when a message is received
        self._notifier = self.setup_notifier(self._bus, [self._listener])

        # Create a write object for the DCDC class to send CAN messages
        self.write = DCDC.Write(self)

        # Initialize periodic CAN messages
        self._periodic_can_messages = self.write.initialise_periodic_messages()

        # Create a read object for the DCDC class to receive CAN messages, have to setup read after periodic messages
        self.read = DCDC.Read(self)

        # Write the periodic CAN messages at a specified time interval based on configuration
        self.write_periodic_message(self._periodic_can_messages, self._config["can"]["period"])

        # Update CAN values on every message received by the listener
        self._listener.on_message_received = self.read.update_can_values

    def __del__(self):
        """
        Clean up the DCDC converter by turning off its output voltage and current limits, disabling its output,
        shutting down the bus, and deleting the instance.

        Returns
        -------
        None
        """

        try:
            # Turn off the output voltage and current limits
            self.write.output_voltage(0.0)
            self.write.output_current_limits(0.0, 0.0)
            time.sleep(0.5)

            # Disable the output and shutdown the bus
            self.write.output_enable(False)
            self._bus.shutdown()

        except Exception as e:
            print(e)

    class Write:
        def __init__(self, dcdc):
            self._dcdc = dcdc
            self._topology = dcdc._topology
            self._message = dcdc._message
            self._signal = dcdc._signal
            self._parameters = dcdc._parameters
            self._init_vals = dcdc._init_vals
            self._limits = dcdc._limits

        def initialise_periodic_messages(self):
            """
            Initialise the periodic CAN messages, using the configuration.

            Returns

            -------
            periodic_can_messages : dict
                The periodic CAN messages.

            """

            periodic_can_messages = {
                # TRANSMITTED MESSAGES
                self._message["one"]["tx"]["control"]["interval"]: {
                    # Control Signals
                    self._signal["tx"]["control"]["interval"]: self._init_vals["tx"]["control"]["interval"],
                    self._signal["tx"]["control"]["mode"]: self._init_vals["tx"]["control"]["mode"],
                    self._signal["tx"]["control"]["word"]: self._init_vals["tx"]["control"]["word"],
                    self._signal["tx"]["control"]["sequence"]: self._init_vals["tx"]["control"]["sequence"],
                },
                # Ref 1
                self._message["one"]["tx"]["current"]["out"]["target"]: {
                    # Current Out Signals
                    self._signal["tx"]["current"]["out"]["target"]: self._init_vals["tx"]["current"]["out"]["target"],
                    # Gains Signals
                    self._signal["tx"]["gains"]["kp"]["positive"]: self._init_vals["tx"]["gains"]["kp"]["positive"],
                    self._signal["tx"]["gains"]["kp"]["negative"]: self._init_vals["tx"]["gains"]["kp"]["negative"],
                    # Voltage In Signals
                    self._signal["tx"]["voltage"]["in"]["set"]: self._init_vals["tx"]["voltage"]["in"]["set"],
                    self._signal["tx"]["voltage"]["in"]["set_low"]: self._init_vals["tx"]["voltage"]["in"]["set_low"],
                },
                # Voltage Out Message
                self._message["one"]["tx"]["voltage"]["out"]["maximum"]: {
                    # Voltage Out Signals
                    self._signal["tx"]["voltage"]["out"]["maximum"]: self._init_vals["tx"]["voltage"]["out"]["maximum"],
                    self._signal["tx"]["voltage"]["out"]["minimum"]: self._init_vals["tx"]["voltage"]["out"]["minimum"],
                    # Current Out Signals
                    self._signal["tx"]["current"]["out"]["positive"]: self._init_vals["tx"]["current"]["out"]["positive"],
                    self._signal["tx"]["current"]["out"]["negative"]: self._init_vals["tx"]["current"]["out"]["negative"],
                },
                # Power Out Message
                self._message["one"]["tx"]["power"]["out"]["set"]: {
                    self._signal["tx"]["power"]["out"]["set"]: self._init_vals["tx"]["power"]["out"],
                },
                # RECEIVED MESSAGES
                # Status Message
                self._message["one"]["rx"]["status"]["words"]["status"]: {
                    self._signal["rx"]["status"]["words"]["status"]: self._init_vals["rx"]["status"]["words"]["status"],
                    self._signal["rx"]["status"]["words"]["warn"]: self._init_vals["rx"]["status"]["words"]["warn"],
                    self._signal["rx"]["status"]["words"]["error_1"]: self._init_vals["rx"]["status"]["words"]["error_1"],
                    self._signal["rx"]["status"]["words"]["error_2"]: self._init_vals["rx"]["status"]["words"]["error_2"],
                    self._signal["rx"]["status"]["control_mode"]: self._init_vals["rx"]["status"]["control_mode"],
                    self._signal["rx"]["status"]["sequence"]: self._init_vals["rx"]["status"]["sequence"],
                },
                # Voltage and currents in / out.
                self._message["one"]["rx"]["voltage"]["in"]: {
                    self._signal["rx"]["voltage"]["in"]: self._init_vals["rx"]["voltage"]["in"],
                    self._signal["rx"]["voltage"]["out"]: self._init_vals["rx"]["voltage"]["out"],
                    self._signal["rx"]["current"]["in"]: self._init_vals["rx"]["current"]["in"],
                    self._signal["rx"]["current"]["out"]: self._init_vals["rx"]["current"]["out"],
                },
                # Power temp / derating
                self._message["one"]["rx"]["power"]["out"]: {
                    self._signal["rx"]["power"]["out"]: self._init_vals["rx"]["power"]["out"],
                    self._signal["rx"]["temperature"]["igbt"]: self._init_vals["rx"]["temperature"]["igbt"],
                    self._signal["rx"]["derate"]["temperature"]: self._init_vals["rx"]["derate"]["temperature"],
                    self._signal["rx"]["derate"]["voltage_dc"]: self._init_vals["rx"]["derate"]["voltage_dc"],
                    self._signal["rx"]["derate"]["voltage_out"]: self._init_vals["rx"]["derate"]["voltage_out"],
                },
            }

            if self._topology == DCDC.DCDC_PARALLEL:
                # update self._periodic_can_messages with parallel topology messages
                periodic_can_messages.update(
                    {
                        # TRANSMITTED MESSAGES
                        self._message["two"]["tx"]["control"]["interval"]: {
                            # Control Signals
                            self._signal["tx"]["control"]["interval"]: self._init_vals["tx"]["control"]["interval"],
                            self._signal["tx"]["control"]["mode"]: self._init_vals["tx"]["control"]["mode"],
                            self._signal["tx"]["control"]["word"]: self._init_vals["tx"]["control"]["word"],
                            self._signal["tx"]["control"]["sequence"]: self._init_vals["tx"]["control"]["sequence"],
                        },
                        # Ref 1
                        self._message["two"]["tx"]["current"]["out"]["target"]: {
                            # Current Out Signals
                            self._signal["tx"]["current"]["out"]["target"]: self._init_vals["tx"]["current"]["out"]["target"],
                            # Gains Signals
                            self._signal["tx"]["gains"]["kp"]["positive"]: self._init_vals["tx"]["gains"]["kp"]["positive"],
                            self._signal["tx"]["gains"]["kp"]["negative"]: self._init_vals["tx"]["gains"]["kp"]["negative"],
                            # Voltage In Signals
                            self._signal["tx"]["voltage"]["in"]["set"]: self._init_vals["tx"]["voltage"]["in"]["set"],
                            self._signal["tx"]["voltage"]["in"]["set_low"]: self._init_vals["tx"]["voltage"]["in"]["set_low"],
                        },
                        # Voltage Out Message
                        self._message["two"]["tx"]["voltage"]["out"]["maximum"]: {
                            # Voltage Out Signals
                            self._signal["tx"]["voltage"]["out"]["maximum"]: self._init_vals["tx"]["voltage"]["out"]["maximum"],
                            self._signal["tx"]["voltage"]["out"]["minimum"]: self._init_vals["tx"]["voltage"]["out"]["minimum"],
                            # Current Out Signals
                            self._signal["tx"]["current"]["out"]["positive"]: self._init_vals["tx"]["current"]["out"]["positive"],
                            self._signal["tx"]["current"]["out"]["negative"]: self._init_vals["tx"]["current"]["out"]["negative"],
                        },
                        # Power Out Message
                        self._message["two"]["tx"]["power"]["out"]["set"]: {
                            self._signal["tx"]["power"]["out"]["set"]: self._init_vals["tx"]["power"]["out"]["set"],
                        },
                        # RECEIVED MESSAGES
                        # Status Message
                        self._message["two"]["rx"]["status"]["words"]["status"]: {
                            self._signal["rx"]["status"]["words"]["status"]: self._init_vals["rx"]["status"]["words"]["status"],
                            self._signal["rx"]["status"]["words"]["warn"]: self._init_vals["rx"]["status"]["words"]["warn"],
                            self._signal["rx"]["status"]["words"]["error_1"]: self._init_vals["rx"]["status"]["words"]["error_1"],
                            self._signal["rx"]["status"]["words"]["error_2"]: self._init_vals["rx"]["status"]["words"]["error_2"],
                            self._signal["rx"]["status"]["control_mode"]: self._init_vals["rx"]["status"]["control_mode"],
                            self._signal["rx"]["status"]["sequence"]: self._init_vals["rx"]["status"]["sequence"],
                        },
                        # Voltage and currents in / out.
                        self._message["two"]["rx"]["voltage"]["in"]: {
                            self._signal["rx"]["voltage"]["in"]: self._init_vals["rx"]["voltage"]["in"],
                            self._signal["rx"]["voltage"]["out"]: self._init_vals["rx"]["voltage"]["out"],
                            self._signal["rx"]["current"]["in"]: self._init_vals["rx"]["current"]["in"],
                            self._signal["rx"]["current"]["out"]: self._init_vals["rx"]["current"]["out"],
                        },
                        # Power temp / derating
                        self._message["two"]["rx"]["power"]["out"]: {
                            self._signal["rx"]["power"]["out"]: self._init_vals["rx"]["power"]["out"],
                            self._signal["rx"]["temperature"]["igbt"]: self._init_vals["rx"]["temperature"]["igbt"],
                            self._signal["rx"]["derate"]["temperature"]: self._init_vals["rx"]["derate"]["temperature"],
                            self._signal["rx"]["derate"]["voltage_dc"]: self._init_vals["rx"]["derate"]["voltage_dc"],
                            self._signal["rx"]["derate"]["voltage_out"]: self._init_vals["rx"]["derate"]["voltage_out"],
                        },
                    }
                )

            return periodic_can_messages

        def reset_can_signals(self):
            """
            Reset the can signals.

            Returns
            -------
            None.

            """
            self._dcdc.write_periodic_signal(self._message["one"]["tx"]["control"]["interval"], self._signal["tx"]["control"]["interval"], self._init_vals["tx"]["control"]["interval"])
            self._dcdc.write_periodic_signal(self._message["one"]["tx"]["control"]["mode"], self._signal["tx"]["control"]["mode"], self._init_vals["tx"]["control"]["mode"])
            self._dcdc.write_periodic_signal(self._message["one"]["tx"]["control"]["word"], self._signal["tx"]["control"]["word"], self._init_vals["tx"]["control"]["word"])
            self._dcdc.write_periodic_signal(self._message["one"]["tx"]["current"]["out"]["target"], self._signal["tx"]["current"]["out"]["target"], self._init_vals["tx"]["current"]["out"]["target"])
            self._dcdc.write_periodic_signal(self._message["one"]["tx"]["gains"]["kp"]["positive"], self._signal["tx"]["gains"]["kp"]["positive"], self._init_vals["tx"]["gains"]["kp"]["positive"])
            self._dcdc.write_periodic_signal(self._message["one"]["tx"]["gains"]["kp"]["negative"], self._signal["tx"]["gains"]["kp"]["negative"], self._init_vals["tx"]["gains"]["kp"]["negative"])
            self._dcdc.write_periodic_signal(self._message["one"]["tx"]["voltage"]["in"]["set"], self._signal["tx"]["voltage"]["in"]["set"], self._init_vals["tx"]["voltage"]["in"]["set"])
            self._dcdc.write_periodic_signal(self._message["one"]["tx"]["voltage"]["in"]["set_low"], self._signal["tx"]["voltage"]["in"]["set_low"], self._init_vals["tx"]["voltage"]["in"]["set_low"])
            self._dcdc.write_periodic_signal(
                self._message["one"]["tx"]["voltage"]["out"]["maximum"], self._signal["tx"]["voltage"]["out"]["maximum"], self._init_vals["tx"]["voltage"]["out"]["maximum"]
            )
            self._dcdc.write_periodic_signal(
                self._message["one"]["tx"]["voltage"]["out"]["minimum"], self._signal["tx"]["voltage"]["out"]["minimum"], self._init_vals["tx"]["voltage"]["out"]["minimum"]
            )
            self._dcdc.write_periodic_signal(
                self._message["one"]["tx"]["current"]["out"]["positive"], self._signal["tx"]["current"]["out"]["positive"], self._init_vals["tx"]["current"]["out"]["positive"]
            )
            self._dcdc.write_periodic_signal(
                self._message["one"]["tx"]["current"]["out"]["negative"], self._signal["tx"]["current"]["out"]["negative"], self._init_vals["tx"]["current"]["out"]["negative"]
            )
            self._dcdc.write_periodic_signal(self._message["one"]["tx"]["power"]["out"]["set"], self._signal["tx"]["power"]["out"]["set"], self._init_vals["tx"]["power"]["out"]["set"])

            if self._topology == DCDC.DCDC_PARALLEL:
                self._dcdc.write_periodic_signal(self._message["two"]["tx"]["control"]["interval"], self._signal["tx"]["control"]["interval"], self._init_vals["control"]["interval"])
                self._dcdc.write_periodic_signal(self._message["two"]["tx"]["control"]["mode"], self._signal["tx"]["control"]["mode"], self._init_vals["control"]["mode"])
                self._dcdc.write_periodic_signal(self._message["two"]["tx"]["control"]["word"], self._signal["tx"]["control"]["word"], self._init_vals["control"]["word"])
                self._dcdc.write_periodic_signal(self._message["two"]["tx"]["current"]["out"]["target"], self._signal["tx"]["current"]["out"]["target"], self._init_vals["current"]["out"]["target"])
                self._dcdc.write_periodic_signal(self._message["two"]["tx"]["gains"]["kp"]["positive"], self._signal["tx"]["gains"]["kp"]["positive"], self._init_vals["gains"]["kp"]["positive"])
                self._dcdc.write_periodic_signal(self._message["two"]["tx"]["gains"]["kp"]["negative"], self._signal["tx"]["gains"]["kp"]["negative"], self._init_vals["gains"]["kp"]["negative"])
                self._dcdc.write_periodic_signal(self._message["two"]["tx"]["voltage"]["in"]["set"], self._signal["tx"]["voltage"]["in"]["set"], self._init_vals["voltage"]["in"]["set"])
                self._dcdc.write_periodic_signal(self._message["two"]["tx"]["voltage"]["in"]["set_low"], self._signal["tx"]["voltage"]["in"]["set_low"], self._init_vals["voltage"]["in"]["set_low"])
                self._dcdc.write_periodic_signal(self._message["two"]["tx"]["voltage"]["out"]["maximum"], self._signal["tx"]["voltage"]["out"]["maximum"], self._init_vals["voltage"]["out"]["maximum"])
                self._dcdc.write_periodic_signal(self._message["two"]["tx"]["voltage"]["out"]["minimum"], self._signal["tx"]["voltage"]["out"]["minimum"], self._init_vals["voltage"]["out"]["minimum"])
                self._dcdc.write_periodic_signal(
                    self._message["two"]["tx"]["current"]["out"]["positive"], self._signal["tx"]["current"]["out"]["positive"], self._init_vals["current"]["out"]["positive"]
                )
                self._dcdc.write_periodic_signal(
                    self._message["two"]["tx"]["current"]["out"]["negative"], self._signal["tx"]["current"]["out"]["negative"], self._init_vals["current"]["out"]["negative"]
                )
                self._dcdc.write_periodic_signal(self._message["two"]["tx"]["power"]["out"]["set"], self._signal["tx"]["power"]["out"]["set"], self._init_vals["power"]["out"]["set"])

        def reset(self):
            """
            Reset the DCDC converter.

            Returns
            -------
            None.

            """
            self._dcdc.write_periodic_signal(self._message["one"]["tx"]["control"]["word"], self._signal["tx"]["control"]["word"], 0x2)

            if self._topology == DCDC.DCDC_PARALLEL:
                self._dcdc.write_periodic_signal(self._message["two"]["tx"]["control"]["word"], self._signal["tx"]["control"]["word"], 0x2)

        def output_enable(self, enabled):
            """
            Set the output enable.

            Parameters
            ----------
            value :
                The output enable.

            Returns
            -------
            None.

            """
            control_word = 0x19

            if enabled:
                self.reset()
                time.sleep(1)
                control_word |= 0x4

            else:
                self.output_voltage(0.0)
                time.sleep(1)

            self._dcdc.write_periodic_signal(self._message["one"]["tx"]["control"]["word"], self._signal["tx"]["control"]["word"], control_word)

            if self._topology == DCDC.DCDC_PARALLEL:
                self._dcdc.write_periodic_signal(self._message["two"]["tx"]["control"]["word"], self._signal["tx"]["control"]["word"], control_word)

        def output_current_limits(self, current_limit_positive, current_limit_negative):
            """
            Set the output current limits.

            Parameters
            ----------
            current_limit :
                The output current limit.

            Returns
            -------
            None.

            """
            # Set current limits to absolute values, as the dcdc handles the sign internally
            current_limit_positive = abs(current_limit_positive)
            current_limit_negative = abs(current_limit_negative)

            # Set current limits
            self._dcdc.write_periodic_signal(self._message["one"]["tx"]["current"]["out"]["positive"], self._signal["tx"]["current"]["out"]["positive"], current_limit_positive)
            self._dcdc.write_periodic_signal(self._message["one"]["tx"]["current"]["out"]["negative"], self._signal["tx"]["current"]["out"]["negative"], current_limit_negative)

            if self._topology == DCDC.DCDC_PARALLEL:
                self._dcdc.write_periodic_signal(self._message["two"]["tx"]["current"]["out"]["positive"], self._signal["tx"]["current"]["out"]["positive"], current_limit_positive)
                self._dcdc.write_periodic_signal(self._message["two"]["tx"]["current"]["out"]["negative"], self._signal["tx"]["current"]["out"]["negative"], current_limit_negative)

        def output_voltage(self, voltage_target):
            """
            Set the output voltage.

            Parameters
            ----------
            voltage_target : float
                The output voltage target.

            Returns
            -------
            None.

            """
            # Set voltage target to absolute value.
            voltage_max = abs(voltage_target)

            # Set minimum voltage to 10V below maximum voltage.
            voltage_min = max(voltage_max - 10.0, 0.0)

            if voltage_max < self._init_vals["tx"]["voltage"]["out"]["maximum"]:
                voltage_max = self._init_vals["tx"]["voltage"]["out"]["maximum"]

            if voltage_min < self._init_vals["tx"]["voltage"]["out"]["minimum"]:
                voltage_min = self._init_vals["tx"]["voltage"]["out"]["minimum"]

            # Set voltage target maximuim
            self._dcdc.write_periodic_signal(self._message["one"]["tx"]["voltage"]["out"]["maximum"], self._signal["tx"]["voltage"]["out"]["maximum"], voltage_max)
            if self._topology == DCDC.DCDC_PARALLEL:
                self._dcdc.write_periodic_signal(self._message["two"]["tx"]["voltage"]["out"]["maximum"], self._signal["tx"]["voltage"]["out"]["maximum"], voltage_max)

            # Set voltage target minimum
            self._dcdc.write_periodic_signal(self._message["one"]["tx"]["voltage"]["out"]["minimum"], self._signal["tx"]["voltage"]["out"]["minimum"], voltage_min)
            if self._topology == DCDC.DCDC_PARALLEL:
                self._dcdc.write_periodic_signal(self._message["two"]["tx"]["voltage"]["out"]["minimum"], self._signal["tx"]["voltage"]["out"]["minimum"], voltage_min)

    class Read:
        def __init__(self, dcdc):
            self._dcdc = dcdc
            self._topology = self._dcdc._topology
            self._message = self._dcdc._message
            self._signal = self._dcdc._signal
            self._parameters = self._dcdc._parameters
            self._limits = self._dcdc._limits
            self._periodic_can_messages = self._dcdc._periodic_can_messages

        def update_can_values(self, message_received):
            self._dcdc.read_can_values(self._periodic_can_messages, message_received)

        def input_voltages(self) -> tuple:
            """
            Get the input voltages.

            Returns
            -------
            tuple
                The input voltages.

            """

            voltage_value = (0.0, 0.0)

            if self._topology == DCDC.DCDC_SINGLE:
                voltage_value = (self._dcdc.read_periodic_signal(self._periodic_can_messages, self._message["one"]["rx"]["voltage"]["in"], self._signal["rx"]["voltage"]["in"]), None)

            elif self._topology == DCDC.DCDC_PARALLEL:
                voltage_value = (
                    self._dcdc.read_periodic_signal(self._periodic_can_messages, self._message["one"]["rx"]["voltage"]["in"], self._signal["rx"]["voltage"]["in"]),
                    self._dcdc.read_periodic_signal(self._periodic_can_messages, self._message["two"]["rx"]["voltage"]["in"], self._signal["rx"]["voltage"]["in"]),
                )

            return voltage_value

        def average_input_voltage(self) -> float:
            """
            Get the average input voltage.

            Returns
            -------
            float
                The average input voltage.

            """

            voltage_value = (0.0, 0.0)

            if self._topology == DCDC.DCDC_SINGLE:
                voltage_value = self._dcdc.read_periodic_signal(self._periodic_can_messages, self._message["one"]["rx"]["voltage"]["in"], self._signal["rx"]["voltage"]["in"])
                voltage_value = (voltage_value, voltage_value)

            elif self._topology == DCDC.DCDC_PARALLEL:
                voltage_value = (
                    self._dcdc.read_periodic_signal(self._periodic_can_messages, self._message["one"]["rx"]["voltage"]["in"], self._signal["rx"]["voltage"]["in"]),
                    self._dcdc.read_periodic_signal(self._periodic_can_messages, self._message["two"]["rx"]["voltage"]["in"], self._signal["rx"]["voltage"]["in"]),
                )

            voltage_value = (voltage_value[0] + voltage_value[1]) / len(voltage_value)

            return voltage_value

        def input_currents(self) -> tuple:
            """
            Get the input current.

            Returns
            -------
            tuple
                The input current.

            """

            current_value = (0.0, 0.0)

            if self._topology == DCDC.DCDC_SINGLE:
                current_value = (self._dcdc.read_periodic_signal(self._periodic_can_messages, self._message["one"]["rx"]["current"]["in"], self._signal["rx"]["current"]["in"]), None)

            elif self._topology == DCDC.DCDC_PARALLEL:
                current_value = (
                    self._dcdc.read_periodic_signal(self._periodic_can_messages, self._message["one"]["rx"]["current"]["in"], self._signal["rx"]["current"]["in"]),
                    self._dcdc.read_periodic_signal(self._periodic_can_messages, self._message["two"]["rx"]["current"]["in"], self._signal["rx"]["current"]["in"]),
                )

            return current_value

        def average_input_current(self) -> float:
            """
            Get the average input current.

            Returns
            -------
            float
                The average input current.

            """

            current_value = (0.0, 0.0)

            if self._topology == DCDC.DCDC_SINGLE:
                current_value = self._dcdc.read_periodic_signal(self._periodic_can_messages, self._message["one"]["rx"]["current"]["in"], self._signal["rx"]["current"]["in"])
                current_value = (current_value, current_value)

            elif self._topology == DCDC.DCDC_PARALLEL:
                current_value = (
                    self._dcdc.read_periodic_signal(self._periodic_can_messages, self._message["one"]["rx"]["current"]["in"], self._signal["rx"]["voltage"]["in"]),
                    self._dcdc.read_periodic_signal(self._periodic_can_messages, self._message["two"]["rx"]["current"]["in"], self._signal["rx"]["voltage"]["in"]),
                )

            current_value = (current_value[0] + current_value[1]) / len(current_value)

            return current_value

        def output_voltages(self) -> tuple:
            """
            Get the output voltages of the DCDC converter.

            Returns
            -------
            tuple
                The output voltages of the DCDC converter.
            """

            voltage_value = (0.0, 0.0)

            if self._topology == DCDC.DCDC_SINGLE:
                voltage_value = (self._dcdc.read_periodic_signal(self._periodic_can_messages, self._message["one"]["rx"]["voltage"]["out"], self._signal["rx"]["voltage"]["out"]), None)

            elif self._topology == DCDC.DCDC_PARALLEL:
                voltage_value = (
                    self._dcdc.read_periodic_signal(self._periodic_can_messages, self._message["one"]["rx"]["voltage"]["out"], self._signal["rx"]["voltage"]["out"]),
                    self._dcdc.read_periodic_signal(self._periodic_can_messages, self._message["two"]["rx"]["voltage"]["out"], self._signal["rx"]["voltage"]["out"]),
                )

            return voltage_value

        def average_output_voltage(self) -> float:
            """
            Get the average output voltage of the DCDC converter.

            Returns
            -------
            float
                The average output voltage of the DCDC converter.

            """

            voltage_value = (0.0, 0.0)

            if self._topology == DCDC.DCDC_SINGLE:
                voltage_value = self._dcdc.read_periodic_signal(self._periodic_can_messages, self._message["one"]["rx"]["voltage"]["out"], self._signal["rx"]["voltage"]["out"])
                voltage_value = (voltage_value, voltage_value)

            elif self._topology == DCDC.DCDC_PARALLEL:
                voltage_value = (
                    self._dcdc.read_periodic_signal(self._periodic_can_messages, self._message["one"]["rx"]["voltage"]["out"], self._signal["rx"]["voltage"]["out"]),
                    self._dcdc.read_periodic_signal(self._periodic_can_messages, self._message["two"]["rx"]["voltage"]["out"], self._signal["rx"]["voltage"]["out"]),
                )

            voltage_value = (voltage_value[0] + voltage_value[1]) / len(voltage_value)

            return voltage_value

        def output_currents(self) -> tuple:
            """
            Get the output currents of the DCDC converter.

            Returns
            -------
            tuple
                The output currents of the DCDC converter.

            """

            current_value = (0.0, 0.0)

            if self._topology == DCDC.DCDC_SINGLE:
                current_value = (self._dcdc.read_periodic_signal(self._periodic_can_messages, self._message["one"]["rx"]["current"]["out"], self._signal["rx"]["current"]["out"]), None)

            elif self._topology == DCDC.DCDC_PARALLEL:
                current_value = (
                    self._dcdc.read_periodic_signal(self._periodic_can_messages, self._message["one"]["rx"]["current"]["out"], self._signal["rx"]["current"]["out"]),
                    self._dcdc.read_periodic_signal(self._periodic_can_messages, self._message["two"]["rx"]["current"]["out"], self._signal["rx"]["current"]["out"]),
                )

            return current_value

        def average_output_current(self) -> float:
            """
            Get the average output current.

            Returns
            -------
            float
                The average output current.

            """

            current_value = (0.0, 0.0)

            if self._topology == DCDC.DCDC_SINGLE:
                current_value = self._dcdc.read_periodic_signal(self._periodic_can_messages, self._message["one"]["rx"]["current"]["out"], self._signal["rx"]["current"]["out"])
                current_value = (current_value, current_value)

            elif self._topology == DCDC.DCDC_PARALLEL:
                current_value = (
                    self._dcdc.read_periodic_signal(self._periodic_can_messages, self._message["one"]["rx"]["current"]["out"], self._signal["rx"]["current"]["out"]),
                    self._dcdc.read_periodic_signal(self._periodic_can_messages, self._message["one"]["rx"]["current"]["out"], self._signal["rx"]["current"]["out"]),
                )

            # TODO: Better in loop ie if there was three dcdc.
            current_value = (current_value[0] + current_value[1]) / len(current_value)

            return current_value

        def igbt_temperatures(self) -> tuple:
            """
            Get the IGBT temperatures of the DCDC.

            Returns
            -------
            tuple
                The IGBT temperatures of the DCDC.

            """

            igbt_temperature = (0.0, 0.0)

            if self._topology == DCDC.DCDC_SINGLE:
                igbt_temperature = (self._dcdc.read_periodic_signal(self._periodic_can_messages, self._message["one"]["rx"]["temperature"]["igbt"], self._signal["rx"]["temperature"]["igbt"]), None)

            elif self._topology == DCDC.DCDC_PARALLEL:
                igbt_temperature = (
                    self._dcdc.read_periodic_signal(self._periodic_can_messages, self._message["one"]["rx"]["temperature"]["igbt"], self._signal["rx"]["temperature"]["igbt"]),
                    self._dcdc.read_periodic_signal(self._periodic_can_messages, self._message["two"]["rx"]["temperature"]["igbt"], self._signal["rx"]["temperature"]["igbt"]),
                )

            return igbt_temperature

        def average_igbt_temperature(self) -> float:
            """
            Get the average IGBT temperature.

            Returns
            -------
            float
                The average IGBT temperature.

            """

            igbt_temperature = (0.0, 0.0)

            if self._topology == DCDC.DCDC_SINGLE:
                igbt_temperature = self._dcdc.read_periodic_signal(self._periodic_can_messages, self._message["one"]["rx"]["temperature"]["igbt"], self._signal["rx"]["temperature"]["igbt"])
                igbt_temperature = (igbt_temperature, igbt_temperature)

            elif self._topology == DCDC.DCDC_PARALLEL:
                igbt_temperature = (
                    self._dcdc.read_periodic_signal(self._periodic_can_messages, self._message["one"]["rx"]["temperature"]["igbt"], self._signal["rx"]["temperature"]["igbt"]),
                    self._dcdc.read_periodic_signal(self._periodic_can_messages, self._message["two"]["rx"]["temperature"]["igbt"], self._signal["rx"]["temperature"]["igbt"]),
                )

            igbt_temperature = (igbt_temperature[0] + igbt_temperature[1]) / len(igbt_temperature)

            return igbt_temperature
