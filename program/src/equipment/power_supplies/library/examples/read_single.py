from program.src.constants import DEFAULT_PATHS
from program.src.equipment.power_supplies.library.dcdc import DCDC
from program.src.file_ops import read_yaml_file

print("Start of example script to read the DCDC power supply")

dcdc_path = DEFAULT_PATHS["equipment"]["configs"]["power_supplies"]["dcdc"]
topology = DCDC.DCDC_SINGLE

print(f"DCDC path: {dcdc_path}")
print(f"DCDC topology: {topology}")

config = read_yaml_file(dcdc_path)


# Create an instance of the DCDC class, pass the path to the config file and connect to the device.
dcdc = DCDC(topology, config["dcdc"])

print("Start of print statements for DCDC object")

print(f"DCDC BUS: {dcdc.bus}")
print(f"DCDC Database: {dcdc.db}")

# Input Voltages
print(f"DCDC Input Voltage: {dcdc.read.input_voltages()}")
print(f"DCDC Average Input Voltage: {dcdc.read.average_input_voltage()}")

# Input Currents
print(f"DCDC Input Current: {dcdc.read.input_currents()}")
print(f"DCDC Average Input Current: {dcdc.read.average_input_current()}")

# Output Voltages
print(f"DCDC Output Voltage: {dcdc.read.output_voltages()}")
print(f"DCDC Average Output Voltage: {dcdc.read.average_output_voltage()}")

# Output Currents
print(f"DCDC Output Current: {dcdc.read.output_currents()}")
print(f"DCDC Average Output Current: {dcdc.read.average_output_current()}")

# Igbt Temperatures
print(f"DCDC IGBT Temperature: {dcdc.read.igbt_temperatures()}")
print(f"DCDC Average IGBT Temperature: {dcdc.read.average_igbt_temperature()}")

print("End of print statements for DCDC object")

print("End of example script to read the DCDC power supply")
