import time

import can  # https://python-can.readthedocs.io/en/stable/index.html
import cantools  # https://cantools.readthedocs.io/en/latest/#

# Used to zero torque transducer
# +---------------------------------+--------------------+-------------+------------+--------------+--------+
# |           Description           | Byte 0:1 (Command) |    Byte 2   |  Byte 3:4  |   Byte 5:6   | Byte 7 |
# +---------------------------------+--------------------+-------------+------------+--------------+--------+
# | Set zero balancing of a channel | 0x8040             | Recorder ID | Channel ID | Zero control | X      |
# +---------------------------------+--------------------+-------------+------------+--------------+--------+

lastVal = {}


def load_database(database_path):
    """
    Load a can database.

    Parameters
    ----------
    database_path : str
        The path to the can database.

    Returns
    -------
    db : cantools.database.Database
        The can database.

    """

    return cantools.database.load_file(database_path)


def zero_torque_measurement(bus):
    """
    Zero the torque measurement.

    Parameters
    ----------
    bus : can.interface.Bus
        The can bus.

    Returns None

    """

    bus.send(
        can.Message(
            arbitration_id=0x3DD,
            data=[0x80, 0x40, 0x01, 0x00, 0x08, 0x00, 0x01, 0x00],
            is_extended_id=False,
            is_remote_frame=False,
            is_error_frame=False,
            dlc=8,
            is_fd=False,
            is_rx=False,
        )
    )
    return


# Setup Bus
def setup_can_bus(interface="vector", channel=1, baud_rate=1000000, filters=None):
    # create docstring
    """
    Setup a can bus.

    Parameters
    ----------
    interface : str, optional
        The can interface. The default is "vector".
    channel : int, optional
        The can channel. The default is 1.
    baud_rate : int, optional
        The can baud rate. The default is 1000000.
    filters : list, optional
        The can filters. The default is None.

    Returns
    -------
    bus : can.interface.Bus
        The can bus.

    """

    if interface == "vector":
        channel = channel - 1  # channel 1 in vector is channel 0
    return can.interface.Bus(bustype=interface, channel=channel, bitrate=baud_rate, filters=filters)


def receive(db, bus, signal_id, signal_name):
    """
    Receive a can signal.

    Parameters
    ----------
    db : cantools.database.Database
        The can database.
    bus : can.interface.Bus
        The can bus.
    signal_id : int
        The can signal id.
    signal_name : str
        The can signal name.

    Returns
    -------
    lastVal[signal_id] : int
        The can signal value (or last value if None found).

    """
    global lastVal
    if signal_id not in lastVal.keys():
        lastVal[signal_id] = 0

    while time.time() < time.time() + 1:
        message = bus.recv(0)
        if message is not None:
            if message.arbitration_id == signal_id:
                message_data = db.decode_message(message.arbitration_id, message.data)
                lastVal[signal_id] = message_data.get(signal_name)
        else:
            break
    return lastVal[signal_id]
