import time

import serial
from serial.tools import list_ports

# Make into class  simular to the Can classes


def voltage_target(tenma, v_target: float = 16.00):
    # create docstring
    """
    Set voltage target.

    Parameters
    ----------
    tenma : _type_
        _description_
    v_target : float, optional
        _description_. The default is 16.00.

    Returns
    -------
    _type_
        _description_

    """

    tenma.write(str.encode("BEEP1"))
    time.sleep(0.1)
    return tenma.write(str.encode("VSET1:" + str(v_target)))


def enable_output(tenma):
    # create docstring
    """
    Set Tenma output to enable.

    Parameters
    ----------
    tenma : _type_
        _description_

    Returns
    -------
    _type_
        _description_

    """

    time.sleep(0.1)
    return tenma.write(str.encode("OUT1"))


def disable_output(tenma):
    """
    Set Tenma output to disable.

    Parameters
    ----------
    tenma : _type_
        _description_

    Returns
    -------
    _type_
        _description_

    """

    time.sleep(0.1)
    return tenma.write(str.encode("OUT0"))


def get_device():
    """
    Search for a tenma psu

    Returns
    -------
    tenma : _type_
        _description_

    """

    foundPorts = list(list_ports.comports())

    for port in foundPorts:
        tempPort = serial.Serial(timeout=1)
        tempPort.port = port.device
        tempPort.open()
        if tempPort.is_open is True:
            tempPort.write(str.encode("*IDN?"))
            time.sleep(0.1)

            if "TENMA" in str(tempPort.read(5)):
                tenma = tempPort
                time.sleep(0.1)
                break
            else:
                pass

        else:
            pass

    return tenma


def release_device(tenma):
    """
    Release a tenma psu

    Parameters
    ----------
    tenma : _type_
        _description_

    Returns
    -------
    _type_
        _description_

    """

    tenma.close()

    return


def key_on(tenma, voltage: float = 16.00):
    # create docstring
    """
    Set voltage target and enable output.

    Parameters
    ----------
    tenma : _type_
        _description_
    voltage : float, optional
        _description_. The default is 16.00.

    Returns
    -------
    _type_
        _description_

    """

    disable_output(tenma)
    # time to let micro reset
    time.sleep(2)

    voltage_target(tenma, 0.0)
    time.sleep(1)
    voltage_target(tenma, voltage)
    enable_output(tenma)
