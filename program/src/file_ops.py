import os
import shutil
import zipfile
from os.path import exists

import pandas as pd
import yaml


def read_yaml_file(file_path):
    """
    Reads a yaml file and returns a dictionary.

    Args:
        file_path (str): The path to the yaml file.

    Returns:
        dict: The dictionary of the yaml file.
    """
    with open(file_path, "r") as file:
        return yaml.load(file, Loader=yaml.FullLoader)


def write_yaml_file(file_path, data):
    """
    Writes a dictionary to a yaml file.

    Args:
        file_path (str): The path to the yaml file.
        data (dict): The dictionary to write to the yaml file.

    Returns:
        None
    """

    with open(file_path, "w") as file:
        yaml.dump(data, file, default_flow_style=False)


def csv_to_dict(path, dict):
    """
    Get the I sense table from path, if does not exsists, fill.
    """

    if exists(path):
        data = pd.read_csv(filepath_or_buffer=path)

    else:
        data = [dict]

    data = pd.DataFrame(data)

    return data


def zip_report(report_path, zip_path):
    """
    Add report to compressed archive on the input path.
    """
    try:
        with zipfile.ZipFile(file=zip_path, mode="w", compression=zipfile.ZIP_DEFLATED, compresslevel=6) as archive:
            archive.write(filename=report_path, arcname=report_path)

            archive.close()
            success = True

    except:
        success = False

    return success


def delete_file(file_path):
    """
    Delete file on input path.

    Args:
        file_path (str): The path to the file to delete.

    Returns:
        None

    """
    try:
        os.remove(file_path)
        print("File Deleted")

    except Exception as e:
        print(e)
        print("Could not Delete File")


def move_file(source_path, destination_path):
    """
    Move file from source to destination.

    Args:
        source_path (str): The path to the file to move.
        destination_path (str): The path to move the file to.

        Returns:
            bool: True if the file was moved, False if not.

    """
    try:
        shutil.move(src=source_path, dst=destination_path)

        message = True

    except Exception as e:
        message = e
        print("Could not Move File")

    return message


def sort_asc_col(df):
    """
    Sort a pandas DataFrame by column name in ascending order.

    Args:
        df (pandas.DataFrame): The DataFrame to sort.

    Returns:
        pandas.DataFrame: The sorted DataFrame.
    """

    col = list(df.columns.values)
    col.sort(reverse=True)
    df = df[col]

    return df


def csv_to_series(file_path, series_dictionary):
    """
    Read a CSV file and return it as a pandas Series, if the file does not exist, create it with the series dictionary.

    Args:
        file_path (str): The path to the CSV file.
        series_dictionary (dict): The dictionary to create the CSV file with if it does not exist.

    Returns:
        pandas.Series: The pandas Series of the CSV file.

    """

    if exists(file_path):
        data = pd.read_csv(file_path)
        data = data.squeeze()

    else:
        data = pd.Series(series_dictionary)

    data = pd.DataFrame(data)

    return data


def file_merge(first_file_path, second_file_path):
    """
    Merge two files together.

    Args:
        first_file_path (str): The path to the first file.
        second_file_path (str): The path to the second file.

    Returns:
        None

    """

    f1 = open(first_file_path, "a+")
    f1.seek(0)
    f2 = open(second_file_path, "r")

    f1.write(f2.read())

    f1.close()
    f2.close()
