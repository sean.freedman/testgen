import os

import numpy as np
import pandas as pd
import streamlit as st
from scipy.interpolate import interp2d
from src.constants import BRIDGE_DISABLE, BRIDGE_ENABLE, FIXED_POINT_SCALE_D, FIXED_POINT_SCALE_Q, STATUS_WORD_DISABLED, STATUS_WORD_ENABLED


def torque_speed_ref(gen_cfg):
    """
    generate torque speed references
    """
    torque_demand = []
    demand_period = []
    speed_demand = []
    speed_lim_fwd = []
    speed_lim_rev = []
    test_point = []

    # Torque
    torque_max = gen_cfg["torque"]["maximum"] * gen_cfg["torque"]["direction"]
    torque_min = gen_cfg["torque"]["minimum"] * gen_cfg["torque"]["direction"]
    torque_step_up = gen_cfg["torque"]["ramp_up_value"] * gen_cfg["torque"]["direction"]
    torque_step_down = gen_cfg["torque"]["ramp_down_value"] * gen_cfg["torque"]["direction"]
    torque_demand_period_up = gen_cfg["torque"]["ramp_up_period"]
    torque_demand_period_down = gen_cfg["torque"]["ramp_down_period"]
    torque_direction = gen_cfg["torque"]["direction"]
    torque_skip = gen_cfg["torque"]["skip"]

    # Speed
    speed_lim_fwd_threshold = gen_cfg["speed"]["limit_value"]
    speed_lim_rev_threshold = gen_cfg["speed"]["limit_value"]
    speed_lim_method = gen_cfg["speed"]["Limit Type"]
    speed_step = gen_cfg["speed"]["step"]
    speed_direction = gen_cfg["speed"]["direction"]
    speed_step_current = gen_cfg["speed"]["minimum"] * speed_direction
    speed_max_table = gen_cfg["speed"]["max_table"]
    speed_max_requested = gen_cfg["speed"]["maximum"]

    # Voltage
    voltage_target = gen_cfg["voltage"]["voltage"]

    # Logging
    log_up = gen_cfg["logging"]["Log Up"]
    log_down = gen_cfg["logging"]["Log Down"]

    # Interoplation
    interp_function = gen_cfg["Interpolation"]["Function"]

    if speed_lim_method == "Percentage":
        speed_lim_fwd_current = max(speed_step_current * speed_lim_fwd_threshold, 0)
        speed_lim_rev_current = min(speed_step_current * speed_lim_rev_threshold, 0)

    speed_max = min(abs(speed_max_table), abs(speed_max_requested)) * speed_direction

    # Start of Loop
    while abs(speed_step_current) <= abs(speed_max):
        # Get Torque Max value after percentage gain applied
        torque_max_available = interp_function(voltage_target, speed_step_current)
        torque_max_available = torque_max_available[0] * torque_max

        # Every speed step, start torque from minimum torque step
        torque_step_current = torque_min

        if torque_direction > 0:
            # Start torque increment
            while torque_step_current < torque_max_available:
                # Store current values
                torque_demand.append(torque_step_current)
                demand_period.append(torque_demand_period_up)
                speed_demand.append(speed_step_current)
                speed_lim_fwd.append(speed_lim_fwd_current)
                speed_lim_rev.append(speed_lim_rev_current)
                test_point.append(log_up)

                # Step torque up using requested torque step
                torque_step_current = torque_step_current + torque_step_up

                # If torque plus the step is going to be above the amaximum available torque
                # Take a incremenet off for the ramp down, else there would be two points at the maximum torque.
                if torque_step_current >= torque_max_available:
                    torque_step_current = torque_step_current - torque_step_up
                    break

            if (torque_step_current >= torque_max_available) and (torque_skip is False):
                torque_step_current = torque_max_available

                # Store current values
                torque_demand.append(torque_step_current)
                demand_period.append(torque_demand_period_up)
                speed_demand.append(speed_step_current)
                speed_lim_fwd.append(speed_lim_fwd_current)
                speed_lim_rev.append(speed_lim_rev_current)
                test_point.append(log_up)

                # ramp down
            while torque_step_current > torque_min:
                torque_step_current = torque_step_current - torque_step_down

                if torque_step_current <= torque_min:
                    torque_step_current = 0

                    # Store current values
                    torque_demand.append(torque_step_current)
                    demand_period.append(torque_demand_period_down)
                    speed_demand.append(speed_step_current)
                    speed_lim_fwd.append(speed_lim_fwd_current)
                    speed_lim_rev.append(speed_lim_rev_current)
                    test_point.append(log_down)
                    break

                # Store current values
                torque_demand.append(torque_step_current)
                demand_period.append(torque_demand_period_down)
                speed_demand.append(speed_step_current)
                speed_lim_fwd.append(speed_lim_fwd_current)
                speed_lim_rev.append(speed_lim_rev_current)
                test_point.append(log_down)

        if torque_direction < 0:
            # Start torque increment

            while torque_step_current > torque_max_available:
                # Store current values
                torque_demand.append(torque_step_current)
                demand_period.append(torque_demand_period_up)
                speed_demand.append(speed_step_current)
                speed_lim_fwd.append(speed_lim_fwd_current)
                speed_lim_rev.append(speed_lim_rev_current)
                test_point.append(log_up)

                # Step torque up using requested torque step
                torque_step_current = torque_step_current + torque_step_up

                # If torque plus the step is going to be above the amaximum available torque
                # Take a incremenet off for the ramp down, else there would be two points at the maximum torque.
                if torque_step_current <= torque_max_available:
                    torque_step_current = torque_step_current - torque_step_up
                    break

            if (torque_step_current <= torque_max_available) and (torque_skip is False):
                torque_step_current = torque_max_available

                # Store current values
                torque_demand.append(torque_step_current)
                demand_period.append(torque_demand_period_up)
                speed_demand.append(speed_step_current)
                speed_lim_fwd.append(speed_lim_fwd_current)
                speed_lim_rev.append(speed_lim_rev_current)
                test_point.append(log_up)

            while torque_step_current < torque_min:
                torque_step_current = torque_step_current - torque_step_down

                if torque_step_current >= torque_min:
                    torque_step_current = 0

                    # Store current values
                    torque_demand.append(torque_step_current)
                    demand_period.append(torque_demand_period_down)
                    speed_demand.append(speed_step_current)
                    speed_lim_fwd.append(speed_lim_fwd_current)
                    speed_lim_rev.append(speed_lim_rev_current)
                    test_point.append(log_down)
                    break

                # Store current values
                torque_demand.append(torque_step_current)
                demand_period.append(torque_demand_period_down)
                speed_demand.append(speed_step_current)
                speed_lim_fwd.append(speed_lim_fwd_current)
                speed_lim_rev.append(speed_lim_rev_current)
                test_point.append(log_down)

        speed_step_current = speed_step_current + speed_step * speed_direction

        if speed_lim_method == "Percentage":
            speed_lim_fwd_current = max(speed_step_current * speed_lim_fwd_threshold, 0)
            speed_lim_rev_current = min(speed_step_current * speed_lim_rev_threshold, 0)

    ref = pd.DataFrame(
        data={
            "torque_demand": torque_demand,
            "demand_period": demand_period,
            "speed_demand": speed_demand,
            "speed_lim_fwd": speed_lim_fwd,
            "speed_lim_rev": speed_lim_rev,
            "test_point": test_point,
        }
    )

    return ref


def envolope_ref(torque_dir, speed_dir, speed_breakpoints, voltage_ref, interp_function):
    """
    generate envelope references
    """

    torque_envelope = []
    speed_envelope = []

    speed_max = max(speed_breakpoints) * speed_dir
    envolope_speed = np.linspace(0, max(speed_breakpoints) * speed_dir, 50)

    speed_envelope.append(0)
    torque_envelope.append(0)

    torque_max_available = 0

    for speeds in envolope_speed:
        torque_max_available = interp_function(voltage_ref, speeds)
        torque_max_available = torque_max_available[0]

        torque_envelope.append(torque_max_available * torque_dir)
        speed_envelope.append(speeds)

    speed_envelope.append(speed_max)
    torque_envelope.append(0)

    speed_envelope.append(0)
    torque_envelope.append(0)

    env = pd.DataFrame(data={"torque_envelope": torque_envelope, "speed_envelope": speed_envelope})

    return env


def idq_ref(ref_dict):
    """
    Returns a Dataframe of Iq, Id, Dyno Speed Demand,  MCU Speed Limit Forward,MCU Speed Limit Reverse.

        Parameters:
            idq_mode (string)       : The method to generate Idq Injection Values: [idq] - Generate a grid of Iq (+/-), Id (-) Values, whose combination shall not exceed that defined by the maximum_current, [angle] - Generate  Iq (+/-), Id (-) Values by creating a vector whos maginude and angle is sweeped,the magnitude will not exceed that defined by the maximum current
            maximum_current (float) : The maximum stator current defined by the lower of; Inverter Manufacturer, Motor Manufacturer.
            iq_dir (int)             : Interger to determine Iq current generation.
            speed_val (float)        : Speed point to generate at.
            speed_dir (int)          : Interger to determine speed related reference points.
            speed_lim_gain (float)    : Gain used to determine how far away the speed limit is to the speed demand.
            current_step (float)           : The step between the current Idq value and next during generation.
            demand_period (float)   : Time between the injection points

        Returns:
            ref (Dataframe)         :  Dataframe of Iq, Id, Dyno Speed Demand,  MCU Speed Limit Forward,MCU Speed Limit Reverse.
    """

    # Initialise Variables
    iq_val = 0

    # Initilise Lists
    iq_inject = []
    id_inject = []
    is_inject = []
    speed_demand = []
    speed_lim_fwd = []
    speed_lim_rev = []
    time_period = []
    test_point = []

    log_up = True
    log_down = False

    iq_dir = ref_dict["idq"]["iq_direction"]
    maximum_current = ref_dict["idq"]["maximum"]
    speed_val = ref_dict["speed"]["target"]
    speed_dir = ref_dict["speed"]["direction"]
    speed_lim_gain = ref_dict["speed"]["limit"]
    time_val = ref_dict["idq"]["step_period"]
    current_step = ref_dict["idq"]["step"]

    step_down_method = ref_dict["idq"]["step_method"]
    step_up_method = ref_dict["idq"]["step_method"]
    ramp_period = ref_dict["idq"]["ramp_period"]

    # If the Iq direction is positive
    if iq_dir > 0:
        # While the Iq Value is less than the stator current
        while iq_val <= maximum_current * iq_dir:
            id_val = 0

            if step_up_method == "Ramp":
                iq_val_ramp = 0
                id_val_ramp = id_val

                if iq_val != 0:
                    while iq_val_ramp != iq_val - current_step:
                        iq_val_ramp = iq_val_ramp + current_step * iq_dir
                        iq_inject.append(iq_val_ramp)
                        id_inject.append(id_val_ramp)
                        is_inject.append(np.sqrt((id_val_ramp**2) + (iq_val_ramp**2)))
                        speed_demand.append(speed_val * speed_dir)
                        speed_lim_fwd.append(speed_val * (speed_lim_gain / 100))
                        speed_lim_rev.append(speed_val * (speed_lim_gain / 100))
                        time_period.append(ramp_period)
                        test_point.append(log_down)

            # While the Id Value is then the stator current (*-1)
            while id_val >= maximum_current * -1:
                # If the stator current for the combination of Idq Values is going to be greater than the Maximum stator current, stop.
                if np.sqrt((id_val**2) + (iq_val**2)) > maximum_current:
                    if step_down_method == "Ramp":
                        iq_val_ramp = iq_val
                        id_val_ramp = id_val
                        while np.sqrt((id_val_ramp**2) + (iq_val_ramp**2)) > 0:
                            if iq_val_ramp != 0:
                                iq_val_ramp = iq_val_ramp - (current_step * iq_dir)
                            if id_val_ramp != 0:
                                id_val_ramp = id_val_ramp + (current_step)

                            iq_inject.append(iq_val_ramp)
                            id_inject.append(id_val_ramp)
                            is_inject.append(np.sqrt((id_val_ramp**2) + (iq_val_ramp**2)))
                            speed_demand.append(speed_val * speed_dir)
                            speed_lim_fwd.append(speed_val * (speed_lim_gain / 100))
                            speed_lim_rev.append(speed_val * (speed_lim_gain / 100))
                            time_period.append(ramp_period)
                            test_point.append(log_down)

                    break

                # Otherwise add the point to a list
                else:
                    iq_inject.append(iq_val)
                    id_inject.append(id_val)
                    is_inject.append(np.sqrt((id_val**2) + (iq_val**2)))
                    speed_demand.append(speed_val * speed_dir)
                    speed_lim_fwd.append(speed_val * (speed_lim_gain / 100))
                    speed_lim_rev.append(speed_val * (speed_lim_gain / 100))
                    time_period.append(time_val)
                    test_point.append(log_up)

                    # Once the value is added to the list, increase by the requested current step
                    id_val = id_val - current_step
            iq_val = iq_val + current_step * iq_dir

    elif iq_dir < 0:
        # While the Iq Value is less than the stator current (*-1)
        while iq_val >= maximum_current * iq_dir:
            id_val = 0

            if step_up_method == "Ramp":
                iq_val_ramp = 0
                id_val_ramp = id_val

                if iq_val != 0:
                    while iq_val_ramp != iq_val + current_step:
                        iq_val_ramp = iq_val_ramp + current_step * iq_dir
                        iq_inject.append(iq_val_ramp)
                        id_inject.append(id_val_ramp)
                        is_inject.append(np.sqrt((id_val_ramp**2) + (iq_val_ramp**2)))
                        speed_demand.append(speed_val * speed_dir)
                        speed_lim_fwd.append(speed_val * (speed_lim_gain / 100))
                        speed_lim_rev.append(speed_val * (speed_lim_gain / 100))
                        time_period.append(ramp_period)
                        test_point.append(log_down)

            # While the Id Value is then the stator current (*-1)
            while id_val >= maximum_current * -1:
                # If the stator current for the combination of Idq Values is going to be greater than the Maximum stator current, stop.
                if np.sqrt((id_val**2) + (iq_val**2)) > maximum_current:
                    if step_down_method == "Ramp":
                        iq_val_ramp = iq_val
                        id_val_ramp = id_val
                        while np.sqrt((id_val_ramp**2) + (iq_val_ramp**2)) > 0:
                            if iq_val_ramp != 0:
                                iq_val_ramp = iq_val_ramp - (current_step * iq_dir)
                            if id_val_ramp != 0:
                                id_val_ramp = id_val_ramp + (current_step)

                            iq_inject.append(iq_val_ramp)
                            id_inject.append(id_val_ramp)
                            is_inject.append(np.sqrt((id_val_ramp**2) + (iq_val_ramp**2)))
                            speed_demand.append(speed_val * speed_dir)
                            speed_lim_fwd.append(speed_val * (speed_lim_gain / 100))
                            speed_lim_rev.append(speed_val * (speed_lim_gain / 100))
                            time_period.append(ramp_period)
                            test_point.append(log_down)

                    break

                # Otherwise add the point to a list
                else:
                    iq_inject.append(iq_val)
                    id_inject.append(id_val)
                    is_inject.append(np.sqrt((id_val**2) + (iq_val**2)))
                    speed_demand.append(speed_val * speed_dir)
                    speed_lim_fwd.append(speed_val * speed_dir * (speed_lim_gain / 100))
                    speed_lim_rev.append(speed_val * speed_dir * (speed_lim_gain / 100))
                    time_period.append(time_val)
                    test_point.append(log_up)

                    # Once the value is added to the list, increase by the requested current step
                    id_val = id_val - current_step
            iq_val = iq_val + current_step * iq_dir

    ref = pd.DataFrame(
        data={
            "iq_injected": iq_inject,
            "id_injected": id_inject,
            "is_injected": is_inject,
            "speed_demand": speed_demand,
            "speed_lim_fwd": speed_lim_fwd,
            "speed_lim_rev": speed_lim_fwd,
            "demand_period": time_period,
            "test_point": test_point,
        }
    )

    return ref


# TABLE GENERATOR FUNCTIONS


def torque_speed_table(gen_cfg):
    """
    generate torque speed table
    """
    reference_list = []
    torque_max_pc = gen_cfg["torque"]["maximum"] / 100
    speed_lim_val = gen_cfg["speed"]["limit_value"] / 100

    ref_dict = {
        "torque": {
            "maximum": torque_max_pc,
            "minimum": gen_cfg["torque"]["minimum"],
            "ramp_up_value": gen_cfg["torque"]["ramp_up_value"],
            "ramp_down_value": gen_cfg["torque"]["ramp_down_value"],
            "ramp_up_period": gen_cfg["torque"]["ramp_up_period"],
            "ramp_down_period": gen_cfg["torque"]["ramp_down_period"],
            "skip": gen_cfg["torque"]["skip"],
        },
        "speed": {
            "max_table": gen_cfg["speed"]["max_table"],
            "breakpoints": gen_cfg["speed"]["breakpoints"],
            "maximum": gen_cfg["speed"]["maximum"],
            "minimum": gen_cfg["speed"]["minimum"],
            "step": gen_cfg["speed"]["step"],
            "Limit Type": gen_cfg["speed"]["Limit Type"],
            "limit_value": speed_lim_val,
        },
        "voltage": {"voltage": gen_cfg["voltage"]["voltage"]},
        "logging": {
            "Log Up": gen_cfg["logging"]["up"],
            "Log Down": gen_cfg["logging"]["down"],
        },
        "Interpolation": {"Function": None},
    }

    xx, yy = np.meshgrid(gen_cfg["voltage"]["breakpoints"], gen_cfg["speed"]["breakpoints"])

    if gen_cfg["test"]["profile"] == "Forward_Motoring":
        profile_order = ["Q1"]

    elif gen_cfg["test"]["profile"] == "Reverse_Generating":
        profile_order = ["Q2"]

    elif gen_cfg["test"]["profile"] == "Reverse_Motoring":
        profile_order = ["Q3"]

    elif gen_cfg["test"]["profile"] == "Forward_Generating":
        profile_order = ["Q4"]

    elif gen_cfg["test"]["profile"] == "Motoring":
        profile_order = ["Q1", "Q3"]

    elif gen_cfg["test"]["profile"] == "Generating":
        profile_order = ["Q2", "Q4"]

    elif gen_cfg["test"]["profile"] == "Forward":
        profile_order = ["Q1", "Q4"]

    elif gen_cfg["test"]["profile"] == "Reverse":
        profile_order = ["Q2", "Q3"]

    elif gen_cfg["test"]["profile"] == "All":
        profile_order = ["Q1", "Q2", "Q3", "Q4"]

    for quadrant in profile_order:
        if quadrant == "Q1":
            ref_dict["speed"]["direction"] = 1
            ref_dict["torque"]["direction"] = 1

            ref_dict["Interpolation"]["Function"] = interp2d(
                x=xx,
                y=yy * ref_dict["speed"]["direction"],
                z=gen_cfg["torque"]["peak"],
                kind="linear",
                copy=True,
                bounds_error=False,
                fill_value=None,
            )

            reference_list.append(torque_speed_ref(ref_dict))

        elif quadrant == "Q2":
            ref_dict["speed"]["direction"] = -1
            ref_dict["torque"]["direction"] = 1

            ref_dict["Interpolation"]["Function"] = interp2d(
                x=xx,
                y=yy * ref_dict["speed"]["direction"],
                z=gen_cfg["torque"]["peak"],
                kind="linear",
                copy=True,
                bounds_error=False,
                fill_value=None,
            )

            reference_list.append(torque_speed_ref(ref_dict))

        elif quadrant == "Q3":
            ref_dict["speed"]["direction"] = -1
            ref_dict["torque"]["direction"] = -1

            ref_dict["Interpolation"]["Function"] = interp2d(
                x=xx,
                y=yy * ref_dict["speed"]["direction"],
                z=gen_cfg["torque"]["peak"],
                kind="linear",
                copy=True,
                bounds_error=False,
                fill_value=None,
            )

            reference_list.append(torque_speed_ref(ref_dict))

        elif quadrant == "Q4":
            ref_dict["speed"]["direction"] = 1
            ref_dict["torque"]["direction"] = -1

            ref_dict["Interpolation"]["Function"] = interp2d(
                x=xx,
                y=yy * ref_dict["speed"]["direction"],
                z=gen_cfg["torque"]["peak"],
                kind="linear",
                copy=True,
                bounds_error=False,
                fill_value=None,
            )

            reference_list.append(torque_speed_ref(ref_dict))

    ref_out = pd.concat(reference_list, ignore_index=True)
    ref_out["speed_demand_rads"] = ref_out["speed_demand"].multiply(2 * 3.14 * (1 / 60))
    ref_out["power_mech"] = abs(ref_out["speed_demand_rads"].multiply(ref_out["torque_demand"]))

    return ref_out


def envelope_table(torque_peak, speed_breakpoints, voltage_breakpoints, voltage_ref):
    """
    generate envelope table
    """
    env_list = []

    xx, yy = np.meshgrid(voltage_breakpoints, speed_breakpoints)

    profile_order = ["Q1", "Q2", "Q3", "Q4"]

    for quadrant in profile_order:
        if quadrant == "Q1":
            speed_dir = 1
            torque_dir = 1
            interp_function = interp2d(
                x=xx,
                y=yy * speed_dir,
                z=torque_peak,
                kind="linear",
                copy=True,
                bounds_error=False,
                fill_value=None,
            )

            env_list.append(
                envolope_ref(
                    torque_dir=torque_dir,
                    speed_dir=speed_dir,
                    speed_breakpoints=speed_breakpoints,
                    voltage_ref=voltage_ref,
                    interp_function=interp_function,
                )
            )

        elif quadrant == "Q2":
            speed_dir = -1
            torque_dir = 1
            interp_function = interp2d(
                x=xx,
                y=yy * speed_dir,
                z=torque_peak,
                kind="linear",
                copy=True,
                bounds_error=False,
                fill_value=None,
            )

            env_list.append(
                envolope_ref(
                    torque_dir=torque_dir,
                    speed_dir=speed_dir,
                    speed_breakpoints=speed_breakpoints,
                    voltage_ref=voltage_ref,
                    interp_function=interp_function,
                )
            )

        elif quadrant == "Q3":
            speed_dir = -1
            torque_dir = -1
            interp_function = interp2d(
                x=xx,
                y=yy * speed_dir,
                z=torque_peak,
                kind="linear",
                copy=True,
                bounds_error=False,
                fill_value=None,
            )

            env_list.append(
                envolope_ref(
                    torque_dir=torque_dir,
                    speed_dir=speed_dir,
                    speed_breakpoints=speed_breakpoints,
                    voltage_ref=voltage_ref,
                    interp_function=interp_function,
                )
            )

        elif quadrant == "Q4":
            speed_dir = 1
            torque_dir = -1
            interp_function = interp2d(
                x=xx,
                y=yy * speed_dir,
                z=torque_peak,
                kind="linear",
                copy=True,
                bounds_error=False,
                fill_value=None,
            )

            env_list.append(
                envolope_ref(
                    torque_dir=torque_dir,
                    speed_dir=speed_dir,
                    speed_breakpoints=speed_breakpoints,
                    voltage_ref=voltage_ref,
                    interp_function=interp_function,
                )
            )

        env_out = pd.concat(env_list, ignore_index=True)

    return env_out


def encoder_table(gen_cfg):
    """
    generate idq table
    """

    init_count = 0

    ref_init = pd.DataFrame(
        data={
            "speed_demand": [],
            "speed_lim_fwd": [],
            "speed_lim_rev": [],
        }
    )


def idq_table(gen_cfg):
    """
    generate idq table
    """

    ref_dict = gen_cfg

    reference_list = []

    if ref_dict["test"]["profile"] == "Forward_Motoring":
        profile_order = ["Q1"]

    elif ref_dict["test"]["profile"] == "Reverse_Generating":
        profile_order = ["Q2"]

    elif ref_dict["test"]["profile"] == "Reverse_Motoring":
        profile_order = ["Q3"]

    elif ref_dict["test"]["profile"] == "Forward_Generating":
        profile_order = ["Q4"]

    elif ref_dict["test"]["profile"] == "Motoring":
        profile_order = ["Q1", "Q3"]

    elif ref_dict["test"]["profile"] == "Generating":
        profile_order = ["Q2", "Q4"]

    elif ref_dict["test"]["profile"] == "Forward":
        profile_order = ["Q1", "Q4"]

    elif ref_dict["test"]["profile"] == "Reverse":
        profile_order = ["Q2", "Q3"]

    elif ref_dict["test"]["profile"] == "All":
        profile_order = ["Q1", "Q2", "Q3", "Q4"]

    for quadrant in profile_order:
        if quadrant == "Q1":
            ref_dict["speed"]["direction"] = 1
            ref_dict["idq"]["iq_direction"] = 1
            reference_list.append(idq_ref(ref_dict))

        elif quadrant == "Q2":
            ref_dict["speed"]["direction"] = -1
            ref_dict["idq"]["iq_direction"] = 1
            reference_list.append(idq_ref(ref_dict))

        elif quadrant == "Q3":
            ref_dict["speed"]["direction"] = -1
            ref_dict["idq"]["iq_direction"] = -1
            reference_list.append(idq_ref(ref_dict))

        elif quadrant == "Q4":
            ref_dict["speed"]["direction"] = 1
            ref_dict["idq"]["iq_direction"] = -1
            reference_list.append(idq_ref(ref_dict))

    ref_out = pd.concat(reference_list, ignore_index=True)

    ref_out["statorCurrent"] = np.sqrt((ref_out["iq_injected"] * ref_out["iq_injected"]) + (ref_out["id_injected"] * ref_out["id_injected"]))
    ref_out["speed_demand_rads"] = (ref_out["speed_demand"] / 60) * (2 * np.pi)

    return ref_out


# SCRIPT GENERATOR FUNCTIONS


def torque_speed_script(input):
    """
    Generate the top of the test script for torque speed sweep
    """

    test_name = str(input["test"]["name"])
    test_type = str(input["test"]["type"])
    test_profile = str(input["test"]["profile"])
    test_export_name = str(input["test"]["export_name"])
    test_export_format = str(input["test"]["export_format"])

    path_logging = str(input["test"]["logging_path"])
    path_export = str(input["test"]["export_path"])
    path_measured = str(input["paths"]["files"]["idq"]["data"]["results"]["measured"])
    path_alert = str(input["paths"]["files"]["idq"]["data"]["results"]["alerts"])
    path_meta_data = str(input["paths"]["files"]["idq"]["data"]["inverter"]["meta_data"])
    path_current_sensor_latencies = str(input["paths"]["files"]["idq"]["data"]["inverter"]["current_sensor_latencies"])

    controller_name = str(input["controller"]["name"])
    controller_sample_letter = str(input["controller"]["sample"]["letter"])
    controller_sample_number = str(input["controller"]["sample"]["number"])
    controller_notes = str(input["controller"]["notes"])
    controller_i_lim_charge = str(input["controller"]["config"]["dc_link"]["current"]["charge"])
    controller_i_lim_discharge = str(input["controller"]["config"]["dc_link"]["current"]["discharge"])

    motor_name = str(input["motor"]["name"])
    motor_sample_letter = str(input["motor"]["sample"]["letter"])
    motor_sample_number = str(input["motor"]["sample"]["number"])
    motor_notes = str(input["motor"]["notes"])

    dyno_name = str(input["dyno"]["name"])
    dyno_ip = str(input["dyno"]["modbus"]["ip"])
    dyno_port = str(input["dyno"]["modbus"]["port"])
    dyno_id = str(input["dyno"]["modbus"]["id"])
    dyno_gearbox_ratio = str(input["dyno"]["gearbox_ratio"])
    dyno_torque_sensor = str(input["dyno"]["torque_sensor"])

    torque_sensor_channel = str(input["can"]["torque_sensor"]["channel"])
    torque_sensor_dongle = str(input["can"]["torque_sensor"]["dongle"])
    torque_sensor_id = str(input["can"]["torque_sensor"]["hwid"])
    torque_sensor_bitrate = str(input["can"]["torque_sensor"]["bitrate"])
    torque_sensor_database = str(input["torque_sensor"]["can"]["database"])
    torque_sensor_torque_signal = str(input["torque_sensor"]["can"]["signals"]["motor"]["torque"]["measured"])
    torque_sensor_speed_signal = str(input["torque_sensor"]["can"]["signals"]["motor"]["torque"]["measured"])
    torque_sensor_torque_message = str(input["torque_sensor"]["can"]["messages"]["motor"]["torque"]["measured"])
    torque_sensor_speed_message = str(input["torque_sensor"]["can"]["messages"]["motor"]["speed"]["measured"]["mechanical"]["rpm"])
    torque_sensor_can_filter = "None"

    can_mcu_id = str(input["can"]["controller"]["hwid"])
    can_dcdc_id = str(input["can"]["dcdc"]["hwid"])

    dcdc_i_lim_pos = str(input["dcdc"]["config"]["positive"])
    dcdc_i_lim_neg = str(input["dcdc"]["config"]["negative"])
    dcdc_v_target = str(input["dcdc"]["config"]["target"])

    temp_dwell_enable = str(input["temperature"]["enable"])
    temp_limit_upper = str(input["temperature"]["limit"]["upper"])
    temp_limit_lower = str(input["temperature"]["limit"]["lower"])
    temp_ramp_step = str(input["temperature"]["step"])

    speed_demands = str(input["test_input"]["speed_demands"])
    torque_demands = str(input["test_input"]["torque_demands"])
    speed_limit_forward = str(input["test_input"]["speed_limits_forward"])
    speed_limit_reverse = str(input["test_input"]["speed_limits_reverse"])
    demand_period = str(input["test_input"]["demand_period"])
    log_point = str(input["test_input"]["logged_points"])

    login_password = str(input["login"]["password"])
    login_user = str(input["login"]["login"])

    logging_sample_period = str(input["logging"]["sample_period"])
    logging_delay_period = str(input["logging"]["delay_period"])
    logging_delay = str(input["logging"]["delay"])

    script = []
    script.append("import os")
    script.append("import sys")
    script.append("import time")
    script.append("import ctypes")
    script.append("from enum import Enum")
    script.append("import numpy as np")
    script.append("import pandas as pd")
    script.append("from loguru import logger as lg")
    script.append("import munchpy as mpy")
    script.append("import program.src.equipment.tenma as tenma")
    script.append("")
    script.append("#------------------ INPUT DATA ------------------")
    script.append("")
    script.append("###### Test Setup ")
    script.append("# Name: " + test_name)
    script.append("# Log Directory: " + path_logging)
    script.append("")
    script.append("###### Inverter ")
    script.append("# Name: " + controller_name)
    script.append("# Sample Letter: " + controller_sample_letter)
    script.append("# Sample Number: " + controller_sample_number)
    script.append("# Notes: " + controller_notes)
    script.append("")
    script.append("###### Motor ")
    script.append("# Manufacturer: " + motor_name)
    script.append("# Sample Letter: " + motor_sample_letter)
    script.append("# Sample Number: " + motor_sample_number)
    script.append("# Notes: " + motor_notes)
    script.append("")
    script.append("###### Dynamometer ")
    script.append("# Location: " + dyno_name)
    script.append("# IP Address: " + dyno_ip)
    script.append("# Port: " + dyno_port)
    script.append("# ID: " + dyno_id)
    script.append("")
    script.append("##### CAN Hardware ")
    script.append("# DCDC Ixxat ID: " + can_dcdc_id)
    script.append("# MCU Ixxat ID: " + can_mcu_id)
    script.append("")
    script.append("##### Test ")
    script.append("# Test Type: " + test_type)
    script.append("# Test Profile: " + test_profile)
    script.append("")
    script.append("###### DCDC ")
    script.append("# Target DC Link Voltage: " + dcdc_v_target)
    script.append("# DC Link Current Limit +: " + dcdc_i_lim_pos)
    script.append("# DC Link Current Limit -: " + dcdc_i_lim_neg)
    script.append("")
    # script.append("# Test Dwell ")
    # script.append("# Motor Temperature Test Upper Limit     = " + str(input_struct.motor_temp_upper))
    # script.append("# Motor Temperature Test Lower Limit     = " + str(input_struct.motor_temp_lower))
    # script.append("# Motor Temperature Test Ramp Step  = " + str(input_struct.motor_temp_step))
    script.append("")
    script.append("#================== START OF TEST SCRIPT ==================")
    script.append("")
    script.append("#------------------ TEST POINTS ------------------")
    script.append("")
    script.append("SPEED_DEMANDS               = " + speed_demands)
    script.append("TORQUE_DEMANDS              = " + torque_demands)
    script.append("SPEED_LIMITS_FORWARD        = " + speed_limit_forward)
    script.append("SPEED_LIMITS_REVERSE        = " + speed_limit_reverse)
    script.append("DEMAND_TIME                 = " + demand_period)
    script.append("LOG_POINT                   = " + log_point)
    script.append("")
    script.append("DCDC_I_LIMIT_POS            = " + dcdc_i_lim_pos)
    script.append("DCDC_I_LIMIT_NEG            = " + dcdc_i_lim_neg)
    script.append("DCDC_V_TARGET               = " + dcdc_v_target)
    script.append("")
    script.append("MCU_DISCHARGE_I_LIMIT       = " + controller_i_lim_discharge)
    script.append("MCU_CHARGE_I_LIMIT          = " + controller_i_lim_charge)
    script.append("")
    script.append("#------------------ INITIALISATION ------------------")
    script.append("")
    script.append("LOGGING_PATH                = " + 'r"' + path_logging + r"/MunchPy" + '"')
    script.append("TEST_NAME                   = " + '"' + test_name + '"')
    script.append("CAN_HWID_DCDC               = " + '"' + can_dcdc_id + '"')
    script.append("CAN_HWID_MCU                = " + '"' + can_mcu_id + '"')
    script.append("DYNO_LOCATION               = " + '"' + dyno_name + '"')
    script.append("DYNO_IP                     = " + '"' + dyno_ip + '"')
    script.append("DYNO_PORT                   = " + dyno_port)
    script.append("DYNO_ID                     = " + dyno_id)
    script.append("GEARBOX_RATIO               = " + dyno_gearbox_ratio)
    script.append("")
    if login_user is True:
        script.append("PASSWORD                    = " + '"' + login_password + '"')
    script.append("TORQUE_SENSOR               = " + '"' + dyno_torque_sensor + '"')
    if dyno_torque_sensor == "HBM T40B":
        script.append("# HBM CAN Channel: " + torque_sensor_channel)
        script.append("# HBM CAN Device: " + torque_sensor_dongle)
        script.append("# HBM CAN Database: " + torque_sensor_database)
        script.append("# HBM CAN Bitrate: " + torque_sensor_bitrate)
        script.append("# HBM CAN Torque ID: " + torque_sensor_speed_signal)
        script.append("# HBM CAN Speed ID: " + torque_sensor_torque_signal)
        script.append("# HBM CAN Torque Signal: " + torque_sensor_torque_message)
        script.append("# HBM CAN Speed Signal: " + torque_sensor_speed_message)
        script.append("# HBM CAN Filter: " + torque_sensor_can_filter)
    #!!! TODO:Issue with DLL reading on the path for libusb0.dll - needs inverstigating.
    else:
        script.append("import program.src.equipment.torque_sense as ts")
        script.append("TORQUE_SENSOR_TORQUE_FILTER = " + str(input["dyno"]["transducer_torque_filter"]))
        script.append("HBM_CAN_CHANNEL             = None")
        script.append("HBM_CAN_DEVICE              = None")
        script.append("HBM_CAN_DATABASE            = None")
        script.append("HBM_CAN_BITRATE             = None")
        script.append("HBM_CAN_TORQUE_ID           = None")
        script.append("HBM_CAN_SPEED_ID            = None")
        script.append("HBM_CAN_TORQUE_SIGNAL       = None")
        script.append("HBM_CAN_SPEED_SIGNAL        = None")
        script.append("HBM_CAN_FILTER              = None")
    script.append("")
    script.append("LOOP_SAMPLE_TIME            = " + logging_sample_period)
    script.append("LOG_DELAY_PERIOD            = " + logging_delay_period)
    if dyno_torque_sensor == "TS":
        script.append("TORQUE_SENSOR_TORQUE_FILTER = " + str(input["dyno"]["transducer_torque_filter"]))
    script.append("LOGIN_USER                  = " + login_user)
    script.append("DELAY_LOGGING               = " + logging_delay)
    script.append("")
    script.append("PATH_MEASURED               = " + 'r"' + path_measured + '"')
    script.append("PATH_ALERT                  = " + 'r"' + path_alert + '"')
    script.append("PATH_INVERTER               = " + 'r"' + path_meta_data + '"')
    script.append("PATH_I_SENSE                = " + 'r"' + path_current_sensor_latencies + '"')
    script.append("")
    script.append("#================== END OF FIRST FILE ==================")

    file_name = path_export + test_export_name + test_export_format
    if os.path.exists(file_name):
        interp_function = open(file_name, "w")
        interp_function.writelines("%s\n" % i for i in script)
        interp_function.close()
    else:
        interp_function = open(file_name, "w")
        interp_function.writelines("%s\n" % i for i in script)
        interp_function.close()
    path = file_name

    return path


def idq_script(input):
    """
    Generate the top of the test script for idq testing
    """

    test_name = str(input["test"]["name"])
    test_type = str(input["test"]["type"])
    test_profile = str(input["test"]["profile"])
    test_export_name = str(input["test"]["export_name"])
    test_export_format = str(input["test"]["export_format"])

    path_logging = str(input["test"]["logging_path"])
    path_export = str(input["test"]["export_path"])
    path_measured = str(input["paths"]["files"]["idq"]["data"]["results"]["measured"])
    path_alert = str(input["paths"]["files"]["idq"]["data"]["results"]["alerts"])
    path_meta_data = str(input["paths"]["files"]["idq"]["data"]["inverter"]["meta_data"])
    path_current_sensor_latencies = str(input["paths"]["files"]["idq"]["data"]["inverter"]["current_sensor_latencies"])

    controller_name = str(input["controller"]["name"])
    controller_sample_letter = str(input["controller"]["sample"]["letter"])
    controller_sample_number = str(input["controller"]["sample"]["number"])
    controller_notes = str(input["controller"]["notes"])
    controller_i_lim_charge = str(input["controller"]["config"]["dc_link"]["current"]["charge"])
    controller_i_lim_discharge = str(input["controller"]["config"]["dc_link"]["current"]["discharge"])

    motor_name = str(input["motor"]["name"])
    motor_sample_letter = str(input["motor"]["sample"]["letter"])
    motor_sample_number = str(input["motor"]["sample"]["number"])
    motor_notes = str(input["motor"]["notes"])

    dyno_name = str(input["dyno"]["name"])
    dyno_ip = str(input["dyno"]["modbus"]["ip"])
    dyno_port = str(input["dyno"]["modbus"]["port"])
    dyno_id = str(input["dyno"]["modbus"]["id"])
    dyno_gearbox_ratio = str(input["dyno"]["gearbox_ratio"])
    dyno_torque_sensor = str(input["dyno"]["torque_sensor"])

    torque_sensor_channel = str(input["can"]["torque_sensor"]["channel"])
    torque_sensor_dongle = str(input["can"]["torque_sensor"]["dongle"])
    torque_sensor_id = str(input["can"]["torque_sensor"]["hwid"])
    torque_sensor_bitrate = str(input["can"]["torque_sensor"]["bitrate"])

    torque_sensor_database = str(input["torque_sensor"]["can"]["database"])
    torque_sensor_torque_signal = str(input["torque_sensor"]["can"]["signals"]["motor"]["torque"]["measured"])
    torque_sensor_speed_signal = str(input["torque_sensor"]["can"]["signals"]["motor"]["speed"]["measured"]["mechanical"]["rpm"])
    torque_sensor_torque_message = str(input["torque_sensor"]["can"]["messages"]["motor"]["torque"]["measured"])
    torque_sensor_speed_message = str(input["torque_sensor"]["can"]["messages"]["motor"]["speed"]["measured"]["mechanical"]["rpm"])
    torque_sensor_can_filter = "None"

    can_mcu_id = str(input["can"]["controller"]["hwid"])
    can_dcdc_id = str(input["can"]["dcdc"]["hwid"])

    dcdc_i_lim_pos = str(input["dcdc"]["config"]["positive"])
    dcdc_i_lim_neg = str(input["dcdc"]["config"]["negative"])
    dcdc_v_target = str(input["dcdc"]["config"]["target"])

    speed_target = str(input["speed"]["target"])
    speed_limit = str(input["speed"]["limit_value"])

    temp_dwell_enable = str(input["temperature"]["enable"])
    temp_limit_upper = str(input["temperature"]["limit"]["upper"])
    temp_limit_lower = str(input["temperature"]["limit"]["lower"])
    temp_ramp_step = str(input["temperature"]["step"])

    speed_demands = str(input["test_input"]["speed_demands"])
    id_demands = str(input["test_input"]["iq_demands"])
    iq_demands = str(input["test_input"]["iq_demands"])
    speed_limit_forward = str(input["test_input"]["speed_limits_forward"])
    speed_limit_reverse = str(input["test_input"]["speed_limits_reverse"])
    demand_period = str(input["test_input"]["demand_period"])
    log_point = str(input["test_input"]["logged_points"])

    login_password = str(input["login"]["password"])
    login_user = str(input["login"]["login"])

    logging_sample_period = str(input["logging"]["sample_period"])
    logging_delay_period = str(input["logging"]["delay_period"])
    logging_delay = str(input["logging"]["delay"])

    idq_test_mode = str(input["idq"]["Mode ID"])
    idq_axis_mode = str(input["idq"]["Axis ID"])
    idq_axis_value = str(input["idq"]["Axis Value"])
    idq_id_id = str(input["idq"]["Id ID"])
    idq_iq_id = str(input["idq"]["Iq ID"])

    script = []
    script.append("import os")
    script.append("import sys")
    script.append("import time")
    script.append("import ctypes")
    script.append("from enum import Enum")
    script.append("import numpy as np")
    script.append("import pandas as pd")
    script.append("from loguru import logger as lg")
    script.append("import munchpy as mpy")
    script.append("import program.src.equipment.tenma as tenma")
    script.append("import program.src.equipment.t40b as t40b")
    script.append("")
    script.append("#------------------ INPUT DATA ------------------")
    script.append("")
    script.append("###### Test Setup ")
    script.append("# Name: " + test_name)
    script.append("# Log Directory: " + path_logging)
    script.append("")
    script.append("###### Inverter ")
    script.append("# Name: " + controller_name)
    script.append("# Sample Letter: " + controller_sample_letter)
    script.append("# Sample Number: " + controller_sample_number)
    script.append("# Notes: " + controller_notes)
    script.append("")
    script.append("###### Motor ")
    script.append("# Manufacturer: " + motor_name)
    script.append("# Sample Letter: " + motor_sample_letter)
    script.append("# Sample Number: " + motor_sample_number)
    script.append("# Notes: " + motor_notes)
    script.append("")
    script.append("###### Dynamometer ")
    script.append("# Location: " + dyno_name)
    script.append("# IP Address: " + dyno_ip)
    script.append("# Port: " + dyno_port)
    script.append("# ID: " + dyno_id)
    script.append("")
    script.append("##### CAN Hardware ")
    script.append("# DCDC Ixxat ID: " + can_dcdc_id)
    script.append("# MCU Ixxat ID: " + can_mcu_id)
    if dyno_torque_sensor == "HBM T40B":
        script.append("# HBM CAN Channel: " + torque_sensor_channel)
        script.append("# HBM CAN Device: " + torque_sensor_dongle)
        script.append("# HBM CAN Database: " + torque_sensor_database)
        script.append("# HBM CAN Bitrate: " + torque_sensor_bitrate)
        script.append("# HBM CAN Torque ID: " + torque_sensor_speed_signal)
        script.append("# HBM CAN Speed ID: " + torque_sensor_torque_signal)
        script.append("# HBM CAN Torque Signal: " + torque_sensor_torque_message)
        script.append("# HBM CAN Speed Signal: " + torque_sensor_speed_message)
        script.append("# HBM CAN Filter: " + torque_sensor_can_filter)
    #!!! TODO:Issue with DLL reading on the path for libusb0.dll - needs inverstigating.
    else:
        script.append("import program.src.equipment.torque_sense as ts")
    script.append("")
    script.append("##### Test ")
    script.append("# Test Type: " + test_type)
    script.append("# Test Profile: " + test_profile)
    script.append("")
    script.append("###### DCDC ")
    script.append("# Target DC Link Voltage: " + dcdc_v_target)
    script.append("# DC Link Current Limit +: " + dcdc_i_lim_pos)
    script.append("# DC Link Current Limit -: " + dcdc_i_lim_neg)
    script.append("")
    script.append("###### Speed ")
    script.append("# Test Speed Point: " + speed_target)
    script.append("# Value of Speed Limit: " + speed_limit)
    script.append("")
    if temp_dwell_enable is True:
        script.append("###### Test Dwell ")
        script.append("# Motor Temperature Test Upper Limit     = " + temp_limit_upper)
        script.append("# Motor Temperature Test Lower Limit     = " + temp_limit_lower)
        script.append("# Motor Temperature Test Ramp Step  = " + temp_ramp_step)
        script.append("")
    script.append("#****** START OF TEST SCRIPT ******")
    script.append("")
    script.append("#****** TEST POINTS ******")
    script.append("")
    script.append("SPEED_DEMANDS               = " + speed_demands)
    script.append("IQ_DEMANDS                  = " + iq_demands)
    script.append("ID_DEMANDS                  = " + id_demands)
    script.append("SPEED_LIMITS_FORWARD        = " + speed_limit_forward)
    script.append("SPEED_LIMITS_REVERSE        = " + speed_limit_reverse)
    script.append("DEMAND_TIME                 = " + demand_period)
    script.append("LOG_POINT                   = " + log_point)
    script.append("")
    script.append("DCDC_I_LIMIT_POS            = " + dcdc_i_lim_pos)
    script.append("DCDC_I_LIMIT_NEG            = " + dcdc_i_lim_neg)
    script.append("DCDC_V_TARGET               = " + dcdc_v_target)
    script.append("")
    script.append("MCU_DISCHARGE_I_LIMIT       = " + controller_i_lim_discharge)
    script.append("MCU_CHARGE_I_LIMIT          = " + controller_i_lim_charge)
    script.append("")
    script.append("#------------------ INITIALISATION ------------------")
    script.append("")
    script.append("LOGGING_PATH                = " + 'r"' + path_logging + r"/MunchPy" + '"')
    script.append("TEST_NAME                   = " + '"' + test_name + '"')
    script.append("CAN_HWID_DCDC               = " + '"' + can_dcdc_id + '"')
    script.append("CAN_HWID_MCU                = " + '"' + can_mcu_id + '"')
    script.append("DYNO_LOCATION               = " + '"' + dyno_name + '"')
    script.append("DYNO_IP                     = " + '"' + dyno_ip + '"')
    script.append("DYNO_PORT                   = " + dyno_port)
    script.append("DYNO_ID                     = " + dyno_id)
    script.append("GEARBOX_RATIO               = " + dyno_gearbox_ratio)
    script.append("")
    script.append("TORQUE_SENSOR               = " + '"' + dyno_torque_sensor + '"')
    if dyno_torque_sensor == "TS":
        script.append("TORQUE_SENSOR_TORQUE_FILTER = " + str(input["dyno"]["transducer_torque_filter"]))
        script.append("HBM_CAN_CHANNEL             = None")
        script.append("HBM_CAN_DEVICE              = None")
        script.append("HBM_CAN_DATABASE            = None")
        script.append("HBM_CAN_BITRATE             = None")
        script.append("HBM_CAN_TORQUE_ID           = None")
        script.append("HBM_CAN_SPEED_ID            = None")
        script.append("HBM_CAN_TORQUE_SIGNAL       = None")
        script.append("HBM_CAN_SPEED_SIGNAL        = None")
        script.append("HBM_CAN_FILTER              = None")
    if dyno_torque_sensor == "HBM T40B":
        script.append("TORQUE_SENSOR_TORQUE_FILTER = 0")
        script.append("# HBM CAN Channel: " + torque_sensor_channel)
        script.append("# HBM CAN Device: " + torque_sensor_dongle)
        script.append("# HBM CAN Database: " + torque_sensor_database)
        script.append("# HBM CAN Bitrate: " + torque_sensor_bitrate)
        script.append("# HBM CAN Torque ID: " + torque_sensor_speed_signal)
        script.append("# HBM CAN Speed ID: " + torque_sensor_torque_signal)
        script.append("# HBM CAN Torque Signal: " + torque_sensor_torque_message)
        script.append("# HBM CAN Speed Signal: " + torque_sensor_speed_message)
        script.append("# HBM CAN Filter: " + torque_sensor_can_filter)
    script.append("")
    if input["login"]["login"] is True:
        script.append("PASSWORD                    = " + '"' + login_password + '"')
    script.append("LOOP_SAMPLE_TIME            = " + logging_sample_period)
    script.append("LOG_DELAY_PERIOD            = " + logging_delay_period)
    script.append("LOGIN_USER                  = " + login_user)
    script.append("DELAY_LOGGING               = " + logging_delay)
    script.append("")
    script.append("TEST_MODE_ID                = " + idq_test_mode)
    script.append("TMODE_MODE_VAL              = 1.0")
    script.append("TMODE_AXIS_ID               = " + idq_axis_mode)
    script.append("TMODE_AXIS_VAL              = " + idq_axis_value)
    script.append("ID_INJ_ID                   = " + idq_id_id)
    script.append("IQ_INJ_ID                   = " + idq_iq_id)
    script.append("")
    if temp_dwell_enable is True:
        script.append("MOTOR_DWELL_ENABLE          = " + temp_dwell_enable)
        script.append("MOTOR_TEMP_UPPER            = " + temp_limit_upper)
        script.append("MOTOR_TEMP_LOWER            = " + temp_limit_lower)
        script.append("MOTOR_TEMP_IDQ_STEP         = " + temp_ramp_step)
    script.append("")
    script.append("PATH_MEASURED               = " + 'r"' + path_measured + '"')
    script.append("PATH_ALERT                  = " + 'r"' + path_alert + '"')
    script.append("PATH_INVERTER               = " + 'r"' + path_meta_data + '"')
    script.append("PATH_I_SENSE                = " + 'r"' + path_current_sensor_latencies + '"')
    script.append("")
    script.append("#================== END OF FIRST FILE ==================")
    script.append("")

    file_name = path_export + test_export_name + test_export_format
    if os.path.exists(file_name):
        interp_function = open(file_name, "w")
        interp_function.writelines("%s\n" % i for i in script)
        interp_function.close()
    else:
        interp_function = open(file_name, "w")
        interp_function.writelines("%s\n" % i for i in script)
        interp_function.close()
    path = file_name

    return path


def idq_script_gen_4(input):
    """
    Generate the top of the test script for torque speed sweep
    """
    script = []
    script.append("import os")
    script.append("import time")
    script.append("import ctypes")
    script.append("import numpy as np")
    script.append("import pandas as pd")
    script.append("import program.src.equipment.tenma as tenma")
    script.append("import program.src.equipment.torque_sense as ts")
    script.append("import program.src.equipment.g4_function as g4")
    script.append("import canopen")
    script.append("from tkinter import messagebox")
    script.append("from datetime import datetime")
    script.append("import plotly.graph_objects as go")
    script.append("")
    script.append("#------------------ INPUT DATA ------------------")
    script.append("")
    script.append("###### Test Setup ")
    script.append("# Name: " + str(input["test"]["name"]))
    script.append("# Log Directory: " + str(input["test"]["logging_path"]))
    script.append("")
    script.append("###### Inverter ")
    script.append("# Name: " + str(input["controller"]["name"]))
    script.append("# Sample Letter: " + str(input["controller"]["sample_letter"]))
    script.append("# Sample Number: " + str(input["controller"]["sample_number"]))
    script.append("# Notes: " + str(input["controller"]["note"]))
    script.append("")
    script.append("###### Motor ")
    script.append("# Manufacturer: " + str(input["motor"]["manufacturer"]))
    script.append("# Sample Letter: " + str(input["motor"]["sample_letter"]))
    script.append("# Sample Number: " + str(input["motor"]["sample_number"]))
    script.append("# Notes: " + str(input["motor"]["note"]))
    script.append("")
    script.append("###### Dynamometer ")
    script.append("# Location: " + str(input["dyno"]["location"]))
    script.append("# IP Address: " + str(input["dyno"]["ip"]))
    script.append("# Port: " + str(input["dyno"]["port"]))
    script.append("# ID: " + str(input["dyno"]["id"]))
    script.append("")
    script.append("##### CAN Hardware ")
    script.append("# DCDC Ixxat ID: " + str(input["can"]["hw_id"]["dcdc"]))
    script.append("# MCU Ixxat ID: " + str(input["can"]["hw_id"]["controller"]))
    script.append("")
    script.append("##### Test ")
    script.append("# Test Type: " + str(input["test"]["type"]))
    script.append("# Test Profile: " + str(input["test"]["profile"]))
    script.append("")
    script.append("###### DCDC ")
    script.append("# Target DC Link Voltage: " + str(input["dcdc"]["voltage"]))
    script.append("# DC Link Current Limit +: " + str(input["dcdc"]["current_positive"]))
    script.append("# DC Link Current Limit -: " + str(input["dcdc"]["current_negative"]))
    script.append("")
    script.append("###### Speed ")
    script.append("# Test Speed Point: " + str(input["speed"]["target"]))
    script.append("# Value of Speed Limit: " + str(input["speed"]["limit_value"]))
    script.append("")
    if input["temp"]["Enable"] is True:
        script.append("###### Test Dwell ")
        script.append("# Motor Temperature Test Upper Limit     = " + str(input["temp"]["Limit Upper"]))
        script.append("# Motor Temperature Test Lower Limit     = " + str(input["temp"]["Limit Lower"]))
        script.append("# Motor Temperature Test Ramp Step  = " + str(input["temp"]["step"]))
        script.append("")
    script.append("#------------------ START OF TEST SCRIPT ------------------")
    script.append("")
    script.append("#------------------ TEST POINTS ------------------")
    script.append("")
    script.append("SPEED_DEMANDS        = " + str(input["test_input"]["speed_demands"]))
    script.append("IQ_DEMANDS           = " + str(input["test_input"]["iq_demands"]))
    script.append("ID_DEMANDS           = " + str(input["test_input"]["id_demands"]))
    script.append("SPEED_LIMITS_FORWARD = " + str(input["test_input"]["speed_limits_forward"]))
    script.append("SPEED_LIMITS_REVERSE = " + str(input["test_input"]["speed_limits_reverse"]))
    script.append("DEMAND_TIME          = " + str(input["test_input"]["demand_period"]))
    script.append("LOG_POINT            = " + str(input["test_input"]["logged_points"]))
    script.append("")
    script.append("DCDC_I_LIMIT_POS      = " + str(input["dcdc"]["current_positive"]))
    script.append("DCDC_I_LIMIT_NEG      = " + str(input["dcdc"]["current_negative"]))
    script.append("DCDC_V_TARGET         = " + str(input["dcdc"]["voltage"]))
    script.append("")
    script.append("MCU_DISCHARGE_I_LIMIT = " + str(input["controller"]["discharge_limit"]))
    script.append("MCU_CHARGE_I_LIMIT    = " + str(input["controller"]["charge_limit"]))
    script.append("")
    script.append("#------------------ INITIALISATION ------------------")
    script.append("")
    script.append("LOGGING_PATH                = " + 'r"' + str(input["test"]["logging_path"]) + '"')
    script.append("TEST_NAME                   = " + '"' + str(input["test"]["name"]) + '"')
    script.append("CAN_HWID_DCDC               = " + '"' + str(input["can"]["hw_id"]["dcdc"]) + '"')
    script.append("CAN_HWID_MCU                = " + '"' + str(input["can"]["hw_id"]["controller"]) + '"')
    script.append("CAN_MCU_BITRATE             = " + str(input["can"]["gen_4"]["bitrate"]))
    script.append("CAN_MCU_CHANNEL             = " + str(input["can"]["gen_4"]["channel"]))
    script.append("DYNO_LOCATION               = " + '"' + str(input["dyno"]["location"]) + '"')
    script.append("DYNO_IP                     = " + '"' + str(input["dyno"]["ip"]) + '"')
    script.append("DYNO_PORT                   = " + str(input["dyno"]["port"]))
    script.append("DYNO_ID                     = " + str(input["dyno"]["id"]))
    script.append("GEARBOX_RATIO               = " + str(input["dyno"]["gearbox_ratio"]))
    if input["login"]["login"] is True:
        script.append("PASSWORD                    = " + '"' + str(input["login"]["password"]) + '"')
    script.append("TORQUE_SENSOR               = " + '"' + str(input["dyno"]["transducer"]) + '"')
    script.append("LOOP_SAMPLE_TIME            = " + str(input["logging"]["sample_period"]))
    script.append("LOG_DELAY_PERIOD            = " + str(input["logging"]["delay_period"]))
    if input["dyno"]["transducer"] == "TS":
        script.append("TORQUE_SENSOR_TORQUE_FILTER = " + str(input["dyno"]["transducer_torque_filter"]))
    script.append("LOGIN_USER                  = " + str(input["login"]["login"]))
    script.append("DELAY_LOGGING               = " + str(input["logging"]["delay"]))
    script.append("")
    script.append("TEST_MODE_ID         = " + str(input["idq"]["Mode ID"]))
    script.append("TMODE_MODE_VAL       = 1.0")
    script.append("TMODE_AXIS_ID        = " + str(input["idq"]["Axis ID"]))
    script.append("TMODE_AXIS_VAL       = " + str(input["idq"]["Axis Value"]))
    script.append("ID_INJ_ID            = " + str(input["idq"]["Id ID"]))
    script.append("IQ_INJ_ID            = " + str(input["idq"]["Iq ID"]))
    script.append("START_POINT          = " + str(input["idq"]["Start Point"]))
    script.append("")
    script.append("FIXED_POINT_SCALE_D  = " + str(FIXED_POINT_SCALE_D))
    script.append("FIXED_POINT_SCALE_Q  = " + str(FIXED_POINT_SCALE_Q))
    script.append("")
    script.append("BRIDGE_ENABLE        = " + str(BRIDGE_ENABLE))
    script.append("BRIDGE_DISABLE       = " + str(BRIDGE_DISABLE))
    script.append("")
    script.append("STATUS_WORD_ENABLED  = " + str(STATUS_WORD_ENABLED))
    script.append("STATUS_WORD_DISABLED = " + str(STATUS_WORD_DISABLED))
    script.append("")
    script.append("EDS                  = " + '"' + str(input["controller"]["EDS"]) + '"')
    if input["temp"]["Enable"] is True:
        script.append("MOTOR_DWELL_ENABLE   = " + str(input["temp"]["Enable"]))
        script.append("MOTOR_TEMP_UPPER     = " + str(input["temp"]["Limit Upper"]))
        script.append("MOTOR_TEMP_LOWER     = " + str(input["temp"]["Limit Lower"]))
        script.append("MOTOR_TEMP_IDQ_STEP  = " + str(input["temp"]["step"]))
    script.append("")
    script.append("PATH_MEASURED       = " + 'r"' + str(input["paths"]["files"]["idq"]["data"]["results"]["measured"]) + '"')
    script.append("PATH_TRANSDUCER     = " + 'r"' + str(input["paths"]["files"]["idq"]["data"]["results"]["transducer"]) + '"')
    script.append("")
    script.append("#================== END OF FIRST FILE ==================")
    script.append("")

    file_name = input["test"]["export_path"] + input["test"]["export_name"] + input["test"]["export_format"]

    if os.path.exists(file_name):
        interp_function = open(file_name, "w")
        interp_function.writelines("%s\n" % i for i in script)
        interp_function.close()
    else:
        interp_function = open(file_name, "w")
        interp_function.writelines("%s\n" % i for i in script)
        interp_function.close()
    path = file_name

    return path
