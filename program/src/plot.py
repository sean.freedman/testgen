import numpy as np
import plotly.graph_objects as go
import streamlit as st
from plotly.subplots import make_subplots
from scipy.interpolate import griddata
from src import constants

template_white = "plotly_white"


def torque_speed_scatter(profile: dict, profile_voltage: float, boundery: dict):
    """Generate plot based on torque and speed demands, that scales with available voltage. Also display boundary.

    Args:
        profile (dict): _description_
        profile_voltage (float): _description_
        project_speeds (list): _description_
        project_torques (list): _description_

    Returns:
        _type_: _description_
    """
    scatter = go.Figure()

    # if project speeds is not empty
    if boundery is not None:

        scatter.add_trace(
            go.Scattergl(
                x=boundery["speed_envelope"],
                y=boundery["torque_envelope"],
                name="Avaiable Envelope",
                mode="lines",
            )
        )

    scatter.add_trace(
        go.Scattergl(
            x=profile.speed_demand,
            y=profile.torque_demand,
            name="Test Points",
            mode="markers",
        )
    )

    scatter.update_layout(
        template=template_white,
        plot_bgcolor="white",
        title="Test Points: " + str(profile_voltage) + "V, " + str(min(profile.speed_demand)) + "rpm to " + str(max(profile.speed_demand)) + " rpm, ",
        xaxis_title="Speed [rpm]",
        yaxis_title="Torque Demanded [Nm]",
    )

    return scatter


def torque_speed_timeline(
    profile: dict,
    profile_voltage: float,
    speed_limit_forward: float,
    speed_limit_reverse: float,
):
    """Generate "Timeline" base on torque and speed demands.

    Args:
        profile (dict): _description_
        profile_voltage (float): _description_
        speed_limit_forward (float): _description_
        speed_limit_reverse (float): _description_

    Returns:
        _type_: _description_
    """
    timeline = go.Figure()

    timeline.add_trace(go.Scattergl(y=profile.speed_demand, name="Speed (rpm)", yaxis="y1"))

    timeline.add_trace(go.Scattergl(y=speed_limit_forward, name="Speed Limit Forward (rpm)", yaxis="y1"))

    timeline.add_trace(go.Scattergl(y=speed_limit_reverse, name="Speed Limit Reverse (rpm)", yaxis="y1"))

    timeline.add_trace(go.Scattergl(y=profile.torque_demand, name="Torque (Nm)", yaxis="y3"))

    timeline.add_trace(go.Scattergl(y=profile.power_mech, name="Mech Power (W)", yaxis="y4"))

    timeline.update_traces(mode="markers+lines")

    timeline.update_layout(
        template=template_white,
        plot_bgcolor="white",
        xaxis=dict(domain=[0, 0.8]),
        yaxis=dict(
            title="Speed [rpm]",
        ),
        yaxis3=dict(title="Torque [Nm]", anchor="x", overlaying="y", side="right"),
        yaxis4=dict(
            title="Mech Power [W]",
            anchor="free",
            overlaying="y",
            side="right",
            position=0.9,
        ),
        title="Test Input Timeline: " + str(profile_voltage) + "V, " + str(min(profile.speed_demand)) + "rpm to " + str(max(profile.speed_demand)) + " rpm, ",
        xaxis_title="Test Point",
        hovermode="x unified",
        legend=dict(orientation="h", yanchor="bottom", y=1.0, xanchor="right", x=1),
    )

    return timeline


def idq_scatter_report(profile: dict):
    """Generate Scatter plot based on idq demands.

    Args:
        profile (dict): _description_
        voltage_ref (float): _description_

    Returns:
        _type_: _description_
    """
    scatter = go.Figure()

    scatter.add_trace(
        go.Scattergl(
            x=profile["Id Current Injected (RMS) [A]"],
            y=profile["Iq Current Injected (RMS) [A]"],
            name="Test Points",
            mode="markers",
            marker=dict(
                color=profile["Is Current Injected (RMS) [A]"],
                colorscale=constants.GLOBAL_COLORMAP,
                colorbar=dict(
                    title="Stator Current (RMS) [A]",
                    thickness=20,
                    tick0=0,
                    dtick=50,
                ),
            ),
        )
    )

    scatter.update_layout(
        template=template_white,
        plot_bgcolor="white",
        title="Idq Test Points: " + str(np.mean(profile["DC Link Voltage"])) + "V, " + str(np.mean(profile["Speed Measured [rpm]"])) + "rpm",
        xaxis_title="Id Current Injected (RMS) [A]",
        yaxis_title="Iq Current Injected (RMS) [A]",
    )

    return scatter


def idq_scatter(profile: dict, voltage_ref: float):
    """Generate Scatter plot based on idq demands.

    Args:
        profile (dict): _description_
        voltage_ref (float): _description_

    Returns:
        _type_: _description_
    """

    if len(profile["speed_demand"].unique()) == 1:

        scatter = go.Figure()

        scatter.add_trace(
            go.Scattergl(
                x=profile.id_injected,
                y=profile.iq_injected,
                name="Test Points",
                mode="markers",
                marker=dict(
                    color=profile.statorCurrent,
                    colorscale=constants.GLOBAL_COLORMAP,
                    colorbar=dict(
                        title="Stator Current (RMS) [A]",
                        thickness=20,
                        tick0=0,
                        dtick=50,
                    ),
                ),
            )
        )

        scatter.update_layout(
            template=template_white,
            plot_bgcolor="white",
            title="Idq Test Points: " + str(voltage_ref) + "V, " + str(min(profile.speed_demand)) + "rpm",
            xaxis_title="Id Current Injected (RMS) [A]",
            yaxis_title="Iq Current Injected (RMS) [A]",
        )

    else:
        scatter = go.Figure()

        scatter.add_trace(
            go.Scatter3d(
                x=profile.id_injected,
                y=profile.iq_injected,
                z=profile.speed_demand,
                name="Test Points",
                mode="markers",
                hovertemplate="Id (RMS) [A]" + ": %{x:.2f}" + "<br>" + "Iq (Arms)" + ": %{y:.2f}</br>" + "Speed (rpm)" + ": %{z:.2f}",
                marker=dict(
                    size=5,
                    color=profile.statorCurrent,
                    colorscale=constants.GLOBAL_COLORMAP,
                    colorbar=dict(
                        title="Stator Current (RMS) [A]",
                        thickness=20,
                        tick0=0,
                        dtick=50,
                    ),
                    opacity=1.0,
                ),
            )
        )

        scatter.update_layout(
            scene=dict(
                xaxis_title="Id (RMS) [A]",
                yaxis_title="Iq (RMS) [A]",
                zaxis_title="Speed (rpm)",
            ),
        )
    return scatter


def idq_timeline(profile: dict, target_voltage: float):
    """Generate "timeline" plot based on Idq demands.

    Args:
        profile (dict): _description_

    Returns:
        _type_: _description_
    """
    line = go.Figure()

    line.add_trace(go.Scattergl(y=profile.id_injected, name="Id Injected Current (RMS) [A]", yaxis="y1"))

    line.add_trace(go.Scattergl(y=profile.iq_injected, name="Iq Injected Current (RMS) [A]", yaxis="y1"))

    line.add_trace(
        go.Scattergl(
            y=np.sqrt((profile.id_injected * profile.id_injected) + (profile.iq_injected * profile.iq_injected)),
            name="Stator Current Injected (RMS) [A]",
            yaxis="y1",
        )
    )

    line.add_trace(go.Scattergl(y=profile.speed_demand, name="Speed (Rpm)", yaxis="y2"))

    line.update_traces(mode="markers+lines")

    line.update_layout(
        template=template_white,
        plot_bgcolor="white",
        xaxis=dict(domain=[0, 0.8]),
        yaxis=dict(
            title="Current (RMS) [A]",
        ),
        yaxis2=dict(title="Speed [rpm]", anchor="x", overlaying="y", side="right"),
        title="Test Input Timeline: " + str(target_voltage) + "V, " + str(min(profile.speed_demand)) + "rpm to " + str(max(profile.speed_demand)) + " rpm, ",
        xaxis_title="Test Point",
        hovermode="x unified",
        legend=dict(orientation="h", yanchor="bottom", y=1.0, xanchor="right", x=1),
    )

    return line


def surface_col(
    df: list,
    x_string: list,
    y_string: list,
    z_string: list,
    plot_title: list,
):
    """_summary_

    Args:
        df (list): _description_
        x_string (list): _description_
        y_string (list): _description_
        z_string (list): _description_
        plot_title (list): _description_

    Returns:
        _type_: _description_
    """

    if len(df) == 1:
        plot = go.Figure()
    elif len(df) > 1:
        scenes = [dict(zip(["type"], ["surface"])) for x in range(0, len(df))]
        cbar_offset = 0.1
        cbar_loc = []
        x = len(df)
        while x > 0:
            cbar_loc.append((len(df) / (x * len(df))) - cbar_offset)
            x = x - 1

        plot = make_subplots(
            rows=1,
            cols=len(df),
            subplot_titles=(plot_title),
            specs=[scenes],
            shared_xaxes=True,
            shared_yaxes=True,
        )

    for plots in range(0, len(df)):

        x = df[plots][x_string[plots]]
        y = df[plots][y_string[plots]]
        z = df[plots][z_string[plots]]

        x_i = np.linspace(start=float(min(x)), stop=float(max(x)), num=int(50))
        y_i = np.linspace(start=float(min(y)), stop=float(max(y)), num=int(50))

        X, Y = np.meshgrid(x_i, y_i)

        z_out = griddata((x, y), z, (X, Y), method="linear")

        x_out = x_i
        y_out = y_i

        plot.add_trace(
            go.Surface(
                z=z_out,
                x=x_out,
                y=y_out,
                name=plot_title[plots],
                contours_x=dict(show=True),
                contours_y=dict(show=True),
                contours_z=dict(show=True, project=dict(z=True), usecolormap=True),
                colorscale=constants.GLOBAL_COLORMAP,
                hovertemplate=x_string[plots] + ": %{x:.3f}" + "<br>" + y_string[plots] + ": %{y:.3f}</br>" + z_string[plots] + ": %{z:.3f}",
                colorbar=dict(
                    title=z_string[plots],
                    titleside="right",
                    titlefont=dict(size=12, family="Arial, sans"),
                    x=cbar_loc[plots],
                ),
            ),
            row=1,
            col=plots + 1,
        )

        plot.add_trace(
            go.Scatter3d(
                z=z,
                x=x,
                y=y,
                name=plot_title[plots],
                hovertemplate=x_string[plots] + ": %{x:.5f}" + "<br>" + y_string[plots] + ": %{y:.5f}</br>" + z_string[plots] + ": %{z:.5f}",
                mode="markers",
                marker_color="black",
                marker_size=4,
            ),
            row=1,
            col=plots + 1,
        )

        camera = dict(
            up=dict(x=0, y=0, z=1),
            center=dict(x=0, y=0, z=0),
            eye=dict(x=1.25, y=1.25, z=1.25),
        )

    if len(df) == 1:
        plot.update_layout(
            template=template_white,
            plot_bgcolor="white",
            scene=dict(
                scene_camera=camera,
                xaxis_title=x_string[0],
                yaxis_title=y_string[0],
                zaxis_title=z_string[0],
            ),
        )

    elif len(df) == 2:
        plot.update_layout(
            template=template_white,
            plot_bgcolor="white",
            scene=dict(
                camera=camera,
                xaxis_title=x_string[0],
                yaxis_title=y_string[0],
                zaxis_title=z_string[0],
            ),
            scene2=dict(
                camera=camera,
                xaxis_title=x_string[1],
                yaxis_title=y_string[1],
                zaxis_title=z_string[1],
            ),
        )

    return plot


def psi_m(df_0, report_df):
    """Generate Scatter/Line plot representing the Psim of a motor, based on the idq characterisation results.

    Args:
        df_0 (float): _description_
        interp_vals (list): _description_

    Returns:
        _type_: _description_
    """
    plot = go.Figure()

    plot.add_trace(
        go.Scatter(
            x=report_df["Iq Current Injected (RMS) [A]"],
            y=report_df["Psi m (RMS) [Wb]"],
            name="Psi m (RMS) [Wb] ISaturated",
        )
    )

    plot.add_trace(
        go.Scatter(
            x=report_df["Iq Current Injected (RMS) [A]"],
            y=report_df["Psi m presat (RMS) [Wb]"],
            name="Psi m (RMS) [Wb] Interpolated",
            line=dict(dash="dot"),
        )
    )

    plot.add_trace(
        go.Scatter(
            x=df_0["Iq Current Injected (RMS) [A]"],
            y=df_0["Psi m (RMS) [Wb]"],
            name="Psi m (RMS) [Wb]",
            mode="markers",
            marker_size=10,
        )
    )

    plot.update_layout(
        template=template_white,
        plot_bgcolor="white",
        title="Psi m (RMS) [Wb] interpolated Values against Iq Current (RMS) [A]",
        xaxis_title="Iq Injected Current (RMS) [A] [0 Degrees]",
        yaxis_title="Psi m (RMS) [Wb]",
    )

    return plot


def ld_minus_lq(df_45: dict):
    """Generate Ld - Lq plot based on Idq characterisation results.

    Args:
        df_45 (dict): _description_

    Returns:
        _type_: _description_
    """
    plot = go.Figure()

    plot.add_trace(go.Scatter(x=df_45["Is Current Injected (RMS) [A]"], y=df_45["Ld - Lq"]))

    plot.update_layout(
        template=template_white,
        plot_bgcolor="white",
        title="L [H]",
        xaxis_title="Is Current Injected (RMS) [A]",
        yaxis_title="L [H]",
    )

    return plot


def speed_torque_error_contour(df_all: dict, error_type: str):
    """Generate Contour plot using torque speed sweep results.

    Args:
        df_all (dict): _description_
        error_type (str): _description_

    Returns:
        _type_: _description_
    """
    x_in = df_all["Speed Demanded [rpm]"]
    y_in = df_all["Torque Demanded Actual"]
    z_in = df_all["Torque Demanded Actual Error " + str(error_type)]

    x_i = np.linspace(start=float(min(x_in)), stop=float(max(x_in)), num=int(50))

    y_i = np.linspace(start=float(min(y_in)), stop=float(max(y_in)), num=int(50))

    X, Y = np.meshgrid(x_i, y_i)

    z_out = griddata(points=(x_in, y_in), values=z_in, xi=(X, Y), method="linear")

    x_out = x_i
    y_out = y_i

    plot = go.Figure()

    plot.add_trace(
        go.Contour(
            z=z_out,
            x=x_out,
            y=y_out,
            name="Contour",
            opacity=0.98,
            colorscale="turbo",
            hovertemplate="Speed Demanded" + ": %{x:.2f} rpm" + "<br>" + "Torque Demanded Actual" + ": %{y:.2f} Nm</br>" + "Torque Measured Error" + ": %{z:.2f} " + str(error_type),
            colorbar=dict(
                title="Torque Error " + str(error_type),
                titleside="right",
                titlefont=dict(size=12, family="Arial, sans"),
            ),
        )
    )

    plot.update_traces(
        zmid=0,
        showlegend=True,
        ncontours=20,
        contours=dict(
            coloring="heatmap",
            showlabels=True,
        ),
    )

    plot.add_trace(
        go.Scatter(
            x=x_in,
            y=y_in,
            mode="markers",
            name="Logged Point",
            marker_symbol="cross",
            marker_color="black",
            hovertemplate="Speed Demanded" + ": %{x:.2f} rpm" + "<br>" + "Torque Demanded Actual" + ": %{y:.2f} Nm</br>",
        )
    )

    plot.update_layout(
        template=template_white,
        plot_bgcolor="white",
        title="Torque Demanded Actual Error " + str(error_type),
        xaxis_title="Speed [rpm]",
        yaxis_title="Torque Demanded Actual [Nm]",
        legend=dict(orientation="h", yanchor="bottom", y=1.02, xanchor="right", x=1),
    )

    return plot


def speed_torque_error_line(df_all: dict, error_type: str):
    """Generate line plot representing torque error from torque speed sweep results.

    Args:
        df_all (dict): _description_
        error_type (str): _description_

    Returns:
        _type_: _description_
    """
    x_0 = df_all["Test Point"]
    y_0 = df_all["Torque Demanded"]
    y_1 = df_all["Torque Demanded Actual"]
    y_2 = df_all["Torque Measured [Nm]"]
    y_3 = df_all["Torque Demanded Actual Error " + str(error_type)]
    y_4 = df_all["Speed Demanded [rpm]"]

    plot = go.Figure()

    plot.add_trace(
        go.Scatter(
            x=x_0,
            y=y_0,
            name="Torque Demanded [Nm]",
            yaxis="y1",
        )
    )

    plot.add_trace(
        go.Scatter(
            x=x_0,
            y=y_1,
            name="Torque Demanded Actual [Nm]",
            yaxis="y1",
        )
    )

    plot.add_trace(
        go.Scatter(
            x=x_0,
            y=y_2,
            name="Torque Measured [Nm]",
            yaxis="y1",
        )
    )

    plot.add_trace(
        go.Scatter(
            x=x_0,
            y=y_3,
            yaxis="y3",
            name="Torque Demanded Actual Error " + error_type,
        )
    )

    plot.add_trace(
        go.Scatter(
            x=x_0,
            y=y_4,
            yaxis="y4",
            name="Speed Demanded [rpm]",
        )
    )

    plot.update_layout(
        hovermode="x unified",
    )

    plot.update_traces(mode="markers+lines")

    plot.update_layout(
        template=template_white,
        plot_bgcolor="white",
        xaxis=dict(domain=[0, 0.8]),
        yaxis=dict(
            title="Torque [Nm]",
        ),
        yaxis3=dict(
            title="Torque Error " + str(error_type),
            anchor="x",
            overlaying="y",
            side="right",
        ),
        yaxis4=dict(
            title="Speed [rpm]",
            anchor="free",
            overlaying="y",
            side="right",
            position=0.9,
        ),
        title="Torque Actual Error " + str(error_type),
        xaxis_title="Test Point",
        hovermode="x unified",
        legend=dict(orientation="h", yanchor="bottom", y=1.0, xanchor="right", x=1),
    )

    return plot


def current_sensor_latency(data: dict):
    """Generate line plot representing current sensor latency against frequency

    Args:
        data (dict): _description_

    Returns:
        _type_: _description_
    """
    plot = go.Figure()

    plot.add_trace(go.Scattergl(x=data["speed"], y=data["Trim Angle"], name="Current Sensor Latency"))

    plot.update_traces(mode="markers+lines")

    plot.update_layout(
        template=template_white,
        plot_bgcolor="white",
        title="Current Sensor Latency",
        xaxis_title="Frequency",
        yaxis_title="Trim Angle",
    )

    return plot
