import streamlit as st
from src.constants import FLOATS, NUMBER_FORMATTERS

# BOWFELL
BOWFELL = {
    "inverter_parameters": {
        "switch": {
            "label": {
                "voltage_threshold": "Voltage Threshold",
                "resistance": "Resistance",
                "diode_drip_voltage": "Diode Drip Voltage",
                "diode_drip_resistance": "Diode Drip Resistance",
                "breakdown_voltage": "Breakdown Voltage",
                "current_limit": "Current Limit",
                "energy_on": "Energy On",
                "energy_off": "Energy Off",
                "frequency": "Frequency",
            },
            "key": {
                "voltage_threshold": "switch_voltage_threshold",
                "resistance": "switch_resistance",
                "diode_drip_voltage": "switch_diode_drip_voltage",
                "diode_drip_resistance": "switch_diode_drip_resistance",
                "breakdown_voltage": "switch_breakdown_voltage",
                "current_limit": "switch_current_limit",
                "energy_on": "switch_energy_on",
                "energy_off": "switch_energy_off",
                "frequency": "switch_frequency",
            },
            "value": {
                "voltage_threshold": FLOATS["zero"],
                "resistance": FLOATS["zero"],
                "diode_drip_voltage": FLOATS["zero"],
                "diode_drip_resistance": FLOATS["zero"],
                "breakdown_voltage": FLOATS["zero"],
                "current_limit": FLOATS["zero"],
                "energy_on": FLOATS["zero"],
                "energy_off": FLOATS["zero"],
                "frequency": FLOATS["zero"],
            },
            "min": {
                "voltage_threshold": FLOATS["zero"],
                "resistance": FLOATS["zero"],
                "diode_drip_voltage": FLOATS["zero"],
                "diode_drip_resistance": FLOATS["zero"],
                "breakdown_voltage": FLOATS["zero"],
                "current_limit": FLOATS["zero"],
                "energy_on": FLOATS["zero"],
                "energy_off": FLOATS["zero"],
                "frequency": FLOATS["zero"],
            },
            "max": {
                "voltage_threshold": FLOATS["mega"],
                "resistance": FLOATS["mega"],
                "diode_drip_voltage": FLOATS["mega"],
                "diode_drip_resistance": FLOATS["mega"],
                "breakdown_voltage": FLOATS["mega"],
                "current_limit": FLOATS["mega"],
                "energy_on": FLOATS["mega"],
                "energy_off": FLOATS["mega"],
                "frequency": FLOATS["mega"],
            },
            "step": {
                "voltage_threshold": FLOATS["micro"],
                "resistance": FLOATS["micro"],
                "diode_drip_voltage": FLOATS["micro"],
                "diode_drip_resistance": FLOATS["micro"],
                "breakdown_voltage": FLOATS["micro"],
                "current_limit": FLOATS["micro"],
                "energy_on": FLOATS["micro"],
                "energy_off": FLOATS["micro"],
                "frequency": FLOATS["micro"],
            },
            "format": {
                "voltage_threshold": NUMBER_FORMATTERS["micro"],
                "resistance": NUMBER_FORMATTERS["micro"],
                "diode_drip_voltage": NUMBER_FORMATTERS["micro"],
                "diode_drip_resistance": NUMBER_FORMATTERS["micro"],
                "breakdown_voltage": NUMBER_FORMATTERS["micro"],
                "current_limit": NUMBER_FORMATTERS["micro"],
                "energy_on": NUMBER_FORMATTERS["micro"],
                "energy_off": NUMBER_FORMATTERS["micro"],
                "frequency": NUMBER_FORMATTERS["micro"],
            },
        },
        "diode": {
            "label": {
                "reverse_voltage": "Reverse Voltage",
                "forward_current": "Forward Current",
                "reverse_energy": "Reverse Energy",
            },
            "key": {
                "reverse_voltage": "diode_reverse_voltage",
                "forward_current": "diode_forward_current",
                "reverse_energy": "diode_reverse_energy",
            },
            "value": {
                "reverse_voltage": FLOATS["zero"],
                "forward_current": FLOATS["zero"],
                "reverse_energy": FLOATS["zero"],
            },
            "min": {
                "reverse_voltage": FLOATS["zero"],
                "forward_current": FLOATS["zero"],
                "reverse_energy": FLOATS["zero"],
            },
            "max": {
                "reverse_voltage": FLOATS["mega"],
                "forward_current": FLOATS["mega"],
                "reverse_energy": FLOATS["mega"],
            },
            "step": {
                "reverse_voltage": FLOATS["micro"],
                "forward_current": FLOATS["micro"],
                "reverse_energy": FLOATS["micro"],
            },
            "format": {
                "reverse_voltage": NUMBER_FORMATTERS["micro"],
                "forward_current": NUMBER_FORMATTERS["micro"],
                "reverse_energy": NUMBER_FORMATTERS["micro"],
            },
        },
        "dc_link": {
            "label": {
                "current_limit": "Current Limit",
            },
            "key": {
                "current_limit": "dc_current_limit",
            },
            "value": {
                "current_limit": FLOATS["zero"],
            },
            "min": {
                "current_limit": FLOATS["zero"],
            },
            "max": {
                "current_limit": FLOATS["mega"],
            },
            "step": {
                "current_limit": FLOATS["micro"],
            },
            "format": {
                "current_limit": NUMBER_FORMATTERS["micro"],
            },
        },
    }
}

# OXFORD
OXFORD = {
    "inverter_parameters": {
        "switch": {
            "label": {
                "voltage_threshold": "Voltage Threshold",
                "resistance": "Resistance",
                "diode_drip_voltage": "Diode Drip Voltage",
                "diode_drip_resistance": "Diode Drip Resistance",
                "breakdown_voltage": "Breakdown Voltage",
                "current_limit": "Current Limit",
                "energy_on": "Energy On",
                "energy_off": "Energy Off",
                "frequency": "Frequency",
            },
            "key": {
                "voltage_threshold": "switch_voltage_threshold",
                "resistance": "switch_resistance",
                "diode_drip_voltage": "switch_diode_drip_voltage",
                "diode_drip_resistance": "switch_diode_drip_resistance",
                "breakdown_voltage": "switch_breakdown_voltage",
                "current_limit": "switch_current_limit",
                "energy_on": "switch_energy_on",
                "energy_off": "switch_energy_off",
                "frequency": "switch_frequency",
            },
            "value": {
                "voltage_threshold": FLOATS["zero"],
                "resistance": FLOATS["zero"],
                "diode_drip_voltage": FLOATS["zero"],
                "diode_drip_resistance": FLOATS["zero"],
                "breakdown_voltage": FLOATS["zero"],
                "current_limit": FLOATS["zero"],
                "energy_on": FLOATS["zero"],
                "energy_off": FLOATS["zero"],
                "frequency": FLOATS["zero"],
            },
            "min": {
                "voltage_threshold": FLOATS["zero"],
                "resistance": FLOATS["zero"],
                "diode_drip_voltage": FLOATS["zero"],
                "diode_drip_resistance": FLOATS["zero"],
                "breakdown_voltage": FLOATS["zero"],
                "current_limit": FLOATS["zero"],
                "energy_on": FLOATS["zero"],
                "energy_off": FLOATS["zero"],
                "frequency": FLOATS["zero"],
            },
            "max": {
                "voltage_threshold": FLOATS["mega"],
                "resistance": FLOATS["mega"],
                "diode_drip_voltage": FLOATS["mega"],
                "diode_drip_resistance": FLOATS["mega"],
                "breakdown_voltage": FLOATS["mega"],
                "current_limit": FLOATS["mega"],
                "energy_on": FLOATS["mega"],
                "energy_off": FLOATS["mega"],
                "frequency": FLOATS["mega"],
            },
            "step": {
                "voltage_threshold": FLOATS["micro"],
                "resistance": FLOATS["micro"],
                "diode_drip_voltage": FLOATS["micro"],
                "diode_drip_resistance": FLOATS["micro"],
                "breakdown_voltage": FLOATS["micro"],
                "current_limit": FLOATS["micro"],
                "energy_on": FLOATS["micro"],
                "energy_off": FLOATS["micro"],
                "frequency": FLOATS["micro"],
            },
            "format": {
                "voltage_threshold": NUMBER_FORMATTERS["micro"],
                "resistance": NUMBER_FORMATTERS["micro"],
                "diode_drip_voltage": NUMBER_FORMATTERS["micro"],
                "diode_drip_resistance": NUMBER_FORMATTERS["micro"],
                "breakdown_voltage": NUMBER_FORMATTERS["micro"],
                "current_limit": NUMBER_FORMATTERS["micro"],
                "energy_on": NUMBER_FORMATTERS["micro"],
                "energy_off": NUMBER_FORMATTERS["micro"],
                "frequency": NUMBER_FORMATTERS["micro"],
            },
        },
        "diode": {
            "label": {
                "reverse_voltage": "Reverse Voltage",
                "forward_current": "Forward Current",
                "reverse_energy": "Reverse Energy",
            },
            "key": {
                "reverse_voltage": "diode_reverse_voltage",
                "forward_current": "diode_forward_current",
                "reverse_energy": "diode_reverse_energy",
            },
            "value": {
                "reverse_voltage": FLOATS["zero"],
                "forward_current": FLOATS["zero"],
                "reverse_energy": FLOATS["zero"],
            },
            "min": {
                "reverse_voltage": FLOATS["zero"],
                "forward_current": FLOATS["zero"],
                "reverse_energy": FLOATS["zero"],
            },
            "max": {
                "reverse_voltage": FLOATS["mega"],
                "forward_current": FLOATS["mega"],
                "reverse_energy": FLOATS["mega"],
            },
            "step": {
                "reverse_voltage": FLOATS["micro"],
                "forward_current": FLOATS["micro"],
                "reverse_energy": FLOATS["micro"],
            },
            "format": {
                "reverse_voltage": NUMBER_FORMATTERS["micro"],
                "forward_current": NUMBER_FORMATTERS["micro"],
                "reverse_energy": NUMBER_FORMATTERS["micro"],
            },
        },
        "dc_link": {
            "label": {
                "current_limit": "Current Limit",
            },
            "key": {
                "current_limit": "dc_current_limit",
            },
            "value": {
                "current_limit": FLOATS["zero"],
            },
            "min": {
                "current_limit": FLOATS["zero"],
            },
            "max": {
                "current_limit": FLOATS["mega"],
            },
            "step": {
                "current_limit": FLOATS["micro"],
            },
            "format": {
                "current_limit": NUMBER_FORMATTERS["micro"],
            },
        },
    }
}

# DERWENT
DERWENT = {
    "inverter_parameters": {
        "switch": {
            "label": {
                "voltage_threshold": "Voltage Threshold",
                "resistance": "Resistance",
                "diode_drip_voltage": "Diode Drip Voltage",
                "diode_drip_resistance": "Diode Drip Resistance",
                "breakdown_voltage": "Breakdown Voltage",
                "current_limit": "Current Limit",
                "energy_on": "Energy On",
                "energy_off": "Energy Off",
                "frequency": "Frequency",
            },
            "key": {
                "voltage_threshold": "switch_voltage_threshold",
                "resistance": "switch_resistance",
                "diode_drip_voltage": "switch_diode_drip_voltage",
                "diode_drip_resistance": "switch_diode_drip_resistance",
                "breakdown_voltage": "switch_breakdown_voltage",
                "current_limit": "switch_current_limit",
                "energy_on": "switch_energy_on",
                "energy_off": "switch_energy_off",
                "frequency": "switch_frequency",
            },
            "value": {
                "voltage_threshold": FLOATS["zero"],
                "resistance": FLOATS["zero"],
                "diode_drip_voltage": FLOATS["zero"],
                "diode_drip_resistance": FLOATS["zero"],
                "breakdown_voltage": FLOATS["zero"],
                "current_limit": FLOATS["zero"],
                "energy_on": FLOATS["zero"],
                "energy_off": FLOATS["zero"],
                "frequency": FLOATS["zero"],
            },
            "min": {
                "voltage_threshold": FLOATS["zero"],
                "resistance": FLOATS["zero"],
                "diode_drip_voltage": FLOATS["zero"],
                "diode_drip_resistance": FLOATS["zero"],
                "breakdown_voltage": FLOATS["zero"],
                "current_limit": FLOATS["zero"],
                "energy_on": FLOATS["zero"],
                "energy_off": FLOATS["zero"],
                "frequency": FLOATS["zero"],
            },
            "max": {
                "voltage_threshold": FLOATS["mega"],
                "resistance": FLOATS["mega"],
                "diode_drip_voltage": FLOATS["mega"],
                "diode_drip_resistance": FLOATS["mega"],
                "breakdown_voltage": FLOATS["mega"],
                "current_limit": FLOATS["mega"],
                "energy_on": FLOATS["mega"],
                "energy_off": FLOATS["mega"],
                "frequency": FLOATS["mega"],
            },
            "step": {
                "voltage_threshold": FLOATS["micro"],
                "resistance": FLOATS["micro"],
                "diode_drip_voltage": FLOATS["micro"],
                "diode_drip_resistance": FLOATS["micro"],
                "breakdown_voltage": FLOATS["micro"],
                "current_limit": FLOATS["micro"],
                "energy_on": FLOATS["micro"],
                "energy_off": FLOATS["micro"],
                "frequency": FLOATS["micro"],
            },
            "format": {
                "voltage_threshold": NUMBER_FORMATTERS["micro"],
                "resistance": NUMBER_FORMATTERS["micro"],
                "diode_drip_voltage": NUMBER_FORMATTERS["micro"],
                "diode_drip_resistance": NUMBER_FORMATTERS["micro"],
                "breakdown_voltage": NUMBER_FORMATTERS["micro"],
                "current_limit": NUMBER_FORMATTERS["micro"],
                "energy_on": NUMBER_FORMATTERS["micro"],
                "energy_off": NUMBER_FORMATTERS["micro"],
                "frequency": NUMBER_FORMATTERS["micro"],
            },
        },
        "diode": {
            "label": {
                "reverse_voltage": "Reverse Voltage",
                "forward_current": "Forward Current",
                "reverse_energy": "Reverse Energy",
            },
            "key": {
                "reverse_voltage": "diode_reverse_voltage",
                "forward_current": "diode_forward_current",
                "reverse_energy": "diode_reverse_energy",
            },
            "value": {
                "reverse_voltage": FLOATS["zero"],
                "forward_current": FLOATS["zero"],
                "reverse_energy": FLOATS["zero"],
            },
            "min": {
                "reverse_voltage": FLOATS["zero"],
                "forward_current": FLOATS["zero"],
                "reverse_energy": FLOATS["zero"],
            },
            "max": {
                "reverse_voltage": FLOATS["mega"],
                "forward_current": FLOATS["mega"],
                "reverse_energy": FLOATS["mega"],
            },
            "step": {
                "reverse_voltage": FLOATS["micro"],
                "forward_current": FLOATS["micro"],
                "reverse_energy": FLOATS["micro"],
            },
            "format": {
                "reverse_voltage": NUMBER_FORMATTERS["micro"],
                "forward_current": NUMBER_FORMATTERS["micro"],
                "reverse_energy": NUMBER_FORMATTERS["micro"],
            },
        },
        "dc_link": {
            "label": {
                "current_limit": "Current Limit",
            },
            "key": {
                "current_limit": "dc_current_limit",
            },
            "value": {
                "current_limit": FLOATS["zero"],
            },
            "min": {
                "current_limit": FLOATS["zero"],
            },
            "max": {
                "current_limit": FLOATS["mega"],
            },
            "step": {
                "current_limit": FLOATS["micro"],
            },
            "format": {
                "current_limit": NUMBER_FORMATTERS["micro"],
            },
        },
    }
}

# JACOB
JACOB = {
    "inverter_parameters": {
        "switch": {
            "label": {
                "voltage_threshold": "Voltage Threshold",
                "resistance": "Resistance",
                "diode_drip_voltage": "Diode Drip Voltage",
                "diode_drip_resistance": "Diode Drip Resistance",
                "breakdown_voltage": "Breakdown Voltage",
                "current_limit": "Current Limit",
                "energy_on": "Energy On",
                "energy_off": "Energy Off",
                "frequency": "Frequency",
            },
            "key": {
                "voltage_threshold": "switch_voltage_threshold",
                "resistance": "switch_resistance",
                "diode_drip_voltage": "switch_diode_drip_voltage",
                "diode_drip_resistance": "switch_diode_drip_resistance",
                "breakdown_voltage": "switch_breakdown_voltage",
                "current_limit": "switch_current_limit",
                "energy_on": "switch_energy_on",
                "energy_off": "switch_energy_off",
                "frequency": "switch_frequency",
            },
            "value": {
                "voltage_threshold": 0.7000,
                "resistance": 0.0013,
                "diode_drip_voltage": 0.9000,
                "diode_drip_resistance": 0.001,
                "breakdown_voltage": 400.0,
                "current_limit": 450.0,
                "energy_on": 0.0269,
                "energy_off": 0.0311,
                "frequency": 8000.0,
            },
            "min": {
                "voltage_threshold": FLOATS["zero"],
                "resistance": FLOATS["zero"],
                "diode_drip_voltage": FLOATS["zero"],
                "diode_drip_resistance": FLOATS["zero"],
                "breakdown_voltage": FLOATS["zero"],
                "current_limit": FLOATS["zero"],
                "energy_on": FLOATS["zero"],
                "energy_off": FLOATS["zero"],
                "frequency": FLOATS["zero"],
            },
            "max": {
                "voltage_threshold": FLOATS["mega"],
                "resistance": FLOATS["mega"],
                "diode_drip_voltage": FLOATS["mega"],
                "diode_drip_resistance": FLOATS["mega"],
                "breakdown_voltage": FLOATS["mega"],
                "current_limit": FLOATS["mega"],
                "energy_on": FLOATS["mega"],
                "energy_off": FLOATS["mega"],
                "frequency": FLOATS["mega"],
            },
            "step": {
                "voltage_threshold": FLOATS["micro"],
                "resistance": FLOATS["micro"],
                "diode_drip_voltage": FLOATS["micro"],
                "diode_drip_resistance": FLOATS["micro"],
                "breakdown_voltage": FLOATS["micro"],
                "current_limit": FLOATS["micro"],
                "energy_on": FLOATS["micro"],
                "energy_off": FLOATS["micro"],
                "frequency": FLOATS["micro"],
            },
            "format": {
                "voltage_threshold": NUMBER_FORMATTERS["micro"],
                "resistance": NUMBER_FORMATTERS["micro"],
                "diode_drip_voltage": NUMBER_FORMATTERS["micro"],
                "diode_drip_resistance": NUMBER_FORMATTERS["micro"],
                "breakdown_voltage": NUMBER_FORMATTERS["micro"],
                "current_limit": NUMBER_FORMATTERS["micro"],
                "energy_on": NUMBER_FORMATTERS["micro"],
                "energy_off": NUMBER_FORMATTERS["micro"],
                "frequency": NUMBER_FORMATTERS["micro"],
            },
        },
        "diode": {
            "label": {
                "reverse_voltage": "Reverse Voltage",
                "forward_current": "Forward Current",
                "reverse_energy": "Reverse Energy",
            },
            "key": {
                "reverse_voltage": "diode_reverse_voltage",
                "forward_current": "diode_forward_current",
                "reverse_energy": "diode_reverse_energy",
            },
            "value": {
                "reverse_voltage": 450.0,
                "forward_current": 700.0,
                "reverse_energy": 0.0065,
            },
            "min": {
                "reverse_voltage": FLOATS["zero"],
                "forward_current": FLOATS["zero"],
                "reverse_energy": FLOATS["zero"],
            },
            "max": {
                "reverse_voltage": FLOATS["mega"],
                "forward_current": FLOATS["mega"],
                "reverse_energy": FLOATS["mega"],
            },
            "step": {
                "reverse_voltage": FLOATS["micro"],
                "forward_current": FLOATS["micro"],
                "reverse_energy": FLOATS["micro"],
            },
            "format": {
                "reverse_voltage": NUMBER_FORMATTERS["micro"],
                "forward_current": NUMBER_FORMATTERS["micro"],
                "reverse_energy": NUMBER_FORMATTERS["micro"],
            },
        },
        "dc_link": {
            "label": {
                "current_limit": "Current Limit",
            },
            "key": {
                "current_limit": "dc_current_limit",
            },
            "value": {
                "current_limit": 450.0,
            },
            "min": {
                "current_limit": FLOATS["zero"],
            },
            "max": {
                "current_limit": FLOATS["mega"],
            },
            "step": {
                "current_limit": FLOATS["micro"],
            },
            "format": {
                "current_limit": NUMBER_FORMATTERS["micro"],
            },
        },
    }
}

# PLACEHOLDER
CUSTOM = {
    "inverter_parameters": {
        "switch": {
            "label": {
                "voltage_threshold": "Voltage Threshold",
                "resistance": "Resistance",
                "diode_drip_voltage": "Diode Drip Voltage",
                "diode_drip_resistance": "Diode Drip Resistance",
                "breakdown_voltage": "Breakdown Voltage",
                "current_limit": "Current Limit",
                "energy_on": "Energy On",
                "energy_off": "Energy Off",
                "frequency": "Frequency",
            },
            "key": {
                "voltage_threshold": "switch_voltage_threshold",
                "resistance": "switch_resistance",
                "diode_drip_voltage": "switch_diode_drip_voltage",
                "diode_drip_resistance": "switch_diode_drip_resistance",
                "breakdown_voltage": "switch_breakdown_voltage",
                "current_limit": "switch_current_limit",
                "energy_on": "switch_energy_on",
                "energy_off": "switch_energy_off",
                "frequency": "switch_frequency",
            },
            "value": {
                "voltage_threshold": FLOATS["zero"],
                "resistance": FLOATS["zero"],
                "diode_drip_voltage": FLOATS["zero"],
                "diode_drip_resistance": FLOATS["zero"],
                "breakdown_voltage": FLOATS["zero"],
                "current_limit": FLOATS["zero"],
                "energy_on": FLOATS["zero"],
                "energy_off": FLOATS["zero"],
                "frequency": FLOATS["zero"],
            },
            "min": {
                "voltage_threshold": FLOATS["zero"],
                "resistance": FLOATS["zero"],
                "diode_drip_voltage": FLOATS["zero"],
                "diode_drip_resistance": FLOATS["zero"],
                "breakdown_voltage": FLOATS["zero"],
                "current_limit": FLOATS["zero"],
                "energy_on": FLOATS["zero"],
                "energy_off": FLOATS["zero"],
                "frequency": FLOATS["zero"],
            },
            "max": {
                "voltage_threshold": FLOATS["mega"],
                "resistance": FLOATS["mega"],
                "diode_drip_voltage": FLOATS["mega"],
                "diode_drip_resistance": FLOATS["mega"],
                "breakdown_voltage": FLOATS["mega"],
                "current_limit": FLOATS["mega"],
                "energy_on": FLOATS["mega"],
                "energy_off": FLOATS["mega"],
                "frequency": FLOATS["mega"],
            },
            "step": {
                "voltage_threshold": FLOATS["micro"],
                "resistance": FLOATS["micro"],
                "diode_drip_voltage": FLOATS["micro"],
                "diode_drip_resistance": FLOATS["micro"],
                "breakdown_voltage": FLOATS["micro"],
                "current_limit": FLOATS["micro"],
                "energy_on": FLOATS["micro"],
                "energy_off": FLOATS["micro"],
                "frequency": FLOATS["micro"],
            },
            "format": {
                "voltage_threshold": NUMBER_FORMATTERS["micro"],
                "resistance": NUMBER_FORMATTERS["micro"],
                "diode_drip_voltage": NUMBER_FORMATTERS["micro"],
                "diode_drip_resistance": NUMBER_FORMATTERS["micro"],
                "breakdown_voltage": NUMBER_FORMATTERS["micro"],
                "current_limit": NUMBER_FORMATTERS["micro"],
                "energy_on": NUMBER_FORMATTERS["micro"],
                "energy_off": NUMBER_FORMATTERS["micro"],
                "frequency": NUMBER_FORMATTERS["micro"],
            },
        },
        "diode": {
            "label": {
                "reverse_voltage": "Reverse Voltage",
                "forward_current": "Forward Current",
                "reverse_energy": "Reverse Energy",
            },
            "key": {
                "reverse_voltage": "diode_reverse_voltage",
                "forward_current": "diode_forward_current",
                "reverse_energy": "diode_reverse_energy",
            },
            "value": {
                "reverse_voltage": FLOATS["zero"],
                "forward_current": FLOATS["zero"],
                "reverse_energy": FLOATS["zero"],
            },
            "min": {
                "reverse_voltage": FLOATS["zero"],
                "forward_current": FLOATS["zero"],
                "reverse_energy": FLOATS["zero"],
            },
            "max": {
                "reverse_voltage": FLOATS["mega"],
                "forward_current": FLOATS["mega"],
                "reverse_energy": FLOATS["mega"],
            },
            "step": {
                "reverse_voltage": FLOATS["micro"],
                "forward_current": FLOATS["micro"],
                "reverse_energy": FLOATS["micro"],
            },
            "format": {
                "reverse_voltage": NUMBER_FORMATTERS["micro"],
                "forward_current": NUMBER_FORMATTERS["micro"],
                "reverse_energy": NUMBER_FORMATTERS["micro"],
            },
        },
        "dc_link": {
            "label": {
                "current_limit": "Current Limit",
            },
            "key": {
                "current_limit": "dc_current_limit",
            },
            "value": {
                "current_limit": FLOATS["zero"],
            },
            "min": {
                "current_limit": FLOATS["zero"],
            },
            "max": {
                "current_limit": FLOATS["mega"],
            },
            "step": {
                "current_limit": FLOATS["micro"],
            },
            "format": {
                "current_limit": NUMBER_FORMATTERS["micro"],
            },
        },
    }
}
# PLACEHOLDER
PLACEHOLDER = {
    "inverter_parameters": {
        "switch": {
            "label": {
                "voltage_threshold": "Voltage Threshold",
                "resistance": "Resistance",
                "diode_drip_voltage": "Diode Drip Voltage",
                "diode_drip_resistance": "Diode Drip Resistance",
                "breakdown_voltage": "Breakdown Voltage",
                "current_limit": "Current Limit",
                "energy_on": "Energy On",
                "energy_off": "Energy Off",
                "frequency": "Frequency",
            },
            "key": {
                "voltage_threshold": "switch_voltage_threshold",
                "resistance": "switch_resistance",
                "diode_drip_voltage": "switch_diode_drip_voltage",
                "diode_drip_resistance": "switch_diode_drip_resistance",
                "breakdown_voltage": "switch_breakdown_voltage",
                "current_limit": "switch_current_limit",
                "energy_on": "switch_energy_on",
                "energy_off": "switch_energy_off",
                "frequency": "switch_frequency",
            },
            "value": {
                "voltage_threshold": FLOATS["zero"],
                "resistance": FLOATS["zero"],
                "diode_drip_voltage": FLOATS["zero"],
                "diode_drip_resistance": FLOATS["zero"],
                "breakdown_voltage": FLOATS["zero"],
                "current_limit": FLOATS["zero"],
                "energy_on": FLOATS["zero"],
                "energy_off": FLOATS["zero"],
                "frequency": FLOATS["zero"],
            },
            "min": {
                "voltage_threshold": FLOATS["zero"],
                "resistance": FLOATS["zero"],
                "diode_drip_voltage": FLOATS["zero"],
                "diode_drip_resistance": FLOATS["zero"],
                "breakdown_voltage": FLOATS["zero"],
                "current_limit": FLOATS["zero"],
                "energy_on": FLOATS["zero"],
                "energy_off": FLOATS["zero"],
                "frequency": FLOATS["zero"],
            },
            "max": {
                "voltage_threshold": FLOATS["mega"],
                "resistance": FLOATS["mega"],
                "diode_drip_voltage": FLOATS["mega"],
                "diode_drip_resistance": FLOATS["mega"],
                "breakdown_voltage": FLOATS["mega"],
                "current_limit": FLOATS["mega"],
                "energy_on": FLOATS["mega"],
                "energy_off": FLOATS["mega"],
                "frequency": FLOATS["mega"],
            },
            "step": {
                "voltage_threshold": FLOATS["micro"],
                "resistance": FLOATS["micro"],
                "diode_drip_voltage": FLOATS["micro"],
                "diode_drip_resistance": FLOATS["micro"],
                "breakdown_voltage": FLOATS["micro"],
                "current_limit": FLOATS["micro"],
                "energy_on": FLOATS["micro"],
                "energy_off": FLOATS["micro"],
                "frequency": FLOATS["micro"],
            },
            "format": {
                "voltage_threshold": NUMBER_FORMATTERS["micro"],
                "resistance": NUMBER_FORMATTERS["micro"],
                "diode_drip_voltage": NUMBER_FORMATTERS["micro"],
                "diode_drip_resistance": NUMBER_FORMATTERS["micro"],
                "breakdown_voltage": NUMBER_FORMATTERS["micro"],
                "current_limit": NUMBER_FORMATTERS["micro"],
                "energy_on": NUMBER_FORMATTERS["micro"],
                "energy_off": NUMBER_FORMATTERS["micro"],
                "frequency": NUMBER_FORMATTERS["micro"],
            },
        },
        "diode": {
            "label": {
                "reverse_voltage": "Reverse Voltage",
                "forward_current": "Forward Current",
                "reverse_energy": "Reverse Energy",
            },
            "key": {
                "reverse_voltage": "diode_reverse_voltage",
                "forward_current": "diode_forward_current",
                "reverse_energy": "diode_reverse_energy",
            },
            "value": {
                "reverse_voltage": FLOATS["zero"],
                "forward_current": FLOATS["zero"],
                "reverse_energy": FLOATS["zero"],
            },
            "min": {
                "reverse_voltage": FLOATS["zero"],
                "forward_current": FLOATS["zero"],
                "reverse_energy": FLOATS["zero"],
            },
            "max": {
                "reverse_voltage": FLOATS["mega"],
                "forward_current": FLOATS["mega"],
                "reverse_energy": FLOATS["mega"],
            },
            "step": {
                "reverse_voltage": FLOATS["micro"],
                "forward_current": FLOATS["micro"],
                "reverse_energy": FLOATS["micro"],
            },
            "format": {
                "reverse_voltage": NUMBER_FORMATTERS["micro"],
                "forward_current": NUMBER_FORMATTERS["micro"],
                "reverse_energy": NUMBER_FORMATTERS["micro"],
            },
        },
        "dc_link": {
            "label": {
                "current_limit": "Current Limit",
            },
            "key": {
                "current_limit": "dc_current_limit",
            },
            "value": {
                "current_limit": FLOATS["zero"],
            },
            "min": {
                "current_limit": FLOATS["zero"],
            },
            "max": {
                "current_limit": FLOATS["mega"],
            },
            "step": {
                "current_limit": FLOATS["micro"],
            },
            "format": {
                "current_limit": NUMBER_FORMATTERS["micro"],
            },
        },
    }
}
