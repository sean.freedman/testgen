import matlab.engine
import pandas as pd
import streamlit as st
from src.constants import DEFAULT_FILE_PREFIX, FILE_EXTENSIONS
from src.file_ops import csv_to_dict, csv_to_series, delete_file


def results_to_dictionary(paths, hw_dict, test_dict, cfg_dict, test_points, test_name):
    """_summary_

    Args:
        paths (_type_): _description_
        hw_dict (_type_): _description_
        test_dict (_type_): _description_
        cfg_dict (_type_): _description_
        test_points (_type_): _description_
        test_name (_type_): _description_

    Returns:
        _type_: _description_
    """

    i_sense_dict = {"speed": "Not Read", "Trim Angle": "Not Read"}

    inverter_dict = {
        "Ke": "Not Read",
        "Offset": "Not Read",
        "pole_pairs": "Not Read",
        "Build Time": "Not Read",
        "Build Name": "Not Read",
        "IOP CRC": "Not Read",
        "MCP CRC": "Not Read",
        "MCP Serial": "Not Read",
        "IOP Serial": "Not Read",
    }

    measured_dict = {}

    soft_data = csv_to_series(paths["files"]["idq"]["data"]["inverter"]["meta_data"], inverter_dict)
    i_sense_data = csv_to_dict(paths["files"]["idq"]["data"]["inverter"]["current_sensor_latencies"], i_sense_dict)
    results = csv_to_dict(paths["files"]["idq"]["data"]["results"]["measured"], measured_dict)

    output = {
        "config": {
            "name": test_name,
            "controller": hw_dict["project"]["controller"],
            "motor": hw_dict["project"]["motor"],
            "dyno": hw_dict["dyno"],
            "dcdc": hw_dict["dcdc"],
            "can": hw_dict["can"],
            "temp": cfg_dict["temp"],
            "logging": cfg_dict["logging"],
            "test": test_dict,
        },
        "paths": paths,
        "data": {
            "measured": results,
            "input": test_points,
            "current_sensor": i_sense_data,
            "inverter_properties": soft_data,
        },
    }

    return output


def generate(paths, hw_dict, test_dict, cfg_dict, test_points, test_name):
    """_summary_

    Args:
        paths (_type_): _description_
        hw_dict (_type_): _description_
        test_dict (_type_): _description_
        cfg_dict (_type_): _description_
        test_points (_type_): _description_
        test_name (_type_): _description_

    Returns:
        _type_: _description_
    """
    report_dict = results_to_dictionary(paths, hw_dict, test_dict, cfg_dict, test_points, test_name)

    if report_dict["config"]["test"]["type"] == "Torque Speed Sweep":
        report_dict["paths"]["base"] = DEFAULT_FILE_PREFIX["torque_speed"]["results"]["sweep"] + report_dict["config"]["name"]
        report_dict["paths"]["report"] = report_dict["paths"]["base"] + FILE_EXTENSIONS["CSV"]

    # TODO: determine test type depending on tab open , not test selected

    if report_dict["config"]["test"]["type"] in [
        "Idq Characterisation Gen 5",
        "Idq Characterisation Gen 4",
    ]:
        report_dict["paths"]["files"]["idq"]["data"]["results"]["base"] = DEFAULT_FILE_PREFIX["idq"]["results"]["characterisation"] + report_dict["config"]["name"]
        report_dict["paths"]["files"]["idq"]["data"]["results"]["report"] = report_dict["paths"]["files"]["idq"]["data"]["results"]["base"]
        report_dict["paths"]["files"]["idq"]["data"]["results"]["zip"] = report_dict["paths"]["files"]["idq"]["data"]["results"]["base"] + FILE_EXTENSIONS["ZIP"]

        with st.spinner("Generating MATLAB results..."):
            eng = matlab.engine.start_matlab()
            eng.addpath(report_dict["paths"]["script"]["analysis"]["root"])
            eng.idq_characterisation(
                test_name,
                paths["files"]["idq"]["data"]["results"]["measured"],
                paths["files"]["idq"]["data"]["inverter"]["current_sensor_latencies"],
                paths["files"]["idq"]["data"]["inverter"]["meta_data"],
                report_dict["paths"]["files"]["idq"]["data"]["results"]["report"],
                nargout=0,
            )
            eng.quit()
            zipped = True

        if zipped is True:
            delete_file(report_dict["paths"]["files"]["idq"]["data"]["results"]["report"])

    return report_dict["paths"]["files"]["idq"]["data"]["results"]["zip"]
