function idet_table_setup(config)
    %% Information
    % Script to Generate current LUTs for a salient PMSM with
    % flux Saturation assuming PU TMAX as an axis. Works for either the motoring or regen quadrants. 
    % Also generates plots of the output as well as losses and efficiency.
    % Calculation includes parameter variation for the Fluxes, Rs and Core Loss

    %% Parameter Description
    % Parameters imported from folder called "Input_Motor_Data.mat":
        % id_ind = Id axis values for Inductance LUTs in Amperes (rms) (1 x n)
        % iq_ind = Iq axis values for Inductance LUTs in Amperes (rms) (1 x n)
        % rpmMech_array = speed axis for the core loss and Rs input 3D
            % matricies. Min of 2 input. Must cover full speed rage you wish to
            % compute tables for (1 x m). {Automatically calculated in this
            % script for JV)
        % Psi_d_input = D axis flux table data in Webers rms as a fuction of Id (rms) 
            % and Iq (rms) (n x n)
        % Psi_q_input = Q axis flux table data in Webers (rms) (n x n)
        % Npp = No of Pole Pairs. Single even positive integers only.
        % Rs_const = constant value of Rs. Used in this version of the script
            % for apps to set the Rs matix to be a constant value throughout. 
        % Rs = Table of Stator resistance in Ohms as a fuction of Id (rms), Iq
            % (rms) and mechanical speed (rpm) (n x n x m). In this version of
            % the script for JV all values of this matrix are set to Rs_const
        % Core_loss = Table of Total Motor Core Loss in Watts as a fuction of
            % Id (rms), Iq (rms) and mechanical speed (rpm) (n x n x m). {Automatically calculated in this
            % script for JV and set to zero)
            
    % Parameters imported from folder called "Input_Inverter_Data.mat":
        % V_thr_z_0 =  Threshold Voltage for Switching device in Volts (should be zero for a FET)
        % R_thr_z = Switching Device resistive element in Ohms (Rdson for a FET)
        % V_thr_d_0 =  Threshold diode drip for switching device in Volts
        % R_thr_d =  Resistive element of the diode drop in Ohms 
        % Vdssbr =  Breakdown voltage of switching device in Volts
        % Idss =  Max current of the switching device in Amperes
        % Eon =  Turn on energy of the switching device in Joules
        % Eoff=  Turn off energy of the switching device in Joules
        % Fsw =  Switching frequency    
        % V_RRM = Diode Repetitive peak reverse voltage
        % I_F_MAX = Diode Continuous Forward Current
        % Err = Reverse Recovery Energy of the Diode in Joules
    %% Folders to add to path
    addpath config.source_path;       % adding subfolders to the path 

    %% Load data to worksapce from unzipped folder
    load(config.input.data.interpolated_data);
    load(config.input.data.interpolated_table);

    %% Input commands
    % Define the axis limits and resolution you require
    if config.input.table.profile == "Motoring"
        Regen = false; % Decide if motoring or Regen is required. (false for Motoring; true for Regenerating)
    else
        Regen = true; % Decide if motoring or Regen is required. (false for Motoring; true for Regenerating)
    end

    if config.input.table.methods.max_torque == "MTPA"
        MTPSL = false;                             % Decide if MTPA or MTPSL is required. (false for MTPA; true for MTPSL)
    else
        MTPSL = true;                             % Decide if MTPA or MTPSL is required. (false for MTPA; true for MTPSL)
    end

    if config.input.table.methods.svm == "SPWM"
        SixStep = false;                             % Six step or SPWM
    else
        SixStep = true;                             % Six step or SPWM
    end

    test_name = config.output.file_names.test + "_"; % Name of the test to be used in the output file name

    PUT_tables = config.input.table.methods.per_unit_torque;                      % true to output LUTs with PU Torque dimension

    %% Parameters to be Defined here

    %% Table Parameters
    T_LUT_size = double(config.input.table.breakpoints.torque_size);    % Dimesion size for LUT Torque Axis
    w_array_length = double(config.input.table.breakpoints.speed_size); % Number of speed operating points required
    Vdclink_array = eval(config.input.table.breakpoints.vdc);     % Creating array of DC link Voltages for Tables
    MI_max = pi/(2*sqrt(3))*double(config.input.table.limits.max.mod_index);  % Max allowable Mod Index
    istep = double(config.input.table.interpolation_resolution);    % Defining step sizes to use in the current determination calculation (the smaller the value the longer the run time, but the greater the accuracy) 
    id_ind = double(interpolated_data.id_demand_rms); % Defining the Id axis values for the LUTs
    iq_ind = double(interpolated_data.iq_demand_rms); % Defining the Iq axis values for the LUTs
    Psi_q_input = double(interpolated_data.psi_q_rms); % Defining the Q axis flux table data in Webers (rms)
    Psi_d_input = double(interpolated_data.psi_d_rms); % Defining the D axis flux table data in Webers (rms)
    idc_limit = double(config.input.table.limits.max.idc); % Maximum Current limit to generate to.
    
    %% Motor Parameters
    Npp = double(config.input.parameters.motor.pole_pairs); % Defining the No of Pole Pairs
    w_max = double(config.input.table.limits.max.speed);       % Maximum desired operating speed

    %% Inverter Parameters
    %% Switch Parameters
    V_thr_z_0 = double(config.input.parameters.inverter.switch.volt_limit); %  Threshold Voltage for Switching device in Volts (should be zero for a FET)
    R_thr_z = double(config.input.parameters.inverter.switch.resistance); % Switching Device resistive element in Ohms (Rdson for a FET)
    V_thr_d_0 = double(config.input.parameters.inverter.switch.diode_drip_voltage);  %  Threshold diode drip for switching device in Volts
    R_thr_d = double(config.input.parameters.inverter.switch.diode_drip_resistance);  %  Resistive element of the diode drop in Ohms 
    Vdssbr = double(config.input.parameters.inverter.switch.breakdown_voltage); %  Breakdown voltage of switching device in Volts
    Idss = double(config.input.parameters.inverter.switch.i_limit); %  Max current of the switching device in Amperes
    Eon = double(config.input.parameters.inverter.switch.energy_on); %  Turn on energy of the switching device in Joules
    Eoff= double(config.input.parameters.inverter.switch.energy_off); %  Turn off energy of the switching device in Joules
    Fsw = double(config.input.parameters.inverter.switch.frequency); %  Switching frequency   

    %% Diode Parameters
    V_RRM = double(config.input.parameters.inverter.diode.reverse_voltage); % Diode Repetitive peak reverse voltage
    I_F_MAX = double(config.input.parameters.inverter.diode.forward_current); % Diode Continuous Forward Current
    Err = double(config.input.parameters.inverter.diode.reverse_energy);  % Reverse Recovery Energy of the Diode in Joules
    
    %% DC Link Parameters
    i_limit = double(config.input.parameters.inverter.dc_link.current_limit); % DC current limit

    % -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    %% Cheat for Apps
    % If Rs is entered as a single (constant) value, and no Core Losses are
    % provided, then leave the following lines uncommented, else comment out
    rpmMech_array = [0, w_max];  % making the smallest array of speed inputs for the core loss and Rs input                      
    Rs = zeros(numel(id_ind),numel(iq_ind),numel(rpmMech_array));        % preconditioning Rs table
    Rs(:,:,:) = config.input.parameters.motor.stator_resistance; clear stator_resistance                                                                   %Assigning Constant value to every value in the Rs array
    Core_loss = zeros(numel(id_ind),numel(iq_ind),numel(rpmMech_array));             % Setting core loss to zero
    %------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    %% Output configuration
    %% Plots
    Plot_Mot_Input = config.output.options.plot_motor_input;                                         % Set to true to plot input data, false if you don't want to
    Plot_Table_Output = config.output.options.plot_motor_output;                                     % Set to  true to plot output data, false if you don't want to
    
    %% File Formats
    excel_export = config.output.options.output_to_excel;                                             % true to export tables to excel

    %% Filenames
    output_mat_filename  = test_name + config.output.file_names.mat;    % file name for output .mat file. Do not include file extension
    xls_export_filename = test_name + config.output.file_names.excel;          % file name for output excel file if requested. Must include .xlsx file extension
    output_path = config.output.paths.temp; % path to save output files
    

    run 'Table_Generator.m';
end