%% Current Determination Function - Regen
function [id_desired,...
    iq_desired,...
    TmaxVw,...
    Vd_desired,...
    Vq_desired,...
    P_mech,...
    P_ac_desired,...
    P_dc_desired,...
    P_mot_loss_desired,...
    P_inv_loss_tot_desired,...
    Eff_mot_desired,...
    Eff_inv_desired,...
    Eff_sys_desired] ...
    =IDet_Regen(N_mech,Vdclink,MI_max,istep,id_ind,iq_ind,rpmMech_array, Psi_d_input,Psi_q_input,...
                           Npp,Rs,Core_loss,i_limit,idc_limit,T_LUT_size,Tmaxregen0,...
                            V_thr_z_0,R_thr_z,V_thr_d_0,R_thr_d,...
                            Vdssbr,Idss,Eon,Eoff,Fsw,...
                            V_RRM,I_F_MAX,Err,SixStep,PUT_tables,MTPSL)
%% Description
% Script to calculate current data points for a set voltage and electrical frequency in regen

% Frequency Conversions
w_mech = (2 * pi / 60) * N_mech;                           % Mechanical speed in rad/s

%% Torque Calculation
% Function Call of Torque Calculation
                [TmaxVw,T_meshgrid] =TmaxRegen(N_mech,Vdclink,MI_max,istep,id_ind,iq_ind,rpmMech_array, Psi_d_input,Psi_q_input,...
                                Npp,Rs,Core_loss,i_limit,idc_limit,...
                                V_thr_z_0,R_thr_z,V_thr_d_0,R_thr_d,...
                                Vdssbr,Idss,Eon,Eoff,Fsw,...
                                V_RRM,I_F_MAX,Err,SixStep);

%% Creating a Meshgrids for Lq and Ld
% iq_ind = iq_ind .* -1;                                          % Since assuming +ve Iq is regen but data is negative Iq for regen so flip axis data

[Id_lowres,Iq_lowres]=meshgrid(id_ind,iq_ind);                                                                                  % for original Inductance Table Data
[Id,Iq]=meshgrid(min(id_ind):istep:max(id_ind)/10,max(iq_ind):-istep:min(iq_ind)/10); % for the interpolated data used in the rest of the m file

Rs_2D = interp3(Id_lowres(1,:),Iq_lowres(:,1),rpmMech_array,Rs,Id(1,:),Iq(:,1),N_mech);                     % Creating Interpolated table of Rs to fit the Id and Iq step sizes as well as speed step
Core_loss_2D = (interp3(Id_lowres(1,:),Iq_lowres(:,1),rpmMech_array,Core_loss,Id(1,:),Iq(:,1),N_mech))./3;                     % Creating Interpolated table of Core loss to fit the Iod and Ioq step sizes as well as speed step
 
%% MTPA Calculation
% Array Pre-allocation for speed
T_data_step = zeros(T_LUT_size,1);                              
id_desired = zeros(T_LUT_size,1);                                        
iq_desired = zeros(T_LUT_size,1);                                        
Vd_desired = zeros(T_LUT_size,1);   
Vq_desired = zeros(T_LUT_size,1);
P_mech = zeros(T_LUT_size,1);  
P_ac_desired = zeros(T_LUT_size,1);   
P_dc_desired = zeros(T_LUT_size,1);   
P_mot_loss_desired = zeros(T_LUT_size,1);   
P_inv_loss_tot_desired = zeros(T_LUT_size,1);   
Eff_mot_desired = zeros(T_LUT_size,1);   
Eff_inv_desired = zeros(T_LUT_size,1);   
Eff_sys_desired = zeros(T_LUT_size,1);   
TrqContour_edited = zeros(1,1);
equivCircData = zeros(11,1);

for n=1:1:T_LUT_size
    % Create array of Torque Steps for MTPA Axis data
    if PUT_tables == true
        T_data_step(n,1) = (n-1)*(TmaxVw/(T_LUT_size-1));                    % Torque step
    else
        T_data_step(n,1) = (n-1)*(Tmaxregen0/(T_LUT_size-1));                    % Torque step
            
            if T_data_step(n,1)>=TmaxVw
                T_data_step(n,1) = TmaxVw;
            end
    end  
    
%************************** 
% It appears contourc is not always happy with T = 0Nm, so small fudge to avoid issue    
    if T_data_step(n,1)<0.001
        T_data_step(n,1)=0.001;
    end
  %*************************  
    
    P_mech(n,1) = T_data_step(n,1) * w_mech;                % Motor Mechanical Power step
    
    %Select a contour line for given Torque Step
    TrqContour = contourc(Id(1,:),Iq(:,1),T_meshgrid,[T_data_step(n,1) T_data_step(n,1)]);     %find data for corresponding torque contour line
    TrqContour(:,end)= round(TrqContour(:,end),4);  % contour curve fitting issues seem to add error in extreme data point, therfore round to remove additional error
    TrqContour(:,1)= round(TrqContour(:,1),4); % contour curve fitting issues seem to add error in extreme data point, therfore round to remove additional error
    clear TrqContour_edited;
    TrqContour_edited(1,:) = TrqContour(1,2:TrqContour(2,1)+1);
    TrqContour_edited(2,:) = TrqContour(2,2:TrqContour(2,1)+1);  
    
    TrqContour_edited(:, TrqContour_edited(1,:) > 0) = [];                                                   % Removes all columns where the endry in row 1 is positive (as row1 is id should always be zero or less)
     if TrqContour_edited(1,1) < 0
            % First entry  for id may still be 0 and shouldn't be
            % This selects only relevant current data from the contour line data
            Id_cont_line = TrqContour_edited(1,:);   
            Iq_cont_line = TrqContour_edited(2,:);
     else
         Id_cont_line = TrqContour_edited(1,2:end);   
         Iq_cont_line = TrqContour_edited(2,2:end);
     end
     
     if numel(Id_cont_line)~=numel(unique(Id_cont_line)) %Check if there are non unique values
            [~, id_unique_ind] = unique(Id_cont_line);        % determines which index positions are unique in the Id array
            duplicate_ind = setdiff(1:numel(Id_cont_line), id_unique_ind); % returns the position of all duplicated values, minus the original value

            id_non_unique = Id_cont_line(duplicate_ind); % Finds which values of Id are repeated
            id_non_unique = unique(id_non_unique);       % Filters to unique enties of the repeated Id values only
            duplicate_ind_MASTER =[];
            for nn = 1:numel(id_non_unique)
                duplicate_ind = find(Id_cont_line == id_non_unique(nn)); % finds the position of all the repeated Id values
                duplicate_ind_MASTER = [duplicate_ind_MASTER duplicate_ind];
            end

            id_non_unique= Id_cont_line(duplicate_ind_MASTER); % all non unique Id entries
            iq_val_for_id_non_unique= Iq_cont_line(duplicate_ind_MASTER);   % corresponding Iq value for all non unique Id values
            Is_val_for_id_non_unique = hypot(id_non_unique,iq_val_for_id_non_unique); % corresponding Is value for all non unique Id values

            [~,id_unique_ind2] = min(Is_val_for_id_non_unique);                        % finding the Is value of smallest magnitude from the duplicates
            duplicate_ind_MASTER(id_unique_ind2) =[];                    % removing the Is value of smallest magnitude from the dupliate position array

            Id_cont_line(duplicate_ind_MASTER) = [];           % removing all duplicates from Id array leaving the value with the smallest Id mag
            Iq_cont_line(duplicate_ind_MASTER) = [];           % removing all duplicates from Id array leaving the value with the smallest Id mag
     end
     
    % Interpolating up the dataset size
    if abs(T_data_step(n,1) ) > 50 && abs(T_data_step(n,1) ) < max(T_data_step)  && abs(N_mech) >1000
        Id_cont_line_1 = linspace(min(Id_cont_line),max(Id_cont_line),20000);
        Iq_cont_line_1 = interp1(Id_cont_line,Iq_cont_line,Id_cont_line_1,'spline');
    else
        Id_cont_line_1 = Id_cont_line;
        Iq_cont_line_1 = Iq_cont_line;
    end
    
    Rs_cont_line_1 = interp2(Id(1,:),Iq(:,1),Rs_2D,Id_cont_line_1,Iq_cont_line_1);
    Core_loss_line_1 = interp2(Id(1,:),Iq(:,1),Core_loss_2D,Id_cont_line_1,Iq_cont_line_1);
    Psi_q_cont_line_1 = interp2(Id_lowres,Iq_lowres,Psi_q_input,Id_cont_line_1,Iq_cont_line_1); % creates corresponding grid for Psi_q
    Psi_d_cont_line_1 = interp2(Id_lowres,Iq_lowres,Psi_d_input,Id_cont_line_1,Iq_cont_line_1); % creates corresponding grid for Psi_q    
    
    % Equivalent circuit calulations on the contour line data
     [~,~,~,Vd_cont_line_1,Vq_cont_line_1,Vs_cont_line_1,~,~,~,~,~,...
         Is_cont_line_1,Vdrop_cont_line_1,~,PF_cont_line_1,MI_cont_line_1]...
         = equivCircuitEqns(Npp,N_mech,Psi_d_cont_line_1,Psi_q_cont_line_1,Id_cont_line_1,Iq_cont_line_1,Vdclink,...
         Rs_cont_line_1,Core_loss_line_1,V_thr_z_0,R_thr_z,true);
     
    % Determining the spread of points that satisfy condition of V<=Vdclink_max &
    % i<= i_limit
    index_cont_line_1_first = max(find(MI_cont_line_1 <=MI_max,1,"first"),find(Is_cont_line_1<=i_limit,1,"first"));
    index_cont_line_1_last = min(find(MI_cont_line_1<=MI_max,1,"last"),find(Is_cont_line_1<=i_limit,1,"last"));    
      
    if isempty(index_cont_line_1_first)==true || isempty(index_cont_line_1_last)==true % Ignore data if V<Vdclink_max is never true
        id_desired(n,1)=NaN;      % write corresponding value of id to array of id for MTPA
        iq_desired(n,1)=NaN;      % write corresponding value of iq to array of id for MTPA       
    else
            % Create new arrays which satisfy condition of V<=Vdclink_max
             clear equivCircData;
             equivCircData(1,:) = MI_cont_line_1(index_cont_line_1_first:index_cont_line_1_last);      % create temporary array of iod for Torque contour line
             equivCircData(2,:) = PF_cont_line_1(index_cont_line_1_first:index_cont_line_1_last);      % create temporary array of ioq for Torque contour line
             equivCircData(3,:) = Id_cont_line_1(index_cont_line_1_first:index_cont_line_1_last);  %***    % create temporary array of id for Torque contour line
             equivCircData(4,:) = Iq_cont_line_1(index_cont_line_1_first:index_cont_line_1_last);   %***   % create temporary array of iq for Torque contour line
             equivCircData(5,:) = Is_cont_line_1(index_cont_line_1_first:index_cont_line_1_last);          % calculate current magnitude for id and iq points on Torque contour
             equivCircData(6,:) = Vd_cont_line_1(index_cont_line_1_first:index_cont_line_1_last);  % create temporary array of Vd for Torque contour line
             equivCircData(7,:) = Vq_cont_line_1(index_cont_line_1_first:index_cont_line_1_last);  % create temporary array of Vq for Torque contour line
             equivCircData(8,:) = Vs_cont_line_1(index_cont_line_1_first:index_cont_line_1_last);      % create temporary array of V for Torque contour line
             equivCircData(9,:) = Vdrop_cont_line_1(index_cont_line_1_first:index_cont_line_1_last);      % create temporary array of Vdrop for Torque contour line
             equivCircData(10,:) = Rs_cont_line_1(index_cont_line_1_first:index_cont_line_1_last);
             equivCircData(11,:) = Core_loss_line_1(index_cont_line_1_first:index_cont_line_1_last);
                      
             equivCircData(:, equivCircData(3,:) > 0) = []; % Removes all entries corresponding to an Id greater than 0
             
                if sum(equivCircData(4,:) > 0) > 0.1 && T_data_step(n,1) > 0.5
                    equivCircData(:, equivCircData(4,:) < 0) = []; % removes all entries corresponding to an Iq of less than 0
                end
                [~,Id_cont_line_2min_pos] = min(equivCircData(3,:)); % finding position of the minimum in Id_cont_line_temp_2 
                equivCircData = equivCircData(:,Id_cont_line_2min_pos:end); % removing any data to the left of the mimumum in Id_cont_line_temp_2
             
            MI_cont_line_2 = equivCircData(1,:);
            PF_cont_line_2 = equivCircData(2,:);            
            Id_cont_line_2 = equivCircData(3,:);
            Iq_cont_line_2 = equivCircData(4,:);
            Is_cont_line_2 = equivCircData(5,:);
            Vd_cont_line_2 = equivCircData(6,:);
            Vq_cont_line_2 = equivCircData(7,:);
            Rs_cont_line_2 = equivCircData(10,:);
            Core_loss_line_2 = equivCircData(11,:);
                                     
             % Calculation of Inverter Losses                      
             P_inv_loss_tot = pLossInv(Is_cont_line_2,Vdclink,Fsw,V_thr_z_0,R_thr_z,V_thr_d_0,R_thr_d,PF_cont_line_2,MI_cont_line_2,Eon,Eoff,Err,Vdssbr,Idss,V_RRM,I_F_MAX,true,SixStep);

             % Calculation of Inverter Losses
             P_mot_loss = 3 .*( (Is_cont_line_2 .^2) .* Rs_cont_line_2 + Core_loss_line_2);
             P_ac = - P_mot_loss + P_mech(n,1);                         % AC power into Inverter
             P_dc = - P_inv_loss_tot + P_ac;                                   % DC Input Power (Assuming no resistive loss in inverter of cabling)
            
            % Efficiency Calculations
             Eff_mot = (P_ac ./ P_mech(n,1)).*100;                        % Motor Efficiency (accounting for Stator Losses only at this stage)
                Eff_mot(abs(P_mech(n,1))<1e-3) = NaN;                        % Accounting for divide by zero condition
                Eff_mot(abs(T_data_step(n,1))<0.01) = NaN;                        % Accounting for divide by zero condition
            Eff_inv = (P_dc ./ P_ac).*100;                                      % Inverter Efficiency Calculation
                Eff_inv(abs(P_mech(n,1))<1e-3) = NaN;                          
                Eff_inv(Eff_inv>100) = NaN; 
            Eff_sys = (P_dc ./ P_mech(n,1)).*100;                        % System Efficiency (with assumptions above)
                Eff_sys(abs(P_mech(n,1))<1e-3) = NaN;                        % Accounting for divide by zero condition
                        
             % ***Calculate MTPA points from spread of contour line
             % satisfying condition of V<=Vdclink_max***
             [~, MTPA_pos]= min(Is_cont_line_2);   % finding position of smallest current magnitude on Torque contour
             [~, MTPSL_pos]= max(P_dc);   % finding position of smallest current magnitude on Torque contour
               
             if MTPSL ==false
                 
                 if abs(Is_cont_line_2(MTPA_pos))<= i_limit
                      id_desired(n,1) = Id_cont_line_2(1,MTPA_pos);      % write corresponding value of id to array for MTPA
                      iq_desired(n,1) = Iq_cont_line_2(1,MTPA_pos);     % write corresponding value of iq to array for MTPA  
                      Vd_desired(n,1) = Vd_cont_line_2(1,MTPA_pos);      % write corresponding value of Vd to array for MTPA
                      Vq_desired(n,1) = Vq_cont_line_2(1,MTPA_pos);      % write corresponding value of Vq to array for MTPA
                      P_ac_desired(n,1) = P_ac(1,MTPA_pos);      % write corresponding value of Pac to array for MTPA
                      P_dc_desired(n,1) = P_dc(1,MTPA_pos);      % write corresponding value of Pdc to array for MTPA
                      P_mot_loss_desired(n,1) = P_mot_loss(1,MTPA_pos);      % write corresponding value of P_mot_loss to array for MTPA
                      P_inv_loss_tot_desired(n,1) = P_inv_loss_tot(1,MTPA_pos);      % write corresponding value of P_inv_loss to array for MTPA
                      Eff_mot_desired(n,1) =  Eff_mot(1,MTPA_pos);      % write corresponding value of motor efficiency to array for MTPA
                      Eff_inv_desired(n,1) = Eff_inv(1,MTPA_pos);      % write corresponding value of inverter efficiency to array for MTPA
                      Eff_sys_desired(n,1) = Eff_sys(1,MTPA_pos);      % write corresponding value of system efficiency to array for MTPA
                 else
                      id_desired(n,1)=NaN;      % write corresponding value of id to array of id for MTPA
                      iq_desired(n,1)=NaN;      % write corresponding value of iq to array of id for MTPA
                      Vd_desired(n,1) = NaN;      % write corresponding value of Vd to array for MTPA
                      Vq_desired(n,1) = NaN;      % write corresponding value of Vq to array for MTPA
                      P_ac_desired(n,1) = NaN;      % write corresponding value of Pac to array for MTPA
                      P_dc_desired(n,1) = NaN;      % write corresponding value of Pdc to array for MTPA
                      P_mot_loss_desired(n,1) = NaN;      % write corresponding value of P_mot_loss to array for MTPA
                      P_inv_loss_tot_desired(n,1) = NaN;      % write corresponding value of P_inv_loss to array for MTPA
                      Eff_mot_desired(n,1) =  NaN;      % write corresponding value of motor efficiency to array for MTPA
                      Eff_inv_desired(n,1) = NaN;      % write corresponding value of inverter efficiency to array for MTPA
                      Eff_sys_desired(n,1) = NaN;      % write corresponding value of system efficiency to array for MTPA
                end            % end if      
                
             else
                 
                if abs(Is_cont_line_2(MTPSL_pos))<= i_limit
                      id_desired(n,1) = Id_cont_line_2(1,MTPSL_pos);      % write corresponding value of id to array for MTPA
                      iq_desired(n,1) = Iq_cont_line_2(1,MTPSL_pos);      % write corresponding value of iq to array for MTPA  
                      Vd_desired(n,1) = Vd_cont_line_2(1,MTPSL_pos);      % write corresponding value of Vd to array for MTPA
                      Vq_desired(n,1) = Vq_cont_line_2(1,MTPSL_pos);      % write corresponding value of Vq to array for MTPA
                      P_ac_desired(n,1) = P_ac(1,MTPSL_pos);      % write corresponding value of Pac to array for MTPA
                      P_dc_desired(n,1) = P_dc(1,MTPSL_pos);      % write corresponding value of Pdc to array for MTPA
                      P_mot_loss_desired(n,1) = P_mot_loss(1,MTPSL_pos);      % write corresponding value of P_mot_loss to array for MTPA
                      P_inv_loss_tot_desired(n,1) = P_inv_loss_tot(1,MTPSL_pos);      % write corresponding value of P_inv_loss to array for MTPA
                      Eff_mot_desired(n,1) =  Eff_mot(1,MTPSL_pos);      % write corresponding value of motor efficiency to array for MTPA
                      Eff_inv_desired(n,1) = Eff_inv(1,MTPSL_pos);      % write corresponding value of inverter efficiency to array for MTPA
                      Eff_sys_desired(n,1) = Eff_sys(1,MTPSL_pos);      % write corresponding value of system efficiency to array for MTPA
                elseif abs(Is_cont_line_2(MTPA_pos))<= i_limit
                      id_desired(n,1) = Id_cont_line_2(1,MTPA_pos);      % write corresponding value of id to array for MTPA
                      iq_desired(n,1) = Iq_cont_line_2(1,MTPA_pos);      % write corresponding value of iq to array for MTPA  
                      Vd_desired(n,1) = Vd_cont_line_2(1,MTPA_pos);      % write corresponding value of Vd to array for MTPA
                      Vq_desired(n,1) = Vq_cont_line_2(1,MTPA_pos);      % write corresponding value of Vq to array for MTPA
                      P_ac_desired(n,1) = P_ac(1,MTPA_pos);      % write corresponding value of Pac to array for MTPA
                      P_dc_desired(n,1) = P_dc(1,MTPA_pos);      % write corresponding value of Pdc to array for MTPA
                      P_mot_loss_desired(n,1) = P_mot_loss(1,MTPA_pos);      % write corresponding value of P_mot_loss to array for MTPA
                      P_inv_loss_tot_desired(n,1) = P_inv_loss_tot(1,MTPA_pos);      % write corresponding value of P_inv_loss to array for MTPA
                      Eff_mot_desired(n,1) =  Eff_mot(1,MTPA_pos);      % write corresponding value of motor efficiency to array for MTPA
                      Eff_inv_desired(n,1) = Eff_inv(1,MTPA_pos);      % write corresponding value of inverter efficiency to array for MTPA
                      Eff_sys_desired(n,1) = Eff_sys(1,MTPA_pos);      % write corresponding value of system efficiency to array for MTPA
                else
                      id_desired(n,1)=NaN;      % write corresponding value of id to array of id for MTPA
                      iq_desired(n,1)=NaN;      % write corresponding value of iq to array of id for MTPA
                      Vd_desired(n,1) = NaN;      % write corresponding value of Vd to array for MTPA
                      Vq_desired(n,1) = NaN;      % write corresponding value of Vq to array for MTPA
                      P_ac_desired(n,1) = NaN;      % write corresponding value of Pac to array for MTPA
                      P_dc_desired(n,1) = NaN;      % write corresponding value of Pdc to array for MTPA
                      P_mot_loss_desired(n,1) = NaN;      % write corresponding value of P_mot_loss to array for MTPA
                      P_inv_loss_tot_desired(n,1) = NaN;      % write corresponding value of P_inv_loss to array for MTPA
                      Eff_mot_desired(n,1) =  NaN;      % write corresponding value of motor efficiency to array for MTPA
                      Eff_inv_desired(n,1) = NaN;      % write corresponding value of inverter efficiency to array for MTPA
                      Eff_sys_desired(n,1) = NaN;      % write corresponding value of system efficiency to array for MTPA
                end            % end if                
             end    
               
    end % end if   
     
%     fprintf('   T = %d Nm,      ',T_data_step(n,1));
%     fprintf('id = %d A,      ',id_desired(n,1));
%     fprintf('iq = %d A\n',iq_desired(n,1));
end % end for loop for MTPA plot points


