%% TMax Deterination Function - Motoring
function [TmaxVw,T_meshgrid] =TmaxMotor(N_mech, Vdclink,MI_max,istep,id_ind,iq_ind,rpmMech_array, Psi_d_input,Psi_q_input,...
                                Npp,Rs,Core_loss,i_limit,idc_limit,...
                                V_thr_z_0,R_thr_z,V_thr_d_0,R_thr_d,...
                                Vdssbr,Idss,Eon,Eoff,Fsw,...
                                V_RRM,I_F_MAX,Err,SixStep)                            
%% Description
% Script to calculate Maximum Torque achievable while motoring for the voltage and current limits defined
% Also outputs the grid of Torque values for input values of Id and Iq

%% Creating a Meshgrids for Lq and Ld
[Id_lowres,Iq_lowres]=meshgrid(id_ind,iq_ind);  
[Id,Iq]=meshgrid(min(id_ind):istep:0,0:istep:max(iq_ind));                                                  % for the interpolated data used in the rest of the m file 

Psi_q= interp2(Id_lowres,Iq_lowres,Psi_q_input,Id,Iq);                                                                          % Creating Interpolated Flux tables to fit the Id and Iq step sizes
Psi_d= interp2(Id_lowres,Iq_lowres,Psi_d_input,Id,Iq); 
            

% Creating Interpolated Flux tables to fit the Id and Iq step sizes
Rs_2D = interp3(Id_lowres(1,:),Iq_lowres(:,1),rpmMech_array,Rs,Id(1,:),Iq(:,1),N_mech);                     % Creating Interpolated table of Rs to fit the Id and Iq step sizes as well as speed step
Core_loss_2D = (interp3(Id_lowres(1,:),Iq_lowres(:,1),rpmMech_array,Core_loss,Id(1,:),Iq(:,1),N_mech))./3;                     % Creating Interpolated table of Core loss to fit the Iod and Ioq step sizes as well as speed step

%% Equivalent circuit Equations
[~,~,~,Vd,Vq,~,~,~,~,Iod,Ioq,Is,~,~,PF,MI] = equivCircuitEqns(Npp,N_mech,Psi_d,Psi_q,Id,Iq,Vdclink,Rs_2D,Core_loss_2D,V_thr_z_0,R_thr_z,false);

%% Inverter Losses
% Calculation of Inverter Losses                   
P_inv_loss = pLossInv(Is,Vdclink,Fsw,V_thr_z_0,R_thr_z,V_thr_d_0,R_thr_d,PF,MI,Eon,Eoff,Err,Vdssbr,Idss,V_RRM,I_F_MAX,false,SixStep);

%% Torque Calculation
T_meshgrid = 3*Npp*(Psi_d.*Ioq - Psi_q.*Iod);                                                  % Master Torque Grid

%% System Loss
P_ac = 3 .* (Vd .* Id + Vq .* Iq);                % AC Output power to Motor
P_dc = P_inv_loss + P_ac;                           % DC link Power

idc = P_dc ./Vdclink;                                   % DC link current

%% Determining Current and MI Limits
% Logic to check if current vecor is possible
Is_possible = Is;                                                      % Creating meshgrid of Is to determine if it is possible
Is_possible(Is_possible>i_limit) = NaN;                  % Removing Is mag values greater than the current limit
Is_possible(Is_possible>0) = 1;                               % Converting to Boolean for logic test                      

% Logic to check if Mod Index is possible
MI_allowed = MI;
MI_allowed(MI_allowed>MI_max) = NaN;
MI_allowed(MI_allowed>=0) = 1;

% Logic to check if Mod Index is possible            
idc_allowed = idc;
idc_allowed(idc_allowed>idc_limit) = NaN;
idc_allowed(idc_allowed>=0) = 1;            

%% Torque Rationalisation
% Create subset of T to calcultae max possible Torque with the current limit setting
T_subset = T_meshgrid;                                                                                                       % Create duplicate Torque meshgrid
T_subset  = T_subset .*Is_possible .* MI_allowed .*idc_allowed;                   % Remove Torque values not possible with the current and MI Limit
TmaxVw = max(T_subset(:));                                                                               % Max Torque in array of all Possible torque values

end

