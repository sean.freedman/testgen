function [Voq,Vod,Vo,Vd,Vq,Vs,Rc,Icd,Icq,Iod,Ioq,Is,Vdrop,theta,PF,MI] = equivCircuitEqns(Npp,N_mech,Psi_d,Psi_q,Id,Iq,Vdclink,Rs,Core_loss,V_thr_z_0,R_thr_z,Regen)
%equivCircuitEqns PMAC Equivalent Circuit Calculations
%   This function performs the PMAC equivalent circuit equations assuming
%   the per phase rotor oriented model

% Frequency Conversions
w_elec = (2 * pi / 60) * Npp* N_mech;                    % Electrical speed in rad/s

% Determining the size of the meshgrid used
Gridrows = size(Id); Gridrows = Gridrows(1);                   
Gridcolumns = size(Id,2); Gridcolumns = Gridcolumns(1);

if Regen == true
    % Calculations for Regen
    if abs(w_elec) < 1e-3
        Voq =  zeros(Gridrows,Gridcolumns);
        Vod =  zeros(Gridrows,Gridcolumns);
        Vo = zeros(Gridrows,Gridcolumns);
        Rc = Inf(Gridrows,Gridcolumns);
        Icd = zeros(Gridrows,Gridcolumns);
        Icq = zeros(Gridrows,Gridcolumns);
    else
        Voq = + w_elec.*Psi_d;
        Vod = + w_elec.*Psi_q;
        Vo = hypot(Voq,Vod);
        Rc = (Vo.^2)./Core_loss;
            Rc(abs(Core_loss)<1e-9) = Inf; 
        Icd = Vod./Rc;
        Icq = Voq./Rc;
    end

    Iod = Id + Icd;
    Ioq = Iq + Icq;
    Vd = - Id.*Rs + Vod;
    Vq = - Iq .* Rs + Voq;
    Is = hypot(Id,Iq);
    Vs = hypot(Vd,Vq);
    Vdrop = (sqrt(2) .* Is .* R_thr_z + V_thr_z_0) .* 1.5;               % Voltage drop across inverter
    theta = atan2(Vd,Vq) - atan2(Id,Iq);   
    PF = cos(theta);
    MI = (sqrt(2) .* Vs) ./ ((2 ./ pi) .* (Vdclink + Vdrop));
    
else
    % Calculations for Motoring
    if abs(w_elec) < 1e-3
        Voq =  zeros(Gridrows,Gridcolumns);
        Vod =  zeros(Gridrows,Gridcolumns);
        Vo = zeros(Gridrows,Gridcolumns);
        Rc = Inf(Gridrows,Gridcolumns);
        Icd = zeros(Gridrows,Gridcolumns);
        Icq = zeros(Gridrows,Gridcolumns);
    else
        Voq =  w_elec.*Psi_d;
        Vod =  - w_elec.*Psi_q;
        Vo = hypot(Voq,Vod);
        Rc = (Vo.^2)./Core_loss;
            Rc(abs(Core_loss)<1e-9) = Inf; 
        Icd = Vod./Rc;
        Icq = Voq./Rc;
    end

    Iod = Id - Icd;
    Ioq = Iq - Icq;
    Vd = Id.*Rs + Vod;
    Vq = Iq .* Rs + Voq;
    Is = hypot(Id,Iq);
    Vs = hypot(Vd,Vq);    
    Vdrop = (sqrt(2) .* Is .* R_thr_z + V_thr_z_0) .* 1.5;               % Voltage drop across inverter
    theta = atan2(Vd,Vq) - atan2(Id,Iq);   
    PF = cos(theta); 
    MI = (sqrt(2) .* Vs) ./ ((2 ./ pi) .* (Vdclink - Vdrop));
    
end % end of if 
end

