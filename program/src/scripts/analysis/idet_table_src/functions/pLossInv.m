function P_inv_loss = pLossInv(Is,Vdclink,Fsw,V_thr_z_0,R_thr_z,V_thr_d_0,R_thr_d,PF,MI,Eon,Eoff,Err,Vdssbr,Idss,V_RRM,I_F_MAX,Regen,SixStep)
%pLossInv Calculation of inverter Losses
%   Assumes only IGBT/MOSFET losses. Calculation for SVM or 6-Step

%% Inverter Losses
 % Calculation of Inverter Losses   
 if Regen == true
     % Calculations for Regenerating   
     if SixStep == true
        % Calculations for Regenerating, 6 Step
        P_cond_Q = (V_thr_z_0 .* Is .* sqrt(2)) / (2 .* pi) .* (1 + PF) + R_thr_z .* (((Is .* sqrt(2)).^2) ./ (4 .* pi)) .* (pi - acos(PF) + 0.5 .* sin(2 .* acos(PF)));
        P_cond_D = (V_thr_d_0 .* Is .* sqrt(2)) / (2 .* pi) .* (1 - PF) + R_thr_d .* (((Is .* sqrt(2)).^2) ./ (4 .* pi)) .* (acos(PF) - 0.5 .* sin(2 .* acos(PF)));
        P_sw_Q = ((Eon+Eoff) ./ (Vdssbr .* Idss)) .* Vdclink .* Is .* sqrt(2) .* sin(pi + acos(PF)) .* (F_elec./2);
        P_sw_D = (Err ./ (V_RRM .* I_F_MAX)) .* Vdclink .* Is .* sqrt(2) .* sin(pi + acos(PF)) .* (F_elec./2);               
     else
        % Calculations for Regenerating, SVM
        P_cond_Q = (V_thr_z_0 .* Is .*sqrt(2)) ./ (2 .*pi) .* (1 + MI .* PF) + R_thr_z .* (((Is .* sqrt(2)).^2) ./ 8) .* ((1 - (32 ./ (3 .* pi.^2))) .* PF);
        P_cond_D = (V_thr_d_0 .* Is .*sqrt(2)) ./ (2 .*pi) .* (1 - MI .* PF) + R_thr_d .* (((Is .* sqrt(2)).^2) ./ 8) .* ((1 - (32 ./ (3 .* pi.^2))) .* PF);
        P_sw_Q = ((Eon+Eoff)./(Vdssbr.*Idss)) .* Vdclink .* Is .*sqrt(2) .* Fsw ./ pi;
        P_sw_D = (Err./ (V_RRM .* I_F_MAX)) .* Vdclink * Is .*sqrt(2) .* Fsw ./ pi;      
    end
    
 else % Regen is not true     
     % Calculations for Motoring
     if SixStep == true
         % Calculations for Motoring, 6 Step
        P_cond_Q = (V_thr_z_0 .* Is .* sqrt(2)) / (2 .* pi) .* (1 + PF) + R_thr_z .* (((Is .* sqrt(2)).^2) ./ (4 .* pi)) .* (pi - acos(PF) + 0.5 .* sin(2 .* acos(PF)));
        P_cond_D = (V_thr_d_0 .* Is .* sqrt(2)) / (2 .* pi) .* (1 - PF) + R_thr_d .* (((Is .* sqrt(2)).^2) ./ (4 .* pi)) .* (acos(PF) - 0.5 .* sin(2 .* acos(PF)));
        P_sw_Q = ((Eon+Eoff) ./ (Vdssbr .* Idss)) .* Vdclink .* Is .* sqrt(2) .* sin(pi-acos(PF)) .* (F_elec./2);
        P_sw_D = (Err ./ (V_RRM .* I_F_MAX)) .* Vdclink .* Is .* sqrt(2) .* sin(pi-acos(PF)) .* (F_elec./2);
     else
        % Calculations for Motoring, SVM
        P_cond_Q = (V_thr_z_0 .* Is .*sqrt(2)) ./ (2 .*pi) .* (1 + MI .* PF) + R_thr_z .* (((Is .* sqrt(2)).^2) ./ 8) .* ((1 + (32 ./ (3 .* pi.^2))) .* PF);
        P_cond_D = (V_thr_d_0 .* Is .*sqrt(2)) ./ (2 .*pi) .* (1 - MI .* PF) + R_thr_d .* (((Is .* sqrt(2)).^2) ./ 8) .* ((1 - (32 ./ (3 .* pi.^2))) .* PF);
        P_sw_Q = ((Eon+Eoff)./(Vdssbr.*Idss)) .* Vdclink .* Is .*sqrt(2) .* Fsw ./ pi;
        P_sw_D = (Err./ (V_RRM .* I_F_MAX)) .* Vdclink .* Is .*sqrt(2) .* Fsw ./ pi;
     end
     
 end
 
P_inv_loss = 6 .* ((P_cond_Q + P_cond_D) + (P_sw_Q + P_sw_D));     
 
end

