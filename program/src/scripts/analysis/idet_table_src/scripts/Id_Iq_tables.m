
% Basic script to populate the voltage vector spreadsheet directly from
% Output mat files.

%% Text to output
txt_tmax_title = ['Tmax (Nm)'];
txt_Vlimit_title = ['Voltage limit (V)'];
txt_Ilimit_title = ['Current limit (V)'];
txt_spd_title = ['Speed (rpm)'];
txt_id_title = ['id'];
txt_iq_title = ['iq'];

if PUT_tables ==0 
    txt_trq_title = ['Torque (Nm)'];
else
    txt_trq_title = ['Torque (per unit)'];
    T_array = Tpu_array;
end

%% Indexing

%id indexing
idindex_spd_array = ['E4:E',num2str(w_array_length+3)];
% iq indexing
iqindex_trq_title = ['F',num2str(w_array_length+7)];
iqindex_spd_title = ['D',num2str(w_array_length+9)];
iqindex_iq_title = ['E',num2str(w_array_length+8)];
iqindex_spd_array = ['E',num2str(w_array_length+9),':E',num2str(w_array_length*2+8)];
% Tmax indexing
Tmaxindex_trq_title = ['E',num2str(w_array_length*2+11)];
Tmaxindex_spd_title = ['E',num2str(w_array_length*2+10)];

if T_LUT_size <= 21            % corresponding to z in the alphabet
    %id indexing
    idindex_trq_array = ['F3:',(char('F'+ T_LUT_size-1)),'3'];
    idindex_id_array = ['F4:',(char('F'+ T_LUT_size-1)),num2str(w_array_length+3)];
    % iq indexing
    iqindex_trq_array = ['F',num2str(w_array_length+8),':',(char('F'+ T_LUT_size-1)),num2str(w_array_length+8)];
    iqindex_iq_array = ['F',num2str(w_array_length+9),':',(char('F'+ T_LUT_size-1)),num2str(w_array_length*2+8)];
    % Tmax indexing
    Tmaxindex_trq_array = ['F',num2str(w_array_length*2+11),':',(char('F'+ T_LUT_size-1)),num2str(w_array_length*2+11)];
    Tmaxindex_spd_array = ['F',num2str(w_array_length*2+10),':',(char('F'+ T_LUT_size-1)),num2str(w_array_length*2+10)];
else
    %id indexing
    idindex_trq_array = ['F3:A',(char('A'+ T_LUT_size-22)),'3'];
    idindex_id_array = ['F4:A',(char('A'+ T_LUT_size-22)),num2str(w_array_length+3)];
    % iq indexing
    iqindex_trq_array = ['F',num2str(w_array_length+8),':A',(char('A'+ T_LUT_size-22)),num2str(w_array_length+8)];
    iqindex_iq_array = ['F',num2str(w_array_length+9),':A',(char('A'+ T_LUT_size-22)),num2str(w_array_length*2+8)];
    % Tmax indexing
    Tmaxindex_trq_array = ['F',num2str(w_array_length*2+11),':A',(char('A'+ T_LUT_size-22)),num2str(w_array_length*2+11)];
    Tmaxindex_spd_array = ['F',num2str(w_array_length*2+10),':A',(char('A'+ T_LUT_size-22)),num2str(w_array_length*2+10)];
    
end

%% Writing to file

for Vloop_count=1:numel(Vdclink_array)
    txt7 = ['V',num2str(Vloop_count)];
    
    xlswrite(xls_export_filename,Vdclink_array(Vloop_count),(txt7),'B1') ; % write Voltage limit to spreadsheet
    xlswrite(xls_export_filename,i_limit,(txt7),'B2') ;   % write max stator current to spreadsheet  
    writematrix((txt_Vlimit_title),xls_export_filename,'Sheet',(txt7),'Range','A1');
    writematrix((txt_Ilimit_title),xls_export_filename,'Sheet',(txt7),'Range','A2');
        
    %id
    xlswrite(xls_export_filename,id(:,:,Vloop_count),(txt7),(idindex_id_array)) ; % Add id table
    xlswrite(xls_export_filename,T_array,(txt7),(idindex_trq_array)) ; % Add torque axis
    xlswrite(xls_export_filename,w_array(Vloop_count,:)',(txt7),(idindex_spd_array)) ; % Add speed axis
    writematrix((txt_trq_title),xls_export_filename,'Sheet',(txt7),'Range','F2'); % Add torque axis title
    writematrix((txt_spd_title),xls_export_filename,'Sheet',(txt7),'Range','D4'); % Add speed axis title
    writematrix((txt_id_title),xls_export_filename,'Sheet',(txt7),'Range','E3'); % Add id  title
    
    % iq
    xlswrite(xls_export_filename,iq(:,:,Vloop_count),(txt7),(iqindex_iq_array)); % Add iq table
    xlswrite(xls_export_filename,T_array,(txt7),(iqindex_trq_array)) ; % Add torque axis
    xlswrite(xls_export_filename,w_array(Vloop_count,:)',(txt7),(iqindex_spd_array)) ; % Add speed axis
    writematrix((txt_trq_title),xls_export_filename,'Sheet',(txt7),'Range',(iqindex_trq_title)); % Add torque axis title
    writematrix((txt_spd_title),xls_export_filename,'Sheet',(txt7),'Range',(iqindex_spd_title)); % Add speed axis title
    writematrix((txt_iq_title),xls_export_filename,'Sheet',(txt7),'Range',(iqindex_iq_title)); % Add speed axis title
    
    % Tmax
    xlswrite(xls_export_filename,w_array(Vloop_count,:),(txt7),(Tmaxindex_spd_array)) ; % Drop the 14 point speed range   
    xlswrite(xls_export_filename,Tmax(:,Vloop_count)',(txt7),(Tmaxindex_trq_array)) ; % Drop max torque for power limit map
    writematrix((txt_spd_title),xls_export_filename,'Sheet',(txt7),'Range',(Tmaxindex_spd_title)); % Add speed label to speed data
    writematrix((txt_tmax_title),xls_export_filename,'Sheet',(txt7),'Range',(Tmaxindex_trq_title)); % Add tmax label to speed data
    
end

clear iqindex_iq_array iqindex_iq_title iqindex_spd_array iqindex_spd_title iqindex_trq_array iqindex_trq_title ...
    idindex_id_array idindex_spd_array idindex_trq_array ...
    Tmaxindex_spd_array Tmaxindex_spd_title Tmaxindex_trq_array Tmaxindex_trq_title txt7 ...
    txt_id_title txt_Ilimit_title txt_iq_title txt_spd_title txt_tmax_title txt_trq_title txt_Vlimit_title


