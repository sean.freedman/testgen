% Script to Plot the Motor Data provided as an input to the Motor Current
% Determination Argorithm
%% Plots

fig_Mot_Data = figure('Name','Motor Data Plots'); 
       fig_Mot_Data.WindowState = 'maximized';
        tg = uitabgroup; % tabgroup
        
        thistab = uitab(tg);
        axes('Parent',thistab); % somewhere to plot
        % Flux Data Plots
        subplot(1,2,1);
        surf(id_ind,iq_ind,Psi_q_input);
            title('Q Axis Flux Saturation');
            xlabel('I_d (A_r_m_s)'); ylabel('I_q (A_r_m_s)'); zlabel('\psi_q (Wb_r_m_s)')
            grid on; grid minor;
        subplot(1,2,2);
        surf(id_ind,iq_ind,Psi_d_input);
            title('D Axis Flux Saturation');
            xlabel('I_d (A_r_m_s)'); ylabel('I_q (A_r_m_s)'); zlabel('\psi_d (Wb_r_m_s)')
            grid on; grid minor;
        
%         for n=1:numel(rpmMech_array)
%             % Stator Resistance and Motor Core Losses
%                     thistab = uitab(tg);
%                     axes('Parent',thistab); % somewhere to plot
%                     subplot(1,2,1);
%                     surf(id_ind,iq_ind,Rs(:,:,n));
%                         txt = ['Stator Resistance for \omega_m_e_c_h = ',num2str(rpmMech_array(n)),' rpm'];
%                         title(txt);
%                         xlabel('I_d (A_r_m_s)'); ylabel('I_q (A_r_m_s)'); zlabel('Stator Resistance (\Omega)')
%                         grid on; grid minor;
%                     subplot(1,2,2);
%                     surf(id_ind,iq_ind,Core_loss(:,:,n));
%                         txt = ['Core Loss for \omega_m_e_c_h = ',num2str(rpmMech_array(n)),' rpm'];
%                         title(txt);
%                         xlabel('I_d (A_r_m_s)'); ylabel('I_q (A_r_m_s)'); zlabel('Core Loss (W)')
%                         grid on; grid minor;              
%         end