% Script to Plot the Current tables for the provided Motor Data
%% Plot Title
if Regen == false && MTPSL == false
    fig1 = figure('Name','MTPA Plots- Motoring'); 
elseif Regen == true && MTPSL == false
    fig1 = figure('Name','MTPA Plots- Regen'); 
elseif Regen == false && MTPSL == true
    fig1 = figure('Name','MTPSL Plots- Motoring'); 
else
    fig1 = figure('Name','MTPSL Plots- Regen'); 
end
    
%% Plots

if PUT_tables == true
    % Plot with xis of PU Torque
    
            fig1.WindowState = 'maximized';
                tg = uitabgroup; % tabgroup
                % Tmax Plot
                thistab = uitab(tg);
                axes('Parent',thistab); % somewhere to plot
                for ii = 1:numel(Vdclink_array)
                        plot(w_array(ii,:),Tmax(:,ii),'-o','LineWidth',2)
                        title('T_m_a_x @ \omega_m_e_c_h and Voltage');
                        xlabel('\omega_m_e_c_h (rpm)'); ylabel('T_m_a_x (Nm)')
                        xlim([0 w_max])
                        grid on; grid minor; 
                        hold on
                end
                hold off


                % Plots per voltage interval
                for ii = 1:numel(Vdclink_array)
                    thistab = uitab(tg); % build next tab
                    axes('Parent',thistab); % somewhere to plot
                        subplot(2,4,1);
                        surf(Tpu_array,w_array(ii,:),id(:,:,ii))
                        set(gca, 'XDir','reverse')
                        set(gca, 'YDir','reverse')
                            txt = ['I_D for V_M_A_X = ',num2str(Vdclink_array(ii)),' V'];
                            title(txt);
                            xlabel('PU Torque'); ylabel('Speed (rpm)'); zlabel('I_d (A_R_M_S)')
                            grid on; grid minor;

                        subplot(2,4,5);
                        surf(Tpu_array,w_array(ii,:),iq(:,:,ii))
                        set(gca, 'YDir','reverse')
                            txt = ['I_Q for V_M_A_X = ',num2str(Vdclink_array(ii)),' V'];
                            title(txt);
                            xlabel('PU Torque'); ylabel('Speed (rpm)'); zlabel('I_q (A_R_M_S)')
                            grid on; grid minor;

                        subplot(3,4,2);
                        surf(Tpu_array,w_array(ii,:),P_mech(:,:,ii))
                            txt = ['P_M_E_C_H for V_M_A_X = ',num2str(Vdclink_array(ii)),' V'];
                            title(txt);
                            xlabel('PU Torque'); ylabel('Speed (rpm)'); zlabel('P_M_E_C_H (W_R_M_S)')
                            grid on; grid minor;

                        subplot(3,4,6);
                        surf(Tpu_array,w_array(ii,:),P_ac(:,:,ii))
                            txt = ['P_a_c for V_M_A_X = ',num2str(Vdclink_array(ii)),' V'];
                            title(txt);
                            xlabel('PU Torque'); ylabel('Speed (rpm)'); zlabel('P_a_c (W_R_M_S)')
                            grid on; grid minor;

                        subplot(3,4,10);
                        surf(Tpu_array,w_array(ii,:),P_dc(:,:,ii))
                            txt = ['P_d_c for V_M_A_X = ',num2str(Vdclink_array(ii)),' V'];
                            title(txt);
                            xlabel('PU Torque'); ylabel('Speed (rpm)'); zlabel('P_d_c (W_R_M_S)')
                            grid on; grid minor;

                        subplot(2,4,3);    
                        surf(Tpu_array,w_array(ii,:),P_mot_loss(:,:,ii))
                            txt = ['P_m_o_t_o_r _l_o_s_s for V_M_A_X = ',num2str(Vdclink_array(ii)),' V'];
                            title(txt);
                            xlabel('PU Torque'); ylabel('Speed (rpm)'); zlabel('P_m_o_t_o_r _l_o_s_s (W_R_M_S)')
                            grid on; grid minor;

                        subplot(2,4,7);
                        surf(Tpu_array,w_array(ii,:),P_inv_loss(:,:,ii))
                            txt = ['P_i_n_v _l_o_s_s for V_M_A_X = ',num2str(Vdclink_array(ii)),' V'];
                            title(txt);
                            xlabel('PU Torque'); ylabel('Speed (rpm)'); zlabel('P_i_n_v _l_o_s_s (W_R_M_S)')
                            grid on; grid minor;        

                        subplot(3,4,4);
                        surf(Tpu_array,w_array(ii,:),Eff_mot(:,:,ii))
                        set(gca, 'XDir','reverse')
                            txt = ['Motor Efficiency for V_M_A_X = ',num2str(Vdclink_array(ii)),' V'];
                            title(txt);
                            xlabel('PU Torque'); ylabel('Speed (rpm)'); zlabel('\epsilon_m_o_t_o_r (%)')
                            grid on; grid minor; 
                                    if max(max(Eff_mot(:,:,ii)))>100
                                        zlim([min(min(Eff_mot(:,:,ii))) 100]);
                                    end
                                    if min(min(Eff_mot(:,:,ii)))<0
                                        zlim([0 max(max(Eff_mot(:,:,ii)))]);
                                    end

                        subplot(3,4,8);
                        surf(Tpu_array,w_array(ii,:),Eff_inv(:,:,ii))
                        set(gca, 'XDir','reverse')
                            txt = ['Inverter Efficiency for V_M_A_X = ',num2str(Vdclink_array(ii)),' V'];
                            title(txt);
                            xlabel('PU Torque'); ylabel('Speed (rpm)'); zlabel('\epsilon_i_n_v_e_r_t_e_r (%)')
                            grid on; grid minor; 
                                    if max(max(Eff_inv(:,:,ii)))>100
                                        zlim([min(min(Eff_inv(:,:,ii))) 100]);
                                    end
                                    if min(min(Eff_inv(:,:,ii)))<0
                                        zlim([0 max(max(Eff_inv(:,:,ii)))]);
                                    end                         

                        subplot(3,4,12);
                        surf(Tpu_array,w_array(ii,:),Eff_sys(:,:,ii))
                        set(gca, 'XDir','reverse')
                            txt = ['System Efficiency for V_M_A_X = ',num2str(Vdclink_array(ii)),' V'];
                            title(txt);
                            xlabel('PU Torque'); ylabel('Speed (rpm)'); zlabel('\epsilon_s_y_s_t_e_m (%)')
                            grid on; grid minor; 
                                    if max(max(Eff_sys(:,:,ii)))>100
                                        zlim([min(min(Eff_sys(:,:,ii))) 100]);
                                    end
                                    if min(min(Eff_sys(:,:,ii)))<0
                                        zlim([0 max(max(Eff_sys(:,:,ii)))]);
                                    end

                end
else
    % Plot with Torque in Nm
    
                fig1.WindowState = 'maximized';
                    tg = uitabgroup; % tabgroup
                    % Tmax Plot
                    thistab = uitab(tg);
                    axes('Parent',thistab); % somewhere to plot
                for ii = 1:numel(Vdclink_array)
                        plot(w_array(ii,:),Tmax(:,ii),'-o','LineWidth',2)
                        title('T_m_a_x @ \omega_m_e_c_h and Voltage');
                        xlabel('\omega_m_e_c_h (rpm)'); ylabel('T_m_a_x (Nm)')
                        grid on; grid minor; 
                        xlim([0 w_max])
                        hold on                  
                end
                hold off


                    % Plots per voltage interval
                    for ii = 1:numel(Vdclink_array)
                        thistab = uitab(tg); % build next tab
                        axes('Parent',thistab); % somewhere to plot
                            subplot(2,4,1);
                            surf(T_array,w_array(ii,:),id(:,:,ii))
                            set(gca, 'XDir','reverse')
                            set(gca, 'YDir','reverse')
                                txt = ['I_D for V_M_A_X = ',num2str(Vdclink_array(ii)),' V'];
                                title(txt);
                                xlabel('T_d_e_m_a_n_d (Nm)'); ylabel('Speed (rpm)'); zlabel('I_d (A_R_M_S)')
                                grid on; grid minor;

                            subplot(2,4,5);
                            surf(T_array,w_array(ii,:),iq(:,:,ii))
                            set(gca, 'YDir','reverse')
                                txt = ['I_Q for V_M_A_X = ',num2str(Vdclink_array(ii)),' V'];
                                title(txt);
                                xlabel('T_d_e_m_a_n_d (Nm)'); ylabel('Speed (rpm)'); zlabel('I_q (A_R_M_S)')
                                grid on; grid minor;

                            subplot(3,4,2);
                            surf(T_array,w_array(ii,:),P_mech(:,:,ii))
                                txt = ['P_M_E_C_H for V_M_A_X = ',num2str(Vdclink_array(ii)),' V'];
                                title(txt);
                                xlabel('T_d_e_m_a_n_d (Nm)'); ylabel('Speed (rpm)'); zlabel('P_M_E_C_H (W_R_M_S)')
                                grid on; grid minor;

                            subplot(3,4,6);
                            surf(T_array,w_array(ii,:),P_ac(:,:,ii))
                                txt = ['P_a_c for V_M_A_X = ',num2str(Vdclink_array(ii)),' V'];
                                title(txt);
                                xlabel('T_d_e_m_a_n_d (Nm)'); ylabel('Speed (rpm)'); zlabel('P_a_c (W_R_M_S)')
                                grid on; grid minor;

                            subplot(3,4,10);
                            surf(T_array,w_array(ii,:),P_dc(:,:,ii))
                                txt = ['P_d_c for V_M_A_X = ',num2str(Vdclink_array(ii)),' V'];
                                title(txt);
                                xlabel('T_d_e_m_a_n_d (Nm)'); ylabel('Speed (rpm)'); zlabel('P_d_c (W_R_M_S)')
                                grid on; grid minor;

                            subplot(2,4,3);    
                            surf(T_array,w_array(ii,:),P_mot_loss(:,:,ii))
                                txt = ['P_m_o_t_o_r _l_o_s_s for V_M_A_X = ',num2str(Vdclink_array(ii)),' V'];
                                title(txt);
                                xlabel('T_d_e_m_a_n_d (Nm)'); ylabel('Speed (rpm)'); zlabel('P_m_o_t_o_r _l_o_s_s (W_R_M_S)')
                                grid on; grid minor;

                            subplot(2,4,7);
                            surf(T_array,w_array(ii,:),P_inv_loss(:,:,ii))
                                txt = ['P_i_n_v _l_o_s_s for V_M_A_X = ',num2str(Vdclink_array(ii)),' V'];
                                title(txt);
                                xlabel('T_d_e_m_a_n_d (Nm)'); ylabel('Speed (rpm)'); zlabel('P_i_n_v _l_o_s_s (W_R_M_S)')
                                grid on; grid minor;        

                            subplot(3,4,4);
                            surf(T_array,w_array(ii,:),Eff_mot(:,:,ii))
                            set(gca, 'XDir','reverse')
                                txt = ['Motor Efficiency for V_M_A_X = ',num2str(Vdclink_array(ii)),' V'];
                                title(txt);
                                xlabel('T_d_e_m_a_n_d (Nm)'); ylabel('Speed (rpm)'); zlabel('\epsilon_m_o_t_o_r (%)')
                                grid on; grid minor; 
                                    if max(max(Eff_mot(:,:,ii)))>100
                                        zlim([min(min(Eff_mot(:,:,ii))) 100]);
                                    end
                                    if min(min(Eff_mot(:,:,ii)))<0
                                        zlim([0 max(max(Eff_mot(:,:,ii)))]);
                                    end

                            subplot(3,4,8);
                            surf(T_array,w_array(ii,:),Eff_inv(:,:,ii))
                            set(gca, 'XDir','reverse')
                                txt = ['Inverter Efficiency for V_M_A_X = ',num2str(Vdclink_array(ii)),' V'];
                                title(txt);
                                xlabel('T_d_e_m_a_n_d (Nm)'); ylabel('Speed (rpm)'); zlabel('\epsilon_i_n_v_e_r_t_e_r (%)')
                                grid on; grid minor; 
                                    if max(max(Eff_inv(:,:,ii)))>100
                                        zlim([min(min(Eff_inv(:,:,ii))) 100]);
                                    end
                                    if min(min(Eff_inv(:,:,ii)))<0
                                        zlim([0 max(max(Eff_inv(:,:,ii)))]);
                                    end                        

                            subplot(3,4,12);
                            surf(T_array,w_array(ii,:),Eff_sys(:,:,ii))
                            set(gca, 'XDir','reverse')
                                txt = ['System Efficiency for V_M_A_X = ',num2str(Vdclink_array(ii)),' V'];
                                title(txt);
                                xlabel('T_d_e_m_a_n_d (Nm)'); ylabel('Speed (rpm)'); zlabel('\epsilon_s_y_s_t_e_m (%)')
                                grid on; grid minor; 
                                    if max(max(Eff_sys(:,:,ii)))>100
                                        zlim([min(min(Eff_sys(:,:,ii))) 100]);
                                    end
                                    if min(min(Eff_sys(:,:,ii)))<0
                                        zlim([0 max(max(Eff_sys(:,:,ii)))]);
                                    end

                    end
end

saveas(gcf, output_path + "\\" + test_name + "output_tables.fig")

clear txt