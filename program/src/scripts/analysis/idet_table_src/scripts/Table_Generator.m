%% Matrix Pre-allocation
TmaxVw0 = zeros(numel(Vdclink_array),1);
TmaxVw = zeros(numel(Vdclink_array),w_array_length);
KPindex = zeros(numel(Vdclink_array),1);
id = zeros(T_LUT_size,w_array_length,numel(Vdclink_array));                          % pre allocating for speed
iq = zeros(T_LUT_size,w_array_length,numel(Vdclink_array));                            % pre allocating for speed
Tmax = zeros(1,w_array_length,numel(Vdclink_array));                                    % pre allocating for speed
Vdmin = zeros(T_LUT_size,w_array_length,numel(Vdclink_array));                          % pre allocating for speed
Vqmin = zeros(T_LUT_size,w_array_length,numel(Vdclink_array));                          % pre allocating for speed
P_mech = zeros(T_LUT_size,w_array_length,numel(Vdclink_array));                          % pre allocating for speed
P_ac = zeros(T_LUT_size,w_array_length,numel(Vdclink_array));                          % pre allocating for speed
P_dc = zeros(T_LUT_size,w_array_length,numel(Vdclink_array));                          % pre allocating for speed
P_mot_loss = zeros(T_LUT_size,w_array_length,numel(Vdclink_array));                          % pre allocating for speed
P_inv_loss = zeros(T_LUT_size,w_array_length,numel(Vdclink_array));                          % pre allocating for speed
Eff_mot = zeros(T_LUT_size,w_array_length,numel(Vdclink_array));                          % pre allocating for speed
Eff_inv = zeros(T_LUT_size,w_array_length,numel(Vdclink_array));                          % pre allocating for speed
Eff_sys = zeros(T_LUT_size,w_array_length,numel(Vdclink_array));                          % pre allocating for speed
w_array = zeros(numel(Vdclink_array),w_array_length);
KP = zeros(numel(Vdclink_array),1);
KPplus1 = zeros(numel(Vdclink_array),1);

%% Maximum Torque Determination

for Vloop = 1:numel(Vdclink_array)

        % Set the DC link voltage
        Vquery = Vdclink_array(Vloop);

        if Regen == false
        [TmaxVw0(Vloop,1),~] =TmaxMotor(10, Vquery,MI_max,istep,id_ind,iq_ind,rpmMech_array, Psi_d_input,Psi_q_input,...
                        Npp,Rs,Core_loss,i_limit,idc_limit,...
                        V_thr_z_0,R_thr_z,V_thr_d_0,R_thr_d,...
                        Vdssbr,Idss,Eon,Eoff,Fsw,...
                        V_RRM,I_F_MAX,Err,SixStep);

        else
        [TmaxVw0(Vloop,1),~] =TmaxRegen(10,Vquery,MI_max,istep,id_ind,iq_ind,rpmMech_array, Psi_d_input,Psi_q_input,...
                        Npp,Rs,Core_loss,i_limit,idc_limit,...
                        V_thr_z_0,R_thr_z,V_thr_d_0,R_thr_d,...
                        Vdssbr,Idss,Eon,Eoff,Fsw,...
                        V_RRM,I_F_MAX,Err,SixStep);
        end
end

Tmax0 = max(TmaxVw0);


 %% Knee Point Determination     
 
KPSearchSteps = 5;
   
for Vloop = 1:numel(Vdclink_array)
    
        % Set the DC link voltage
        Vquery = Vdclink_array(Vloop);
        % initial extremum points for the speed array
        KP(Vloop,1)= 10;
        KPplus1(Vloop,1)= w_max;
        
        while abs(KP(Vloop,1) - KPplus1(Vloop,1))> 10
    
            w_KP_array = linspace(KP(Vloop,1),KPplus1(Vloop,1),KPSearchSteps);
        
        for Nloop = 1:KPSearchSteps

                %Set the machine speed
                N_mech = w_KP_array(Nloop);
                
                if Regen == false
                    [TmaxVw(Vloop,Nloop),~] =TmaxMotor(N_mech, Vquery,MI_max,istep,id_ind,iq_ind,rpmMech_array, Psi_d_input,Psi_q_input,...
                                    Npp,Rs,Core_loss,i_limit,idc_limit,...
                                    V_thr_z_0,R_thr_z,V_thr_d_0,R_thr_d,...
                                    Vdssbr,Idss,Eon,Eoff,Fsw,...
                                    V_RRM,I_F_MAX,Err,SixStep);
                else
                    [TmaxVw(Vloop,Nloop),~] =TmaxRegen(N_mech,Vquery,MI_max,istep,id_ind,iq_ind,rpmMech_array, Psi_d_input,Psi_q_input,...
                                    Npp,Rs,Core_loss,i_limit,idc_limit,...
                                    V_thr_z_0,R_thr_z,V_thr_d_0,R_thr_d,...
                                    Vdssbr,Idss,Eon,Eoff,Fsw,...
                                    V_RRM,I_F_MAX,Err,SixStep);

                end

        end

KPindex(Vloop,1) = find(TmaxVw(Vloop,:) >=TmaxVw0(Vloop,1),1,"last"); % finding index of highest known speed point which satisfies Tmax
KP(Vloop,1) =  w_KP_array(KPindex(Vloop,1));
KPplus1(Vloop,1) =  w_KP_array(KPindex(Vloop,1)+1);
        end
    
end
   
for Vloop=1:numel(Vdclink_array)
    w_array(Vloop,:) = [0,linspace(KP(Vloop),w_max,w_array_length-1)];
end

%% MTPA Calculation

for Vloop_count=1:numel(Vdclink_array)
    Vdclink_max = Vdclink_array(Vloop_count);
    for wloop_count=1:w_array_length
            w = w_array(Vloop_count,wloop_count);
%             Call up  Function to calculate Id and Iq
             if Regen == false 
                 % for Motoring
                         [id(:,wloop_count,Vloop_count),...
                         iq(:,wloop_count,Vloop_count),...
                         Tmax(1,wloop_count,Vloop_count),...
                         Vdmin(:,wloop_count,Vloop_count),...
                         Vqmin(:,wloop_count,Vloop_count),...
                         P_mech(:,wloop_count,Vloop_count), ...
                         P_ac(:,wloop_count,Vloop_count),...
                         P_dc(:,wloop_count,Vloop_count),...
                         P_mot_loss(:,wloop_count,Vloop_count),...
                         P_inv_loss(:,wloop_count,Vloop_count),...
                         Eff_mot(:,wloop_count,Vloop_count),...
                         Eff_inv(:,wloop_count,Vloop_count),...
                         Eff_sys(:,wloop_count,Vloop_count)] ...
                 = IDet_Motor(w,Vdclink_max,MI_max,istep,id_ind,iq_ind,rpmMech_array,Psi_d_input,Psi_q_input,...
                                Npp,Rs,Core_loss,i_limit,idc_limit,T_LUT_size,Tmax0,...
                                V_thr_z_0,R_thr_z,V_thr_d_0,R_thr_d,...
                                Vdssbr,Idss,Eon,Eoff,Fsw,...
                                V_RRM,I_F_MAX,Err,SixStep,PUT_tables,MTPSL);
             else
                 % for Regen
                     [id(:,wloop_count,Vloop_count),...
                     iq(:,wloop_count,Vloop_count),...
                     Tmax(1,wloop_count,Vloop_count),...
                     Vdmin(:,wloop_count,Vloop_count),...
                     Vqmin(:,wloop_count,Vloop_count),...
                     P_mech(:,wloop_count,Vloop_count), ...
                     P_ac(:,wloop_count,Vloop_count),...
                     P_dc(:,wloop_count,Vloop_count),...
                     P_mot_loss(:,wloop_count,Vloop_count),...
                     P_inv_loss(:,wloop_count,Vloop_count),...
                     Eff_mot(:,wloop_count,Vloop_count),...
                     Eff_inv(:,wloop_count,Vloop_count),...
                     Eff_sys(:,wloop_count,Vloop_count)] ...
                 = IDet_Regen(w,Vdclink_max,MI_max,istep,id_ind,iq_ind,rpmMech_array,Psi_d_input,Psi_q_input,...
                                    Npp,Rs,Core_loss,i_limit,idc_limit,T_LUT_size,Tmax0,...
                                    V_thr_z_0,R_thr_z,V_thr_d_0,R_thr_d,...
                                    Vdssbr,Idss,Eon,Eoff,Fsw,...
                                    V_RRM,I_F_MAX,Err,SixStep,PUT_tables,MTPSL);
             end
    end   % end speed for loop
end    % end VMax for loop

%% Transposition of the Data

iq= permute(iq,[2 1 3]);                                        % Transposing Rows and Columns of Iq table
id= permute(id,[2 1 3]);                                        % Transposing Rows and Columns of Id table
Tmax= permute(Tmax,[2 3 1]);                            % Transposing Rows and Columns of Tmax table
Vdmin = permute(Vdmin,[2 1 3]);                         % Transposing Rows and Columns of Vd table
Vqmin = permute(Vqmin,[2 1 3]);                         % Transposing Rows and Columns of Vq table
P_mech = permute(P_mech,[2 1 3]);                     % Transposing Rows and Columns of Pmech table  
P_ac = permute(P_ac,[2 1 3]);                               % Transposing Rows and Columns of P_ac table
P_dc = permute(P_dc,[2 1 3]);                               % Transposing Rows and Columns of P_dc table
P_mot_loss = permute(P_mot_loss,[2 1 3]);                     % Transposing Rows and Columns of P_mot_loss table
P_inv_loss = permute(P_inv_loss,[2 1 3]);                     % Transposing Rows and Columns of P_inv_loss table
Eff_mot = permute(Eff_mot,[2 1 3]);                     % Transposing Rows and Columns of Eff_mot table
Eff_inv = permute(Eff_inv,[2 1 3]);                     % Transposing Rows and Columns of Eff_inv table
Eff_sys = permute(Eff_sys,[2 1 3]);                     % Transposing Rows and Columns of Eff_sys table


%% Creating Output file for Current LUTs
% Determining the file to write to (either for PU tables or non PU tables

flnmlcn = append(output_path + "\", output_mat_filename);           % finding file name and adding fileloaction to text string
output_mat_file = matfile(flnmlcn,'Writable',true);                     % defining the mat file with file name and location

clear flnmlcn output_mat_filename

if PUT_tables == false
    T_array = linspace(0,Tmax0,T_LUT_size);                                   % Creating axis points for Torque in Nm
    
    IDet_tables = struct;
        IDet_tables.iq = iq;
        IDet_tables.id = id;
        IDet_tables.Tmax = Tmax;
        IDet_tables.w_array = w_array;
        IDet_tables.T_array = T_array;
        IDet_tables.Vdclink_array = Vdclink_array; 
else
    Tpu_array = linspace(0,1,T_LUT_size);                                   % Creating axis points for pu Torque
    
    IDet_PU_tables = struct;
        IDet_PU_tables.iq = iq;
        IDet_PU_tables.id = id;
        IDet_PU_tables.Tmax = Tmax;
        IDet_PU_tables.w_array = w_array;
        IDet_PU_tables.Tpu_array = Tpu_array;
        IDet_PU_tables.Vdclink_array = Vdclink_array; 
end

if Regen == false && MTPSL == false && PUT_tables == false
    output_mat_file.MTPA_mot_table = IDet_tables;    
elseif Regen == true && MTPSL == false && PUT_tables == false
    output_mat_file.MTPA_reg_table = IDet_tables;       
elseif Regen == false && MTPSL == true && PUT_tables == false
    output_mat_file.MTPSL_mot_table = IDet_tables;      
elseif Regen == true && MTPSL == true && PUT_tables == false    
    output_mat_file.MTPSL_reg_table = IDet_tables;      
elseif Regen == false && MTPSL == false && PUT_tables == true
    output_mat_file.MTPA_mot_PU_table = IDet_PU_tables;
elseif Regen == true && MTPSL == false && PUT_tables == true
    output_mat_file.MTPA_reg_PU_table = IDet_PU_tables;
elseif Regen == false && MTPSL == true && PUT_tables == true
    output_mat_file.MTPSL_mot_PU_table = IDet_PU_tables;
else    
    output_mat_file.MTPSL_reg_PU_table = IDet_PU_tables;
end
        

%% Plot Motor Input
    if Plot_Mot_Input == true
        run Plotting_Motor_Input.m
    end

%% Plot Current Tables
    if Plot_Table_Output == true
        run Plotting_Table_Output.m
    end    
    
%% Export generated tables to excel
if excel_export == true
    run Id_Iq_tables.m
    clear xls_export_filename
    clc; 
end

%% Finishing Script
clc; 
clear excel_export MTPSL Plot_Mot_Input Plot_Table_Output PUT_tables Regen SixStep ...
    Nloop Vloop Vloop_count Vquery w N_mech KPindex KPplus1 KPSearchSteps ...
    MI_max T_LUT_size Tmax0 TmaxVw TmaxVw0 w_array_length w_max wloop_count

