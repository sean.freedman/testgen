function idq_characterisation(test_name, file, current_sensor_latencies, meta_data, zip_path)
    %{
    This function takes in a CSV file and calculates the idq characterisation data for the motor.

    Inputs:
        test_name - string - name of the test, this will be used to name the output file.
        file - string - path to the CSV file containing the data.
        current_sensor_latencies - string - path to the CSV file containing the current sensor latencies.
        meta_data - string - path to the CSV file containing the meta data.
        zip_path - string - path to the zip file containing the CSV file.

    Outputs:
        zip file containing the idq characterisation data.

    %}

    % Read in CSV file as a table
    data = readtable(fullfile(file));

    % Read current sensor latency table, not needed in the characterisation process, but useful to have attached.
    current_sensor_latencies = readtable(fullfile(current_sensor_latencies));

    % Read meta data, the only relevent property in this data is the pole pairs, however CRCs, encoder offset etc are useful to have attached.
    meta_data = readtable(fullfile(meta_data));
    
    % save inverter meta data as a struct
    meta_data = table2struct(meta_data);

    % Get pole pairs from inverter meta data.
    pole_pairs = meta_data.PolePairs
    % Idq step: SF - this should determined be from the recorded data. i.e. take the differnece between id demand in one row and the next.
    idq_step = 25;
    % As matlab cannot read unbroken strings it will reformat the headers to the following, for ease of use we will define them here.
    id_name = 'IdCurrentInjected_RMS__A_';
    iq_name = 'IqCurrentInjected_RMS__A_';
    torque_name = 'TorqueMeasured_Nm_';
    speed_name = 'SpeedEMotor_rpm_';
    ud_name = 'UdVoltageFeedback_RMS__V_';
    uq_name = 'UqVoltageFeedback_RMS__V_';

    % Plot current sensor latencies and marker points
    current_sensor_latency_figure = figure("name" , "Current Sensor Latencies");
    plot(current_sensor_latencies.Speed, current_sensor_latencies.TrimAngle, '-o');
    xlabel("Speed (Hz)");
    ylabel("Trim Angle (degrees)");
    title("Current Sensor Latencies");

    % Create interpolated data structure and table.
    interpolated_data = struct('torque_measured',[],'ud_feedback_rms',[],'uq_feedback_rms',[],'speed_rpm',[]);
    interpolated_table_headers = fieldnames(interpolated_data);
    interpolated_table = table();
    
    data_headers = {torque_name,ud_name,uq_name,speed_name};

    id = table2array(data(:,id_name));
    iq = table2array(data(:,iq_name));

    interpolated_data.("id_demand_rms") = [min(id):idq_step:max(id)];
    interpolated_data.("iq_demand_rms") = [min(iq):idq_step:max(iq)];
    
    [XI,YI] = meshgrid(interpolated_data.("id_demand_rms"), interpolated_data.("iq_demand_rms"));
    % For every signal in the data table, interpolate the data and add it to the interpolated data structure.
    for signal = 1:numel(interpolated_table_headers)
        k = 0;
        current_signal = interpolated_table_headers{signal};
        var = table2array(data(:,data_headers{signal}));

        f = scatteredInterpolant(id, iq, var, "natural");

        interpolated_data.(interpolated_table_headers{signal}) = f(XI,YI);

        % On first pass create Idq columns
        if signal == 1

            interpolated_table = addvars(interpolated_table, zeros(length(interpolated_data.("id_demand_rms"))*length(interpolated_data.("iq_demand_rms")),1), zeros(length(interpolated_data.("id_demand_rms"))*length(interpolated_data.("iq_demand_rms")),1), 'NewVariableNames', ["id_demand_rms", "iq_demand_rms"]);

            for i=1:numel(interpolated_data.(interpolated_table_headers{signal})(:,1))
                for j=1:numel(interpolated_data.(interpolated_table_headers{signal})(1,:))
                    interpolated_table.("id_demand_rms")(k+1,:) = interpolated_data.("id_demand_rms")(j);
                    interpolated_table.("iq_demand_rms")(k+1,:) = interpolated_data.("iq_demand_rms")(i);
                    k = k + 1;
                end
            end

            k = 0;
        end

        % Add signal to table, preallocate with zeros to increase speed.
        interpolated_table = addvars(interpolated_table, zeros(length(interpolated_data.("id_demand_rms"))*length(interpolated_data.("iq_demand_rms")),1), 'NewVariableNames', current_signal);
        % Populate table with interpolated data
        for i=1:numel(interpolated_data.(interpolated_table_headers{signal})(:,1))
            for j=1:numel(interpolated_data.(interpolated_table_headers{signal})(1,:))
                interpolated_table.(current_signal)(k+1,:) = interpolated_data.(interpolated_table_headers{signal})(i,j);
                k = k + 1;
            end
        end

    end

    % Calculate psi_m
    psi_m = psim(interpolated_table.("torque_measured"), pole_pairs, interpolated_table.("iq_demand_rms") );
    interpolated_table = addvars(interpolated_table, psi_m, 'NewVariableNames', "psi_m");
    interpolated_table.("psi_m")(isinf(interpolated_table.("psi_m")) | isnan(interpolated_table.("psi_m"))) = 0;

    % Calculate stator_current_angle_degrees
    stator_current_angle = atan2d(interpolated_table.("id_demand_rms"), interpolated_table.("iq_demand_rms"));
    interpolated_table = addvars(interpolated_table, stator_current_angle,'NewVariableNames',"stator_current_angle_degrees");
    interpolated_table.("stator_current_angle_degrees")(isinf(interpolated_table.("stator_current_angle_degrees")) | isnan(interpolated_table.("stator_current_angle_degrees"))) = 0;

    % Calculate Rad/s from rpm.
    interpolated_table.("speed_rads") = rpm2rads(interpolated_table.("speed_rpm"));

    % When stator_current_angle_degrees is Zero (i.e all demanded current in Q-axis), calculate psi 
    x = table2array(interpolated_table(interpolated_table.("stator_current_angle_degrees") == 0,"iq_demand_rms"));
    y = table2array(interpolated_table(interpolated_table.("stator_current_angle_degrees") == 0,"psi_m"));
    v = table2array(interpolated_table(interpolated_table.("iq_demand_rms") > 0,"iq_demand_rms"));

    % Interpolate psi_m for all values of iq_demand_rms
    psi_m_presat = interp1(x,y,[0:1:max(interpolated_table.("iq_demand_rms"))]);

    % Find were the maximum value of Psim is
    [psi_m_max_val, psi_m_max_ind] = max(psi_m_presat);
    psi_m_max_indices = [1:psi_m_max_ind];

    % Replace values before this the maximum value; saturate to the maximum value.
    psi_m_sat = psi_m_presat;
    psi_m_sat(psi_m_max_indices) = psi_m_max_val;

    signals_to_add = {'l_s','l_d','l_q','psi_q_rms','psi_q_peak','psi_d_rms','psi_d_peak','torque_em_rms', 'ud_feedback_peak', 'uq_feedback_peak'};

    for signal=1:length(signals_to_add)
        interpolated_table = addvars(interpolated_table, zeros(height(interpolated_table),1), 'NewVariableNames', signals_to_add{signal});
    end

    % Calculate Inductances
    interpolated_table.("l_s") = inductance_rms(interpolated_table.("torque_measured"),pole_pairs, interpolated_table.("id_demand_rms"), interpolated_table.("iq_demand_rms"), interpolated_table.("psi_m"));
    
    % Replace inf or Nan values with 0's
    interpolated_table.("l_s")(isinf(interpolated_table.("l_s")) | isnan(interpolated_table.("l_s"))) = 0;
    interpolated_table.("l_q") = inductance_q(interpolated_table.("ud_feedback_rms"),interpolated_table.("speed_rads"), pole_pairs, interpolated_table.("iq_demand_rms"));
    interpolated_table.("l_q")(isinf(interpolated_table.("l_q")) | isnan(interpolated_table.("l_q"))) = 0;
    interpolated_table.("l_d") = interpolated_table.("l_s") + interpolated_table.("l_q");

    % Calculate Fluxes
    interpolated_table.("psi_q_rms") = psi_q_rms(interpolated_table.("l_q"), interpolated_table.("iq_demand_rms"));
    interpolated_table.("psi_d_rms") = psi_d_rms(interpolated_table.("l_d"), interpolated_table.("id_demand_rms"), interpolated_table.("psi_m"));

    % Calculate Electromagentic Torque
    interpolated_table.("torque_em_rms") = torque_em_rms(pole_pairs, interpolated_table.("psi_m"),interpolated_table.("id_demand_rms"), interpolated_table.("iq_demand_rms"),interpolated_table.("l_s"));

    % Calculate Peaks for info
    interpolated_table.("psi_q_peak") = rms_2_peak(interpolated_table.("psi_q_rms"));
    interpolated_table.("psi_d_peak") = rms_2_peak(interpolated_table.("psi_d_rms"));
    interpolated_table.("ud_feedback_peak") = rms_2_peak(interpolated_table.("ud_feedback_rms"));
    interpolated_table.("uq_feedback_peak") = rms_2_peak(interpolated_table.("uq_feedback_rms"));

    % Create lookup tables of fluxes and inducatances, with injected currents as axis.
    interp_data_calculated = {'l_d', 'l_q', 'psi_d_rms', 'psi_q_rms'};
    for interp_data=1:numel(interp_data_calculated)
        var = interpolated_table.(interp_data_calculated{interp_data})
        f = scatteredInterpolant(interpolated_table.("id_demand_rms"),interpolated_table.("iq_demand_rms"), var, "natural");
        interpolated_data.(interp_data_calculated{interp_data}) = f(XI,YI);
    end

    % Plot Results in large figure.
    surface_plots = ["torque_measured","torque_em_rms","ud_feedback_rms","uq_feedback_rms","l_d","l_q", "psi_d_rms", "psi_q_rms"];
    surface_plot_titles = ["Torque Measured [Nm]", "Torque Electro-Magnetic [Nm]", "U_d Feedback [Vrms]", "U_q Feedback [Vrms]", "L_d [H]", "L_q [H]","\psi_d [RMS]", "\psi_q [RMS]"];
    original_plots = ["torque_measured", "ud_feedback_rms", "uq_feedback_rms"];
    psi_m_plots = ["psi_m_presat","psi_m_sat"];
    
    % Setup Figure
    surf_plots_figure = figure('Name', 'Surface Plots', 'NumberTitle', 'off', 'Position', [100, 100, 1000, 1000]);

    id = table2array(interpolated_table(:,'id_demand_rms'));
    iq = table2array(interpolated_table(:,'iq_demand_rms'));

    for sub_plot=1:length(surface_plots)
        subplot(4,2,sub_plot)
        
        var = table2array(interpolated_table(:,surface_plots(sub_plot)));
        f = scatteredInterpolant(id, iq, var, "natural");
        
        p = surfc(interpolated_data.("id_demand_rms"), interpolated_data.("iq_demand_rms"), f(XI,YI));
        title(surface_plot_titles(sub_plot));
        xlabel("Id Demand (Arms)");
        ylabel("Iq Demand (Arms)");
        zlabel(surface_plots(sub_plot));
        
        % If the values were not calculated, plot the original data as a 3d
        % scatter on top
        if surface_plots(sub_plot) == "torque_measured"
            hold on
            scatter3(data.(id_name),data.(iq_name), data.(torque_name), 'filled', 'k')
            hold off
        end
        if surface_plots(sub_plot) == "ud_feedback_rms"
            hold on
            scatter3(data.(id_name),data.(iq_name), data.(ud_name), 'filled', 'k')
            hold off
        end    
        if surface_plots(sub_plot) == "uq_feedback_rms"
            hold on
            scatter3(data.(id_name),data.(iq_name), data.(uq_name), 'filled', 'k')
            hold off
        end

    end

    psi_m_figure = figure("name" ,"Psi m");
    plot(psi_m_presat,'DisplayName','psi_m_presat');
    hold on;
    plot(psi_m_sat,'DisplayName','psi_m_sat');
    hold off;
    legend;
    stacked_plot_figure = figure("name" ,"Stacked Plot");
    stackedplot(interpolated_table);

    % zip the figures
    savefig(surf_plots_figure, test_name + "_surf_plots.fig");
    savefig(psi_m_figure, test_name + "_psi_m.fig");
    savefig(stacked_plot_figure, test_name + "_stacked_plot.fig");
    savefig(current_sensor_latency_figure, test_name + "_current_sensor_latency.fig");

    % save interpolated table as mat file
    save(test_name + "_interpolated_table.mat", "interpolated_table");
    save(test_name + "_interpolated_data.mat", "interpolated_data");
    save(test_name + "_original_data.mat", "data");
    save(test_name + "_meta_data.mat", "meta_data");

    zip(zip_path, [test_name + "_surf_plots.fig", test_name + "_psi_m.fig", test_name + "_stacked_plot.fig", test_name + "_current_sensor_latency.fig", test_name + "_interpolated_table.mat", test_name + "_interpolated_data.mat", test_name + "_original_data.mat", test_name + "_meta_data.mat"]);

    % delete the saved figures
    delete(test_name + "_surf_plots.fig");
    delete(test_name + "_psi_m.fig");
    delete(test_name + "_stacked_plot.fig");
    delete(test_name + "_current_sensor_latency.fig")

    % delete the mat files
    delete(test_name + "_interpolated_table.mat");
    delete(test_name + "_interpolated_data.mat");
    delete(test_name + "_original_data.mat");
    delete(test_name + "_meta_data.mat");

end

function psi_m_val = psim(torque, pole_pairs, i_q)
    %{
    This function calculates the flux linkage of the motor
        Inputs:
            torque - torque of the motor (Nm)
            pole_pairs - the number of pole pairs of the motor
            i_q - the q-axis current of the motor (A)
        Outputs:
            psi_m_val - the flux linkage of the motor (Vs)
    %}
    
psi_m_val = torque ./ (3 .* pole_pairs .* i_q);
end
    
function l = inductance_rms(torque, pole_pairs, i_d, i_q, psi_m)
%{
This function calculates the inductance of the motor
    Inputs:
        torque - torque of the motor (Nm)
        pole_pairs - the number of pole pairs of the motor
        i_d - the d-axis current of the motor (A)
        i_q - the q-axis current of the motor (A)
        psi_m - the flux linkage of the motor (Vs)
    Outputs:
        l - the inductance of the motor (H)
%}

l = ((torque) ./ (3 .* pole_pairs .* i_d .* i_q)) - (psi_m ./ i_d);
end
    
function l_d = inductance_d(u_q, w_e, pole_pairs, psi_m, i_d)
%{
This function calculates the d-axis inductance of the motor
    Inputs:
        u_q - the q-axis voltage of the motor (V)
        w_e - anglular velocity of the motor
        pole_pairs - the number of pole pairs of the motor
        psi_m - the flux linkage of the motor (Vs)
        i_d - the d-axis current of the motor (A)
    Outputs:
        l_d - the d-axis inductance of the motor (H)
%}

l_d = ((u_q ./ (w_e .* pole_pairs)) - psi_m) ./ i_d;
l_d(l_d < 0) = abs(l_d(l_d < 0));

end

function l_q = inductance_q(u_d, n_rads, pole_pairs, i_q)
%{
This function calculates the q-axis inductance of the motor
    Inputs:
        u_d - the d-axis voltage of the motor (V)
        n_rads - the angular velocity of the motor (rad/s)
        pole_pairs - the number of pole pairs of the motor
        i_q - the q-axis current of the motor (A)
    Outputs:
        l_q - the q-axis inductance of the motor (H)
%}
l_q = (-1 .* u_d) ./ (n_rads .* pole_pairs .* i_q);
l_q(l_q < 0) = abs(l_q(l_q < 0));

end

function flux_d =  psi_d_rms(l_d, i_d, psi_m)
%{
    This function calculates the d-axis flux of the motor
    Inputs:
        l_d - the d-axis inductance of the motor (H)
        i_d - the d-axis current of the motor (A)
        psi_m - the flux linkage of the motor (Vs)
    Outputs:
        psi_d_rms - the d-axis flux of the motor (Vs)
%}
flux_d = l_d .* i_d + psi_m;
end


function flux_q = psi_q_rms(l_q, i_q)
%{
    This function calculates the q-axis flux of the motor
    Inputs:
        l_q - the q-axis inductance of the motor (H)
        i_q - the q-axis current of the motor (A)
    Outputs:
        flux_q - the q-axis flux of the motor (Vs)
%}
flux_q = l_q .* i_q;
end

function rads = rpm2rads(rpm)
%{
    This function converts rpm to rad/s
    Inputs:
        rpm - the angular velocity of the motor (rpm)
    Outputs:
        rads - the angular velocity of the motor (rad/s)
%}
rads = (rpm .* 2 .* pi) ./ 60;
end

function peak = rms_2_peak(rms)
%{
    This function converts rms to peak
    Inputs:
        rms - the rms value of a signal
    Outputs:
        peak - the peak value of a signal
%}
peak = rms .* sqrt(2);
end
    
function torque_em = torque_em_rms(pole_pairs, psi_m, i_d, i_q, l_s)
%{
    This function calculates the rms electromagenetic torque of the motor
    Inputs:
        pole_pairs - the number of pole pairs of the motor
        psi_m - the flux linkage of the motor (Vs)
        i_d - the d-axis current of the motor (A)
        i_q - the q-axis current of the motor (A)
        l_s - the inductance of the motor (H)
    Outputs:
        torque_em - the rms eletromagenitc torque of the motor (Nm)
%}
torque_em = (3 .* (pole_pairs)) .* ((psi_m .* i_q) + (l_s) .* (i_d .* i_q));
end

function txt = displayCoordinates(~,info,x_label,y_label,z_label)
    x = info.Position(1);
    y = info.Position(2);
    z = info.Position(3);
    txt = [x_label num2str(x) '\n' y_label num2str(y) '\n', z_label  num2str(z)];
end
