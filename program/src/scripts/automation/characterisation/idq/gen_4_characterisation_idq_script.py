from program.src.constants import BRIDGE_DISABLE, BRIDGE_ENABLE


# ------------------------Setup Dictionaries----------------------

DVT_log = {
    "Test Point": None,
    "DC Link Voltage": None,
    "DC Link Current": None,
    "Torque Measured [Nm]": None,
    "Speed Limit Forward [rpm]": None,
    "Speed Limit Reverse [rpm]": None,
    "Speed Measured [rpm]": None,
    "Speed Measured [rad/s]": None,
    "Mechanical Power [W]": None,
    "Id Current Injected (RMS) [A]": None,
    "Id Current Feedback (RMS) [A]": None,
    "Iq Current Injected (RMS) [A]": None,
    "Iq Current Feedback (RMS) [A]": None,
    "Is Current Injected (RMS) [A]": None,
    "Is Current Feedback (RMS) [A]": None,
    "Is Current Injected Angle [Degrees]": None,
    "Is Current Feedback Angle [Degrees]": None,
    "Is Current Injected Angle [Radians]": None,
    "Is Current Feedback Angle [Radians]": None,
    "Ud Voltage Feedback (RMS) [V]": None,
    "Uq Voltage Feedback (RMS) [V]": None,
    "Motor Stator Temperature [C]": None,
    "Inverter Temperature [C]": None,
}

master_dict = {}
# ---------------------setting up timesatamp for logs------------------------
datetime_now = datetime.now()
format = "%Y_%m_%d_%H_%M_%S_"
datetime_format = datetime_now.strftime(format)

# -----------------------Torque Sense constants----------------------
devices_found = ctypes.c_ulong(0)
first_device = ctypes.c_ulong(0)
SEARCH_ALL = ctypes.c_ulong(0)
TRUE = ctypes.c_bool(True)
FALSE = ctypes.c_bool(False)
measuredTorque = ctypes.c_float(0.0)
measuredTorqueFilter = ctypes.c_ulong(0)
measuredSpeed = ctypes.c_ulong(0)
measuredSpeedFilter = ctypes.c_ulong(0)

# ------------------ CANopen and IXXAT configuration ------------------
network = canopen.Network()
network.connect(bustype=CAN_HWID_MCU, channel=CAN_MCU_CHANNEL, bitrate=CAN_MCU_BITRATE)

# This will attempt to read an SDO from nodes 1 - 127
network.scanner.search()
# We may need to wait a short while here to allow all nodes to respond
time.sleep(0.05)

for node_id in network.scanner.nodes:

    print(f"Found node {network.scanner.nodes[0]}")

print(network.scanner.nodes[0])

node = network.add_node(
    network.scanner.nodes[0],
    EDS,
    False,
)

time.sleep(1.0)

# --------Startup the TorqSens trasnducer ---------------------

find_device_status = ts.transducerFind(ctypes.byref(devices_found), SEARCH_ALL, 0, TRUE)
time.sleep(1)

print("devices found ", devices_found.value)

while find_device_status == ts.Status.SEARCH_IN_PROGRESS.value:

    print("status is ", find_device_status)

# devices_found.value = 1  # forcing it to satisfy the if condition
if (find_device_status == ts.Status.SUCCESS.value) and (devices_found.value > 0):
    print("Found Device            : {}".format(find_device_status), "SUCCESS.. WOOHOO!! ")
    print("Number of Devices         : {}".format(devices_found.value))
    ts.transducerOpen(first_device)
    ts.torqueZeroAverage(first_device)
    ts.speedGetFilter(first_device, measuredSpeedFilter)
    ts.torqueGetFilter(first_device, measuredTorqueFilter)

    print("Speed Filter Val          : {}".format(measuredSpeedFilter.value))
    print("Torque Filter Val          : {}".format(measuredTorqueFilter.value))

    # ---------------------- Read TPDO from Gen 4 ----------------------
    node.tpdo.read()
    # ------------------ SDO_WNX TO GEN 4 for Idq injection ------------------
    # IdIq injection and acquire transducer measurement

    status_word = g4.bridge_control(node=node, control_word=BRIDGE_ENABLE)

    # print(f"control word: {BRIDGE_ENABLE}")
    # print(f"status word: {status_word}")
    # print(f"status enabled: {STATUS_WORD_ENABLED}")
    # print(f"status disabled: {STATUS_WORD_DISABLED}")

    if status_word == STATUS_WORD_DISABLED:
        print("Bridge status is DISABLED")
        messagebox.showerror("HALT!!!!!", "Bridge DISABLED")

    if status_word == STATUS_WORD_ENABLED:
        message = messagebox.showinfo("Status update", "Bridge ENABLED. Spin the dyno")
        if message:
            count = len(IQ_DEMANDS) + 1
        try:
            for count in range(START_POINT, len(IQ_DEMANDS) - 1, 1):
                g4.poke16(
                    node=node,
                    address=IQ_INJ_ID,
                    value=int(IQ_DEMANDS[count]),
                    fixed_point_scale=FIXED_POINT_SCALE_Q,
                )

                g4.poke16(
                    node=node,
                    address=ID_INJ_ID,
                    value=int(abs(ID_DEMANDS[count])),
                    fixed_point_scale=FIXED_POINT_SCALE_D,
                )

                end_time = time.time() + 2

                # initialize loop variables for transducer and stuff

                DVT_log["Torque Measured [Nm]"] = 0
                DVT_log["Speed Measured [rpm]"] = 0
                DVT_log["Id Current Injected (RMS) [A]"] = 0
                DVT_log["Iq Current Injected (RMS) [A]"] = 0
                DVT_log["Id Current Feedback (RMS) [A]"] = 0
                DVT_log["Iq Current Feedback (RMS) [A]"] = 0
                DVT_log["Ud Voltage Feedback (RMS) [V]"] = 0
                DVT_log["Uq Voltage Feedback (RMS) [V]"] = 0
                DVT_log["DC Link Voltage"] = 0
                DVT_log["DC Link Current"] = 0
                DVT_log["Torque actual Gen 4 (Nm)"] = 0
                DVT_log["Torque demand (Nm)"] = 0
                DVT_log["Motor Stator Temperature [C]"] = 0
                DVT_log["Inverter Temperature [C]"] = 0

                loop_count = 0

                test_bench = 0  # set to 1 if working on the unit in the office; 0 when using the inverter in the lab

                while time.time() < end_time:
                    ts.torqueGet(first_device, measuredTorque)
                    DVT_log["Torque Measured [Nm]"] = DVT_log["Torque Measured [Nm]"] + (measuredTorque.value)

                    ts.speedGetFast(first_device, measuredSpeed)
                    DVT_log["Speed Measured [rpm]"] = DVT_log["Speed Measured [rpm]"] + (measuredSpeed.value)

                    if test_bench == 1:

                        DVT_log["Id Current Injected (RMS) [A]"] = (
                            DVT_log["Id Current Injected (RMS) [A]"]
                            + node.tpdo[1]["Additional Motor Measurements.Target Id (If)"].phys / 16
                        )
                        DVT_log["Iq Current Injected (RMS) [A]"] = (
                            DVT_log["Iq Current Injected (RMS) [A]"]
                            + node.tpdo[1]["Additional Motor Measurements.Target Iq (Ia)"].phys / 16
                        )
                        DVT_log["Id Current Feedback (RMS) [A]"] = (
                            DVT_log["Id Current Feedback (RMS) [A]"]
                            + node.tpdo[1]["Additional Motor Measurements.Id (If)"].phys / 16
                        )
                        DVT_log["Iq Current Feedback (RMS) [A]"] = (
                            DVT_log["Iq Current Feedback (RMS) [A]"]
                            + node.tpdo[1]["Additional Motor Measurements.Iq (Ia)"].phys / 16
                        )
                        DVT_log["Ud Voltage Feedback (RMS) [V]"] = (
                            DVT_log["Ud Voltage Feedback (RMS) [V]"]
                            + node.tpdo[2]["Additional Motor Measurements.Ud (Uf)"].phys / 16
                        )
                        DVT_log["Uq Voltage Feedback (RMS) [V]"] = (
                            DVT_log["Uq Voltage Feedback (RMS) [V]"]
                            + node.tpdo[2]["Additional Motor Measurements.Uq (Ua)"].phys / 16
                        )
                        DVT_log["DC Link Voltage"] = (
                            DVT_log["DC Link Voltage"] + node.tpdo[4]["Device Measurements.Capacitor Voltage"].phys / 16
                        )
                        DVT_log["DC Link Current"] = (
                            DVT_log["DC Link Current"] + node.tpdo[4]["Device Measurements.Battery Current"].phys / 16
                        )
                        DVT_log["Torque actual Gen 4 (Nm)"] = (
                            DVT_log["Torque actual Gen 4 (Nm)"]
                            + node.tpdo[3]["AC Motor Debug Information.Torque actual value (DWork.Td)"].phys / 16
                        )
                        DVT_log["Torque demand (Nm)"] = (
                            DVT_log["Torque demand (Nm)"]
                            + node.tpdo[3]["AC Motor Debug Information.Torque demand value (U.T_d)"].phys / 16
                        )
                    else:
                        DVT_log["Id Current Injected (RMS) [A]"] = (
                            DVT_log["Id Current Injected (RMS) [A]"]
                            + node.tpdo[1]["Additional Motor Measurements.Target Id (If)"].phys / 16
                        )
                        DVT_log["Iq Current Injected (RMS) [A]"] = (
                            DVT_log["Iq Current Injected (RMS) [A]"]
                            + node.tpdo[1]["Additional Motor Measurements.Target Iq (Ia)"].phys / 16
                        )
                        DVT_log["Id Current Feedback (RMS) [A]"] = (
                            DVT_log["Id Current Feedback (RMS) [A]"]
                            + node.tpdo[1]["Additional Motor Measurements.Id (If)"].phys / 16
                        )
                        DVT_log["Iq Current Feedback (RMS) [A]"] = (
                            DVT_log["Iq Current Feedback (RMS) [A]"]
                            + node.tpdo[1]["Additional Motor Measurements.Iq (Ia)"].phys / 16
                        )
                        DVT_log["Ud Voltage Feedback (RMS) [V]"] = (
                            DVT_log["Ud Voltage Feedback (RMS) [V]"]
                            + node.tpdo[2]["Additional Motor Measurements.Ud (Uf)"].phys / 16
                        )
                        DVT_log["Uq Voltage Feedback (RMS) [V]"] = (
                            DVT_log["Uq Voltage Feedback (RMS) [V]"]
                            + node.tpdo[2]["Additional Motor Measurements.Uq (Ua)"].phys / 16
                        )
                        DVT_log["DC Link Voltage"] = (
                            DVT_log["DC Link Voltage"] + node.tpdo[2]["Device Measurements.Capacitor Voltage"].phys / 16
                        )
                        DVT_log["DC Link Current"] = (
                            DVT_log["DC Link Current"] + node.tpdo[4]["Device Measurements.Battery Current"].phys / 16
                        )
                        DVT_log["Torque actual Gen 4 (Nm)"] = (
                            DVT_log["Torque actual Gen 4 (Nm)"]
                            + node.tpdo[4]["AC Motor Debug Information.Torque actual value (DWork.Td)"].phys / 16
                        )
                        DVT_log["Torque demand (Nm)"] = (
                            DVT_log["Torque demand (Nm)"]
                            + node.tpdo[4]["AC Motor Debug Information.Torque demand value (U.T_d)"].phys / 16
                        )

                        DVT_log["Motor Stator Temperature [C]"] = (
                            DVT_log["Motor Stator Temperature [C]"]
                            + node.tpdo[4]["Additional Motor Measurements.Motor Temperature 1 (Measured - T1)"].phys
                        )

                        DVT_log["Inverter Temperature [C]"] = (
                            DVT_log["Inverter Temperature [C]"]
                            + node.tpdo[3]["Device Measurements.Heatsink Temperature"].phys
                        )

                    loop_count = loop_count + 1
                    time.sleep(0.1)

                # Outside of data cap - average results
                DVT_log["Torque Measured [Nm]"] = DVT_log["Torque Measured [Nm]"] / loop_count
                DVT_log["Speed Measured [rpm]"] = DVT_log["Speed Measured [rpm]"] / loop_count
                DVT_log["Id Current Injected (RMS) [A]"] = DVT_log["Id Current Injected (RMS) [A]"] / loop_count
                DVT_log["Iq Current Injected (RMS) [A]"] = DVT_log["Iq Current Injected (RMS) [A]"] / loop_count
                DVT_log["Id Current Feedback (RMS) [A]"] = DVT_log["Id Current Feedback (RMS) [A]"] / loop_count
                DVT_log["Iq Current Feedback (RMS) [A]"] = DVT_log["Iq Current Feedback (RMS) [A]"] / loop_count
                DVT_log["Ud Voltage Feedback (RMS) [V]"] = DVT_log["Ud Voltage Feedback (RMS) [V]"] / loop_count
                DVT_log["Uq Voltage Feedback (RMS) [V]"] = DVT_log["Uq Voltage Feedback (RMS) [V]"] / loop_count
                DVT_log["DC Link Voltage"] = DVT_log["DC Link Voltage"] / loop_count
                DVT_log["DC Link Current"] = DVT_log["DC Link Current"] / loop_count
                DVT_log["Torque actual Gen 4 (Nm)"] = DVT_log["Torque actual Gen 4 (Nm)"] / loop_count
                DVT_log["Torque demand (Nm)"] = DVT_log["Torque demand (Nm)"] / loop_count

                DVT_log["Motor Stator Temperature [C]"] = DVT_log["Motor Stator Temperature [C]"] / loop_count
                DVT_log["Inverter Temperature [C]"] = DVT_log["Inverter Temperature [C]"] / loop_count

                DVT_log["Test Point"] = count
                DVT_log["Speed Demanded [rpm]"] = SPEED_DEMANDS[count]
                DVT_log["Speed Limit Forward [rpm]"] = SPEED_LIMITS_FORWARD[count]
                DVT_log["Speed Limit Reverse [rpm]"] = SPEED_LIMITS_REVERSE[count]
                DVT_log["Speed Measured [rad/s]"] = DVT_log["Speed Measured [rpm]"] * np.radians(6)
                DVT_log["Mechanical Power [W]"] = DVT_log["Torque Measured [Nm]"] * DVT_log["Speed Measured [rad/s]"]
                DVT_log["Id Current Injected (RMS) [A]"] = ID_DEMANDS[count]
                DVT_log["Iq Current Injected (RMS) [A]"] = IQ_DEMANDS[count]
                DVT_log["Id Current Injected (Peak) [A]"] = DVT_log["Id Current Injected (RMS) [A]"] * np.sqrt(2)
                DVT_log["Iq Current Injected (Peak) [A]"] = DVT_log["Iq Current Injected (RMS) [A]"] * np.sqrt(2)
                DVT_log["Id Current Feedback (Peak) [A]"] = DVT_log["Id Current Feedback (RMS) [A]"] * np.sqrt(2)
                DVT_log["Iq Current Feedback (Peak) [A]"] = DVT_log["Iq Current Feedback (RMS) [A]"] * np.sqrt(2)
                DVT_log["Ud Voltage Feedback (Peak) [V]"] = DVT_log["Ud Voltage Feedback (RMS) [V]"] * np.sqrt(2)
                DVT_log["Uq Voltage Feedback (Peak) [A]"] = DVT_log["Ud Voltage Feedback (RMS) [V]"] * np.sqrt(2)
                DVT_log["Is Current Injected (RMS) [A]"] = np.sqrt(
                    (DVT_log["Id Current Injected (RMS) [A]"] ** 2) + (DVT_log["Iq Current Injected (RMS) [A]"] ** 2)
                )
                DVT_log["Is Current Injected (Peak) [A]"] = DVT_log["Is Current Injected (RMS) [A]"] * np.sqrt(2)
                DVT_log["Is Current Injected Angle [Radians]"] = np.arctan2(
                    np.abs(DVT_log["Id Current Injected (RMS) [A]"]),
                    np.abs(DVT_log["Iq Current Injected (RMS) [A]"]),
                )
                DVT_log["Is Current Injected Angle [Degrees]"] = np.rad2deg(
                    DVT_log["Is Current Injected Angle [Radians]"]
                )
                DVT_log["Is Current Feedback (RMS) [A]"] = np.sqrt(
                    (DVT_log["Id Current Feedback (RMS) [A]"] ** 2) + (DVT_log["Iq Current Feedback (RMS) [A]"] ** 2)
                )
                DVT_log["Is Current Feedback Angle [Radians]"] = np.arctan2(
                    np.abs(DVT_log["Id Current Injected (RMS) [A]"]),
                    np.abs(DVT_log["Iq Current Feedback (RMS) [A]"]),
                )
                DVT_log["Is Current Feedback Angle [Degrees]"] = np.rad2deg(
                    DVT_log["Is Current Feedback Angle [Radians]"]
                )

                df = pd.DataFrame(DVT_log, index=[count])

                df.to_csv(
                    PATH_MEASURED,
                    mode="a",
                    header=not os.path.exists(PATH_MEASURED),
                    index=True,
                )

        except Exception as e:
            print(e)
        except KeyboardInterrupt:
            g4.poke16(node, IQ_INJ_ID, 0, 0, 0)
            g4.poke16(node, ID_INJ_ID, 0, 0, 0)

            messagebox.showinfo("Count number", count)


# -------------------------end of Idq injection--------------------------------
else:
    print("Transducer not detected ")

g4.poke16(node, IQ_INJ_ID, 0, 0, 0)
g4.poke16(node, ID_INJ_ID, 0, 0, 0)

# iq_length = len(IQ_DEMANDS)-2
ts.speedGetFast(first_device, measuredSpeed)
while abs(measuredSpeed.value) > 20:
    ts.speedGetFast(first_device, measuredSpeed)
    # time.sleep(0.1)
    if abs(measuredSpeed.value) < 20:
        g4.bridge_control(node=node, control_word=BRIDGE_DISABLE)
        # iq_length = 0
        messagebox.showinfo("Alert!", "Bridge DISABLED & Test complete!")

ts.transducerClose(first_device, FALSE)
