from json import encoder

from program.src.equipment.g4_function import sdo_write
from program.src.scripts.automation.characterisation.idq.gen_4_characterisation_idq_script import DVT_log, master_dict, transducer_log

# ---------------------- encoder offset properies-----------------------

encoder_config = 0x4630  # 0x4630 is address for the encoder configuration
encoder_config_subindex = 16  # sub-index for encoder offset/ angle


# ---------------------setting up timesatamp for logs------------------------
datetime_now = datetime.now()
format = "%Y_%m_%d_%H_%M_%S_"
datetime_format = datetime_now.strftime(format)

# -----------------------Torque Sense constants----------------------
devices_found = ctypes.c_ulong(0)
first_device = ctypes.c_ulong(0)
SEARCH_ALL = ctypes.c_ulong(0)
TRUE = ctypes.c_bool(True)
FALSE = ctypes.c_bool(False)
measuredTorque = ctypes.c_float(0.0)
measuredTorqueFilter = ctypes.c_ulong(0)
measuredSpeed = ctypes.c_ulong(0)
measuredSpeedFilter = ctypes.c_ulong(0)

# ------------------ CANopen and IXXAT configuration ------------------
network = canopen.Network()
network.connect(bustype=CAN_HWID_MCU, channel=CAN_MCU_CHANNEL, bitrate=CAN_MCU_BITRATE)

# This will attempt to read an SDO from nodes 1 - 127
network.scanner.search()
# We may need to wait a short while here to allow all nodes to respond
time.sleep(0.05)

for node_id in network.scanner.nodes:
    print(f"Found node {network.scanner.nodes[0]}")

print(network.scanner.nodes[0])

node = network.add_node(
    network.scanner.nodes[0],
    EDS,
    False,
)

time.sleep(1.0)

# --------Startup the TorqSens trasnducer ---------------------

find_device_status = ts.transducerFind(ctypes.byref(devices_found), SEARCH_ALL, 0, TRUE)
time.sleep(1)

print("devices found ", devices_found.value)

while find_device_status == ts.Status.SEARCH_IN_PROGRESS.value:
    print("status is ", find_device_status)

# devices_found.value = 1  # forcing it to satisfy the if condition
if (find_device_status == ts.Status.SUCCESS.value) and (devices_found.value > 0):
    print("Found Device            : {}".format(find_device_status), "SUCCESS.. WOOHOO!! ")
    print("Number of Devices         : {}".format(devices_found.value))
    ts.transducerOpen(first_device)
    ts.torqueZeroAverage(first_device)
    ts.speedGetFilter(first_device, measuredSpeedFilter)
    ts.torqueGetFilter(first_device, measuredTorqueFilter)

    print("Speed Filter Val          : {}".format(measuredSpeedFilter.value))
    print("Torque Filter Val          : {}".format(measuredTorqueFilter.value))

    # ---------------------- Read TPDO from Gen 4 ----------------------
    node.tpdo.read()

    # ------------------ SDO_WNX TO GEN 4 for Id injection ------------------
    # Id injection and acquire transducer measurement to align encoder

    status_word = g4.bridge_control(node=node, control_word=BRIDGE_ENABLE)

    if status_word == STATUS_WORD_DISABLED:
        print("Bridge status is DISABLED")
        messagebox.showerror("HALT!!!!!", "Bridge DISABLED")

    if status_word == STATUS_WORD_ENABLED:
        message = messagebox.showinfo("Status update", "Bridge ENABLED. Spin the dyno")

    if message:
        g4.poke16(
            node=node,
            address=ID_INJ_ID,
            value=int(abs(ID_DEMANDS[count])),
            fixed_point_scale=FIXED_POINT_SCALE_D,
        )

        time.sleep(0.3)
        ts.torqueGet(first_device, measuredTorque)
        end_time = time.time() + 2

        while abs(measuredTorque.value) > 0.3:
            transducer_log["Torque Measured (Nm)"] = 0
            loop_count = 0
            while time.time() < end_time:
                ts.torqueGet(first_device, measuredTorque)
                transducer_log["Torque Measured (Nm)"] = transducer_log["Torque Measured (Nm)"] + (measuredTorque.value)

                # offset_val = node.sdo[0x4630][16]
                # offset_val.write(f"{(value * fixed_point_scale)}")

            loop_count = loop_count + 1
            time.sleep(0.1)

            transducer_log["Torque Measured (Nm)"] = transducer_log["Torque Measured (Nm)"] / loop_count
            # sdo_write(node=node, encoder_config, encoder_config_subindex, offset)


# ================== WORK IN PROGRESS =======================
