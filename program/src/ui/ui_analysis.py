import streamlit as st
from src.ui import ui_results, ui_tables, ui_tools


def tab_analysis(tab):

    with tab:
        results_tab, table_tab, tools_tab = st.tabs(["📉 Results", "📝 Tables ", "🔨 Tools"])

        hw_dict = st.session_state.hw_dict
        test_dict = st.session_state.test_dict
        cfg_dict = st.session_state.cfg_dict
        test_points = st.session_state.test_points
        test_name = st.session_state.test_name
        paths = st.session_state.paths

        ui_results.tab_results(results_tab, hw_dict, test_dict, cfg_dict, test_points, test_name, paths)
        ui_tables.tab_tables(table_tab, paths)
        ui_tools.tab_tools(tools_tab)

    return
