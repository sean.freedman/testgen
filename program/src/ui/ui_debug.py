import sys
from subprocess import CREATE_NEW_CONSOLE, Popen

import streamlit as st


def tab_debug(tab):
    with tab:
        st.write("Debug Tab")
        st.warning("Still in development")
        example_script = ["read_gen5.py", "read_dcdc.py", "read_gen4t.py"]
        st.selectbox("Select Example Script", example_script, key="example_script")
        if st.button("Run Example Script"):
            if st.session_state.example_script == "read_gen5.py":
                st.write("Running read_gen5.py")
                with st.spinner("Test Script launched and is running in a seperate shell.."):
                    example_script_path = r"read_gen5.py"

                    process = Popen(
                        [f"{sys.executable}", example_script_path],
                        creationflags=CREATE_NEW_CONSOLE,
                        stdout=sys.stdout,
                    )
                    # print the output of the script to the console
                    st.write(process.stdout.read())
                    process.wait()
                    st.info("Test Script Complete or Closed")
            elif st.session_state.example_script == "read_dcdc.py":
                st.write("Running read_dcdc.py")
            elif st.session_state.example_script == "read_gen4t.py":
                st.write("Running read_gen4t.py")

    return
