import streamlit as st
from src.constants import AVAILABLE_PROJECTS, CAN_DEVICES, DEFAULT_PATHS, DYNO_LOCATIONS, FLOATS, THIRTY_TWO_ZEROS
from src.file_ops import read_yaml_file


def tab_hw(tab, test_dict):
    """
    Draw HW tab UI
    """
    project_tab, dyno_tab, dcdc_tab, can_tab, login_tab = tab.tabs(["Project", "Dyno", "DCDC", "CAN", "Login"])
    project_dict = ui_project(project_tab, test_dict)
    dyno_dict = ui_dyno(dyno_tab, project_dict)
    dcdc_dict = ui_dcdc(dcdc_tab, project_dict, dyno_dict)
    can_dict = ui_can(can_tab, dyno_dict)
    login_dict = ui_login(login_tab)

    hw_dict = {
        "project": project_dict,
        "dcdc": dcdc_dict,
        "dyno": dyno_dict,
        "can": can_dict,
        "login": login_dict,
    }

    return hw_dict


def ui_project(tab, test_dict: dict):
    """
    Draw Project tab UI.

    Displays a dropdown menu of available project options. Updates the selected project's motor and
    inverter pairing based on the selected option. User can add notes for both inverter and motor.

    Args:
    - tab: Streamlit `beta_columns` object representing the tab where the UI is to be displayed.
    - test_dict: Unused; kept to maintain function signature consistency with other functions.

    Returns:
    A dictionary containing the selected project's controller and motor details.
    """

    # Display dropdown menu of available projects
    with tab:
        tab.selectbox(label="Project", options=AVAILABLE_PROJECTS, key="requested_project")

        # Update the project's "motor/inverter pairing" based on selected project
        if st.session_state.requested_project == "Blencathra":
            controller_path = DEFAULT_PATHS["equipment"]["configs"]["controllers"]["gen_5"]
            controller = read_yaml_file(controller_path)["blencathra"]

            motor_path = DEFAULT_PATHS["equipment"]["configs"]["motors"]["pmac"]
            motor = read_yaml_file(motor_path)["af130_4t"]

        elif st.session_state.requested_project == "Hawkshead":
            controller_path = DEFAULT_PATHS["equipment"]["configs"]["controllers"]["gen_5"]
            controller = read_yaml_file(controller_path)["hawkshead"]

            motor_path = DEFAULT_PATHS["equipment"]["configs"]["motors"]["pmac"]
            motor = read_yaml_file(motor_path)["af130_4t"]

        elif st.session_state.requested_project == "Bowfell":
            controller_path = DEFAULT_PATHS["equipment"]["configs"]["controllers"]["gen_5"]
            controller = read_yaml_file(controller_path)["bowfell"]

            motor_path = DEFAULT_PATHS["equipment"]["configs"]["motors"]["pmac"]
            motor = read_yaml_file(motor_path)["bowfell"]

        elif st.session_state.requested_project == "Oxford":
            controller_path = DEFAULT_PATHS["equipment"]["configs"]["controllers"]["gen_5"]
            controller = read_yaml_file(controller_path)["oxford"]

            motor_path = DEFAULT_PATHS["equipment"]["configs"]["motors"]["pmac"]
            motor = read_yaml_file(motor_path)["oxford"]

        elif st.session_state.requested_project == "Derwent":
            controller_path = DEFAULT_PATHS["equipment"]["configs"]["controllers"]["gen_5"]
            controller = read_yaml_file(controller_path)["derwent"]

            motor_path = DEFAULT_PATHS["equipment"]["configs"]["motors"]["pmac"]
            motor = read_yaml_file(motor_path)["af130_4t"]

        elif st.session_state.requested_project == "Ultraviolet":
            controller_path = DEFAULT_PATHS["equipment"]["configs"]["controllers"]["gen_4"]
            controller = read_yaml_file(controller_path)["ultraviolet"]

            motor_path = DEFAULT_PATHS["equipment"]["configs"]["motors"]["pmac"]
            motor = read_yaml_file(motor_path)["ultraviolet"]

        elif st.session_state.requested_project == "Virya":
            controller_path = DEFAULT_PATHS["equipment"]["configs"]["controllers"]["gen_4"]
            controller = read_yaml_file(controller_path)["virya"]

            motor_path = DEFAULT_PATHS["equipment"]["configs"]["motors"]["pmac"]
            motor = read_yaml_file(motor_path)["virya"]

        # Add text areas for Inverter Notes and Motor Notes
        tab.text_area(
            label="Inverter Notes",
            placeholder="Hardware modifications, prototype, customer return etc.",
            key="inverter_note",
        )

        tab.text_area(
            label="Motor Notes",
            placeholder="Hardware modifications, prototype, customer return etc.",
            key="motor_note",
        )

        # Update "notes" in the controller and motor dictionaries based on user input
        controller.update(
            {
                "notes": st.session_state.inverter_note,
            }
        )

        motor.update(
            {
                "notes": st.session_state.motor_note,
            }
        )

        # Add maximum torque, speed, and voltage settings to the controller dictionary
        controller["config"].update(
            {
                "max": {
                    "torque": max(max(controller["config"]["tables"]["torque"]["peak"])),
                    "speed": max(controller["config"]["tables"]["speed"]["breakpoints"]),
                    "voltage": max(controller["config"]["tables"]["voltage"]["breakpoints"]),
                }
            }
        )

        # Return a dictionary containing the selected project's controller and motor details
        project_dict = {
            "controller": controller,
            "motor": motor,
        }

    return project_dict


def ui_dyno(tab, project_dict):
    """
    Draw tab UI
    """
    with tab:
        st.selectbox(label="location", options=DYNO_LOCATIONS, key="dyno_location")

        if st.session_state.dyno_location == "D7: 340kW":
            dyno = read_yaml_file(DEFAULT_PATHS["equipment"]["configs"]["controllers"]["nidec"])
            dyno = dyno["340_kw"]

        elif st.session_state.dyno_location == "D8: 160kW":
            dyno = read_yaml_file(DEFAULT_PATHS["equipment"]["configs"]["controllers"]["nidec"])
            dyno = dyno["160_kw"]

        elif st.session_state.dyno_location == "D4: 100kW":
            dyno = read_yaml_file(DEFAULT_PATHS["equipment"]["configs"]["controllers"]["nidec"])
            dyno = dyno["100_kw"]

        elif st.session_state.dyno_location == "DX: B2B":
            dyno = read_yaml_file(DEFAULT_PATHS["equipment"]["configs"]["controllers"]["nidec"])
            dyno = dyno["b2b"]

        if dyno["dc_link"] == "single" or "parallel":
            power_supply = read_yaml_file(DEFAULT_PATHS["equipment"]["configs"]["power_supplies"]["dcdc"])
            power_supply = power_supply["dcdc"]
            power_supply["config"]["topology"] = dyno["dc_link"]

        elif dyno["dc_link"] == "regatron":
            power_supply = read_yaml_file(DEFAULT_PATHS["equipment"]["configs"]["power_supplies"]["dcdc"])
            power_supply = power_supply["regatron"]

        if dyno["torque_sensor"] == "HBM T40B":
            torque_sensor = read_yaml_file(DEFAULT_PATHS["equipment"]["configs"]["power_analysers"]["hbm"])
            torque_sensor = torque_sensor["gen4t"]

        elif dyno["torque_sensor"] == "TS":
            st.warning("need to complete torque sensor config")

        else:
            st.error("not torque sensor")

        st.number_input(label="Ratio", min_value=0.00, max_value=100.00, value=dyno["gearbox_ratio"], step=0.001, format="%0.5f", key="dyno_ratio", help="1, 3.06, 5.X")

        dyno.update(
            {
                "gearbox_ratio": st.session_state.dyno_ratio,
            }
        )

        dyno = {
            "dyno": dyno,
            "power_supply": power_supply,
            "torque_sensor": torque_sensor,
        }

        return dyno


def ui_dcdc(tab, project_dict, dyno_dict):
    """
    Draw tab UI
    """

    with tab:
        mid_volt = int((len(project_dict["controller"]["config"]["tables"]["voltage"]["breakpoints"]) - 1) / 2)

        tab.number_input(
            label="DC Link Voltage",
            min_value=float(dyno_dict["power_supply"]["limits"]["voltage"]["out"]["minimum"]),
            max_value=min(float(max(project_dict["controller"]["config"]["tables"]["voltage"]["breakpoints"])), float(dyno_dict["power_supply"]["limits"]["voltage"]["out"]["maximum"])),
            value=min(float(dyno_dict["power_supply"]["limits"]["voltage"]["out"]["maximum"]), float(project_dict["controller"]["config"]["tables"]["voltage"]["breakpoints"][mid_volt])),
            step=0.5,
            help="Project Breakpoints = " + str(project_dict["controller"]["config"]["tables"]["voltage"]["breakpoints"]),
            key="requested_voltage",
        )

        tab.number_input(
            label="DC Link Current (+)",
            min_value=FLOATS["zero"],
            max_value=min(project_dict["controller"]["config"]["dc_link"]["current"]["discharge"], dyno_dict["power_supply"]["limits"]["current"]["positive"]),
            value=min(project_dict["controller"]["config"]["dc_link"]["current"]["discharge"], dyno_dict["power_supply"]["limits"]["current"]["positive"]),
            step=0.5,
            key="requested_i_lim_pos",
        )

        tab.number_input(
            label="DC Link Current (-)",
            min_value=min(project_dict["controller"]["config"]["dc_link"]["current"]["charge"], dyno_dict["power_supply"]["limits"]["current"]["negative"]),
            max_value=FLOATS["zero"],
            value=max(project_dict["controller"]["config"]["dc_link"]["current"]["charge"], dyno_dict["power_supply"]["limits"]["current"]["negative"]),
            step=0.5,
            key="requested_i_lim_neg",
        )

        dyno_dict["power_supply"].update(
            {
                "config": {
                    "positive": st.session_state.requested_i_lim_pos,
                    "negative": st.session_state.requested_i_lim_neg,
                    "target": st.session_state.requested_voltage,
                },
            }
        )

    return dyno_dict


def ui_can(tab, dyno_dict):
    """
    Draw tab UI
    """

    with tab:
        tab.text_input(label="MCU CAN HWID", value=dyno_dict["dyno"]["can"]["controller"]["hwid"], key="can_hwid_mcu")

        tab.text_input(label="DCDC CAN HWID", value=dyno_dict["dyno"]["can"]["dcdc"]["hwid"], key="can_hwid_dcdc")

        tab.number_input(
            label="MCU CAN Channel",
            min_value=0,
            max_value=20,
            value=dyno_dict["dyno"]["can"]["controller"]["channel"],
            step=1,
            key="can_mcu_channel",
        )

        tab.number_input(
            label="DCDC CAN Channel",
            min_value=0,
            max_value=20,
            value=dyno_dict["dyno"]["can"]["dcdc"]["channel"],
            step=1,
            key="can_dcdc_channel",
        )

        if dyno_dict["dyno"]["torque_sensor"] == "HBM T40B":
            tab.markdown("**Torque Sensor CAN Configuration**")

            tab.selectbox(
                label="Torque Sensor CAN Device",
                options=CAN_DEVICES,
                key="torque_sensor_can_device",
            )

            tab.number_input(
                label="Torque Sensor CAN Channel",
                min_value=0,
                max_value=512,
                value=dyno_dict["dyno"]["can"]["torque_sensor"]["channel"],
                step=1,
                key="torque_sensor_can_channel",
            )

            dyno_dict["dyno"]["can"]["torque_sensor"].update({"dongle": st.session_state.torque_sensor_can_device, "channel": st.session_state.torque_sensor_can_channel})

        dyno_dict["dyno"]["can"]["controller"].update({"hwid": st.session_state.can_hwid_mcu, "channel": st.session_state.can_mcu_channel})
        dyno_dict["dyno"]["can"]["dcdc"].update({"hwid": st.session_state.can_hwid_dcdc, "channel": st.session_state.can_dcdc_channel})

        can_dict = {
            "controller": dyno_dict["dyno"]["can"]["controller"],
            "dcdc": dyno_dict["dyno"]["can"]["dcdc"],
            "torque_sensor": dyno_dict["dyno"]["can"]["torque_sensor"],
        }

    return can_dict


def ui_login(login_tab):
    """
    Draw tab UI
    """
    login_tab.checkbox(label="login", value=True, key="login_user")

    login_tab.text_input(
        label="Password",
        value=THIRTY_TWO_ZEROS,
        placeholder=THIRTY_TWO_ZEROS,
        key="mcu_password",
    )

    login_tab.markdown(f"`{len(st.session_state.mcu_password)}`")

    login_dict = {
        "login": st.session_state.login_user,
        "password": st.session_state.mcu_password,
    }

    return login_dict
