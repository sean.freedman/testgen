import streamlit as st
from src import plot


def tab_plots(tab, envolope, test_points, hw_dict, test_type):
    """
    Draw Plotting Tab
    """

    scatter_tab, line_tab, table_tab = tab.tabs(["Scatter", "Timeline", "Table"])

    scatter_plot = tab_scatter(scatter_tab, envolope, test_points, hw_dict, test_type)
    line_plot = tab_timeline(line_tab, test_points, hw_dict, test_type)
    tab_table(table_tab, test_points)

    plot_dict = {"Scatter": scatter_plot, "Line": line_plot}

    return plot_dict


def tab_scatter(tab, envelope, test_points, hw_dict, test_type):
    """
    Draw UI for scatter plot tab
    """
    target_voltage = hw_dict["dyno"]["power_supply"]["config"]["target"]
    with tab:

        with st.spinner("Gererating Scatter Plot"):

            if test_type == "Torque Speed Sweep":

                scatter_plot = plot.torque_speed_scatter(profile=test_points, profile_voltage=target_voltage, boundery=envelope)

            elif test_type in [
                "Torque Speed Sweep",
                "Idq Characterisation Gen 5",
                "Idq Injection",
                "Idq Injection Gen 4",
                "Idq Characterisation Gen 4",
            ]:

                scatter_plot = plot.idq_scatter(profile=test_points, voltage_ref=target_voltage)

            st.plotly_chart(scatter_plot)

    return scatter_plot


def tab_timeline(tab, test_points, hw_dict, test_type):
    """
    Draw UI for timeline plot tab
    """
    target_voltage = hw_dict["dyno"]["power_supply"]["config"]["target"]
    with tab:

        with st.spinner("Gererating Timeline Plot"):

            if test_type == "Torque Speed Sweep":

                line_plot = plot.torque_speed_timeline(
                    profile=test_points,
                    profile_voltage=target_voltage,
                    speed_limit_forward=test_points["speed_lim_fwd"],
                    speed_limit_reverse=test_points["speed_lim_rev"],
                )

            elif test_type in [
                "Torque Speed Sweep",
                "Idq Characterisation Gen 5",
                "Idq Injection",
                "Idq Injection Gen 4",
                "Idq Characterisation Gen 4",
            ]:

                line_plot = plot.idq_timeline(profile=test_points, target_voltage=target_voltage)

            st.plotly_chart(line_plot)

    return line_plot


def tab_table(tab, test_points: dict):
    """Generate Datatable of generated test points

    Args:
        tab (st.tab): Streamlit Tab to anchor to.
        test_points (dict): Dictionary of generated input data.
    """
    with tab:

        with st.spinner("Gererating Test Input Table"):
            st.dataframe(test_points)

    return
