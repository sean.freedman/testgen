import streamlit as st
from src.constants import OPERATING_QUADRANTS, TEST_TYPES


def tab_profile(tab, paths: dict):
    """Generate tab for Profile configuration parameters

    Args:
        tab (_type_): _description_
        paths (dict): _description_

    Returns:
        _type_: _description_
    """
    with tab:
        st.text_input(
            label="Test Name",
            value="",
            placeholder="AUTO: <DATETIME>_<PROJECT>_<INVERTER>_<MOTOR>_<VOLTAGE>_<PROFILE>",
            key="test_name",
        )

        st.text_input(
            label="Log Directory",
            value=paths["log"]["test"],
            placeholder=paths["log"]["test"],
            key="logging_path",
        )

        st.markdown("---")

        st.selectbox(label="Test Type", options=TEST_TYPES, key="requested_test")

        if st.session_state.requested_test in [
            "Torque Speed Sweep",
            "Idq Characterisation Gen 5",
            "Idq Injection",
        ]:
            operating_quadrants = OPERATING_QUADRANTS
        else:
            st.error("Test type not recognised, under development or placeholder")
            st.stop()

        st.selectbox(label="profile", options=operating_quadrants, key="requested_profile")

        st.markdown("---")

        paths["remote"] = r"N:\UK\Engineering\Documents\Motor Control\Dyno Test Reports" + "\\" + st.session_state.requested_test

        test_dict = {
            "path": st.session_state.logging_path,
            "type": st.session_state.requested_test,
            "profile": st.session_state.requested_profile,
            "name": st.session_state.test_name,
        }

    return test_dict
