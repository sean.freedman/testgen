import os

import pandas as pd
import streamlit as st
from src.constants import FILE_EXTENSIONS

from program.src.report_gen import generate, move_report


def tab_report(report_tab, paths, hw_dict, test_dict, cfg_dict, test_points, test_name):
    """_summary_

    Args:
        report_tab (_type_): _description_
        paths (_type_): _description_
        hw_dict (_type_): _description_
        test_dict (_type_): _description_
        cfg_dict (_type_): _description_
        test_points (_type_): _description_
        test_name (_type_): _description_
    """
    with report_tab:
        st.markdown("#### Report Generation")
        st.markdown("Generate reports from provided csv")
        st.file_uploader(label="Choose Results to Analyse", type=[".csv"], accept_multiple_files=False, key="report_local")
        if st.session_state.report_local is not None:
            test_name = st.session_state.report_local.name.replace(".csv", "")

            st.checkbox("Move report to remote location when generated?", value=True, key="move_to_remote", help=paths["remote"])

            if st.button("Generate Report"):
                paths_measured = {
                    "zip": (os.getcwd() + "\\" + test_name + FILE_EXTENSIONS["CSV"]),
                    "measured": (os.getcwd() + "\\" + test_name + ".csv"),
                    "Alert": (os.getcwd() + "\\" + test_name + "_ALERTS.csv"),
                    "inverter_properties": (os.getcwd() + "\\" + test_name + "_INVERTER_PROPERTIES.csv"),
                    "I Sense": (os.getcwd() + "\\" + test_name + "_CURRENT_SENSOR_LATENCY_TABLE.csv"),
                    "Tables": {
                        "Idq Torque Electromagnetic [Nm]": (os.getcwd() + "\\" + test_name + "_IDQ_TORQUE_EM.csv"),
                        "Idq Torque Measured [Nm]": (os.getcwd() + "\\" + test_name + "_IDQ_TORQUE_MEASURED.csv"),
                        "MATLAB Struct": (os.getcwd() + "\\" + test_name + ".mat"),
                        "Idq Worksheet": (os.getcwd() + "\\" + test_name + "_TABLES" + ".xlsx"),
                    },
                    "torque_averaged": (os.getcwd() + "\\" + test_name + "_TAVG.csv"),
                    "Transducer": (os.getcwd() + "\\" + test_name + "_TRANSDUCER.csv"),
                }

                paths.update(paths_measured)

                try:
                    paths["zip"] = generate(
                        paths,
                        hw_dict,
                        test_dict,
                        cfg_dict,
                        test_points,
                        test_name,
                    )

                    st.success("Report Generated")

                    if st.session_state.move_to_remote is True:
                        moved = move_report(zip_path=paths["zip"], remote_path=paths["remote"])

                        if moved is True:
                            st.success(f"Report Moved")
                            st.markdown(f"`{paths['remote']}/{paths['zip']}`")

                        else:
                            st.error(moved)

                except Exception as e:
                    st.error("Report could not be generated!")
                    st.error(e)

        st.markdown("---")
        st.markdown("#### Report Tools")
        st.markdown("##### Merge Files")
        st.markdown("If multiple CSV's belong to one test, upload here to merge them into one csv.")
        st.file_uploader(label="Results to merge", type=[".csv"], accept_multiple_files=True, key="results_merge")

        df = pd.DataFrame()

        if st.session_state.results_merge != []:
            file_list = []

            for i in range(len(st.session_state.results_merge)):
                file_list.append(pd.read_csv(st.session_state.results_merge[i]))

            df = pd.concat(file_list)

            merge_filename = st.session_state.results_merge[0].name
            merge_filename = merge_filename.replace(".csv", "_MERGED")

            st.text_input(label="File Name", value=merge_filename, key="merge_filename")

            if st.button(label="Merge Files"):
                df.sort_values(by=["Test Point"], inplace=True)
                df.to_csv(
                    path_or_buf=st.session_state.merge_filename + ".csv",
                    header=True,
                    index=False,
                )
                merged_file_location = os.getcwd() + "\\" + st.session_state.merge_filename + ".csv"
                st.success(f"Files Merged: {merged_file_location}")

    return
