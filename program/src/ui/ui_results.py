import streamlit as st
from src.constants import CURRENT_PATH, DEFAULT_FILE_SUFFIX, FILE_EXTENSIONS

from program.src.file_ops import move_file
from program.src.report_gen import generate


def tab_results(tab, hw_dict, test_dict, cfg_dict, test_points, test_name, paths):
    """_summary_

    Args:
        tab (_type_): _description_
        paths (_type_): _description_
        hw_dict (_type_): _description_
        test_dict (_type_): _description_
        cfg_dict (_type_): _description_
        test_points (_type_): _description_
        test_name (_type_): _description_
    """
    with tab:
        idq_characterisation_tab, torque_speed_sweep_tab = st.tabs(["Idq Characterisation Gen 5", "Torque Speed Sweep"])
        idq_characterisation_analysis_tab(idq_characterisation_tab, hw_dict, test_dict, cfg_dict, test_points, test_name, paths)
        torque_speed_sweep_analysis_tab(torque_speed_sweep_tab)

    return


def idq_characterisation_analysis_tab(tab, hw_dict, test_dict, cfg_dict, test_points, test_name, paths):
    with tab:
        st.markdown("#### Idq Characterisation Analysis")
        st.markdown("Generate characterisation data and plots from provided csv")
        st.file_uploader(label="Choose results to analyse", type=FILE_EXTENSIONS["CSV"], accept_multiple_files=False, key="report_local")

        if st.session_state.report_local is not None:
            test_name = st.session_state.report_local.name.replace(FILE_EXTENSIONS["CSV"], "")

            st.checkbox("Move report to remote location when generated?", value=True, key="move_to_remote", help=paths["remote"])

            st.button(
                label="Analyse Results",
                key="generate_report",
                help="Analyse Idq Characterisation Results from provided csv",
            )

            paths["files"]["idq"]["data"]["results"]["measured"] = CURRENT_PATH + "\\" + test_name + FILE_EXTENSIONS["CSV"]
            paths["files"]["idq"]["data"]["results"]["alert"] = CURRENT_PATH + "\\" + test_name + DEFAULT_FILE_SUFFIX["alerts"] + FILE_EXTENSIONS["CSV"]
            paths["files"]["idq"]["data"]["inverter"]["meta_data"] = CURRENT_PATH + "\\" + test_name + DEFAULT_FILE_SUFFIX["inverter_properties"] + FILE_EXTENSIONS["CSV"]
            paths["files"]["idq"]["data"]["inverter"]["current_sensor_latencies"] = CURRENT_PATH + "\\" + test_name + DEFAULT_FILE_SUFFIX["current_sensor_latencies"] + FILE_EXTENSIONS["CSV"]

            report_success = False

            if st.session_state.generate_report == True:
                try:
                    with st.spinner("Analysing Results..."):
                        generate(paths, hw_dict, test_dict, cfg_dict, test_points, test_name)
                        report_success = True
                    st.success("Results analysed successfully!")

                except Exception as e:
                    st.error("Results could not be analysed!")
                    st.error(e)

                if st.session_state.move_to_remote and report_success is True:
                    moved = move_file(paths["files"]["idq"]["data"]["results"]["zip"], paths["folders"]["remote"]["characterisation"]["idq"]["results"]["gen_5"])

                    if moved is True:
                        st.success(f"Results Moved")
                        st.markdown(f"`{paths['folders']['remote']['characterisation']['idq']['results']['gen_5']}/{paths['files']['idq']['data']['results']['zip']}`")

                    else:
                        st.error(moved)


def torque_speed_sweep_analysis_tab(tab):
    with tab:
        st.info("Still in development")
