import glob
import json
import os
import shutil
import zipfile

import h5py
import matlab.engine
import scipy.io as sio
import streamlit as st
from src.constants import (DEFAULT_FILE_PREFIX, DEFAULT_FILE_SUFFIX,
                           EMPTY_JSON, FILE_EXTENSIONS)
from src.profiles import BOWFELL, CUSTOM, DERWENT, JACOB, OXFORD


def tab_tables(tab, paths):

    with tab:

        st.markdown("#### Table Generation")
        st.markdown("Generate table from provided zip")
        st.file_uploader(label="Choose Results to Generate Table Data", type=FILE_EXTENSIONS["ZIP"], accept_multiple_files=False, key="table_zip")

        if st.session_state.table_zip is not None:
            table_profile = ["Motoring", "Generating"]
            maximum_torque_method = ["MTPA", "MTPSL"]
            svm_method = ["SPWM", "SVM"]

            with st.expander("Inverter Parameters"):

                st.selectbox("Inverter Model", ["Custom", "Derwent", "Bowfell", "Oxford", "Jacob"], key="inverter_type")

                if st.session_state.inverter_type == "Custom":
                    inverter_parameters_dict = CUSTOM["inverter_parameters"]
                elif st.session_state.inverter_type == "Derwent":
                    inverter_parameters_dict = DERWENT["inverter_parameters"]
                elif st.session_state.inverter_type == "Bowfell":
                    inverter_parameters_dict = BOWFELL["inverter_parameters"]
                elif st.session_state.inverter_type == "Oxford":
                    inverter_parameters_dict = OXFORD["inverter_parameters"]
                elif st.session_state.inverter_type == "Jacob":
                    inverter_parameters_dict = JACOB["inverter_parameters"]

                l_col, r_col = st.columns(2)
                l_col.markdown("##### Switch Parameters")
                l_col.number_input(
                    label=inverter_parameters_dict["switch"]["label"]["voltage_threshold"],
                    value=inverter_parameters_dict["switch"]["value"]["voltage_threshold"],
                    min_value=inverter_parameters_dict["switch"]["min"]["voltage_threshold"],
                    max_value=inverter_parameters_dict["switch"]["max"]["voltage_threshold"],
                    step=inverter_parameters_dict["switch"]["step"]["voltage_threshold"],
                    format=inverter_parameters_dict["switch"]["format"]["voltage_threshold"],
                    key=inverter_parameters_dict["switch"]["key"]["voltage_threshold"],
                )
                l_col.number_input(
                    label=inverter_parameters_dict["switch"]["label"]["resistance"],
                    value=inverter_parameters_dict["switch"]["value"]["resistance"],
                    min_value=inverter_parameters_dict["switch"]["min"]["resistance"],
                    max_value=inverter_parameters_dict["switch"]["max"]["resistance"],
                    step=inverter_parameters_dict["switch"]["step"]["resistance"],
                    format=inverter_parameters_dict["switch"]["format"]["resistance"],
                    key=inverter_parameters_dict["switch"]["key"]["resistance"],
                )
                l_col.number_input(
                    label=inverter_parameters_dict["switch"]["label"]["diode_drip_voltage"],
                    value=inverter_parameters_dict["switch"]["value"]["diode_drip_voltage"],
                    min_value=inverter_parameters_dict["switch"]["min"]["diode_drip_voltage"],
                    max_value=inverter_parameters_dict["switch"]["max"]["diode_drip_voltage"],
                    step=inverter_parameters_dict["switch"]["step"]["diode_drip_voltage"],
                    format=inverter_parameters_dict["switch"]["format"]["diode_drip_voltage"],
                    key=inverter_parameters_dict["switch"]["key"]["diode_drip_voltage"],
                )
                l_col.number_input(
                    label=inverter_parameters_dict["switch"]["label"]["diode_drip_resistance"],
                    value=inverter_parameters_dict["switch"]["value"]["diode_drip_resistance"],
                    min_value=inverter_parameters_dict["switch"]["min"]["diode_drip_resistance"],
                    max_value=inverter_parameters_dict["switch"]["max"]["diode_drip_resistance"],
                    step=inverter_parameters_dict["switch"]["step"]["diode_drip_resistance"],
                    format=inverter_parameters_dict["switch"]["format"]["diode_drip_resistance"],
                    key=inverter_parameters_dict["switch"]["key"]["diode_drip_resistance"],
                )
                l_col.number_input(
                    label=inverter_parameters_dict["switch"]["label"]["breakdown_voltage"],
                    value=inverter_parameters_dict["switch"]["value"]["breakdown_voltage"],
                    min_value=inverter_parameters_dict["switch"]["min"]["breakdown_voltage"],
                    max_value=inverter_parameters_dict["switch"]["max"]["breakdown_voltage"],
                    step=inverter_parameters_dict["switch"]["step"]["breakdown_voltage"],
                    format=inverter_parameters_dict["switch"]["format"]["breakdown_voltage"],
                    key=inverter_parameters_dict["switch"]["key"]["breakdown_voltage"],
                )
                l_col.number_input(
                    label=inverter_parameters_dict["switch"]["label"]["current_limit"],
                    value=inverter_parameters_dict["switch"]["value"]["current_limit"],
                    min_value=inverter_parameters_dict["switch"]["min"]["current_limit"],
                    max_value=inverter_parameters_dict["switch"]["max"]["current_limit"],
                    step=inverter_parameters_dict["switch"]["step"]["current_limit"],
                    format=inverter_parameters_dict["switch"]["format"]["current_limit"],
                    key=inverter_parameters_dict["switch"]["key"]["current_limit"],
                )
                l_col.number_input(
                    label=inverter_parameters_dict["switch"]["label"]["energy_on"],
                    value=inverter_parameters_dict["switch"]["value"]["energy_on"],
                    min_value=inverter_parameters_dict["switch"]["min"]["energy_on"],
                    max_value=inverter_parameters_dict["switch"]["max"]["energy_on"],
                    step=inverter_parameters_dict["switch"]["step"]["energy_on"],
                    format=inverter_parameters_dict["switch"]["format"]["energy_on"],
                    key=inverter_parameters_dict["switch"]["key"]["energy_on"],
                )
                l_col.number_input(
                    label=inverter_parameters_dict["switch"]["label"]["energy_off"],
                    value=inverter_parameters_dict["switch"]["value"]["energy_off"],
                    min_value=inverter_parameters_dict["switch"]["min"]["energy_off"],
                    max_value=inverter_parameters_dict["switch"]["max"]["energy_off"],
                    step=inverter_parameters_dict["switch"]["step"]["energy_off"],
                    format=inverter_parameters_dict["switch"]["format"]["energy_off"],
                    key=inverter_parameters_dict["switch"]["key"]["energy_off"],
                )
                l_col.number_input(
                    label=inverter_parameters_dict["switch"]["label"]["frequency"],
                    value=inverter_parameters_dict["switch"]["value"]["frequency"],
                    min_value=inverter_parameters_dict["switch"]["min"]["frequency"],
                    max_value=inverter_parameters_dict["switch"]["max"]["frequency"],
                    step=inverter_parameters_dict["switch"]["step"]["frequency"],
                    format=inverter_parameters_dict["switch"]["format"]["frequency"],
                    key=inverter_parameters_dict["switch"]["key"]["frequency"],
                )

                r_col.markdown("##### Diode Parameters")
                r_col.number_input(
                    label=inverter_parameters_dict["diode"]["label"]["reverse_voltage"],
                    value=inverter_parameters_dict["diode"]["value"]["reverse_voltage"],
                    min_value=inverter_parameters_dict["diode"]["min"]["reverse_voltage"],
                    max_value=inverter_parameters_dict["diode"]["max"]["reverse_voltage"],
                    step=inverter_parameters_dict["diode"]["step"]["reverse_voltage"],
                    format=inverter_parameters_dict["diode"]["format"]["reverse_voltage"],
                    key=inverter_parameters_dict["diode"]["key"]["reverse_voltage"],
                )
                r_col.number_input(
                    label=inverter_parameters_dict["diode"]["label"]["forward_current"],
                    value=inverter_parameters_dict["diode"]["value"]["forward_current"],
                    min_value=inverter_parameters_dict["diode"]["min"]["forward_current"],
                    max_value=inverter_parameters_dict["diode"]["max"]["forward_current"],
                    step=inverter_parameters_dict["diode"]["step"]["forward_current"],
                    format=inverter_parameters_dict["diode"]["format"]["forward_current"],
                    key=inverter_parameters_dict["diode"]["key"]["forward_current"],
                )
                r_col.number_input(
                    label=inverter_parameters_dict["diode"]["label"]["reverse_energy"],
                    value=inverter_parameters_dict["diode"]["value"]["reverse_energy"],
                    min_value=inverter_parameters_dict["diode"]["min"]["reverse_energy"],
                    max_value=inverter_parameters_dict["diode"]["max"]["reverse_energy"],
                    step=inverter_parameters_dict["diode"]["step"]["reverse_energy"],
                    format=inverter_parameters_dict["diode"]["format"]["reverse_energy"],
                    key=inverter_parameters_dict["diode"]["key"]["reverse_energy"],
                )

                r_col.markdown("##### Current Limits")
                r_col.number_input(
                    label=inverter_parameters_dict["dc_link"]["label"]["current_limit"],
                    value=inverter_parameters_dict["dc_link"]["value"]["current_limit"],
                    min_value=inverter_parameters_dict["dc_link"]["min"]["current_limit"],
                    max_value=inverter_parameters_dict["dc_link"]["max"]["current_limit"],
                    step=inverter_parameters_dict["dc_link"]["step"]["current_limit"],
                    format=inverter_parameters_dict["dc_link"]["format"]["current_limit"],
                    key=inverter_parameters_dict["dc_link"]["key"]["current_limit"],
                )

            with st.expander("Table Parameters"):
                st.markdown("#### Generator Options")
                st.selectbox("Table Profile", table_profile, key="table_profile")
                st.selectbox("Maximum Torque Method", maximum_torque_method, key="maximum_torque_method")
                st.selectbox("SVM Method", svm_method, key="svm_method")
                st.checkbox("Per Unit Torque Dimensions", value=True, key="per_unit_torque_dimensions")
                st.number_input("Maximum Current Limit", value=10000.0, min_value=0.0, max_value=100000.0, step=0.5, key="maximum_current_limit")

                st.markdown("#### Parameters")
                st.number_input("Number of speed breakpoints", value=14, min_value=1, max_value=100, step=1, key="number_of_speed_breakpoints")
                st.number_input("Number of torque breakpoints", value=14, min_value=1, max_value=100, step=1, key="number_of_torque_breakpoints")
                st.number_input("Maximum speed", value=11500, min_value=1, max_value=100000, step=1, key="maximum_table_speed")
                st.text_input("VDC Breakpoints", value="[150, 180, 220, 220, 240, 260, 280, 300, 320]", placeholder="[150, 180, 220, 220, 240, 260, 280, 300, 320]", key="vdc_breakpoints")
                st.number_input("Maximum Mod Index", value=0.97, min_value=0.01, max_value=1.5, step=0.01, key="maximum_mod_index")
                st.number_input("Interpolation Resolution", value=0.50, min_value=0.01, max_value=1000.0, step=0.01, key="interpolation_resolution")

                st.checkbox("Use Constant Stator Resistance", value=True, key="use_constant_stator_resistance")
                if st.session_state.use_constant_stator_resistance:
                    stator_resistance = st.number_input(
                        "Constant Stator Resistance [Ω]", value=0.00100, min_value=0.0001, max_value=100.0, step=0.0001, key="constant_stator_resistance", format="%0.5f"
                    )
                else:
                    stator_resistance = st.text_input("Stator Resistance [Ω]", value="", placeholder="[]", key="stator_resistance")

            with st.expander("Data/Export"):
                st.markdown("##### Plotting Options")
                st.checkbox("Plot Motor Input", value=True, key="plot_motor_input")
                st.checkbox("Plot Table Output", value=True, key="plot_motor_output")

                st.markdown("##### Filenames")
                st.text_input("MAT Filename", value="I_Table_Generator_Output", placeholder="I_Table_Generator_Output", key="mat_filename")

                st.checkbox("Output to Excel", value=True, key="output_to_excel")
                if st.session_state.output_to_excel == True:
                    st.text_input("Excel Filename", value="IDet_output_tables", placeholder="IDet_output_tables", key="excel_filename")
                    excel_filename = st.session_state.excel_filename + ".xlsx"
                else:
                    excel_filename = ""

                st.markdown("##### JSON File")
                st.checkbox("Merge Exsisting JSON File", value=False, key="merge_json_file")
                if st.session_state.merge_json_file == True:
                    st.file_uploader("JSON File", type=FILE_EXTENSIONS["JSON"], key="json_file")
                    json_file = st.session_state.json_file
                    if json_file is not None:
                        json_og_name = os.path.basename(st.session_state.json_file.name)
                        json_og_name = json_og_name.replace(FILE_EXTENSIONS["JSON"], "")
                        st.text_input("Output JSON Filename", value=json_og_name, placeholder=json_og_name, key="json_filename")
                else:
                    json_file = ""
            # TODO : seperate here to seperate function
            # get the name of the uplaoded file
            test_name_orginal = os.path.basename(st.session_state.table_zip.name)

            # delete prefix from the filename
            test_name_matlab = test_name_orginal.replace(DEFAULT_FILE_PREFIX["idq"]["results"]["characterisation"], "")

            # delete FILE_EXTENSIONS["CSV"] from the filename
            test_name_matlab = test_name_matlab.replace(FILE_EXTENSIONS["ZIP"], "")

            test_name_tables = DEFAULT_FILE_PREFIX["idq"]["tables"]["tables"] + test_name_matlab

            table_generation_dict = {
                "input": {
                    "table": {
                        "breakpoints": {
                            "speed_size": st.session_state.number_of_speed_breakpoints,
                            "torque_size": st.session_state.number_of_torque_breakpoints,
                            "vdc": st.session_state.vdc_breakpoints,
                        },
                        "methods": {
                            "max_torque": st.session_state.maximum_torque_method,
                            "svm": st.session_state.svm_method,
                            "per_unit_torque": st.session_state.per_unit_torque_dimensions,
                        },
                        "limits": {
                            "max": {
                                "speed": st.session_state.maximum_table_speed,
                                "idc": st.session_state.maximum_current_limit,
                                "mod_index": st.session_state.maximum_mod_index,
                            },
                        },
                        "profile": st.session_state.table_profile,
                        "interpolation_resolution": st.session_state.interpolation_resolution,
                    },
                    "parameters": {
                        # TODO : Pole pair needs taken from the motor parameters
                        "motor": {
                            "pole_pairs": 5.0,
                            "stator_resistance": stator_resistance,
                            "use_constant_stator_resistance": st.session_state.use_constant_stator_resistance,
                            "constant_stator_resistance": st.session_state.constant_stator_resistance,
                        },
                        "inverter": {
                            "switch": {
                                "volt_limit": st.session_state[inverter_parameters_dict["switch"]["key"]["voltage_threshold"]],
                                "resistance": st.session_state[inverter_parameters_dict["switch"]["key"]["resistance"]],
                                "diode_drip_voltage": st.session_state[inverter_parameters_dict["switch"]["key"]["diode_drip_voltage"]],
                                "diode_drip_resistance": st.session_state[inverter_parameters_dict["switch"]["key"]["diode_drip_resistance"]],
                                "breakdown_voltage": st.session_state[inverter_parameters_dict["switch"]["key"]["breakdown_voltage"]],
                                "i_limit": st.session_state[inverter_parameters_dict["switch"]["key"]["current_limit"]],
                                "energy_on": st.session_state[inverter_parameters_dict["switch"]["key"]["energy_on"]],
                                "energy_off": st.session_state[inverter_parameters_dict["switch"]["key"]["energy_off"]],
                                "frequency": st.session_state[inverter_parameters_dict["switch"]["key"]["frequency"]],
                            },
                            "diode": {
                                "reverse_voltage": st.session_state[inverter_parameters_dict["diode"]["key"]["reverse_voltage"]],
                                "forward_current": st.session_state[inverter_parameters_dict["diode"]["key"]["forward_current"]],
                                "reverse_energy": st.session_state[inverter_parameters_dict["diode"]["key"]["reverse_energy"]],
                            },
                            "dc_link": {
                                "current_limit": st.session_state[inverter_parameters_dict["dc_link"]["key"]["current_limit"]],
                            },
                        },
                    },
                    "data": {
                        "interpolated_data": test_name_matlab + DEFAULT_FILE_SUFFIX["table_gen"]["data"] + FILE_EXTENSIONS["MAT"],
                        "interpolated_table": test_name_matlab + DEFAULT_FILE_SUFFIX["table_gen"]["table"] + FILE_EXTENSIONS["MAT"],
                    },
                    "paths": {
                        "script": paths["script"]["analysis"]["idet_src"],
                    },
                },
                "output": {
                    "options": {
                        "plot_motor_input": st.session_state.plot_motor_input,
                        "plot_motor_output": st.session_state.plot_motor_output,
                        "output_to_excel": st.session_state.output_to_excel,
                    },
                    "file_names": {
                        "mat": st.session_state.mat_filename,
                        "excel": excel_filename,
                        "test": test_name_matlab,
                    },
                    "paths": {
                        "temp": paths["script"]["analysis"]["temp"],
                    },
                },
            }

            st.checkbox("Move files to remote location on completion", value=True, key="remote_tables")

            if st.button("Run Table Generator", key="run_table_generator"):
                with st.spinner("Unzipping Results.."):
                    # Unzip results

                    with zipfile.ZipFile(st.session_state.table_zip, "r") as z:
                        z.extractall(path=paths["script"]["analysis"]["temp"])

                with st.spinner("Generating Tables.."):
                    # Generate Tables
                    eng = matlab.engine.start_matlab()
                    idet_paths = eng.genpath(paths["script"]["analysis"]["root"])
                    eng.addpath(idet_paths)
                    eng.idet_table_gen(table_generation_dict, nargout=0)
                    eng.quit()
                    st.success("Tables Generated")

                # Tables Generated, now use them to create or merge to JSON.

                with st.spinner("Generating JSON File.."):
                    # Generate a new JSON File

                    # Create Dictionary for JSON
                    if st.session_state.json_file == "":
                        json_dict = EMPTY_JSON
                    else:
                        json_dict = json.loads(st.session_state.json_file.read())

                    # get the keys for the JSON file
                    json_keys = list(json_dict.keys())

                    # get the first key for the JSON file
                    json_key = json_keys[0]

                    # load meta_data.mat
                    meta_mat = sio.loadmat(paths["script"]["analysis"]["temp"] + "\\" + test_name_matlab + "_meta_data" + FILE_EXTENSIONS["MAT"], simplify_cells=True)
                    meta_mat = meta_mat["meta_data"]

                    json_dict[json_key]["Npp"]["Value"] = [meta_mat["PolePairs"]]
                    json_dict[json_key]["Ke_Line_Rms"]["Value"] = [meta_mat["Ke"]]

                    # load table_data.mat
                    table_file = h5py.File(
                        paths["script"]["analysis"]["temp"] + "\\" + test_name_matlab + "_" + table_generation_dict["output"]["file_names"]["mat"] + FILE_EXTENSIONS["MAT"], "r"
                    )

                    # Check if braking table exists, if not set braking table to motor table
                    if "MTPA_brk_PU_table" in table_file:
                        t_max_mtr = table_file["MTPA_mot_PU_table"]["Tmax"][:]
                        t_max_brk = table_file["MTPA_brk_PU_table"]["Tmax"][:]
                    else:
                        t_max_mtr = table_file["MTPA_mot_PU_table"]["Tmax"][:]
                        t_max_brk = table_file["MTPA_mot_PU_table"]["Tmax"][:]

                    t_pu = table_file["MTPA_mot_PU_table"]["Tpu_array"][:]  # In the example JSON file looks like torque per unit is not used ?
                    vdc_link = table_file["MTPA_mot_PU_table"]["Vdclink_array"][:]
                    ids = table_file["MTPA_mot_PU_table"]["id"][:]
                    iqs = table_file["MTPA_mot_PU_table"]["iq"][:]
                    speed = table_file["MTPA_mot_PU_table"]["w_array"][:]

                    table_file.close()

                    json_dict[json_key]["Cfg_VoltageBrkPtr"]["Value"] = vdc_link.flatten().tolist()
                    json_dict[json_key]["Cfg_RotorSpeedBrkPtr"]["Value"] = speed.tolist()
                    json_dict[json_key]["Cfg_IdBrakingTblPtr"]["Value"] = ids.flatten().tolist()
                    json_dict[json_key]["Cfg_IqBrakingTblPtr"]["Value"] = iqs.flatten().tolist()
                    json_dict[json_key]["Cfg_TpeakVdcMtrTblPtr"]["Value"] = t_max_mtr.T.flatten().tolist()  # I think this needs transposed to be correct
                    json_dict[json_key]["Cfg_TpeakVdcBreakTblPtr"]["Value"] = t_max_brk.T.flatten().tolist()  # I think this needs transposed to be correct

                    # Write to file
                    with open(paths["script"]["analysis"]["temp"] + "\\" + json_og_name + FILE_EXTENSIONS["JSON"], "w") as outfile:
                        json.dump(json_dict, outfile, indent=4)
                        st.success("JSON File Generated")

                with st.spinner("Zipping Tables.."):

                    try:
                        # Zip the temp folder call it test_name_tables and move it to a remote path
                        shutil.make_archive(
                            base_name=test_name_tables,
                            format="zip",
                            root_dir=paths["script"]["analysis"]["temp"],
                        )

                        st.success("Results Zipped")

                    except Exception as e:
                        st.error("Failed to zip results")
                        st.error(e)

                with st.spinner("Moving Tables.."):
                    try:

                        shutil.move(
                            src=paths["root"] + "\\" + test_name_tables + FILE_EXTENSIONS["ZIP"],
                            dst=paths["folders"]["remote"]["characterisation"]["idq"]["tables"]["gen_5"],
                        )

                        st.success("Results Moved")

                        with st.spinner("Cleaning Up.."):
                            # Delete the all the files in the temp folder
                            files = glob.glob(paths["script"]["analysis"]["temp"] + r"\*")
                            for f in files:
                                os.remove(f)

                            st.info("Temp Folder Cleared")

                    except Exception as e:
                        st.error("Failed to move results")
                        st.error(e)
