import streamlit as st
from src.constants import FLOATS


def tab_test_cfg(tab, hw_dict: dict, test_dict: dict):
    """Generate tab for configuration parameters

    Args:
        tab (_type_): Tab variable
        hw_dict (dict): Dictionary containing test hardware variables
        test_dict (dict): Dictionary containing test configuration varibales

    Returns:
        _type_: _description_
    """
    with tab:
        if test_dict["type"] == "Torque Speed Sweep":
            logging, speed, torque, temp = st.tabs(["Logging", "Speed", "torque", "Temperature"])

            logging_dict = tab_logging(logging)
            speed_dict = tab_speed(speed, hw_dict, test_dict)
            torque_dict = tab_torque(torque, hw_dict)
            temp_dict = tab_temperature(temp)

            cfg_dict = {
                "logging": logging_dict,
                "speed": speed_dict,
                "torque": torque_dict,
                "temp": temp_dict,
            }

        elif test_dict["type"] in [
            "Torque Speed Sweep",
            "Idq Characterisation Gen 5",
            "Idq Injection",
            "Idq Injection Gen 4",
            "Idq Characterisation Gen 4",
        ]:
            logging, speed, idq, temp = st.tabs(["Logging", "Speed", "Idq", "Temperature"])

            logging_dict = tab_logging(logging)
            speed_dict = tab_speed(speed, hw_dict, test_dict)
            idq_dict = tab_idq(idq, hw_dict, test_dict)
            temp_dict = tab_temperature(temp)

            cfg_dict = {
                "logging": logging_dict,
                "speed": speed_dict,
                "idq": idq_dict,
                "temp": temp_dict,
            }

        elif test_dict["type"] == "Encoder Alignment":
            (
                speed,
                encoder,
            ) = st.tabs(["speed", "Encoder"])

            speed_dict = tab_speed(speed, hw_dict, test_dict)
            encoder_dict = tab_encoder(encoder, hw_dict)

            cfg_dict = {"speed": speed_dict, "Encoder": encoder_dict}

    return cfg_dict


def tab_logging(logging_tab):
    """Generate tab representing logging configuration

    Args:
        logging_tab (_type_): Logging tab variable

    Returns:
        _type_: Dictionary of logging parameters
    """
    with logging_tab:
        st.checkbox(
            label="Log On Increasing Step",
            value=True,
            key="log_up",
            help="Enable Logging once steady state reached during the ramp up test points",
        )

        st.checkbox(
            label="Log On Decreasing Step",
            value=False,
            key="log_down",
            help="Enable Logging once steady state reached during the ramp down test points",
        )

        st.number_input(
            label="Logging Sample Period [s]",
            min_value=0.01,
            max_value=10.0,
            value=0.1,
            step=0.1,
            key="log_period",
        )

        st.checkbox(
            label="Delay Logging",
            value=True,
            key="delay_log",
            help="Delay data capture to allow steady state condition before capturing data.",
        )

        st.number_input(
            label="Delay Period [s]",
            min_value=0.01,
            max_value=10.0,
            value=0.2,
            step=0.01,
            key="delay_period",
            disabled=not st.session_state.delay_log,
        )

        logging_dict = {
            "up": st.session_state.log_up,
            "down": st.session_state.log_down,
            "sample_period": st.session_state.log_period,
            "delay": st.session_state.delay_log,
            "delay_period": st.session_state.delay_period,
        }

    return logging_dict


def tab_speed(tab, hw_dict: dict, test_dict: dict):
    """
    Generate tab representing speed(s) configuration

    Args:
        tab (_type_): _description_
        hw_dict (_type_): _description_

    Returns:
        _type_: _description_
    """
    with tab:
        max_speed_project = hw_dict["project"]["controller"]["config"]["max"]["speed"]
        max_speed_motor = hw_dict["project"]["motor"]["parameters"]["limits"]["max"]["speed"]
        max_speed_dyno = hw_dict["dyno"]["dyno"]["limits"]["max"]["speed"] * hw_dict["dyno"]["dyno"]["gearbox_ratio"]

        max_speed_limit = min([max_speed_project, max_speed_motor, max_speed_dyno])

        tab.selectbox(
            label="Speed Limit Threshold",
            options=["Percentage", "Offset", "Value"],
            key="requested_speed_limit_type",
            disabled=True,
        )

        if st.session_state.requested_speed_limit_type == "Percentage":
            st.number_input(
                label="Percentage of Speed Target ",
                min_value=0,
                max_value=1000,
                value=120,
                step=1,
                key="requested_speed_limit_value",
            )

        elif st.session_state.requested_speed_limit_type == "Offset":
            st.number_input(
                label="Offset",
                min_value=FLOATS["zero"],
                max_value=max_speed_limit,
                value=500.0,
                step=0.01,
                key="requested_speed_limit_value",
            )

        elif st.session_state.requested_speed_limit_type == "Value":
            st.number_input(
                label="Offset",
                min_value=FLOATS["zero"],
                max_value=max_speed_limit,
                value=max_speed_limit,
                step=0.01,
                key="requested_speed_limit_value",
            )

        if test_dict["type"] == "Torque Speed Sweep":
            st.number_input(
                label="Minimum Speed",
                min_value=FLOATS["zero"],
                max_value=max_speed_limit,
                value=500.0,
                step=0.5,
                key="requested_min_speed",
            )

            st.number_input(
                label="Maximum Speed",
                min_value=0.5,
                max_value=max_speed_limit,
                value=max_speed_limit,
                step=0.5,
                key="requested_max_speed",
            )

            st.number_input(
                label="Speed Step Size",
                min_value=0.5,
                max_value=max_speed_limit,
                value=500.0,
                step=0.5,
                key="requested_speed_step",
            )

            speed_dict = {
                "Limit Type": st.session_state.requested_speed_limit_type,
                "limit_value": st.session_state.requested_speed_limit_value,
                "minimum": st.session_state.requested_min_speed,
                "maximum": st.session_state.requested_max_speed,
                "step": st.session_state.requested_speed_step,
            }

        if test_dict["type"] in [
            "Idq Characterisation Gen 5",
            "Idq Injection",
            "Idq Injection Gen 4",
            "Idq Characterisation Gen 4",
            "Encoder Alignment",
        ]:
            st.number_input(
                label="Test Speed",
                min_value=FLOATS["zero"],
                max_value=float(max(hw_dict["project"]["controller"]["config"]["tables"]["speed"]["breakpoints"])),
                value=500.0,
                step=0.5,
                key="requested_speed_idq",
            )

            speed_dict = {
                "Limit Type": st.session_state.requested_speed_limit_type,
                "limit_value": st.session_state.requested_speed_limit_value,
                "target": st.session_state.requested_speed_idq,
            }

    return speed_dict


def tab_torque(tab, hw_dict: dict):
    """Tab representing torque(s) configurations

    Args:
        tab (_type_): _description_
        hw_dict (_type_): _description_

    Returns:
        _type_: _description_
    """

    max_torque_project = hw_dict["project"]["controller"]["config"]["max"]["torque"]
    max_torque_motor = hw_dict["project"]["motor"]["parameters"]["limits"]["max"]["torque"]
    max_torque_dyno = hw_dict["dyno"]["dyno"]["limits"]["max"]["torque"] / hw_dict["dyno"]["dyno"]["gearbox_ratio"]

    max_torque = min([max_torque_project, max_torque_motor, max_torque_dyno])

    with tab:
        st.number_input(
            label="Minimum Torque (abs[Nm])",
            min_value=FLOATS["zero"],
            max_value=max_torque,
            value=FLOATS["zero"],
            step=0.5,
            key="requested_min_torque",
        )

        st.number_input(
            label="Maximum Torque (% of Peak)",
            min_value=0,
            max_value=100,
            value=100,
            step=1,
            key="requested_max_torque",
        )

        st.number_input(
            label="Step Up Size [Nm]",
            min_value=0.5,
            max_value=max_torque_project,
            value=20.0,
            step=0.5,
            key="requested_torque_step_up",
        )

        st.number_input(
            label="Step Up Period [s]",
            min_value=0.01,
            max_value=5000.0,
            value=1.0,
            step=0.01,
            key="requested_torque_step_up_period",
        )

        st.number_input(
            label="Step Down Size [Nm]",
            min_value=0.01,
            max_value=max_torque_project,
            value=20.0,
            step=0.5,
            key="requested_torque_step_down",
        )

        st.number_input(
            label="Step Down Period [s]",
            min_value=0.01,
            max_value=5000.0,
            value=1.0,
            step=0.01,
            key="requested_torque_step_down_period",
        )

        st.checkbox(
            label="Skip last Torque demand per speed step",
            value=True,
            key="skip_max_torque",
        )

        torque_dict = {
            "minimum": st.session_state.requested_min_torque,
            "maximum": st.session_state.requested_max_torque,
            "Ramp Up Torque": st.session_state.requested_torque_step_up,
            "Ramp Down Torque": st.session_state.requested_torque_step_down,
            "ramp_up_period": st.session_state.requested_torque_step_up_period,
            "ramp_down_period": st.session_state.requested_torque_step_down_period,
            "skip": st.session_state.skip_max_torque,
        }

    return torque_dict


def tab_temperature(tab):
    """Tab representing temperature related configurations
        Temperature tab

    Args:
        tab (_type_): _description_

    Returns:
        _type_: _description_
    """
    with tab:
        if st.checkbox(label="Enable Temperature Dwelling", value=True, key="motor_dwell_enable"):
            st.number_input(
                label="Upper Motor [C]",
                min_value=FLOATS["zero"],
                value=80.0,
                max_value=120.0,
                step=1.0,
                key="motor_temp_upper",
                help="Maximum Motor Temperature allowed during current injection.",
            )

            st.number_input(
                label="Lower Motor [C]",
                min_value=FLOATS["zero"],
                value=60.0,
                max_value=120.0,
                step=1.0,
                key="motor_temp_lower",
                help="Motor Temperature starting allowed during current injection.",
            )

            st.number_input(
                label="Ramp Step",
                min_value=FLOATS["zero"],
                value=10.0,
                max_value=100.0,
                step=1.0,
                key="motor_temp_step",
                help="Current or Torque ramp up and down step when temp dwell is running.",
            )

            temp_dict = {
                "enable": st.session_state.motor_dwell_enable,
                "step": st.session_state.motor_temp_step,
                "limit": {
                    "upper": st.session_state.motor_temp_upper,
                    "lower": st.session_state.motor_temp_lower,
                },
            }

        else:
            temp_dict = {"Enable": st.session_state.motor_dwell_enable}

    return temp_dict


def tab_idq(tab, hw_dict: dict, test_dict: dict):
    """Tab representing Idq Current injection configurations

    Args:
        tab (_type_): _description_
        hw_dict (dict): _description_

    Returns:
        _type_: _description_
    """

    max_current_motor = hw_dict["project"]["motor"]["parameters"]["limits"]["max"]["stator"]["current"]
    max_current_controller = hw_dict["project"]["controller"]["parameters"]["limits"]["dc_link"]["current"]["discharge"]
    max_current = float(min([max_current_motor, max_current_controller]))
    id_id = hw_dict["project"]["controller"]["properties"]["idq"]["ids"]["id"]
    iq_id = hw_dict["project"]["controller"]["properties"]["idq"]["ids"]["iq"]
    test_mode_id = hw_dict["project"]["controller"]["properties"]["idq"]["ids"]["mode"]
    test_axis_id = hw_dict["project"]["controller"]["properties"]["idq"]["ids"]["axis"]

    with tab:
        st.number_input(
            label="Minimum Current",
            min_value=FLOATS["zero"],
            max_value=max_current,
            value=FLOATS["zero"],
            step=0.5,
            key="requested_current_min",
        )

        st.number_input(
            label="Maximum Current (Stator)",
            min_value=FLOATS["zero"],
            max_value=max_current,
            value=max_current,
            step=1.0,
            key="requested_current_max",
        )

        st.number_input(
            label="Idq Injection Step Size [A]",
            min_value=0.1,
            max_value=max_current,
            value=25.0,
            step=0.1,
            key="requested_i_injection_step",
        )

        st.number_input(
            label="Step Period [s]",
            min_value=0.01,
            max_value=5000.0,
            value=1.0,
            step=0.01,
            key="requested_i_injection_period",
        )

        st.selectbox(label="step_method", options=["Zero", "Ramp"], key="idq_step_method")

        if st.session_state.idq_step_method == "Ramp":
            st.number_input(
                label="Ramp Period [s]",
                min_value=0.01,
                max_value=5000.0,
                value=1.0,
                step=0.01,
                key="idq_ramp_period",
                help="Period for ramping up and down to target Idq",
            )

        st.text_input(label="Test Mode ID :", value=test_mode_id, key="test_mode_id")

        st.text_input(label="Idq Axis ID :", value=test_axis_id, key="tmode_idq_axis_id")

        st.number_input(
            label="Test Axis Value :",
            min_value=FLOATS["zero"],
            value=FLOATS["zero"],
            max_value=10.0,
            step=1.0,
            key="tmode_idq_axis_value",
        )

        st.text_input(label="Id Current Injection ID :", value=id_id, key="tmode_idq_id_id")

        st.text_input(label="Iq Current Injection ID :", value=iq_id, key="tmode_idq_iq_id")

        st.number_input(
            label="Start Point",
            min_value=0,
            max_value=5000,
            value=0,
            step=1,
            key="start_point",
        )

        idq_dict = {
            "maximum": st.session_state.requested_current_max,
            "minimum": st.session_state.requested_current_min,
            "step": st.session_state.requested_i_injection_step,
            "step_period": st.session_state.requested_i_injection_period,
            "step_method": st.session_state.idq_step_method,
            "ramp_period": None,
            "Mode ID": st.session_state.test_mode_id,
            "Axis ID": st.session_state.tmode_idq_axis_id,
            "Axis Value": st.session_state.tmode_idq_axis_value,
            "Id ID": st.session_state.tmode_idq_id_id,
            "Iq ID": st.session_state.tmode_idq_iq_id,
            "Start Point": st.session_state.start_point,
        }

        if idq_dict["step_method"] == "Ramp":
            idq_dict["step_period"] = st.session_state.idq_ramp_period

    return idq_dict


def tab_encoder(tab):
    """_summary_

    Args:
        one (_type_): _description_
        two (_type_): _description_
    """
    with tab:
        st.number_input(
            label="Angle Step",
            min_value=FLOATS["zero"],
            max_value=360.0,
            value=1.0,
            step=0.1,
            key="angle_step",
        )

    encoder_dict = {"Angle Step": st.session_state.angle_step}

    return encoder_dict
