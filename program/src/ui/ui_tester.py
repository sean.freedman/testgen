import sys
from subprocess import CREATE_NEW_CONSOLE, Popen

import numpy as np
import src.constants as const
import streamlit as st


def convert(seconds: int):
    """Convert seconds to hours, minutes and seconds.

    Args:
        seconds (int): number of seconds to convert

    Returns:
        "{hour}h:{minutes}m:{seconds}s": Time in hours, minutes and seconds.
    """
    seconds = seconds % (24 * 3600)
    hour = seconds // 3600
    seconds %= 3600
    minutes = seconds // 60
    seconds %= 60

    return f"{hour}h:{minutes}m:{seconds}s"


def tab_tester(tab, paths: dict, test_points: dict):
    """Tab containing summery information about the test to run, options and button to run.

    Args:
        tab (_type_): _description_
        paths (dict): _description_
        test_points (dict): _description_
    """
    with tab:

        st.markdown("---")
        st.session_state.Number_of_Points = len(test_points)
        st.markdown(f"Number of Points: `{st.session_state.Number_of_Points}`")
        st.markdown(f"Approximate Test Time: `{convert(np.sum(test_points.demand_period.tolist()))}`")
        st.markdown("---")

        if st.button("Confirm and Start Tests"):

            with st.spinner("Test Script launched and is running in a seperate shell.."):
                # Run the test script in shell so we can see the print statements.
                process = Popen(
                    [f"{sys.executable}", paths["script"]["test"]],
                    creationflags=CREATE_NEW_CONSOLE,
                    stdout=sys.stdout,
                )
                process.wait()
            st.info("Test Script Complete or Closed")
