from datetime import datetime

import streamlit as st
from src.constants import CURRENT_PATH, DATE_TIME_FORMAT, DEFAULT_FILE_SUFFIX, FILE_EXTENSIONS, INTERNAL_NAME
from src.file_ops import file_merge
from src.generator import envelope_table, idq_script, idq_script_gen_4, idq_table, torque_speed_script, torque_speed_table
from src.ui import ui_hw, ui_plots, ui_profile, ui_test_cfg, ui_tester


def tab_testing(tab):
    with tab:
        # Initialise
        test_points = None
        envolope = None
        datetime_now = datetime.now()
        datetime_format = datetime_now.strftime(DATE_TIME_FORMAT)
        test_name = st.session_state.test_name
        paths = st.session_state.paths

        # UI
        l_col, r_col = st.columns(2)
        profile_tab, hw_tab, tes_cfg_tab = l_col.tabs(["💻 Profile", "⚙️ Hardware", "✏️ Test Configuration"])

        test_dict = ui_profile.tab_profile(profile_tab, paths)
        hw_dict = ui_hw.tab_hw(hw_tab, test_dict)
        cfg_dict = ui_test_cfg.tab_test_cfg(tes_cfg_tab, hw_dict, test_dict)

        st.session_state.test_dict = test_dict
        st.session_state.hw_dict = hw_dict
        st.session_state.cfg_dict = cfg_dict

        # If test name is empty, generate test name from test parameters
        if st.session_state.test_name == "":
            test_name = (
                datetime_format
                + str(hw_dict["project"]["controller"]["name"])
                + "_"
                + str(hw_dict["project"]["controller"]["sample"]["letter"])
                + str(hw_dict["project"]["controller"]["sample"]["number"])
                + "_"
                + str(hw_dict["project"]["controller"]["name"])
                + "_"
                + str(hw_dict["project"]["motor"]["sample"]["letter"])
                + str(hw_dict["project"]["motor"]["sample"]["number"])
                + "_"
                + str(int(hw_dict["dyno"]["power_supply"]["config"]["target"]))
                + "V_"
                + test_dict["profile"]
            )

        else:
            test_name = datetime_format + test_dict["name"]

        # TODO: Put code below into a function
        if test_dict["type"] == "Torque Speed Sweep":
            # Update paths for relevent test
            paths["files"]["torque_speed"]["data"]["results"]["measured"] = CURRENT_PATH + "\\" + test_name + FILE_EXTENSIONS["CSV"]
            paths["files"]["torque_speed"]["data"]["results"]["alerts"] = CURRENT_PATH + "\\" + test_name + DEFAULT_FILE_SUFFIX["alerts"] + FILE_EXTENSIONS["CSV"]
            paths["files"]["torque_speed"]["data"]["inverter"]["meta_data"] = CURRENT_PATH + "\\" + test_name + DEFAULT_FILE_SUFFIX["inverter_properties"] + FILE_EXTENSIONS["CSV"]
            paths["files"]["torque_speed"]["data"]["inverter"]["current_sensor_latencies"] = CURRENT_PATH + "\\" + test_name + DEFAULT_FILE_SUFFIX["current_sensor_latencies"] + FILE_EXTENSIONS["CSV"]

            # Setup dictionary for generation of test points using input parameters
            gen_cfg = {
                "test": {"type": test_dict["type"], "profile": test_dict["profile"]},
                "voltage": {
                    "breakpoints": hw_dict["project"]["controller"]["config"]["tables"]["voltage"]["breakpoints"],
                    "voltage": hw_dict["dyno"]["power_supply"]["config"]["target"],
                },
                "torque": {
                    "peak": hw_dict["project"]["controller"]["config"]["tables"]["torque"]["peak"],
                    "maximum": cfg_dict["torque"]["maximum"],
                    "minimum": cfg_dict["torque"]["minimum"],
                    "ramp_up_value": cfg_dict["torque"]["Ramp Up Torque"],
                    "ramp_down_value": cfg_dict["torque"]["Ramp Down Torque"],
                    "ramp_up_period": cfg_dict["torque"]["ramp_up_period"],
                    "ramp_down_period": cfg_dict["torque"]["ramp_down_period"],
                    "skip": cfg_dict["torque"]["skip"],
                },
                "speed": {
                    "max_table": hw_dict["project"]["controller"]["config"]["max"]["speed"],
                    "breakpoints": hw_dict["project"]["controller"]["config"]["tables"]["speed"]["breakpoints"],
                    "maximum": cfg_dict["speed"]["maximum"],
                    "minimum": cfg_dict["speed"]["minimum"],
                    "step": cfg_dict["speed"]["step"],
                    "Limit Type": cfg_dict["speed"]["Limit Type"],
                    "limit_value": cfg_dict["speed"]["limit_value"],
                },
                "logging": {
                    "up": cfg_dict["logging"]["up"],
                    "down": cfg_dict["logging"]["down"],
                },
            }

            test_points = torque_speed_table(gen_cfg)

            # Generate test script using test points
            script_input = {
                "test": {
                    "name": test_name,
                    "export_path": paths["folders"]["export"],
                    "export_name": INTERNAL_NAME,
                    "export_format": FILE_EXTENSIONS["PY"],
                    "type": test_dict["type"],
                    "profile": test_dict["profile"],
                    "logging_path": test_dict["path"],
                },
                "test_input": {
                    "speed_demands": test_points.speed_demand.tolist(),
                    "speed_limits_forward": test_points.speed_lim_fwd.tolist(),
                    "speed_limits_reverse": test_points.speed_lim_rev.tolist(),
                    "torque_demands": test_points.torque_demand.tolist(),
                    "demand_period": test_points.demand_period.tolist(),
                    "logged_points": test_points.test_point.tolist(),
                },
                "controller": hw_dict["project"]["controller"],
                "motor": hw_dict["project"]["motor"],
                "dcdc": hw_dict["dyno"]["power_supply"],
                "dyno": hw_dict["dyno"]["dyno"],
                "torque_sensor": hw_dict["dyno"]["torque_sensor"],
                "can": hw_dict["can"],
                "login": hw_dict["login"],
                "temperature": cfg_dict["temp"],
                "speed": cfg_dict["speed"],
                "logging": cfg_dict["logging"],
                "paths": paths,
            }

            code_path = torque_speed_script(script_input)

            file_merge(code_path, paths["script"]["automation"]["torque_speed"]["sweep"]["gen_5"])

            envolope = envelope_table(
                torque_peak=hw_dict["project"]["controller"]["config"]["tables"]["torque"]["peak"],
                speed_breakpoints=hw_dict["project"]["controller"]["config"]["tables"]["speed"]["breakpoints"],
                voltage_breakpoints=hw_dict["project"]["controller"]["config"]["tables"]["voltage"]["breakpoints"],
                voltage_ref=hw_dict["dyno"]["power_supply"]["config"]["target"],
            )

        # TODO: Put code below into a function
        elif test_dict["type"] in [
            "Torque Speed Sweep",
            "Idq Characterisation Gen 5",
            "Idq Injection",
            "Idq Injection Gen 4",
            "Idq Characterisation Gen 4",
        ]:
            # Update paths for relevent test
            paths["files"]["idq"]["data"]["results"]["measured"] = CURRENT_PATH + "\\" + test_name + FILE_EXTENSIONS["CSV"]
            paths["files"]["idq"]["data"]["results"]["alerts"] = CURRENT_PATH + "\\" + test_name + DEFAULT_FILE_SUFFIX["alerts"] + FILE_EXTENSIONS["CSV"]
            paths["files"]["idq"]["data"]["inverter"]["meta_data"] = CURRENT_PATH + "\\" + test_name + DEFAULT_FILE_SUFFIX["inverter_properties"] + FILE_EXTENSIONS["CSV"]
            paths["files"]["idq"]["data"]["inverter"]["current_sensor_latencies"] = CURRENT_PATH + "\\" + test_name + DEFAULT_FILE_SUFFIX["current_sensor_latencies"] + FILE_EXTENSIONS["CSV"]
            paths["files"]["idq"]["data"]["results"]["transducer"] = CURRENT_PATH + "\\" + test_name + DEFAULT_FILE_SUFFIX["transducer"] + FILE_EXTENSIONS["CSV"]
            # Setup dictionary for generation of test points using input parameters
            gen_cfg = {
                "test": {"type": test_dict["type"], "profile": test_dict["profile"]},
                "voltage": {"voltage_breakpoints": hw_dict["project"]["controller"]["config"]["tables"]["voltage"]["breakpoints"], "voltage": hw_dict["dyno"]["power_supply"]["config"]["target"]},
                "idq": {
                    "maximum": cfg_dict["idq"]["maximum"],
                    "step": cfg_dict["idq"]["step"],
                    "step_method": cfg_dict["idq"]["step_method"],
                    "step_period": cfg_dict["idq"]["step_period"],
                    "ramp_period": cfg_dict["idq"]["step_period"],
                    "iq_direction": None,
                },
                "speed": {
                    "target": cfg_dict["speed"]["target"],
                    "limit": cfg_dict["speed"]["limit_value"],
                    "max_table": hw_dict["project"]["controller"]["config"]["max"]["speed"],
                    "breakpoints": hw_dict["project"]["controller"]["config"]["tables"]["speed"]["breakpoints"],
                    "direction": None,
                },
            }

            test_points = idq_table(gen_cfg)

            # Generate test script using test points
            script_input = {
                "test": {
                    "name": test_name,
                    "export_path": paths["folders"]["export"],
                    "export_name": INTERNAL_NAME,
                    "export_format": FILE_EXTENSIONS["PY"],
                    "type": test_dict["type"],
                    "profile": test_dict["profile"],
                    "logging_path": test_dict["path"],
                },
                "test_input": {
                    "speed_demands": test_points.speed_demand.tolist(),
                    "speed_limits_forward": test_points.speed_lim_fwd.tolist(),
                    "speed_limits_reverse": test_points.speed_lim_rev.tolist(),
                    "id_demands": test_points.iq_injected.tolist(),
                    "iq_demands": test_points.id_injected.tolist(),
                    "demand_period": test_points.demand_period.tolist(),
                    "logged_points": test_points.test_point.tolist(),
                },
                "controller": hw_dict["project"]["controller"],
                "motor": hw_dict["project"]["motor"],
                "dcdc": hw_dict["dyno"]["power_supply"],
                "dyno": hw_dict["dyno"]["dyno"],
                "torque_sensor": hw_dict["dyno"]["torque_sensor"],
                "can": hw_dict["can"],
                "login": hw_dict["login"],
                "speed": cfg_dict["speed"],
                "idq": cfg_dict["idq"],
                "temperature": cfg_dict["temp"],
                "logging": cfg_dict["logging"],
                "paths": paths,
            }

            if test_dict["type"] in [
                "Idq Characterisation Gen 5",
                "Idq Injection",
            ]:
                code_path = idq_script(script_input)
                file_merge(code_path, paths["script"]["automation"]["characterisation"]["idq"]["gen_5"])

            elif test_dict["type"] in [
                "Idq Injection Gen 4",
                "Idq Characterisation Gen 4",
            ]:
                code_path = idq_script_gen_4(script_input)
                file_merge(code_path, paths["script"]["automation"]["characterisation"]["idq"]["gen_4"])

        else:
            st.warning("Other Methods in Development")
            st.stop()

        ui_plots.tab_plots(r_col, envolope, test_points, hw_dict, test_dict["type"])
        ui_tester.tab_tester(r_col, paths, test_points)

    return
