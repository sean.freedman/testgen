import pandas as pd
import streamlit as st
from src.constants import CURRENT_PATH, DEFAULT_PATHS, FILE_EXTENSIONS
from src.file_ops import read_yaml_file


def tab_tools(tab):
    with tab:
        yaml_tab, merge_tab = st.tabs(["YAML", "Merge"])
        tab_yaml(yaml_tab)
        tab_merge(merge_tab)


def tab_merge(tab):
    with tab:
        tab.markdown("##### Merge Files")
        tab.markdown("If multiple CSV's belong to one test, upload here to merge them into one csv.")
        tab.file_uploader(label="Results to merge", type=FILE_EXTENSIONS["CSV"], accept_multiple_files=True, key="results_merge")

        df = pd.DataFrame()

        if st.session_state.results_merge != []:
            file_list = []

            for i in range(len(st.session_state.results_merge)):
                file_list.append(pd.read_csv(st.session_state.results_merge[i]))

            df = pd.concat(file_list)

            merge_filename = st.session_state.results_merge[0].name
            merge_filename = merge_filename.replace(FILE_EXTENSIONS["CSV"], "_MERGED")

            tab.text_input(label="File Name", value=merge_filename, key="merge_filename")

            if tab.button(label="Merge Files"):
                df.sort_values(by=["Test Point"], inplace=True)
                df.to_csv(
                    path_or_buf=st.session_state.merge_filename + FILE_EXTENSIONS["CSV"],
                    header=True,
                    index=False,
                )
                merged_file_location = CURRENT_PATH + "\\" + st.session_state.merge_filename + FILE_EXTENSIONS["CSV"]
                tab.success(f"Files Merged: {merged_file_location}")


def tab_yaml(tab):
    with tab:
        tab.markdown("##### YAML Viewer")
        tab.markdown("Basic YAML viewer for hardware configurations.")
        # get paths of all config / YAML files.
        yaml_paths = DEFAULT_PATHS["equipment"]["configs"]

        hardware_list = yaml_paths.keys()

        l_col, m_col, r_col = tab.columns(3)

        l_col.selectbox(label="Group", options=hardware_list, key="yaml_hardware")

        hardware_sublist = yaml_paths[st.session_state.yaml_hardware].keys()

        m_col.selectbox(label="Sub-Group", options=hardware_sublist, key="yaml_subhardware")

        # read the yaml file and diplay the top level keys
        yaml_path = yaml_paths[st.session_state.yaml_hardware][st.session_state.yaml_subhardware]
        yaml_file = read_yaml_file(yaml_path)

        r_col.selectbox(label="Model", options=yaml_file.keys(), key="yaml_keys")

        def dict_to_html_tree(dictionary, level=1):
            html = ""

            if isinstance(dictionary, dict):
                html += f"<ul style='list-style-type:none;'>"
                for key, value in dictionary.items():
                    bgcolor = "#EEEEEE"
                    keycolor = "#212121"

                    if level == 1:
                        bgcolor = "#FF8A65"  # Material Deep Orange 300
                        keycolor = "#FFFFFF"
                    elif level == 2:
                        bgcolor = "#4DB6AC"  # Material Teal 400
                        keycolor = "#FFFFFF"  # white
                    elif level == 3:
                        bgcolor = "#FFF176"  # Material Yellow 300
                        keycolor = "#000000"  # Black
                    elif level == 4:
                        bgcolor = "#9575CD"  # Material Deep Purple 300
                        keycolor = "#FFFFFF"
                    elif level == 5:
                        bgcolor = "#81D4FA"  # Material Light Blue 200
                        keycolor = "#000000"
                    elif level == 6:
                        bgcolor = "#E57373"  # Material Red 300
                        keycolor = "#FFFFFF"
                    elif level == 7:
                        bgcolor = "#F06292"  # Material Pink 300
                        keycolor = "#FFFFFF"
                    elif value == 8:
                        bgcolor = "#66BB6A"  # Material Green 400
                        keycolor = "#FFFFFF"

                    html += f"<li style='display: table-row; table-layout: fixed;'>"
                    html += f"<span style='background-color:{bgcolor}; display: table-cell; padding: 5px; color: {keycolor}; border: 1px solid black;'>{key}</span>: "

                    if not isinstance(value, dict):
                        html += f"<code style='background-color:{bgcolor}; padding: 5px; color: {keycolor}; border: 1px solid black;'>{value}</code>"
                    else:
                        html += dict_to_html_tree(value, level + 1)

                    html += "</li>"

                html += "</ul>"

            return html

        # convert dictionary to HTML
        html_output = dict_to_html_tree(yaml_file[st.session_state.yaml_keys])

        # display HTML file
        tab.markdown(html_output, unsafe_allow_html=True)
