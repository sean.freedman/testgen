import streamlit as st
from src.constants import DEFAULT_PATHS
from src.ui import ui_analysis, ui_debug, ui_testing

st.set_page_config(layout="wide", page_title="Testify", page_icon="📈", initial_sidebar_state="expanded")

# Add dictionariwes to session state
if "test_dict" not in st.session_state:
    st.session_state.test_dict = {}
if "hw_dict" not in st.session_state:
    st.session_state.hw_dict = {}
if "cfg_dict" not in st.session_state:
    st.session_state.cfg_dict = {}
if "plot_dict" not in st.session_state:
    st.session_state.plot_dict = {}
if "test_points" not in st.session_state:
    st.session_state.test_points = {}
if "test_name" not in st.session_state:
    st.session_state.test_name = ""
if "paths" not in st.session_state:
    st.session_state.paths = DEFAULT_PATHS

testing_tab, analysis_tab, debug_tab = st.tabs(["🧪 Testing", "🔎 Analysis", "🪲 Debug"])

ui_testing.tab_testing(testing_tab)
ui_analysis.tab_analysis(analysis_tab)
ui_debug.tab_debug(debug_tab)
